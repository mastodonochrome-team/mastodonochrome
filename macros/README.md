About
=====

This crate contains `proc_macro`s used by Mastodonochrome. It's part
of the Mastodonochrome project, and not expected to be useful to
anything else.
