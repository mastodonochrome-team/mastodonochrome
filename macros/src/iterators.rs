pub trait MyIterTools: Iterator {
    fn trim_start<F>(self, reject: F) -> std::iter::Peekable<Self>
    where
        Self: Sized,
        F: Fn(&Self::Item) -> bool,
    {
        let mut iter = self.peekable();
        while iter.next_if(&reject).is_some() {}
        iter
    }

    fn trim_end<F>(self, reject: F) -> TrimEndIterator<Self, F>
    where
        Self: Sized,
        F: Fn(&Self::Item) -> bool,
    {
        TrimEndIterator {
            iter: itertools::peek_nth(self),
            reject,
        }
    }
}

impl<T: Iterator> MyIterTools for T {}

pub struct TrimEndIterator<T, F>
where
    T: Iterator,
    F: Fn(&T::Item) -> bool,
{
    iter: itertools::PeekNth<T>,
    reject: F,
}

impl<T, F> Iterator for TrimEndIterator<T, F>
where
    T: Iterator,
    F: Fn(&T::Item) -> bool,
{
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        for i in 0.. {
            match self.iter.peek_nth(i) {
                None => return None,
                Some(item) => {
                    if !(self.reject)(item) {
                        break;
                    }
                }
            }
        }

        self.iter.next()
    }
}
