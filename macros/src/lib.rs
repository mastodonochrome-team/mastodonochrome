use proc_macro::TokenStream;

mod markup;
mod iterators;

/// Translate online help text for Mastodonochrome from an ad-hoc
/// markup format into the HelpVec type known to the parent crate's
/// `help.rs` source file.
///
/// This macro is unlikely to be useful more widely than that one
/// source file.
///
/// The parameter list to this macro is expected to be a single string
/// literal, probably raw, e.g.
///
/// ```
/// help_markup!(r##"
/// hello, world
/// "##)
/// ```
///
/// The macro expands to an expression that generates an instance of
/// the `HelpVec` type defined in `help.rs`.
///
/// The markup format is as follows:
///
/// Paragraphs of help are separated by a blank line. Within a
/// paragraph, whitespace is normalised to single spaces.
///
/// The characters `[`, `]` and `\` are special. `\` acts as an escape
/// to make the next character literal. `[` and `]` delimit all other
/// special markup operations in the language.
///
/// If the text wrapped in `[]` consists of a single character, then
/// `:`, then arbitrary other text, it causes the text to be displayed
/// in the colour specified by the initial character, according to the
/// same mapping table that Mastodonochrome's `ColouredString` type
/// uses. For example, `[_:foo]` causes the word `foo` to be
/// underlined, and `[@:@user@domain]` prints `@user@domain` in the
/// same green colour that Mastodonochrome uses for @mentions.
///
/// If the text consists of a single character, then `.`, then
/// arbitrary other text, it represents some other kind of command.
/// Currently the only command is `i`, which sets the indentation of
/// the containing paragraph. For example, `[i.2]` causes a paragraph
/// to be indented by 2 spaces (both the first line and later lines).
///
/// If neither of those is true, the text is interpreted as a Rust
/// expression that should represent an element of the `OurKey` enum
/// describing a keystroke. This is translated into the consistent
/// representation of that keypress that Mastodonochrome uses
/// throughout its UI. For example, `[Space]`, `[Escape]` and
/// `[Ctrl('F')]` become `[SPACE]`, `[ESC]` and `[^F]` respectively,
/// with the key name inside the square brackets bolded.
///
/// To save typing in a common case, a _single_ character inside the
/// square brackets is treated as a printable character. So `[P]` in
/// the markup is a shorthand for `[Pr('P')]`, which in turn
/// translates back into `[P]` in the output.
///
/// If the keypress is preceded by `!`, such as `[!P]` or `[!Escape]`,
/// it indicates that the keystroke is being discussed in a context
/// that does not mean you can press it in the activity for which the
/// help is being shown. Nothing is yet done with that information,
/// but I hope that at some future point it might be possible to
/// correlate the list of keystrokes mentioned in the help with the
/// list matched by the actual code, and ensure no keystroke has
/// accidentally gone undocumented.
#[proc_macro]
pub fn help_markup(input: TokenStream) -> TokenStream {
    let string_literal: syn::LitStr =
        syn::parse(input).expect("help! expects a string literal argument");

    let document = markup::parse(&string_literal.value());
    let document = markup::emit_doc(&document);
    document.into()
}
