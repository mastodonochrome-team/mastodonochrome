use itertools::Itertools;
//use regex::Regex;
use proc_macro2::TokenStream;
use quote::quote;

use super::iterators::MyIterTools;

pub enum ParaFragment {
    Text(String),
    ColouredText(char, String),
    Key(syn::Expr),
}

pub enum DocFragment {
    BlankLine,
    Paragraph(usize, Vec<ParaFragment>), // first field is indentation
}

struct Lexer<'a> {
    chars: std::iter::Peekable<std::str::Chars<'a>>,
}

#[derive(Debug, Eq, PartialEq)]
enum LexFragment {
    Text(String),
    Space,
    OpenBracket,
    CloseBracket,
    ParaBreak,
}

impl LexFragment {
    fn to_str(&self) -> Option<&str> {
        match self {
            LexFragment::Text(s) => Some(s),
            LexFragment::Space => Some(" "),
            _ => None,
        }
    }
}

impl<'a> Lexer<'a> {
    fn new(text: &'a str) -> Self {
        Lexer {
            chars: text.chars().peekable(),
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = LexFragment;

    fn next(&mut self) -> Option<Self::Item> {
        let ch = match self.chars.next() {
            None => return None,
            Some(ch) => ch,
        };

        if ch == '\\' {
            // Escape the next character, if it's magic in some way
            match self.chars.next() {
                Some(ch) => return Some(LexFragment::Text(String::from(ch))),
                None => panic!("FIXME: report proper error: unterminated \\"),
            }
        }

        if ch == '[' {
            return Some(LexFragment::OpenBracket);
        }
        if ch == ']' {
            return Some(LexFragment::CloseBracket);
        }

        if ch.is_whitespace() {
            // Consume as much whitespace as we can, and count newlines
            let mut newlines = (ch == '\n') as usize;
            while let Some(ch) = self.chars.peek() {
                if !ch.is_whitespace() {
                    break;
                }
                newlines += (*ch == '\n') as usize;
                self.chars.next();
            }

            if newlines > 1 {
                // Multiple newlines mean a paragraph break
                return Some(LexFragment::ParaBreak);
            } else {
                // Otherwise, normalise to a plain space
                return Some(LexFragment::Space);
            }
        }

        let mut s = String::from(ch);
        while let Some(ch) = self.chars.peek() {
            if *ch == '[' || *ch == ']' || *ch == '\\' || ch.is_whitespace() {
                break;
            }
            s.push(*ch);
            self.chars.next();
        }
        return Some(LexFragment::Text(s));
    }
}

pub fn parse(markup: &str) -> Vec<DocFragment> {
    let mut lexer = Lexer::new(markup)
        .trim_start(|f| *f == LexFragment::ParaBreak)
        .trim_end(|f| *f == LexFragment::ParaBreak)
        .peekable();
    let mut doc = Vec::new();

    loop {
        while lexer.next_if(|f| *f == LexFragment::ParaBreak).is_some() {}
        if lexer.peek().is_none() {
            break;
        }

        let mut para_lexer = lexer
            .peeking_take_while(|f| *f != LexFragment::ParaBreak)
            .trim_start(|f| *f == LexFragment::Space)
            .trim_end(|f| *f == LexFragment::Space)
            .peekable();

        fn get_text<T>(para_lexer: &mut T) -> String
        where
            T: itertools::PeekingNext + Iterator<Item = LexFragment>,
        {
            para_lexer
                .peeking_take_while(|f| f.to_str().is_some())
                .map(|f| {
                    f.to_str()
                        .expect(
                            "peeking_take_while should guarantee this is Some",
                        )
                        .to_owned()
                })
                .collect::<Vec<_>>()
                .join("")
        }

        let mut indent = 0;
        let mut para = Vec::new();
        while para_lexer.peek().is_some() {
            if para_lexer
                .next_if(|f| *f == LexFragment::CloseBracket)
                .is_some()
            {
                panic!("FIXME: report proper markup syntax error: stray ]");
            }

            if para_lexer
                .next_if(|f| *f == LexFragment::OpenBracket)
                .is_some()
            {
                let braced_text = get_text(&mut para_lexer);

                if para_lexer
                    .next_if(|f| *f == LexFragment::CloseBracket)
                    .is_none()
                {
                    panic!(
                        "FIXME: report proper markup syntax error: missing ]"
                    );
                }

                {
                    let mut chars = braced_text.char_indices();
                    if let Some((_, c)) = chars.next() {
                        match chars.next() {
                            Some((_, ':')) => {
                                if let Some((pos, _)) = chars.next() {
                                    // {r:some text} means display that text
                                    // in colour 'r'
                                    para.push(ParaFragment::ColouredText(
                                        c,
                                        braced_text[pos..].to_owned(),
                                    ));
                                    continue;
                                }
                            }
                            Some((_, '.')) => {
                                if let Some((pos, _)) = chars.next() {
                                    let param = &braced_text[pos..];
                                    // {c:param} means c is a one-char command
                                    match c {
                                    'i' => indent = param.parse()
                                        .expect("FIXME: report proper markup syntax error: indent should be numeric"),
                                    _ => panic!("FIXME: report proper markup syntax error: bad markup command"),
                                }
                                    continue;
                                }
                            }
                            _ => (),
                        }
                    }
                }

                let braced_text = if braced_text.starts_with("!") {
                    // FIXME: leading ! means this keystroke refers to
                    // something not part of the current activity, so
                    // that it can be omitted from any consistency
                    // checks we add later.
                    &braced_text[1..]
                } else {
                    &braced_text
                };

                let replacement_string;
                let braced_text = match braced_text.chars().exactly_one() {
                    Ok(c) => {
                        replacement_string = format!("Pr({})", quote! {#c});
                        &replacement_string
                    }
                    Err(_) => braced_text,
                };

                let expr: syn::Expr = syn::parse_str(braced_text).expect(
                    "FIXME: report proper markup syntax error: bad key",
                );
                para.push(ParaFragment::Key(expr));
                continue;
            }

            let text = get_text(&mut para_lexer);
            assert!(!text.is_empty());
            para.push(ParaFragment::Text(text));
        }

        doc.push(DocFragment::Paragraph(indent, para));
        doc.push(DocFragment::BlankLine);
    }

    doc
}

fn emit_para_fragment(frag: &ParaFragment) -> TokenStream {
    match frag {
        ParaFragment::Text(s) => quote! {
            .add(ColouredString::plain(#s))
        },
        ParaFragment::ColouredText(c, s) => quote! {
            .add(ColouredString::uniform(#s, #c))
        },
        ParaFragment::Key(s) => quote! {
            .add(ColouredString::plain("["))
            .add(ColouredString::uniform(&crate::text::key_to_string(#s), 'k'))
            .add(ColouredString::plain("]"))
        },
    }
}

fn emit_doc_fragment(frag: &DocFragment) -> TokenStream {
    match frag {
        DocFragment::BlankLine => quote! {
            vec.push(Rc::new(BlankLine::new()));
        },
        DocFragment::Paragraph(indent, frags) => {
            let frags: Vec<_> = frags.iter().map(emit_para_fragment).collect();
            quote! {
                vec.push(
                    Rc::new(
                        Paragraph::new()
                            .set_indent(#indent, #indent)
                            #(#frags)*
                    )
                );
            }
        }
    }
}

pub fn emit_doc(frags: &Vec<DocFragment>) -> TokenStream {
    let frags: Vec<_> = frags.iter().map(emit_doc_fragment).collect();
    quote! {
        {
            let mut vec: Vec<Rc<dyn TextFragment>> = Vec::new();
            #(#frags)*
            HelpVec(vec)
        }
    }
}

#[test]
fn test_parse() {
    assert_eq!(
        parse(r##"hello, world"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Text("hello, world".to_owned()),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[K]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Key(
                    syn::parse_str::<syn::Expr>("Pr('K')").unwrap(),
                ),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[\\]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Key(
                    syn::parse_str::<syn::Expr>("Pr('\\\\')").unwrap(),
                ),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[Ctrl('K')]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Key(
                    syn::parse_str::<syn::Expr>("Ctrl('K')").unwrap(),
                ),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[PgUp]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Key(
                    syn::parse_str::<syn::Expr>("PgUp").unwrap(),
                ),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[r:nonsense]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::ColouredText('r', "nonsense".to_owned()),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"\[r:nonsense\] \\"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Text("[r:nonsense] \\".to_owned()),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[r:\[-\]]"##),
        vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::ColouredText('r', "[-]".to_owned()),
            }),
            DocFragment::BlankLine,
        }
    );

    assert_eq!(
        parse(r##"[i.2]indented"##),
        vec! {
            DocFragment::Paragraph(2, vec!{
                ParaFragment::Text("indented".to_owned()),
            }),
            DocFragment::BlankLine,
        }
    );
}

#[test]
fn test_emit() {
    assert_eq!(
        emit_doc(&vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Text("hello, world".to_owned()),
            }),
        })
        .to_string(),
        quote! {
            {
                let mut vec: Vec<Rc<dyn TextFragment>> = Vec::new();
                vec.push(Rc::new(
                    Paragraph::new()
                        .set_indent(0usize, 0usize)
                        .add(ColouredString::plain("hello, world"))));
                HelpVec(vec)
            }
        }
        .to_string(),
    );

    assert_eq!(
        emit_doc(&vec! {
            DocFragment::Paragraph(0, vec!{
                ParaFragment::Text("foo".to_owned()),
                ParaFragment::ColouredText('g', "bar".to_owned()),
                ParaFragment::Key(
                    syn::parse_str::<syn::Expr>("Ctrl('K')").unwrap(),
                ),
            }),
        })
        .to_string(),
        quote! {
            {
                let mut vec: Vec<Rc<dyn TextFragment>> = Vec::new();
                vec.push(Rc::new(
                    Paragraph::new()
                        .set_indent(0usize, 0usize)
                        .add(ColouredString::plain("foo"))
                        .add(ColouredString::uniform("bar", 'g'))
                        .add(ColouredString::plain("["))
                        .add(ColouredString::uniform(
                            &crate::text::key_to_string(Ctrl('K')), 'k'
                        ))
                        .add(ColouredString::plain("]"))));
                HelpVec(vec)
            }
        }
        .to_string(),
    );

    assert_eq!(
        emit_doc(&vec! {
            DocFragment::BlankLine,
        })
        .to_string(),
        quote! {
            {
                let mut vec: Vec<Rc<dyn TextFragment>> = Vec::new();
                vec.push(Rc::new(BlankLine::new()));
                HelpVec(vec)
            }
        }
        .to_string(),
    );

    assert_eq!(
        emit_doc(&vec! {
            DocFragment::Paragraph(2, vec!{
                ParaFragment::Text("indented".to_owned()),
            }),
        })
        .to_string(),
        quote! {
            {
                let mut vec: Vec<Rc<dyn TextFragment>> = Vec::new();
                vec.push(Rc::new(
                    Paragraph::new()
                        .set_indent(2usize, 2usize)
                        .add(ColouredString::plain("indented"))));
                HelpVec(vec)
            }
        }
        .to_string(),
    );
}
