About
=====

Mastodonochrome is an **UNFINISHED** terminal-based client for the
[Mastodon](https://joinmastodon.org/) federated social network.

It has the following useful properties:

* Terminal-based, so you can run it remotely over SSH
* Single user interface that allows access to both your home timeline
  and your notifications (believe it or not, at least one existing
  terminal client doesn't do this)
* Single-column display, so you can paste URLs out of the terminal to
  a browser without including unrelated text in other columns (though
  the paste will unfortunately include newlines at the wrap points)
* Uses the Mastodon streaming API, so updates to your timeline or
  mentions by other users are brought to your attention promptly

The detailed user interface is modelled after [Monochrome
BBS](https://www.mono.org/), because that was a user interface that
the author happened to be familiar with already, which was compatible
with all the properties listed above.

This means it looks a bit different from the typical Mastodon client
UI:

* Timelines are printed with old stuff at the top and new stuff at the
  bottom
* The client remembers the last thing in your timeline you'd read, so
  that when you restart the client you scroll down from there to see
  new stuff
* Notifications are divided up into separate categories in different
  parts of the UI: @mentions and direct messages live in one place and
  beep to get your attention, while boosts, follows and faves go into
  a separate less prominent log elsewhere.

**Current status**: unfinished, but has enough features to be usable.
No separate manual, but you can press F1 for help anywhere in the
program. Read `TODO.md` for the list of known missing pieces.
