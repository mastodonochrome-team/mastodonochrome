use super::types::{Notification, Status};

/// Thing that reads chunks of data from an io::Read and delivers them
/// as boxed slices
struct Reader<T: std::io::Read>(T);

impl<T: std::io::Read> Iterator for Reader<T> {
    type Item = Box<[u8]>;

    fn next(&mut self) -> Option<Self::Item> {
        const BUFSIZE: usize = 4096;
        let mut buf: [u8; BUFSIZE] = [0; BUFSIZE];
        self.0
            .read(&mut buf)
            .ok()
            .map(|size| Box::from(&buf[0..size]))
    }
}

/// Adapter that takes an iterator delivering boxed slices of u8, and delivers
/// the same but each a single line of the input separated by b'\n'
struct LineAdapter<T: Iterator<Item = Box<[u8]>>> {
    input: T,
    vec: Vec<u8>,
    start: usize,
    pos: usize,
}

impl<T: Iterator<Item = Box<[u8]>>> std::fmt::Debug for LineAdapter<T> {
    fn fmt(
        &self,
        fmt: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        write!(
            fmt,
            "LineAdapter {{ vec: {:?}, start: {:?}, pos: {:?} }}",
            &self.vec, self.start, self.pos
        )
    }
}

impl<T: Iterator<Item = Box<[u8]>>> LineAdapter<T> {
    fn new(input: T) -> Self {
        LineAdapter {
            input,
            vec: Vec::new(),
            start: 0,
            pos: 0,
        }
    }
}

impl<T: Iterator<Item = Box<[u8]>>> Iterator for LineAdapter<T> {
    type Item = Box<[u8]>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            // Deliver a line from the existing vec if we can
            for i in self.pos..self.vec.len() {
                if self.vec[i] == b'\n' {
                    let slice = &self.vec[self.start..i];
                    self.pos = i + 1;
                    self.start = self.pos;
                    return Some(slice.into());
                }
            }

            // No complete line in the vec, so start by deleting
            // everything before the start of the current line
            if self.start > 0 {
                self.vec = self.vec[self.start..].into();
                self.start = 0;
            }
            self.pos = self.vec.len();

            // And now get more data
            match self.input.next() {
                None => return None,
                Some(slice) => {
                    self.vec.extend_from_slice(&slice);
                }
            }
        }
    }
}

#[test]
fn test_line_adapter() {
    let ad = LineAdapter::new(
        [
            Box::from(b"abc\ndef\ng".as_slice()),
            Box::from(b"hijklmnopqr".as_slice()),
            Box::from(b"stu\nvwx\n".as_slice()),
            Box::from(b"yzABCDEF\n".as_slice()),
            Box::from(b"\n".as_slice()),
        ]
        .into_iter(),
    );
    assert_eq!(
        ad.collect::<Vec<_>>(),
        &[
            Box::from(b"abc".as_slice()),
            Box::from(b"def".as_slice()),
            Box::from(b"ghijklmnopqrstu".as_slice()),
            Box::from(b"vwx".as_slice()),
            Box::from(b"yzABCDEF".as_slice()),
            Box::from(b"".as_slice()),
        ]
    );
}

/// Adapter that takes a sequence of Box<[u8]>; converts each into a
/// str; and if two consecutive ones are both valid UTF-8 and start
/// with "event:" and "data:", returns the tails of both as a pair
/// of String.
struct StreamEventAdapter<T: Iterator<Item = Box<[u8]>>> {
    input: T,
    prev: Option<String>,
}

impl<T: Iterator<Item = Box<[u8]>>> StreamEventAdapter<T> {
    fn new(input: T) -> Self {
        StreamEventAdapter { input, prev: None }
    }
}

impl<T: Iterator<Item = Box<[u8]>>> Iterator for StreamEventAdapter<T> {
    type Item = (String, String);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let boxed_slice = match self.input.next() {
                None => return None,
                Some(boxed_slice) => boxed_slice,
            };

            let s: &str = match std::str::from_utf8(&boxed_slice) {
                Err(_) => {
                    self.prev = None;
                    continue;
                }
                Ok(s) => s,
            };

            if let Some(toret) = s
                .strip_prefix("data:")
                .map(|data| data.trim_start_matches(' '))
                .and_then(|data| {
                    self.prev
                        .as_ref()
                        .map(|event| (event.to_owned(), data.to_owned()))
                })
            {
                self.prev = None;
                return Some(toret);
            }

            self.prev = s.strip_prefix("event:").map(|event| event.to_owned());
        }
    }
}

#[test]
fn test_stream_event_adapter() {
    let se = StreamEventAdapter::new(
        [
            Box::from(b"nonsense".as_slice()),
            Box::from(b"event:this".as_slice()),
            Box::from(b"data:that".as_slice()),
            Box::from(b"wiffle".as_slice()),
            Box::from(b"event:unfinished".as_slice()),
        ]
        .into_iter(),
    );
    assert_eq!(
        se.collect::<Vec<_>>(),
        &[(String::from("this"), String::from("that")),]
    );

    let se = StreamEventAdapter::new(
        [
            Box::from(b"event:a".as_slice()),
            Box::from(b"event:b".as_slice()),
            Box::from(b"event:c".as_slice()),
            Box::from(b"data:d".as_slice()),
            Box::from(b"data:e".as_slice()),
            Box::from(b"data:f".as_slice()),
            Box::from(b"event:g".as_slice()),
            Box::from(b"event:h".as_slice()),
            Box::from(b"event:i".as_slice()),
            Box::from(b"data:j".as_slice()),
        ]
        .into_iter(),
    );
    assert_eq!(
        se.collect::<Vec<_>>(),
        &[
            (String::from("c"), String::from("d")),
            (String::from("i"), String::from("j")),
        ]
    );
}

#[derive(Debug, Clone)]
pub enum StreamEvent {
    Update(Status),       // new status posted
    StatusUpdate(Status), // existing status edited
    Delete(String),       // status deleted
    Notification(Notification),
    //
    // Not yet implemented:
    //
    // FiltersChanged,
    // Conversation(Conversation),
    // Announcement(Announcement),
    // AnnouncementReaction(small structure with no formal name),
    // AnnouncementDelete(String),
    // EncryptedMessage,
}

#[derive(Debug, Clone)]
pub struct StreamResponse {
    pub event_raw: String,
    pub data: String,
    pub event: Option<StreamEvent>,
}

/// Function that takes a string pair containing event and data, and
/// emits a StreamResponse. This doesn't need to be a full iterator
/// adapter, because the mapping is one-to-one, so we can just use
/// .map().
fn parse_stream_response_fallible(
    response: &(String, String),
) -> Result<StreamResponse, serde_json::Error> {
    let (event_raw, data) = response;

    let event = match event_raw.trim() {
        "update" => Some(StreamEvent::Update(serde_json::from_str(&data)?)),
        "status.update" => {
            Some(StreamEvent::StatusUpdate(serde_json::from_str(&data)?))
        }
        "delete" => Some(StreamEvent::Delete(data.to_owned())),
        "notification" => {
            Some(StreamEvent::Notification(serde_json::from_str(&data)?))
        }
        _ => None, // unparsable or unsupported
    };

    Ok(StreamResponse {
        event_raw: event_raw.to_owned(),
        data: data.to_owned(),
        event,
    })
}

fn parse_stream_response(response: (String, String)) -> StreamResponse {
    match parse_stream_response_fallible(&response) {
        Ok(response) => response,
        Err(..) => {
            let (event_raw, data) = response;
            StreamResponse {
                event_raw,
                data,
                event: None,
            }
        }
    }
}

pub fn stream_parser<T: std::io::Read>(
    input: T,
) -> impl Iterator<Item = StreamResponse> {
    StreamEventAdapter::new(LineAdapter::new(Reader(input)))
        .map(parse_stream_response)
}
