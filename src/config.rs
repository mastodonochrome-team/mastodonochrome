use std::io::Write;
use std::path::PathBuf;
use tempfile::NamedTempFile;

#[cfg(windows)]
use std::str::FromStr;

#[derive(Debug)]
pub enum ConfigError {
    #[cfg(unix)]
    XDG(xdg::BaseDirectoriesError),

    #[cfg(windows)]
    Env(std::env::VarError),
}

impl super::top_level_error::TopLevelErrorCandidate for ConfigError {}

impl std::fmt::Display for ConfigError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        match self {
            #[cfg(unix)]
            ConfigError::XDG(e) => e.fmt(f),

            #[cfg(windows)]
            ConfigError::Env(e) => {
                // FIXME: how _should_ we include the information
                // about what environment variable?
                write!(f, "%APPDATA%: {}", e)
            }
        }
    }
}

impl std::error::Error for ConfigError {}

#[cfg(unix)]
impl From<xdg::BaseDirectoriesError> for ConfigError {
    fn from(err: xdg::BaseDirectoriesError) -> Self {
        ConfigError::XDG(err)
    }
}

#[cfg(windows)]
impl From<std::env::VarError> for ConfigError {
    fn from(err: std::env::VarError) -> Self {
        ConfigError::Env(err)
    }
}

#[cfg(windows)]
impl From<std::convert::Infallible> for ConfigError {
    fn from(_: std::convert::Infallible) -> Self {
        panic!("I thought you said it was infallible?");
    }
}

#[derive(Clone)]
pub struct ConfigLocation {
    dir: PathBuf,
}

impl ConfigLocation {
    #[cfg(unix)]
    pub fn default_loc() -> Result<Self, ConfigError> {
        let base_dirs = xdg::BaseDirectories::with_prefix("mastodonochrome")?;
        Ok(ConfigLocation {
            dir: base_dirs.get_config_home(),
        })
    }

    #[cfg(windows)]
    pub fn default_loc() -> Result<Self, ConfigError> {
        let appdata = std::env::var("APPDATA")?;
        let mut dir = PathBuf::from_str(&appdata)?;
        dir.push("mastodonochrome");

        Ok(ConfigLocation { dir })
    }

    pub fn from_pathbuf(dir: PathBuf) -> Self {
        ConfigLocation { dir }
    }

    pub fn get_path(&self, leaf: &str) -> PathBuf {
        self.dir.join(leaf)
    }

    pub fn create_file(
        &self,
        leaf: &str,
        contents: &str,
    ) -> Result<(), std::io::Error> {
        std::fs::create_dir_all(&self.dir)?;

        // NamedTempFile creates the file with restricted permissions,
        // so we don't need to specify to be careful about that. (I
        // think if we wanted to make the file _public_ we'd have to
        // fchmod it or similar.)

        let mut file = NamedTempFile::new_in(&self.dir)?;
        file.write_all(contents.as_bytes())?;
        file.persist(self.get_path(leaf))?;
        Ok(())
    }
}
