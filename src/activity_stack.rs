//! Handle the stack of current UI activities in Mastodonochrome.
//!
//! An 'activity' defines the visible state of the user interface:
//! what's drawn on the screen, and how the UI responds to keypresses,
//! apart from `[ESC]`, which is handled centrally to ensure that you
//! can press it anywhere at all.
//!
//! Activities are kept in a stack, and typically the keypress `[RET]`
//! or `[Q]` or both will pop the stack. The topmost activity on the
//! stack can't be popped: it's normally the Main Menu, unless the
//! client isn't logged in yet, in which case the topmost activity
//! will be the login and account registration menu.
//!
//! The contents of the stack are not fully general. There can be any
//! number of some kinds of activity on it, but only one of other
//! kinds. This prevents excess keypresses in situations where you're
//! in the middle of one thing, nip out quickly to the Utilities Menu
//! to check on something, and return to what you were doing: if you
//! had to press `[RET]` repeatly to unwind through all the utility
//! submenus, it would be annoying, so instead, the activity stack is
//! constrained to have at most one 'utility activity' at a time.
//!
//! Activities divide into several broad categories, each defined by a
//! separate enumeration, shown below. The [Activity] super-enum
//! combines all of them into a single type.
//!
//! The order of priority of these activity types on the stack, from
//! bottom to top, is:
//!
//! * [NonUtilityActivity] (any number of these, though at least one)
//! * [ComposeActivity] (at most one)
//! * [UtilityActivity] (at most one)
//! * [OverlayActivity] (at most one, for affecting the previous activities)
//! * [HelpActivity] (at most one)
//! * [OverlayActivity] _again_ (at most one, for searching within the
//! help itself)

use derive_more::From;

use super::client::{Boosts, Replies};
use super::file::SearchDirection;
use super::types::StatusSource;

/// Primary activities, which the stack can hold any nonzero number of.
///
/// This enumeration defines the activities in which the user is
/// expected to spend most time. These include the Main Menu, and most
/// things reachable from it by following keystrokes for menu items.
/// The activity stack contains an arbitrary number of these (but at
/// least one).
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum NonUtilityActivity {
    /// The Main Menu.
    ///
    /// If this activity is on the stack at all, it _must_ be at the
    /// bottom: any attempt to transition to it will pop the stack
    /// right back to the bottom.
    ///
    /// The only case in which it's not even on the stack is because
    /// [NonUtilityActivity::LoginMenu] is at the bottom instead.
    MainMenu,

    /// Activity for viewing your home timeline, i.e. posts from
    /// yourself and from all users who you follow. Reply posts are
    /// excluded, unless they're replies to something also in the same
    /// home timeline feed.
    HomeTimelineFile,

    /// The 'public timeline' of the Mastodon server, consisting of
    /// all posts with `public` visibility, from all servers that this
    /// one federates with. Very fast-moving.
    PublicTimelineFile,

    /// The 'local timeline' of the Mastodon server, consisting of all
    /// posts with `public` visibility, posted by users of this
    /// particular server.
    LocalTimelineFile,

    /// Timeline of posts mentioning a particular hashtag. `.0` is the
    /// hashtag in question, without the leading `#`. For example,
    /// `HashtagTimeline("otter")` would follow posts containing
    /// `#otter` (or `#Otter`, or any other capitalisation).
    HashtagTimeline(String),

    /// File of posts made by a particular user. `.0` is the user's
    /// account [`id`](super::types::Account::id). `.1` and `.2`
    /// indicate whether to include other people's posts that this
    /// user has boosted, and replies made by this user to other
    /// threads. (However, the user's replies to their _own_ top-level
    /// posts – e.g. a multi-toot thread posted in one go – will be
    /// shown.)
    UserPosts(String, Boosts, Replies),

    /// File of posts belonging to a particular discussion thread.
    /// `.0` is the [`id`](super::types::Status::id) of the post. If
    /// `.1` is `false` then we show the context of that post (its
    /// ancestors and descendants); if `.1` is true then we show the
    /// complete thread that it's a part of, by finding all
    /// descendants of its earliest ancestor.
    ThreadFile(String, bool),

    /// The menu that controls logging in to a Mastodon instance, or
    /// registering a new account. If Mastodonochrome does not have
    /// any existing authentication data, then it starts up in this
    /// menu instead of the Main Menu.
    LoginMenu,
}

/// Activities related to composing Mastodon posts.
///
/// These include the actual editor, and the menu you reach after
/// editing your post, where you can set options like visibility
/// before submitting it.
///
/// There can be at most one of these on the stack at a time.
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum ComposeActivity {
    /// Activity for the post editor, when you're writing a new
    /// top-level post.
    ComposeToplevel,

    /// Activity for the post menu, when you're writing a new
    /// top-level post.
    PostComposeMenu,

    /// Activity for the post editor, when you're writing a reply to
    /// an existing post.
    ComposeReply(String),

    /// Activity for the post menu, when you're writing a reply to an
    /// existing post.
    PostReplyMenu(String),

    /// Activity for the post editor, when you're editing an existing
    /// post of your own.
    EditExistingPost(StatusSource),

    /// Activity for the post menu, when you're editing an existing
    /// post of your own.
    PostEditMenu(StatusSource),
}

/// Activities reached by pressing `[ESC]` to go to the Utilities
/// Menu.
///
/// Most activities reachable from the Utilities Menu are in this
/// enum, with the exception of the `[ESC][G]` key sequence for 'go to
/// Main Menu', whose purpose is to pop the whole stack.
///
/// There can be at most one of these on the stack at a time.
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum UtilityActivity {
    /// The primary Utilities Menu, reached by pressing `[ESC]`
    /// anywhere.
    UtilsMenu,

    /// The file you get to via `[ESC][R]` to read your mentions.
    ReadMentions,

    /// The primary logs menu, reached via `[ESC][L]`. Currently
    /// labelled 'Client Logs', on the theory that it's going to
    /// contain logs of things that happened within Mastodonochrome
    /// itself, like the log of recent HTTP transactions.
    LogsMenu1,

    /// The secondary logs menu, reached via `[ESC][L][L]`. Currently
    /// labelled 'Server Logs', on the theory that it contains logs of
    /// events that happened on the Mastodon server, and retrieved via
    /// HTTP requests, such as notifications of followings, favings
    /// and boostings.
    LogsMenu2,

    /// The 'Ego Log' (`[ESC][L][L][E]`): boosts and faves of your
    /// posts, and follow events. That is, the 'content-free' ego
    /// boosts that just tell you you're popular, and don't have
    /// content you need to read or reply to.
    EgoLog,

    /// A list of the people who have requested to follow you.
    ListFollowRequesters,

    /// The trivial menu reached via `[ESC][X]`, from which you press
    /// `[X]` one more time to confirm exiting Mastodonochrome.
    ExitMenu,

    /// A file containing details of a user you asked to examine. The
    /// string parameter gives the account
    /// [`id`](super::types::Account::id).
    ExamineUser(String),

    /// A file listing the people who have followed a given user. The
    /// string parameter gives the account [`id`](super::types::Account::id).
    ListUserFollowers(String),

    /// A file listing the people who a given user follows. The string
    /// parameter gives the account [`id`](super::types::Account::id).
    ListUserFollowees(String),

    /// A file containing full details of a particular post. The
    /// string parameter gives the post's [`id`](super::types::Status::id).
    InfoStatus(String),

    /// The log of recent HTTP transactions made by the client.
    HttpLog,

    /// A file containing full details of a particular HTTP
    /// transaction, from the HTTP Log.
    InfoHttp(String),

    /// The log of recent errors reported to the user after an
    /// operation failed.
    ErrorLog,

    /// A file listing the people who have favourited a given post.
    /// The string parameter gives the post's [`id`](super::types::Status::id).
    ListStatusFavouriters(String),

    /// A file listing the people who have boosted a given post. The
    /// string parameter gives the post's [`id`](super::types::Status::id).
    ListStatusBoosters(String),

    /// A menu allowing the user to set options relating to a
    /// particular user. The string parameter gives the post's
    /// [`id`](super::types::Status::id).
    UserOptions(String),

    /// A file listing the rules of a server instance that you're
    /// considering signing up with.
    InstanceRules,
}

/// Activities that don't take over the entire screen.
///
/// These activities occupy only part of the screen, so that most of
/// the previous activity on the stack is still visible. They
/// generally take the form of prompts in the bottom line of the
/// screen, e.g. for a string to search for in a file, or the name of
/// a user to examine.
///
/// There can be at most one of these on the stack at a time.
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum OverlayActivity {
    /// A bottom-line prompt asking for the id or name of a user to
    /// examine. Goes to `[UtilityActivity::ExamineUser]` after one is
    /// selected.
    GetUserToExamine,

    /// A bottom-line prompt asking for the id or URL of a post to
    /// read. Goes to `[UtilityActivity::InfoStatus]` after one is
    /// selected.
    GetPostIdToRead,

    /// A bottom-line prompt asking for the name of a hashtag to read.
    /// Goes to `[NonUtilityActivity::HashtagTimeline]` after one is
    /// selected.
    GetHashtagToRead,

    /// A bottom-line prompt asking for a regular expression to search
    /// for in the file you're currently viewing.
    GetSearchExpression(SearchDirection),

    /// A bottom-line error message, typically displayed after another
    /// bottom-line prompt received an invalid response (like an
    /// illegal regex, or a nonexistent user or post). These errors
    /// normally indicate that the user made a mistake, so they're
    /// less serious than an Error Log entry, which indicates that
    /// something went wrong within Mastodonochrome itself, or between
    /// it and the server.
    BottomLineError(String),
}

/// Activity of using the online help.
#[derive(PartialEq, Eq, Debug, Clone, Hash)]
pub enum HelpActivity {
    /// Online help is contextual to any of the other activities
    /// defined in this module, so you have to specify what activity
    /// you're asking for help on. Since that would involve
    /// [`Activity`] recursively containing a copy of itself, we have
    /// to put a [`Box`] in between.
    Help(Box<Activity>),
}

/// Enumeration type that stores any activity at all.
///
/// This enumeration has a branch for each of the smaller activity
/// types. It also implements [From]`<T>` for each smaller activity
/// type `T`, so that you can suffix `.into()` to an inner enum value
/// to turn it into this type easily.
#[derive(PartialEq, Eq, Debug, Clone, Hash, From)]
pub enum Activity {
    NonUtil(NonUtilityActivity),
    Compose(ComposeActivity),
    Util(UtilityActivity),
    Overlay(OverlayActivity),
    Help(HelpActivity),
}

/// Stores the full set of activities currently in progress in the UI.
#[derive(PartialEq, Eq, Debug)]
pub struct ActivityStack {
    nonutil: Vec<NonUtilityActivity>,
    compose: Option<ComposeActivity>,
    initial_compose: Option<ComposeActivity>,
    util: Option<UtilityActivity>,
    initial_util: Option<UtilityActivity>,
    overlay: Option<OverlayActivity>,
    help: Option<HelpActivity>,
    helpoverlay: Option<OverlayActivity>,
}

impl Activity {
    /// Returns `true` if this activity is one that should be
    /// interrupted when a user @mentions you, with the effect that
    /// the UI moves into the [UtilityActivity::ReadMentions] activity
    /// so that you can see what the mentioning user just posted.
    ///
    /// Following the UI principles of the original Monochrome, this
    /// applies to most activities, but not the ones involved with
    /// writing a post: your editing of a new post is not interrupted
    /// by a message, and neither is the options menu between editing
    /// and submitting your post.
    ///
    /// For other activities, this always returns `true` – even the
    /// `ReadMentions` activity _itself_.
    pub fn throw_into_mentions(&self) -> bool {
        !matches!(self, Activity::Compose(_))
    }
}

impl ActivityStack {
    /// Makes a new `ActivityStack`. The stack may not be empty, so
    /// the constructor requires a `NonUtilityActivity` to be its
    /// initial single element. This would normally be either
    /// [`MainMenu`](NonUtilityActivity::MainMenu) or
    /// [`LoginMenu`](NonUtilityActivity::LoginMenu).
    pub fn new(top: NonUtilityActivity) -> Self {
        ActivityStack {
            nonutil: vec![top],
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    }

    /// Pop the topmost activity from the stack, unless that would
    /// leave the stack empty, in which case, do nothing.
    ///
    /// No return value, because so far, callers haven't ever needed
    /// to know whether the pop was successful. But of course we could
    /// easily make it return a `bool` or a `Result` or something if
    /// necessary.
    pub fn pop(&mut self) {
        if self.help.is_some() {
            self.help = None;
        } else if self.util.is_some() {
            self.util = None;
        } else if self.compose.is_some() {
            self.compose = None;
        } else if self.nonutil.len() > 1 {
            // never pop the topmost activity
            self.nonutil.pop();
        }
    }

    /// Pop the topmost activity from the stack if it's an overlay
    /// activity. Otherwise, do nothing.
    pub fn pop_overlay(&mut self) {
        if self.help.is_some() {
            self.helpoverlay = None;
        } else {
            self.overlay = None;
        }
    }

    /// Save the utility activity we were in at the start of a
    /// sequence of operations generated by a single keypress.
    ///
    /// That way we can restore it when moving to an overlay, which
    /// means that for example Alt+E (desugaring as a very rapid ESC +
    /// E) can put the `GetUserToExamine` overlay on top of whatever
    /// you were previously looking at, rather than the less helpful
    /// utilities menu.
    pub fn new_event(&mut self) {
        self.initial_util.clone_from(&self.util);
    }

    /// Clear `initial_util`, which was set by `new_event()`. Sent at
    /// the end of a sequence of operations generated by a single
    /// keypress.
    pub fn completed_event(&mut self) {
        self.initial_util = None;
    }

    /// Make the provided activity be the topmost one on the activity
    /// stack, by pushing it as a new item if it's not already
    /// present, or by popping and discarding items above it if it is.
    pub fn goto(&mut self, act: Activity) {
        match act {
            Activity::Util(x) => {
                self.helpoverlay = None;
                self.help = None;
                self.overlay = None;
                self.util = Some(x);
            }
            Activity::Compose(x) => {
                self.helpoverlay = None;
                self.help = None;
                self.overlay = None;
                self.util = None;
                self.compose = Some(x);
            }
            Activity::NonUtil(NonUtilityActivity::MainMenu) => {
                // Special case: going to the Main Menu replaces the
                // topmost activity on the stack _even_ if it was not
                // previously the Main Menu. (Because it might have
                // been a preliminary login step and now we're
                // transitioning into full client operation.)
                self.helpoverlay = None;
                self.help = None;
                self.util = None;
                self.overlay = None;
                self.nonutil.clear();
                self.nonutil.push(NonUtilityActivity::MainMenu);
            }
            Activity::NonUtil(x) => {
                self.helpoverlay = None;
                self.help = None;
                self.util = None;
                self.overlay = None;
                let trunc = match self.nonutil.iter().position(|z| *z == x) {
                    Some(pos) => pos,
                    None => self.nonutil.len(),
                };
                self.nonutil.truncate(trunc);
                self.nonutil.push(x);
            }
            Activity::Overlay(x) => {
                if self.help.is_some() {
                    self.helpoverlay = Some(x);
                } else {
                    self.util.clone_from(&self.initial_util);
                    self.overlay = Some(x);
                }
            }
            Activity::Help(x) => {
                self.helpoverlay = None;
                self.help = Some(x);
            }
        }
    }

    /// Return the activity currently at the top of the stack.
    pub fn top(&self) -> Activity {
        match &self.help {
            Some(x) => Activity::Help(x.clone()),
            _ => match &self.util {
                Some(x) => Activity::Util(x.clone()),
                _ => match &self.compose {
                    Some(x) => Activity::Compose(x.clone()),
                    _ => match self.nonutil.last() {
                        Some(y) => Activity::NonUtil(y.clone()),
                        _ => Activity::NonUtil(NonUtilityActivity::MainMenu),
                    },
                },
            },
        }
    }

    /// Return the activity currently at the top of the stack, if it's
    /// an overlay activity, or `None` if it isn't.
    pub fn overlay(&self) -> Option<Activity> {
        let which = if self.help.is_some() {
            self.helpoverlay.as_ref()
        } else {
            self.overlay.as_ref()
        };

        which.map(|x| Activity::Overlay(x.clone()))
    }

    /// Make the provided activity be the topmost one on the activity
    /// stack, replacing the current topmost one.
    pub fn chain_to(&mut self, act: Activity) {
        assert!(
            self.helpoverlay.is_none(),
            "Don't expect to chain overlay actions"
        );
        assert!(
            self.overlay.is_none(),
            "Don't expect to chain overlay actions"
        );
        self.pop();
        self.goto(act);
    }

    /// Return an iterator that lists all the activities currently on
    /// the stack.
    pub fn iter(&self) -> impl Iterator<Item = Activity> + '_ {
        self.nonutil
            .iter()
            .cloned()
            .map(Activity::NonUtil)
            .chain(self.compose.iter().cloned().map(Activity::Compose))
            .chain(self.initial_compose.iter().cloned().map(Activity::Compose))
            .chain(self.util.iter().cloned().map(Activity::Util))
            .chain(self.initial_util.iter().cloned().map(Activity::Util))
            .chain(self.overlay.iter().cloned().map(Activity::Overlay))
            .chain(self.help.iter().cloned().map(Activity::Help))
            .chain(self.helpoverlay.iter().cloned().map(Activity::Overlay))
    }
}

#[test]
fn test() {
    let mut stk = ActivityStack::new(NonUtilityActivity::MainMenu);

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {NonUtilityActivity::MainMenu},
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(NonUtilityActivity::HomeTimelineFile.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(NonUtilityActivity::HomeTimelineFile.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(NonUtilityActivity::MainMenu.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {NonUtilityActivity::MainMenu},
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(NonUtilityActivity::HomeTimelineFile.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(UtilityActivity::UtilsMenu.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: Some(UtilityActivity::UtilsMenu),
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(UtilityActivity::ReadMentions.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: Some(UtilityActivity::ReadMentions),
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.pop();

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(UtilityActivity::ReadMentions.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: Some(UtilityActivity::ReadMentions),
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.goto(NonUtilityActivity::HomeTimelineFile.into());

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {
                NonUtilityActivity::MainMenu,
                NonUtilityActivity::HomeTimelineFile,
            },
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.pop();

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {NonUtilityActivity::MainMenu},
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );

    stk.pop();

    assert_eq!(
        stk,
        ActivityStack {
            nonutil: vec! {NonUtilityActivity::MainMenu},
            util: None,
            initial_util: None,
            compose: None,
            initial_compose: None,
            overlay: None,
            help: None,
            helpoverlay: None,
        }
    );
}
