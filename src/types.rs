use chrono::{DateTime, NaiveDate, Utc};
use serde::{Deserialize, Serialize};
use std::boxed::Box;
use std::fmt::{self, Display};
use std::option::Option;

use super::coloured_string::ColouredString;

#[derive(Deserialize, Debug, Clone)]
pub struct AccountField {
    pub name: String,
    pub value: String,
    pub verified_at: Option<DateTime<Utc>>,
}

/// Value that came from server (as JSON) that was supposedly a u64
///
/// Apparently some servers sometimes send negative numbers for these,
/// meaning unavailable:
///
/// <https://glitch-soc.github.io/docs/features/hide-follower-count/>
//
// If it turns out that servers send other values eg "null"
// or absent fields, or that such toleration is needed for other
// informational fields from servers, we should make this type
// more relaxed and/or rename it
// (and we may need to apply #[serde(default)] to fields of this type).
#[derive(Deserialize, Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
#[serde(try_from = "i64")]
// Not Option<u64> since that's two words and this is one
pub struct FollowersCount(i64);

impl TryFrom<i64> for FollowersCount {
    type Error = String;
    fn try_from(i: i64) -> Result<FollowersCount, Self::Error> {
        if i >= -1 {
            Ok(FollowersCount(i))
        } else {
            Err("unexpected nonnegative value, would tolerate -1".into())
        }
    }
}

impl Display for FollowersCount {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.0 >= 0 {
            Display::fmt(&self.0, f)
        } else if self.0 == -1 {
            write!(f, "unavailable")
        } else {
            // shouldn't happen but let's not panic
            write!(f, "unavailable ({})", self.0)
        }
    }
}

#[test]
fn test_supposedly_non_negative() {
    #[derive(Deserialize, Debug)]
    struct S {
        a: FollowersCount,
    }

    let chk = |j, exp_0, exp_display| {
        let s: S = serde_json::from_str(j).unwrap();
        assert_eq!(s.a.0, exp_0);
        assert_eq!(s.a.to_string(), exp_display);
    };
    let chk_err = |j| {
        serde_json::from_str::<S>(j).unwrap_err();
    };

    chk(r#"{ "a":  0 }"#, 0, "0");
    chk(r#"{ "a":  1 }"#, 1, "1");
    chk(r#"{ "a": -1 }"#, -1, "unavailable");
    chk_err(r#"{}"#);
    chk_err(r#"{ "a": -42 }"#);
    chk_err(r#"{ "a": null }"#);
}

// Special type wrapping chrono::NaiveDate. This is for use in the
// Account fields 'created_at' and 'last_status_at', which I've
// observed do not in fact contain full sensible ISO 8601 timestamps:
// last_status_at is just a YYYY-MM-DD date with no time or timezone,
// and created_at _looks_ like a full timestamp but the time part is
// filled in as 00:00:00 regardless. So I think the best we can do is
// to parse both of them as a NaiveDate.
//
// But if you just ask serde to expect a NaiveDate, it will complain
// if it _does_ see the T00:00:00Z suffix. So here I have a custom
// deserializer which strips that off.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ApproxDate(pub NaiveDate);

impl<'de> Deserialize<'de> for ApproxDate {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct StrVisitor;
        impl<'de> serde::de::Visitor<'de> for StrVisitor {
            type Value = ApproxDate;
            fn expecting(
                &self,
                formatter: &mut std::fmt::Formatter,
            ) -> std::fmt::Result {
                formatter.write_str("a date in ISO 8601 format")
            }
            fn visit_str<E>(self, orig_value: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                let value = match orig_value.split_once('T') {
                    Some((before, _after)) => before,
                    None => orig_value,
                };
                match NaiveDate::parse_from_str(value, "%Y-%m-%d") {
                    Ok(date) => Ok(ApproxDate(date)),
                    Err(_) => Err(E::custom(format!(
                        "couldn't get an ISO 8601 date from '{orig_value}'"
                    ))),
                }
            }
        }
        deserializer.deserialize_str(StrVisitor {})
    }
}

#[test]
fn test_approx_date() {
    // Works with just the YYYY-MM-DD date string
    let date: ApproxDate = serde_json::from_str("\"2023-09-20\"").unwrap();
    assert_eq!(
        date,
        ApproxDate(NaiveDate::from_ymd_opt(2023, 9, 20).unwrap())
    );

    // Works with a T00:00:00 suffix
    let date: ApproxDate =
        serde_json::from_str("\"2023-09-20T00:00:00\"").unwrap();
    assert_eq!(
        date,
        ApproxDate(NaiveDate::from_ymd_opt(2023, 9, 20).unwrap())
    );

    // Works with that _and_ the Z for timezone
    let date: ApproxDate =
        serde_json::from_str("\"2023-09-20T00:00:00Z\"").unwrap();
    assert_eq!(
        date,
        ApproxDate(NaiveDate::from_ymd_opt(2023, 9, 20).unwrap())
    );

    // Deserializing as an Option<ApproxDate> works too
    let date: Option<ApproxDate> =
        serde_json::from_str("\"2023-09-20T00:00:00\"").unwrap();
    assert_eq!(
        date,
        Some(ApproxDate(NaiveDate::from_ymd_opt(2023, 9, 20).unwrap()))
    );

    // And if you give it JSON 'null' in place of a string it gives None
    let date: Option<ApproxDate> = serde_json::from_str("null").unwrap();
    assert_eq!(date, None);
}

#[derive(Deserialize, Debug, Clone)]
pub struct Account {
    pub id: String,
    pub username: String,
    pub acct: String,
    pub url: String,
    pub display_name: String,
    pub note: String,
    pub avatar: String,
    pub avatar_static: String,
    pub header: String,
    pub header_static: String,
    pub locked: bool,
    pub fields: Vec<AccountField>,
    // pub emojis: Vec<Emoji>,
    pub bot: bool,
    pub group: bool,
    pub discoverable: Option<bool>,
    pub noindex: Option<bool>,
    pub moved: Option<Box<Account>>,
    pub suspended: Option<bool>,
    pub limited: Option<bool>,
    pub created_at: ApproxDate,
    pub last_status_at: Option<ApproxDate>,
    pub statuses_count: u64,
    pub followers_count: FollowersCount,
    pub following_count: u64,

    // In the wire protocol, 'CredentialAccount' is a subclass of
    // 'Account' containing this extra field, only available from some
    // requests, and only for your own account. We regard it as an
    // optional field of Account in general.
    pub source: Option<AccountSourceDetails>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct AccountSourceDetails {
    pub privacy: Visibility,
    pub sensitive: bool,
    pub language: Option<String>,
    pub follow_requests_count: usize,
    pub hide_collections: Option<bool>,
    pub discoverable: Option<bool>,
    pub indexable: Option<bool>,
    // 'fields' and 'note' here differ from the ones in the main
    // Account in that the bio, and the 'value' of each field, are in
    // the source text form rather than renderable HTML.
    pub note: String,
    pub fields: Vec<AccountField>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Application {
    pub name: String,
    pub website: Option<String>,
    pub client_id: Option<String>,
    pub client_secret: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Token {
    pub access_token: String,
    pub token_type: String,
    pub scope: String,
    pub created_at: u64, // Unix timestamp
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,
    #[serde(rename = "unlisted")]
    Unlisted,
    #[serde(rename = "private")]
    Private,
    #[serde(rename = "direct")]
    Direct,
}

impl Visibility {
    pub fn long_descriptions() -> [(Visibility, ColouredString); 4] {
        [
            (Visibility::Public, ColouredString::uniform("public", 'g')),
            (
                Visibility::Unlisted,
                ColouredString::plain(
                    "unlisted (visible but not shown in feeds)",
                ),
            ),
            (
                Visibility::Private,
                ColouredString::general(
                    "private (to followees and @mentioned users)",
                    "rrrrrrr                                    ",
                ),
            ),
            (
                Visibility::Direct,
                ColouredString::general(
                    "direct (only to @mentioned users)",
                    "rrrrrr                           ",
                ),
            ),
        ]
    }
}

#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub enum MediaType {
    #[serde(rename = "unknown")]
    Unknown,
    #[serde(rename = "image")]
    Image,
    #[serde(rename = "gifv")]
    GifV,
    #[serde(rename = "video")]
    Video,
    #[serde(rename = "audio")]
    Audio,
}

#[derive(Deserialize, Debug, Clone)]
pub struct MediaAttachment {
    pub id: String,
    #[serde(rename = "type")]
    pub mediatype: MediaType,
    pub url: String,
    pub preview_url: String,
    pub remote_url: Option<String>,
    pub description: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StatusMention {
    pub id: String,
    pub username: String,
    pub url: String,
    pub acct: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Poll {
    pub id: String,
    pub expires_at: Option<DateTime<Utc>>,
    pub multiple: bool,
    pub expired: bool,
    pub votes_count: u64,
    pub voters_count: Option<u64>, // if !multiple, doesn't need to be separate
    pub options: Vec<PollOption>,
    // pub emojis: Vec<Emoji>,
    pub voted: Option<bool>,
    pub own_votes: Option<Vec<usize>>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct PollOption {
    pub title: String,
    pub votes_count: Option<u64>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Status {
    pub id: String,
    pub uri: String,
    pub created_at: DateTime<Utc>,
    pub account: Account,
    pub content: String,
    pub visibility: Visibility,
    pub sensitive: bool,
    pub spoiler_text: String,
    pub media_attachments: Vec<MediaAttachment>,
    pub application: Option<Application>,
    pub mentions: Vec<StatusMention>,
    // pub tags: Vec<Hashtag>,
    // pub emojis: Vec<Emoji>,
    pub reblogs_count: u64,
    pub favourites_count: u64,
    pub replies_count: u64,
    pub url: Option<String>,
    pub in_reply_to_id: Option<String>,
    pub in_reply_to_account_id: Option<String>,
    pub reblog: Option<Box<Status>>,
    pub poll: Option<Poll>,
    // pub card: Option<PreviewCard>,
    pub language: Option<String>,
    pub text: Option<String>,
    pub edited_at: Option<DateTime<Utc>>,
    pub favourited: Option<bool>,
    pub reblogged: Option<bool>,
    pub muted: Option<bool>,
    pub bookmarked: Option<bool>,
    pub pinned: Option<bool>,
    // pub filtered: Option<Vec<FilterResult>>,
}

impl Status {
    pub fn strip_boost(self) -> Status {
        match self.reblog {
            Some(b) => *b,
            None => self,
        }
    }
}

#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Hash)]
pub struct StatusSource {
    pub id: String,
    pub text: String,
    pub spoiler_text: String,
}

#[derive(Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub enum NotificationType {
    #[serde(rename = "mention")]
    Mention,
    #[serde(rename = "status")]
    Status,
    #[serde(rename = "reblog")]
    Reblog,
    #[serde(rename = "follow")]
    Follow,
    #[serde(rename = "follow_request")]
    FollowRequest,
    #[serde(rename = "favourite")]
    Favourite,
    #[serde(rename = "poll")]
    Poll,
    #[serde(rename = "update")]
    Update,
    #[serde(rename = "admin.sign_up")]
    AdminSignUp,
    #[serde(rename = "admin.report")]
    AdminReport,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Notification {
    pub id: String,
    #[serde(rename = "type")]
    pub ntype: NotificationType,
    pub created_at: DateTime<Utc>,
    pub account: Account,
    pub status: Option<Status>,
    // pub report: Option<AdminReport>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Relationship {
    pub id: String,
    pub following: bool,
    pub showing_reblogs: bool,
    pub notifying: bool,
    pub languages: Option<Vec<String>>,
    pub followed_by: bool,
    pub blocking: bool,
    pub blocked_by: bool,
    pub muting: bool,
    pub muting_notifications: bool,
    pub requested: bool,
    pub requested_by: bool,
    pub domain_blocking: bool,
    pub endorsed: bool,
    pub note: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct InstanceStatusConfig {
    pub max_characters: usize,
    pub max_media_attachments: usize,
    pub characters_reserved_per_url: usize,
}

#[derive(Deserialize, Debug, Clone)]
pub struct InstanceUrlConfig {
    pub streaming: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct InstanceConfig {
    pub statuses: InstanceStatusConfig,
    pub urls: InstanceUrlConfig,
}

#[derive(Deserialize, Debug, Clone)]
pub struct InstanceRule {
    pub id: String,
    pub text: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Instance {
    pub domain: String,
    pub description: String,
    pub configuration: InstanceConfig,
    pub rules: Vec<InstanceRule>,
    // FIXME: lots of things are missing from here!
}

#[derive(Deserialize, Debug, Clone)]
pub struct Context {
    pub ancestors: Vec<Status>,
    pub descendants: Vec<Status>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ServerError {
    pub error: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct SearchResults {
    pub accounts: Vec<Account>,
    pub statuses: Vec<Status>,
    pub hashtags: Vec<Tag>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Tag {
    pub name: String,
    pub url: String,
    pub following: Option<bool>,
    // also history: Vec<something that has "day", "uses", "accounts">
}
