//! Module providing the [`ColouredString`] type central to
//! Mastodonochrome's text UI.
//!
//! Mastodonochrome's text UI makes heavy use of multicoloured
//! strings. This module implements the [`ColouredString`] type, which
//! stores a UTF-8 string together with a 'colour' for each character.
//!
//! Colours are semantic, not physical. Every colour used by
//! Mastodonochrome has a one-character identifier. The mapping from
//! those identifiers to physical terminal attributes, together with
//! comments about what the identifiers are used for, lives in the
//! private function `ratatui_style_from_colour()` in the
//! [`tui`](super::tui) module. The default colour is represented by
//! the space character `' '`.
//!
//! The other most important type in this module is
//! [`ColouredStringSlice`], which has very similar semantics but
//! stores two `&str` instead of two `String`. This can't be made into
//! a pure reference, because [`ColouredString`] stores nothing with
//! the physical representation of a pair of `&str`, so
//! [`ColouredStringSlice`] must be created on demand rather than
//! magicked into existence via [`std::borrow::Borrow`] or similar.

use unicode_width::UnicodeWidthChar;
use unicode_width::UnicodeWidthStr;

/// Trait holding the common functionality between [`ColouredString`]
/// and [`ColouredStringSlice`].
pub trait ColouredStringCommon {
    /// Returns a reference to the text stored in the string.
    fn text(&self) -> &str;

    /// Returns a reference to the colours stored in the string.
    fn colours(&self) -> &str;

    /// Returns `true` if the string contains no characters.
    fn is_empty(&self) -> bool {
        self.text().is_empty()
    }

    /// Returns the number of characters in the string.
    fn nchars(&self) -> usize {
        self.text().chars().count()
    }

    /// Returns the width of the string in a terminal, as measured by
    /// [`UnicodeWidthStr::width`].
    fn width(&self) -> usize {
        UnicodeWidthStr::width(self.text())
    }

    /// Constructs a [`ColouredStringSlice`] describing the whole of
    /// this string.
    fn slice(&self) -> ColouredStringSlice {
        ColouredStringSlice {
            text: self.text(),
            colours: self.colours(),
        }
    }

    /// Constructs a [`ColouredStringSlice`] describing the first
    /// `width` characters of this string, or the whole string,
    /// whichever is shorter.
    fn truncate(&self, width: usize) -> ColouredStringSlice {
        self.split(width).next().unwrap()
    }

    /// Iterates over the characters of the string, returning a
    /// one-character [`ColouredStringSlice`] for each one.
    fn chars(&self) -> ColouredStringCharIterator {
        ColouredStringCharIterator {
            cs: self.slice(),
            textpos: 0,
            colourpos: 0,
        }
    }

    /// Divides the string into maximal fragments of a single colour,
    /// and iterates over those, returning a [`ColouredStringSlice`]
    /// for each one.
    fn frags(&self) -> ColouredStringFragIterator {
        ColouredStringFragIterator {
            cs: self.slice(),
            textpos: 0,
            colourpos: 0,
        }
    }

    /// Divides the string into fragments of terminal width `width`,
    /// or as close as possible, and iterates over those, returning a
    /// [`ColouredStringSlice`] for each one.
    fn split(&self, width: usize) -> ColouredStringSplitIterator {
        ColouredStringSplitIterator {
            cs: self.slice(),
            width,
            textpos: 0,
            colourpos: 0,
            delivered_empty: false,
        }
    }

    /// Constructs a [`ColouredString`] consisting of this string
    /// repeated `count` times.
    fn repeat(&self, count: usize) -> ColouredString {
        ColouredString::general(
            &self.text().repeat(count),
            &self.colours().repeat(count),
        )
    }

    /// Constructs a [`ColouredString`] consisting of the text of this
    /// string, but with every character's colour reset to `colour`.
    fn recolour(&self, colour: char) -> ColouredString {
        ColouredString::uniform(self.text(), colour)
    }

    /// Returns `true` if this string _begins_ with a space character,
    /// or `false` if it's empty, or begins with any other character.
    ///
    /// (It's expected that you call this either on single-character
    /// strings returned from [`chars()`](ColouredStringSlice::chars),
    /// or on strings you've already separated into space and
    /// non-space ones, e.g. during word wrapping.)
    fn is_space(&self) -> bool {
        if let Some(ch) = self.text().chars().next() {
            ch == ' '
        } else {
            false
        }
    }

    /// Returns true if every character in this string has colour
    /// `colour`.
    fn is_colour(&self, colour: char) -> bool {
        self.frags().all(|(_, c)| c == colour)
    }

    /// Returns true if at least one character in this string has
    /// colour `colour`.
    fn has_colour(&self, colour: char) -> bool {
        self.frags().any(|(_, c)| c == colour)
    }
}

impl<T: ColouredStringCommon> ColouredStringCommon for &'_ T {
    fn text(&self) -> &str {
        <T as ColouredStringCommon>::text(self)
    }
    fn colours(&self) -> &str {
        <T as ColouredStringCommon>::colours(self)
    }
}

/// Type that physically stores a string with a colour for each character.
///
/// The most general way to construct one is using the
/// [`general()`][ColouredString::general] constructor, which takes
/// one string for the text, and another string for the colour of each
/// character. If the string is all the same colour, the
/// [`uniform()`][ColouredString::uniform] replaces the string of
/// colours with a single `char`; if that colour is the default one
/// (represented by `' '`), then the
/// [`plain()`][ColouredString::plain] constructor omits the colour
/// parameter completely.
///
/// ```
/// let cs = ColouredString::general("hello", "     ");
/// let cs = ColouredString::uniform("hello", ' ');
/// let cs = ColouredString::plain("hello");
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ColouredString {
    text: String,
    colours: String,
}

impl ColouredStringCommon for ColouredString {
    fn text(&self) -> &str {
        &self.text
    }
    fn colours(&self) -> &str {
        &self.colours
    }
}

// I'd have liked here to write
// impl<T: ColouredStringCommon> From<T> for ColouredString { ... }
// on the basis that this code can sensibly make an owned ColouredString
// out of anything that provides .text() and .colours().
//
// But the problem is that that conflicts with the standard reflexive
// definition in which ColouredString itself _already_ implements
// From<ColouredString> via the identity function. Of course that _is_
// a much more sensible definition in that one case! And I don't know
// of any language feature that lets me offer the definition below for
// only everything _else_ satisfying the type bound.
impl<'a> From<ColouredStringSlice<'a>> for ColouredString {
    fn from(value: ColouredStringSlice<'a>) -> Self {
        ColouredString {
            text: value.text().to_owned(),
            colours: value.colours().to_owned(),
        }
    }
}

impl ColouredString {
    /// Constructs a [`ColouredString`] in which every character of
    /// the string has the default colour `' '`.
    pub fn plain(text: &str) -> Self {
        ColouredString {
            text: text.to_owned(),
            colours: " ".repeat(text.chars().count()),
        }
    }

    /// Constructs a [`ColouredString`] in which every character of
    /// the string has the same colour, specified by `colour`.
    pub fn uniform(text: &str, colour: char) -> Self {
        ColouredString {
            text: text.to_owned(),
            colours: colour.to_string().repeat(text.chars().count()),
        }
    }

    /// Constructs a fully general [`ColouredString`]. The two strings
    /// `text` and `colours` must have the same number of Unicode
    /// characters (as measured by `.chars.count()`). The characters
    /// are matched up one-to-one in order along both strings, with
    /// each character of `colours` specifying the colour of the
    /// corresponding character in `text`.
    pub fn general(text: &str, colours: &str) -> Self {
        assert_eq!(
            text.chars().count(),
            colours.chars().count(),
            "Mismatched lengths in ColouredString::general"
        );
        ColouredString {
            text: text.to_owned(),
            colours: colours.to_owned(),
        }
    }

    /// Constructs a [`ColouredString`] as the concatenation of two
    /// other objects implementing [`ColouredStringCommon`]. This is a
    /// private method, used to implement the [`Add`](std::ops::Add).
    fn concat<T: ColouredStringCommon, U: ColouredStringCommon>(
        lhs: T,
        rhs: U,
    ) -> ColouredString {
        ColouredString {
            text: lhs.text().to_owned() + rhs.text(),
            colours: lhs.colours().to_owned() + rhs.colours(),
        }
    }

    /// Mutates the string by appending the contents of `more`.
    pub fn push_str(&mut self, more: impl ColouredStringCommon) {
        self.text.push_str(more.text());
        self.colours.push_str(more.colours());
    }
}

/// Type that stores a coloured string by reference to text and colour
/// strings elsewhere.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ColouredStringSlice<'a> {
    text: &'a str,
    colours: &'a str,
}

impl<'a> ColouredStringCommon for ColouredStringSlice<'a> {
    fn text(&self) -> &str {
        self.text
    }
    fn colours(&self) -> &str {
        self.colours
    }
}

impl<'a> ColouredStringSlice<'a> {
    /// Constructs a [`ColouredStringSlice`], similarly to the
    /// [`ColouredString::general()`] constructor, except that the two
    /// [&str] are stored as references rather than cloned.
    pub fn general(text: &'a str, colours: &'a str) -> Self {
        assert_eq!(
            text.chars().count(),
            colours.chars().count(),
            "Mismatched lengths in ColouredStringSlice::general"
        );

        ColouredStringSlice { text, colours }
    }
}

// We want to be able to use the + operator to concatenate any two
// types that impl ColouredStringCommon. The logic of _how_ to do this
// is already written, in the ColouredString::concat() constructor
// above. Now we just need to make the + operator invoke it.
//
// I'd like to do this by writing a single item along the lines of
// impl<T: ColouredStringCommon, U: ColouredStringCommon> Add<U> for T { ... }
// but that's not allowed, because if a user of this module implemented
// ColouredStringCommon for one of _their_ types, then this would end up
// trying to implement a trait that's not mine (Add) on a type that's not
// mine (the user's type).
//
// Making ColouredStringCommon into a sealed trait doesn't cause the
// compiler to recognise that risk as nonexistent, which is a shame.
// So instead let's do some macro business, to avoid copy-pasting four
// times.
/// Macro used internally to implement [`Add`](std::ops::Add)` for the
/// types in this module.
macro_rules! impl_Add {
    ($type:ty) => {
        impl<U: ColouredStringCommon> std::ops::Add<U> for $type {
            /// The result of concatenating any two things
            /// implementing [`ColouredStringCommon`] is a new
            /// [`ColouredString`.]
            type Output = ColouredString;

            /// Makes a new [`ColouredString`] containing the
            /// concatenation of the two input strings.
            fn add(self, rhs: U) -> ColouredString {
                ColouredString::concat(self, rhs)
            }
        }
    };
}

impl_Add!(ColouredString);
impl_Add!(&ColouredString);
impl_Add!(ColouredStringSlice<'_>);
impl_Add!(&ColouredStringSlice<'_>);

/// Iterator type returned by [`ColouredStringCommon::chars()`].
pub struct ColouredStringCharIterator<'a> {
    cs: ColouredStringSlice<'a>,
    textpos: usize,
    colourpos: usize,
}

/// Iterator type returned by [`ColouredStringCommon::frags()`].
pub struct ColouredStringFragIterator<'a> {
    cs: ColouredStringSlice<'a>,
    textpos: usize,
    colourpos: usize,
}

/// Iterator type returned by [`ColouredStringCommon::split()`].
pub struct ColouredStringSplitIterator<'a> {
    cs: ColouredStringSlice<'a>,
    width: usize,
    textpos: usize,
    colourpos: usize,
    delivered_empty: bool,
}

impl<'a> Iterator for ColouredStringCharIterator<'a> {
    /// The iterator output type is a [`ColouredStringSlice`],
    /// pointing into the original string on which
    /// [`chars()`](ColouredStringCommon::chars) was called.
    ///
    /// Every string slice returned from this iterator contains
    /// exactly one character.
    type Item = ColouredStringSlice<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        let textslice = &self.cs.text[self.textpos..];
        let mut textit = textslice.char_indices();
        let colourslice = &self.cs.colours[self.colourpos..];
        let mut colourit = colourslice.char_indices();
        if let (Some(_), Some(_)) = (textit.next(), colourit.next()) {
            let (textend, colourend) = match (textit.next(), colourit.next()) {
                (Some((tpos, _)), Some((cpos, _))) => (tpos, cpos),
                _ => (textslice.len(), colourslice.len()),
            };
            self.textpos += textend;
            self.colourpos += colourend;
            Some(ColouredStringSlice {
                text: &textslice[..textend],
                colours: &colourslice[..colourend],
            })
        } else {
            None
        }
    }
}

impl<'a> Iterator for ColouredStringFragIterator<'a> {
    /// The iterator output type is a [`ColouredStringSlice`],
    /// pointing into the original string on which
    /// [`chars()`](ColouredStringCommon::chars) was called.
    ///
    /// Every string slice returned from this iterator contains
    /// exactly one colour.
    type Item = (&'a str, char);
    fn next(&mut self) -> Option<Self::Item> {
        let textslice = &self.cs.text[self.textpos..];
        let mut textit = textslice.char_indices();
        let colourslice = &self.cs.colours[self.colourpos..];
        let mut colourit = colourslice.char_indices();
        if let Some((_, colour)) = colourit.next() {
            // Expect this not to run out, because colour didn't
            textit.next().unwrap();

            let (textend, colourend) = loop {
                match colourit.next() {
                    None => break (textslice.len(), colourslice.len()),
                    Some((colourpos_here, colour_here)) => {
                        let (textpos_here, _) = textit.next().unwrap();
                        if colour_here != colour {
                            break (textpos_here, colourpos_here);
                        }
                    }
                }
            };
            self.textpos += textend;
            self.colourpos += colourend;
            Some((&textslice[..textend], colour))
        } else {
            None
        }
    }
}

/// Return the terminal width of a character, avoiding fallibility by
/// making the assumption that unrecognised characters have terminal
/// width zero.
fn char_width_infallible(c: char) -> usize {
    UnicodeWidthChar::width(c).unwrap_or(0)
}

impl<'a> Iterator for ColouredStringSplitIterator<'a> {
    /// The iterator output type is a [`ColouredStringSlice`],
    /// pointing into the original string on which
    /// [`chars()`](ColouredStringCommon::chars) was called.
    ///
    /// Every string slice returned from this iterator has terminal
    /// width at most `width`.
    type Item = ColouredStringSlice<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        let textslice = &self.cs.text[self.textpos..];
        let mut textit = textslice.char_indices();
        let colourslice = &self.cs.colours[self.colourpos..];
        let mut colourit = colourslice.char_indices();
        match (textit.next(), colourit.next()) {
            (None, None) => {
                if self.textpos == 0 && !self.delivered_empty {
                    self.delivered_empty = true;
                    Some(ColouredStringSlice::general("", ""))
                } else {
                    None
                }
            }
            (Some((_, tc)), Some(_)) => {
                let mut tpos: usize = 0;
                let mut cpos: usize = 0;
                let mut width: usize = 0;
                let mut last_tc = tc;
                let (textend, colourend) = loop {
                    let wc = char_width_infallible(last_tc);
                    if width + wc > self.width {
                        break (tpos, cpos);
                    }
                    width += wc;
                    match (textit.next(), colourit.next()) {
                        (None, None) => {
                            break (textslice.len(), colourslice.len());
                        }
                        (Some((ti, tc)), Some((ci, _))) => {
                            tpos = ti;
                            cpos = ci;
                            last_tc = tc;
                        }
                        _ => panic!("length mismatch in CSSI"),
                    };
                };
                self.textpos += textend;
                self.colourpos += colourend;
                Some(ColouredStringSlice {
                    text: &textslice[..textend],
                    colours: &colourslice[..colourend],
                })
            }
            _ => panic!("length mismatch in CSSI"),
        }
    }
}

#[test]
fn test_constructors() {
    assert_eq!(
        ColouredString::plain("hello"),
        ColouredString::general("hello", "     ")
    );
    assert_eq!(
        ColouredString::uniform("hello", 'a'),
        ColouredString::general("hello", "aaaaa")
    );
}

#[test]
fn test_repeat() {
    assert_eq!(
        ColouredString::general("xyz", "pqr").repeat(3),
        ColouredString::general("xyzxyzxyz", "pqrpqrpqr")
    );
}

#[test]
fn test_concat() {
    assert_eq!(
        ColouredString::general("xyz", "pqr")
            + ColouredString::general("abcde", "ijklm"),
        ColouredString::general("xyzabcde", "pqrijklm")
    );
}

#[test]
fn test_lengths() {
    assert_eq!(ColouredString::general("xyz", "pqr").nchars(), 3);
    assert_eq!(ColouredString::general("xyz", "pqr").width(), 3);

    assert_eq!(ColouredString::general("xy\u{302}z", "pqqr").nchars(), 4);
    assert_eq!(ColouredString::general("xy\u{302}z", "pqqr").width(), 3);
}

#[test]
fn test_chars() {
    let cs = ColouredString::general("ábé", "xyz");
    let mut chars = cs.chars();
    assert_eq!(chars.next(), Some(ColouredStringSlice::general("á", "x")));
    assert_eq!(chars.next(), Some(ColouredStringSlice::general("b", "y")));
    assert_eq!(chars.next(), Some(ColouredStringSlice::general("é", "z")));
    assert_eq!(chars.next(), None);
}

#[test]
fn test_frags() {
    let cs = ColouredString::general("stóat wèasël", "uuuuu vvvvvv");
    let mut frags = cs.frags();
    assert_eq!(frags.next(), Some(("stóat", 'u')));
    assert_eq!(frags.next(), Some((" ", ' ')));
    assert_eq!(frags.next(), Some(("wèasël", 'v')));
    assert_eq!(frags.next(), None);
}

#[test]
fn test_split() {
    let cs = ColouredString::general("abcdefgh", "mnopqrst");
    let mut lines = cs.split(3);
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("abc", "mno"))
    );
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("def", "pqr"))
    );
    assert_eq!(lines.next(), Some(ColouredStringSlice::general("gh", "st")));
    assert_eq!(lines.next(), None);
    let mut lines = cs.split(4);
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("abcd", "mnop"))
    );
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("efgh", "qrst"))
    );
    assert_eq!(lines.next(), None);

    let cs = ColouredStringSlice::general("ab\u{4567}defgh", "mnopqrst");
    let mut lines = cs.split(3);
    assert_eq!(lines.next(), Some(ColouredStringSlice::general("ab", "mn")));
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("\u{4567}d", "op"))
    );
    assert_eq!(
        lines.next(),
        Some(ColouredStringSlice::general("efg", "qrs"))
    );
    assert_eq!(lines.next(), Some(ColouredStringSlice::general("h", "t")));
    assert_eq!(lines.next(), None);

    let cs = ColouredStringSlice::general("", "");
    let mut lines = cs.split(3);
    assert_eq!(lines.next(), Some(ColouredStringSlice::general("", "")));
    assert_eq!(lines.next(), None);
}
