//! Module that runs the top level of the Mastodonochrome TUI.
//!
//! The two main types in this module are [`Tui`] and
//! [`TuiLogicalState`], which contain most of the top-level TUI logic
//! between them. The split between them is, more or less, that `Tui`
//! deals with the mechanics of receiving input from various places,
//! serialising it, and translating it into our own internal
//! description types such as [`OurKey`]; then it hands one event at a
//! time to `TuiLogicalState`, which knows what state the TUI is in
//! (like which menu or file the user is viewing) and can decide what
//! to do with the event; `TuiLogicalState` returns a
//! [`PhysicalAction`] back to `Tui` to tell it what to actually do,
//! and `Tui` takes care of actually producing output on the terminal,
//! or going beep, or exiting the program.
//!
//! `TuiLogicalState`, in turn, delegates a lot of its decision-making
//! to various types representing particular Tui activities. The
//! _descriptions_ of currently active activities are stored in an
//! [`ActivityStack`]; each activity's detailed state is stored in a
//! type specific to that activity, implementing the [`ActivityState`]
//! trait. `TuiLogicalState` passes most keystrokes to the current
//! activity, which returns [`LogicalAction`] to tell it what to do.
//! Some `LogicalAction`s can be handled within `TuiLogicalState`,
//! e.g. by switching to a new activity, pushing or popping the
//! activity stack; others require a physical response from `Tui` and
//! are translated into nontrivial `PhysicalAction`.
//!
//! One very large exception to the policy of `Tui` doing all the
//! interaction with the outside world is that `Tui` also owns a
//! [`Client`] object for making requests to the Mastodon server, and
//! passes it as a mut reference to `TuiLogicalState` which in turn
//! lends it to many of its subroutines in other modules. So _most_ of
//! the Mastodonochrome code can directly make (synchronous) HTTP
//! requests to the Mastodon server, because that `&mut Client` is
//! passed around everywhere.
//!
//! This module is entered from `main.rs` by calling [`Tui::run()`],
//! which doesn't actually _return_ a [`Tui`] object, it just makes
//! one internally, runs the main UI loop to completion, and discards
//! the `Tui` before returning.

use crossterm::{
    event::{self, Event, KeyCode, KeyEvent, KeyEventKind, KeyModifiers},
    terminal::{
        disable_raw_mode, enable_raw_mode, EnterAlternateScreen,
        LeaveAlternateScreen,
    },
    ExecutableCommand,
};
use ratatui::{
    prelude::{Buffer, CrosstermBackend, Rect, Terminal},
    style::{Color, Modifier, Style},
};
use std::cell::RefCell;
use std::cmp::min;
use std::collections::{hash_map, BTreeMap, HashMap, HashSet};
use std::fs::File;
use std::io::{stdout, Stdout, Write};
use std::rc::Rc;
use std::time::Duration;
use unicode_width::UnicodeWidthStr;

use super::activity_stack::*;
use super::auth::{AuthConfig, AuthError};
use super::client::{
    AnyFeedId, Client, ClientError, FeedExtend, FeedId, LocalFeedId, StreamId,
    StreamUpdate,
};
use super::coloured_string::*;
use super::config::{ConfigError, ConfigLocation};
use super::editor::*;
use super::file::*;
use super::login::{finish_account_setup, login_menu, LoginError};
use super::menu::*;
use super::options::*;
use super::posting::*;
use super::text::{CentredInfoLine, DefaultDisplayStyle, TextFragmentOneLine};

/// Translates the `char` colour ids used in [`ColouredString`] to
/// terminal styles.
fn ratatui_style_from_colour(colour: char) -> Style {
    match colour {
        // default
        ' ' => Style::default(),

        // message separator line, other than the date
        'S' => Style::default()
            .fg(Color::Gray)
            .bg(Color::Blue)
            .add_modifier(Modifier::REVERSED | Modifier::BOLD),

        // date on a message separator line
        'D' => Style::default()
            .fg(Color::Gray)
            .bg(Color::Blue)
            .add_modifier(Modifier::REVERSED),

        // username in a From line
        'F' => Style::default()
            .fg(Color::Green)
            .add_modifier(Modifier::BOLD),

        // username in other headers like Via
        'f' => Style::default().fg(Color::Green),

        // <code> tags
        'c' => Style::default().fg(Color::Yellow),

        // #hashtags
        '#' => Style::default().fg(Color::Cyan),

        // @mentions of a user
        '@' => Style::default().fg(Color::Green),

        // <em> tags
        '_' => Style::default().add_modifier(Modifier::UNDERLINED),

        // <strong> tags
        's' => Style::default().add_modifier(Modifier::BOLD),

        // URL
        'u' => Style::default()
            .fg(Color::Blue)
            .add_modifier(Modifier::BOLD | Modifier::UNDERLINED),

        // media URL
        'M' => Style::default()
            .fg(Color::Magenta)
            .add_modifier(Modifier::BOLD),

        // media description
        'm' => Style::default().fg(Color::Magenta),

        // Mastodonochrome logo in file headers
        'J' => Style::default()
            .fg(Color::Blue)
            .bg(Color::Gray)
            .add_modifier(Modifier::REVERSED | Modifier::BOLD),

        // ~~~~~ underline in file headers
        '~' => Style::default().fg(Color::Blue),

        // actual header text in file headers and other client-originated
        // instructions or information
        'H' => Style::default().fg(Color::Cyan),

        // keypress / keypath names in file headers
        'K' => Style::default()
            .fg(Color::Cyan)
            .add_modifier(Modifier::BOLD),

        // underlined text for emphasis in 'H' text
        '=' => Style::default()
            .fg(Color::Cyan)
            .add_modifier(Modifier::UNDERLINED),

        // keypresses in file status lines
        'k' => Style::default().add_modifier(Modifier::BOLD),

        // bolded emphasis in help
        '+' => Style::default().add_modifier(Modifier::BOLD),

        // separator line between editor header and content
        '-' => Style::default()
            .fg(Color::Cyan)
            .bg(Color::Black)
            .add_modifier(Modifier::REVERSED),

        // something really boring, like 'none' in place of data
        '0' => Style::default().fg(Color::Blue),

        // green for success
        'g' => Style::default().fg(Color::Green),
        // amber for ambiguous success
        'a' => Style::default().fg(Color::Yellow),
        // red nastinesses like blocking/muting in Examine User
        'r' => Style::default().fg(Color::Red),

        // # reverse-video > indicating a truncated too-long line
        '>' => Style::default().add_modifier(Modifier::REVERSED),

        // a selected user or status you're about to operate on while
        // viewing a file
        '*' => Style::default()
            .fg(Color::Cyan)
            .bg(Color::Black)
            .add_modifier(Modifier::REVERSED),

        // # error report, or by default any unrecognised colour character
        '!' | _ => Style::default()
            .fg(Color::Red)
            .bg(Color::Yellow)
            .add_modifier(Modifier::REVERSED | Modifier::BOLD),
    }
}

/// Drawing helper to display a [`ColouredString`] in a `ratatui`
/// [`Buffer`].
fn ratatui_set_string(
    buf: &mut Buffer,
    x: usize,
    y: usize,
    text: impl ColouredStringCommon,
) {
    let mut x = x;
    if let Ok(y) = y.try_into() {
        for (frag, colour) in text.frags() {
            if let Ok(x) = x.try_into() {
                buf.set_string(x, y, frag, ratatui_style_from_colour(colour));
            } else {
                break;
            }
            x += UnicodeWidthStr::width(frag);
        }
    }
}

/// Events delivered to the main TUI thread from a subthread receiving input.
#[derive(Debug, Clone)]
enum SubthreadEvent {
    /// An event happened on the terminal (either a keypress or a
    /// window resize).
    TermEv(Event),

    /// A streaming connection to the Mastodon server notified us of
    /// an update.
    StreamEv(StreamUpdate),

    /// A timing subthread sent us a regular heartbeat event, which we
    /// use to do periodic tasks like checkpointing the user's LDB.
    Heartbeat,

    /// It's time to restart a streaming connection to the Mastodon
    /// server, after it died and we scheduled a timer to wait a bit
    /// before restarting it.
    StreamRestart(StreamId),
}

/// Action type returned to `Tui` by `TuiLogicalState`.
///
/// There's no action type for 'update the terminal screen contents',
/// because that's taken as a given. Even the `Nothing` branch of this
/// enum still redraws the screen; it just doesn't do anything _else_.
pub enum PhysicalAction {
    /// No additional action beyond redrawing the screen.
    Nothing,

    /// Make the terminal beep.
    Beep,

    /// Clear the terminal screen completely, so that it will be
    /// redrawn in full rather than via a minimal incremental update.
    /// Triggered by the user pressing `[^L]`, in case of display
    /// corruption.
    Refresh,

    /// Terminate the whole TUI main loop and return from
    /// `Tui::run()`, so that Mastodonochrome exits.
    Exit,

    /// If Mastodonochrome was started without already being logged in
    /// to a Mastodon server, this action indicates that login (or
    /// account registration) is complete, and we're about to put the
    /// Main Menu at the top of the activity stack. Therefore, we must
    /// do any setup requiring the user to be logged in, which would
    /// have been done right at the beginning of the program under
    /// other circumstances.
    MainSessionSetup,

    /// Terminate the TUI main loop and make Mastodonochrome exit with
    /// a fatal error message. This is much worse than the kind of
    /// recoverable error that goes into the Error Log without
    /// terminating the GUI.
    Error(TuiError),
}

/// The outer one of the two main types implementing the top-level TUI.
///
/// As the module description mentions, this type is responsible for
/// receiving input from multiple sources (the terminal, and streaming
/// connections from the Mastodon server), which it does by having a
/// subthread listening to each source and writing [`SubthreadEvent`]
/// events into a [`sync_channel`](std::sync::mpsc::sync_channel);
/// then we listen to the other end of the channel to receive the
/// events from all subthreads together.
///
/// On the output side, we're also responsible for producing terminal
/// output, by owning a [`Terminal`]. But we don't own it _directly_:
/// we own it indirectly, via a [`TuiOutput`] which we keep in an
/// `Rc<RefCell>`, so that the [`Client`] can
/// also have a reference to the `TuiOutput` and use it to indicate
/// 'busy' status whenever it begins a synchronous network operation.
///
/// Apart from terminal output, the other kind of output we generate
/// is in the form of HTTP requests to the Mastodon server. But those
/// are not funnelled through this type. Instead, we lend out our
/// `Client` as a mut reference to `TuiLogicalState`, which lends it
/// out far and wide to many other parts of the code, so that they can
/// all make their requests directly, which is much easier.
///
/// A `Tui` object is never returned to any other part of the code.
/// The only construction of one takes place inside [`Tui::run()`],
/// which runs for the lifetime of the `Tui` and destroys it again
/// before terminating.
///
/// Some incoming events we handle ourselves, for example by
/// restarting a dead subthread. But keystrokes and updates to the
/// Mastodon content are the job of our companion type
/// [`TuiLogicalState`] to decide what to do with, via
/// `handle_keypress` or `handle_feed_updates` respectively. Either of
/// those will return one or more [`PhysicalAction`] values to tell us
/// what physical response to give.
pub struct Tui {
    output: Rc<RefCell<TuiOutput>>,
    subthread_sender: std::sync::mpsc::SyncSender<SubthreadEvent>,
    subthread_receiver: std::sync::mpsc::Receiver<SubthreadEvent>,
    state: TuiLogicalState,
    client: Client,
}

/// Our own enumeration of keypresses.
///
/// Partly this is to keep the `crossterm` terminal handling from
/// spreading throughout the code in case we ever need to migrate to a
/// different TUI crate in future. Partly, it's so that we can do some
/// preprocessing on `crossterm`'s keyboard events, for example
/// converting its representation of Alt+x (which is useful for many
/// other kinds of TUI) back into Escape followed by x (which is the
/// way Monochrome likes it, and so does Mastodonochrome).
#[derive(Eq, PartialEq, Debug, Clone, Copy, Hash)]
pub enum OurKey {
    /// A printable character, other than space.
    Pr(char),

    /// A control character which doesn't have a more specific name
    /// like `Return`. The `char` parameter is expected to be in the
    /// range `0x40..0x60`, e.g. the right way to specify `[^A]` is as
    /// `Ctrl('A')`, not `Ctrl('a')` (even though _as keypresses_
    /// they're the same).
    Ctrl(char),

    /// A numbered function key, e.g. `FunKey(2)` is the `[F2]` key.
    FunKey(u8),

    /// The backspace key, i.e. the one the user expects to delete the
    /// character to the left of the insertion position. (Whether this
    /// was represented as `[^H]` or `[^?]` according to the `stty`
    /// settings is the problem of whatever code _generates_ this
    /// value, and isn't represented here.)
    Backspace,

    /// The Return key
    Return,

    /// The Escape key. Typically in a terminal this generates the
    /// same character that is also a prefix of many function keys'
    /// control sequences. `crossterm` can nonetheless detect Escape
    /// by itself, at the cost of a short delay while it waits to see
    /// if the rest of a control sequence follows.
    Escape,

    /// The space character
    Space,

    /// The up arrow
    Up,

    /// The down arrow
    Down,

    /// The left arrow
    Left,

    /// The right arrow
    Right,

    /// The PgUp key
    PgUp,

    /// The PgDn key
    PgDn,

    /// The Home key
    Home,

    /// The End key
    End,

    /// The Ins key
    Ins,

    /// The Del key. That is, the one that goes with Ins on the keypad
    /// above the arrow keys, which the user probably expects to
    /// delete the character to the _right_ of the insertion position.
    /// `Backspace` is the key on the main keyboard that deletes to
    /// the left.
    Del,
}

/// Error type returned from [`Tui::run()`] if it can't continue.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct TuiError {
    message: String,
}

impl TuiError {
    pub fn new(message: &str) -> Self {
        TuiError {
            message: message.to_owned(),
        }
    }
}

impl super::top_level_error::TopLevelErrorCandidate for TuiError {}

impl From<String> for TuiError {
    fn from(message: String) -> Self {
        TuiError { message }
    }
}
impl From<std::io::Error> for TuiError {
    fn from(err: std::io::Error) -> Self {
        TuiError {
            message: err.to_string(),
        }
    }
}
impl From<ClientError> for TuiError {
    fn from(err: ClientError) -> Self {
        TuiError {
            message: err.to_string(),
        }
    }
}
impl From<ConfigError> for TuiError {
    fn from(err: ConfigError) -> Self {
        TuiError {
            message: err.to_string(),
        }
    }
}
impl From<AuthError> for TuiError {
    fn from(err: AuthError) -> Self {
        TuiError {
            message: format!("unable to read authentication: {}", err),
        }
    }
}
impl From<std::sync::mpsc::RecvError> for TuiError {
    fn from(err: std::sync::mpsc::RecvError) -> Self {
        TuiError {
            message: err.to_string(),
        }
    }
}
impl From<serde_json::Error> for TuiError {
    fn from(err: serde_json::Error) -> Self {
        TuiError {
            message: err.to_string(),
        }
    }
}
impl From<LoginError> for TuiError {
    fn from(err: LoginError) -> Self {
        TuiError {
            message: match err {
                LoginError::Recoverable(err) => err.to_string(),
                LoginError::Fatal(msg) => msg,
            },
        }
    }
}

impl std::fmt::Display for TuiError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for TuiError {}

impl Tui {
    /// The entry point to this module from `main()`.
    ///
    /// `cfgloc` indicates where to try to find Mastodonochrome's
    /// configuration files, including which server to connect to and
    /// who to log in as. If those aren't found, Mastodonochrome
    /// starts up in login / account registration mode; otherwise it
    /// logs in using its existing details.
    ///
    /// If `readonly` is true, the [`Client`] is set up in a way that
    /// will make it refuse any HTTP operation to the Mastodon server
    /// with a method other than `GET`, which should prevent any
    /// attempt to modify the state of the server, even accidental.
    /// However, the TUI will still mostly behave as if modification
    /// is _possible_: e.g. you can edit a toot as if you're about to
    /// post it, and only at the last minute will you be prevented.
    ///
    /// (So this is for developers, rather than users: it's for
    /// testing half-written changes without the risk of accidentally
    /// posting nonsense from your main account. A _user_ readonly
    /// option would want to exclude modifying keystrokes from the
    /// bottom-line help, not let you go into the post editor in the
    /// first place, etc.)
    ///
    /// If `logfile` is not `None`, then the client will log all its
    /// HTTP transactions to the specified file. This option _might_
    /// be obsolete: it was useful during early development, but now
    /// we have the interactive HTTP Log within the TUI, there aren't
    /// many situations where it would still be needed.
    ///
    /// This function constructs a `Tui` object internally, and then
    /// runs its main loop to completion. The `Tui` is not returned.
    pub fn run(
        cfgloc: &ConfigLocation,
        readonly: bool,
        logfile: Option<File>,
    ) -> Result<(), TuiError> {
        let (sender, receiver) = std::sync::mpsc::sync_channel(1);

        let input_sender = sender.clone();

        // I don't think we have any need to join subthreads like this
        let _joinhandle = std::thread::spawn(move || {
            while let Ok(ev) = event::read() {
                if input_sender.send(SubthreadEvent::TermEv(ev)).is_err() {
                    break;
                }
            }
        });

        let auth = match AuthConfig::load(cfgloc) {
            // If we don't have any authentication at all, that's OK,
            // and we put the TUI into login mode
            Err(AuthError::Nonexistent(..)) => Ok(AuthConfig::default()),

            // Pass through any other Ok or Err, the latter to be
            // handled by the ?
            x => x,
        }?;

        let mut client = Client::new(auth)?;
        client.set_writable(!readonly);
        client.set_logfile(logfile);

        if !client.auth.is_logged_in() && client.auth.user_token.is_some() {
            finish_account_setup(&mut client, cfgloc)?;
        }

        let mut state = TuiLogicalState::new(&mut client, cfgloc.clone());
        state.load_ldb()?;

        stdout().execute(EnterAlternateScreen)?;
        enable_raw_mode()?;
        let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
        terminal.clear()?;

        let output = Rc::new(RefCell::new(TuiOutput::new(terminal)));

        client.set_busy_indicator(TuiBusyIndicator {
            output: Rc::clone(&output),
        });

        let mut tui = Tui {
            output,
            subthread_sender: sender,
            subthread_receiver: receiver,
            state,
            client,
        };

        // Run the setup and main loop via a closure, so that if it
        // generates an error we can do our cleanup before retunirng
        // that error.
        let result = (|| {
            if tui.client.auth.is_logged_in() {
                tui.main_session_setup()?;
            }

            tui.main_loop()
        })();

        disable_raw_mode()?;
        stdout().execute(LeaveAlternateScreen)?;

        result
    }

    /// Translate a `crossterm` key event representation into [`OurKey`].
    ///
    /// The return value is a vector, because some `crossterm` keys
    /// (in particular Alt+x) expand into more than one [`OurKey`].
    fn translate_keypress(ev: KeyEvent) -> Vec<OurKey> {
        let main = match ev.code {
            KeyCode::Up => Some(OurKey::Up),
            KeyCode::Down => Some(OurKey::Down),
            KeyCode::Left => Some(OurKey::Left),
            KeyCode::Right => Some(OurKey::Right),
            KeyCode::PageUp => Some(OurKey::PgUp),
            KeyCode::PageDown => Some(OurKey::PgDn),
            KeyCode::Home => Some(OurKey::Home),
            KeyCode::End => Some(OurKey::End),
            KeyCode::Insert => Some(OurKey::Ins),
            KeyCode::Delete => Some(OurKey::Del),
            KeyCode::F(n) => Some(OurKey::FunKey(n)),
            KeyCode::Esc => Some(OurKey::Escape),
            KeyCode::Backspace => Some(OurKey::Backspace),
            KeyCode::Enter => Some(OurKey::Return),
            KeyCode::Tab => Some(OurKey::Ctrl('I')),
            KeyCode::Char(c) => {
                let initial = if ('\0'..' ').contains(&c) {
                    Some(OurKey::Ctrl(
                        char::from_u32((c as u32) + 0x40).unwrap(),
                    ))
                } else if ('\u{80}'..'\u{A0}').contains(&c) {
                    None
                } else if ev.modifiers.contains(KeyModifiers::CONTROL) {
                    Some(OurKey::Ctrl(
                        char::from_u32(((c as u32) & 0x1F) + 0x40).unwrap(),
                    ))
                } else {
                    Some(OurKey::Pr(c))
                };
                match initial {
                    Some(OurKey::Ctrl('H')) => Some(OurKey::Backspace),
                    Some(OurKey::Ctrl('M')) => Some(OurKey::Return),
                    Some(OurKey::Ctrl('[')) => Some(OurKey::Escape),
                    Some(OurKey::Pr(' ')) => Some(OurKey::Space),
                    other => other,
                }
            }
            _ => None,
        };
        if let Some(main) = main {
            if ev.modifiers.contains(KeyModifiers::ALT) {
                vec![OurKey::Escape, main]
            } else {
                vec![main]
            }
        } else {
            vec![]
        }
    }

    /// Do the parts of `Tui` setup that require the user to be logged
    /// in. This must be run before putting the Main Menu on the
    /// activity stack, either at startup (if we find a configuration
    /// file already written) or when login / registration concludes
    /// (signalled by [`PhysicalAction::MainSessionSetup`]).
    fn main_session_setup(&mut self) -> Result<(), TuiError> {
        self.start_streaming_subthread(StreamId::User)?;
        self.start_timing_subthread(
            Duration::from_secs(120),
            SubthreadEvent::Heartbeat,
            true,
        )?;

        // Now fetch the two basic feeds - home and mentions. Most
        // importantly, getting the mentions feed started means that
        // we can spot updates to it after that.
        //
        // We must do this _after_ starting the stream listener, so
        // that we won't miss a notification due to a race condition.
        self.client.fetch_feed(&FeedId::Home, FeedExtend::Initial)?;
        self.client
            .fetch_feed(&FeedId::Mentions, FeedExtend::Initial)?;

        // Now we've fetched the mentions feed, see if we need to beep
        // and throw the user into the mentions activity immediately
        // on startup.
        if self.state.check_startup_mentions(&mut self.client) {
            Self::beep()?;
        }

        Ok(())
    }

    /// Start a subthread to listen to an event stream from the
    /// Mastodon server and deliver events back via our
    /// `sync_channel`.
    fn start_streaming_subthread(
        &mut self,
        id: StreamId,
    ) -> Result<(), TuiError> {
        let sender = self.subthread_sender.clone();
        self.client.start_streaming_thread(
            &id,
            Box::new(move |update| {
                if sender
                    .send(SubthreadEvent::StreamEv(update).clone())
                    .is_err()
                {
                    // It would be nice to do something about this
                    // error, but what _can_ we do? We can hardly send
                    // an error notification back to the main thread,
                    // because that communication channel is just what
                    // we've had a problem with.
                }
            }),
        )?;

        Ok(())
    }

    /// Start a subthread to simply wait, and deliver us a specified
    /// [`SubthreadEvent`] when a timer has elapsed. If `repeat` is
    /// set then the thread will continue delivering regular events
    /// (typically [`SubthreadEvent::Heartbeat`]), otherwise it will
    /// deliver a one-off event.
    fn start_timing_subthread(
        &mut self,
        dur: Duration,
        ev: SubthreadEvent,
        repeat: bool,
    ) -> Result<(), TuiError> {
        let sender = self.subthread_sender.clone();
        let _joinhandle = std::thread::spawn(move || {
            loop {
                std::thread::sleep(dur);
                // Similarly ignore the error (see above)
                let _ = sender.send(ev.clone());
                if !repeat {
                    break;
                }
            }
        });
        Ok(())
    }

    /// The TUI main loop. A subroutine of `run()`: `run()` itself
    /// does all the setup and teardown, and this does the part in the
    /// middle which waits for events and processes them. That way,
    /// this method can return abruptly via `?` if it ever suffers a
    /// fatal error, and `run()` outside it will still do the final
    /// cleanup.
    fn main_loop(&mut self) -> Result<(), TuiError> {
        'outer: loop {
            self.output.borrow_mut().draw(&mut self.state)?;

            // One physical keypress can break down into multiple
            // things we treat as logical keypresses. So we must do an
            // awkward two-step handling here where we first process
            // the incoming event into a vector of things to do, and
            // then loop over that vector handling the PhysicalActions
            // that come back.
            //
            // Repeating the whole match on PhysicalAction branches in
            // the TermEv and StreamEv branches would be worse!

            enum Todo {
                Keypress(OurKey),
                Stream(HashSet<AnyFeedId>),
            }

            let todos = match self.subthread_receiver.recv() {
                Err(e) => break 'outer Err(e.into()),

                Ok(SubthreadEvent::TermEv(ev)) => match ev {
                    Event::Key(key) => {
                        if key.kind == KeyEventKind::Press {
                            self.state.new_event();
                            Self::translate_keypress(key)
                                .into_iter()
                                .map(|key| Todo::Keypress(key))
                                .collect()
                        } else {
                            Vec::new()
                        }
                    }
                    _ => Vec::new(),
                },
                Ok(SubthreadEvent::StreamEv(update)) => {
                    match self.client.process_stream_update(update) {
                        Ok(r) => {
                            if let Some(dead) = r.stream_ended {
                                // If our streaming subthread has died
                                // for some reason, restart it. But
                                // wait a decent interval, so that the
                                // server doesn't get annoyed with us
                                // for constantly pestering it.
                                //
                                // FIXME: if we ever have a variable
                                // set of streams, then we might have
                                // to think harder about how to
                                // respond to one having ended. It
                                // might be one we didn't want any
                                // more anyway, so unconditionally
                                // restarting it isn't always the
                                // right answer. But I think it is for
                                // StreamId::User.
                                self.start_timing_subthread(
                                    Duration::from_secs(60),
                                    SubthreadEvent::StreamRestart(dead),
                                    false,
                                )?;
                            }
                            vec![Todo::Stream(r.feeds_updated)]
                        }

                        Err(err) => {
                            self.client.add_to_error_log(err);
                            self.state.throw_into_error_log(&mut self.client);
                            Vec::new()
                        }
                    }
                }
                Ok(SubthreadEvent::StreamRestart(stream_id)) => {
                    if self
                        .start_streaming_subthread(stream_id.clone())
                        .is_err()
                    {
                        // If the streaming subthread failed to start,
                        // try again in another minute.
                        self.start_timing_subthread(
                            Duration::from_secs(60),
                            SubthreadEvent::StreamRestart(stream_id.clone()),
                            false,
                        )?;
                    }

                    // Whether we successfully started the thread at
                    // all, poll the feeds it was watching. If we
                    // _have_ started a stream, this ensures that we
                    // don't miss any updates that happened on those
                    // feeds while there wasn't one. If we haven't,
                    // then accompanying each stream-restart failure
                    // with this set of polls will be a fallback
                    // method of staying up to date with the feeds
                    // anyway.
                    let mut feeds_updated = HashSet::new();
                    feeds_updated.insert(LocalFeedId::HttpTransactions.into());
                    let mut any_error = false;
                    for feed in stream_id.feeds_affected() {
                        match self.client.fetch_feed(&feed, FeedExtend::Future)
                        {
                            Ok(false) => (),
                            Ok(true) => {
                                feeds_updated.insert(feed.into());
                            }
                            Err(err) => {
                                self.client.add_to_error_log(err);
                                any_error = true;
                            }
                        }
                    }
                    if any_error {
                        self.state.throw_into_error_log(&mut self.client);
                    }

                    vec![Todo::Stream(feeds_updated)]
                }
                Ok(SubthreadEvent::Heartbeat) => {
                    self.state.checkpoint_ldb();
                    self.client.prune_logs();
                    Vec::new()
                }
            };

            for todo in todos {
                let mut o1;
                let mut o2;
                let physacts: &mut [PhysicalAction] = match todo {
                    Todo::Keypress(ourkey) => {
                        o1 = [self
                            .state
                            .handle_keypress(ourkey, &mut self.client)];
                        &mut o1
                    }
                    Todo::Stream(feeds_updated) => {
                        o2 = self.state.handle_feed_updates(
                            feeds_updated,
                            &mut self.client,
                        );
                        &mut o2
                    }
                };

                for physact in physacts {
                    match std::mem::replace(physact, PhysicalAction::Nothing) {
                        PhysicalAction::Beep => Self::beep()?,
                        PhysicalAction::Exit => break 'outer Ok(()),
                        PhysicalAction::Refresh => {
                            self.output.borrow_mut().clear()?
                        }
                        PhysicalAction::Error(err) => break 'outer Err(err),
                        PhysicalAction::MainSessionSetup => {
                            self.main_session_setup()?
                        }
                        PhysicalAction::Nothing => (),
                    }
                }
            }

            self.state.completed_event();
        }
    }

    /// Write a bell character to the terminal. (The one piece of
    /// terminal output that `ratatui` doesn't do for us.)
    fn beep() -> std::io::Result<()> {
        stdout().write_all(b"\x07")?;
        Ok(())
    }
}

/// Wrap a [`Terminal`] to overlay a 'busy' indicator on the bottom
/// line.
///
/// This type is owned by [`Tui`], which calls its
/// [`draw()`](TuiOutput::draw) method to redraw the main user
/// interface of Mastodonochrome, a whole screen at a time. But it's
/// also shared by [`Client`] (via keeping it in an `Rc<RefCell>`),
/// which calls its [`set_busy()`](TuiOutput::set_busy) method to
/// indicate when it's about to perform an HTTP transaction.
/// `TuiOutput` responds by overlaying a 'busy' indicator on the
/// bottom line of the screen, so that the user gets feedback that
/// Mastodonochrome is busy (and hasn't, e.g., just ignored your
/// keystroke).
struct TuiOutput {
    terminal: Terminal<CrosstermBackend<Stdout>>,
    prev_buffer: Option<Buffer>,
    pending_io_error: Result<(), std::io::Error>,
    busy_msg: CentredInfoLine,
}

impl TuiOutput {
    /// Make a new `TuiOutput` wrapping a [`Terminal`].
    fn new(terminal: Terminal<CrosstermBackend<Stdout>>) -> Self {
        TuiOutput {
            terminal,
            prev_buffer: None,
            pending_io_error: Ok(()),
            busy_msg: CentredInfoLine::new(ColouredString::general(
                "<+- please wait, accessing network -+>",
                "HHHHKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKHHHH",
            )),
        }
    }

    /// Redraw the screen, obtaining the new contents by asking the
    /// provided `TuiLogicalState` via its
    /// [`draw_frame()`](TuiLogicalState::draw_frame) method.
    ///
    /// A copy of the screen contents is saved inside the `TuiOutput`,
    /// so that it can be reused to overlay a busy indicator on if
    /// [`set_busy()`](TuiOutput::set_busy) is called later.
    fn draw(
        &mut self,
        state: &mut TuiLogicalState,
    ) -> Result<(), std::io::Error> {
        // In case a previous set_busy reported an I/O error, return it now
        std::mem::replace(&mut self.pending_io_error, Ok(()))?;

        self.terminal.draw(|frame| {
            let area = frame.size();
            let buf = frame.buffer_mut();
            let cursor = state.draw_frame(area, buf);
            self.prev_buffer = Some(buf.clone());
            if let Some((x, y)) = cursor {
                if let (Ok(x), Ok(y)) = (x.try_into(), y.try_into()) {
                    frame.set_cursor(x, y);
                }
            }
        })?;

        Ok(())
    }

    /// Clear the whole screen, so that the next redraw will be full
    /// rather than incremental. Usually in response to the user
    /// explicitly requesting a full redraw via `[^L]`.
    fn clear(&mut self) -> Result<(), std::io::Error> {
        self.terminal.clear()
    }

    /// Overlay a 'busy' indicator on the previous screen contents.
    ///
    /// This method is called from [`Client`], which is about to get
    /// stuck into serious network munging. So it would be very
    /// awkward to return an error of a type completely separate from
    /// [`ClientError`].
    ///
    /// We fudge this just a little bit. In the unlikely event that we
    /// _do_ have a terminal output error while attempting to redraw
    /// the screen with a busy indicator, we don't _return_ the error:
    /// instead, we save it for the next call to `draw()`, which does
    /// return it. So it's only delayed a little, not actually dropped
    /// on the floor.
    fn set_busy(&mut self) {
        let result = self.terminal.draw(|frame| {
            let area = frame.size();
            let buf = frame.buffer_mut();
            buf.reset();
            if let Some(prev) = &self.prev_buffer {
                buf.merge(prev);
            }
            let width = area.width as usize;
            let height = area.height as usize;
            let bottom_line = self.busy_msg.render_oneline(
                width,
                None,
                &DefaultDisplayStyle,
            );
            let rpad = width.saturating_sub(bottom_line.width());
            let bottom_line =
                bottom_line + ColouredString::plain(" ").repeat(rpad);
            ratatui_set_string(
                buf,
                0,
                height.saturating_sub(1),
                bottom_line.truncate(width),
            );
        });

        // This function can't conveniently return its I/O error, so
        // instead we keep it until the network operation has
        // finished, and report it in the next draw()
        match result {
            Ok(_) => (),
            Err(e) => {
                if self.pending_io_error.is_ok() {
                    self.pending_io_error = Err(e);
                }
            }
        }
    }
}

/// Opaque wrapper on an `Rc<RefCell<TuiOutput>>` to give to
/// [`Client`].
///
/// Hides the details of the `Rc<RefCell>` from `Client`, and also
/// gives it access only to the one `set_busy()` operation that it's
/// supposed to use.
pub struct TuiBusyIndicator {
    output: Rc<RefCell<TuiOutput>>,
}

impl TuiBusyIndicator {
    pub fn set_busy(&self) {
        self.output.borrow_mut().set_busy();
    }
}

/// Specification of the cursor position, returned to
/// `TuiLogicalState` by activities.
///
/// The obvious representation would be an `Option` containing x,y
/// coordinates (where `None` means tell the terminal to hide the
/// cursor completely). But it's very common in this UI that the
/// cursor is displayed at the end of the printable content on the
/// final line of the screen. So there's an extra branch `End` in this
/// enum that allows activities to ask for that easily, without having
/// to work out the exact location themselves.
#[derive(Debug)]
pub enum CursorPosition {
    /// Do not display a cursor at all.
    None,

    /// Put the cursor at the end of the text on the last line that has any.
    End,

    /// Put the cursor at a specified position. The two parameters are
    /// x and y, in that order, numbered from 0,0 in top left.
    At(usize, usize),
}

/// Action type returned to `TuiLogicalState` by individual activities.
///
/// This action type is richer than [`PhysicalAction`], because it
/// includes instructions to the `TuiLogicalState` itself (e.g.
/// telling it which activity to switch to next) as well as
/// instructions that are converted into `PhysicalAction` and passed
/// on to the [`Tui`].
#[derive(PartialEq, Eq, Debug, Clone)]
pub enum LogicalAction {
    /// The activity wants to go beep, which is passed back to the
    /// `Tui` as [`PhysicalAction::Beep`].
    Beep,

    /// Pop the activity stack, and resume whatever activity was next
    /// lower down.
    Pop,

    /// Pop the current _overlay_ activity from the actvity stack,
    /// resuming whatever full-screen activity was below it.
    PopOverlaySilent,

    /// Pop the current overlay activity, just like
    /// `PopOverlaySilent`, but then also return a
    /// [`PhysicalAction::Beep`] to the `Tui` to make it go beep.
    PopOverlayBeep,

    /// Pop the current overlay activity, just like
    /// `PopOverlaySilent`. This variant is used when the overlay
    /// activity was triggered from the full-screen activity, and
    /// specifically, was the 'search for a string' prompt. The
    /// details of what to search for are returned to the full-screen
    /// activity via the
    /// [`got_search_expression()`](ActivityState::got_search_expression)
    /// method.
    ///
    /// (This is rather horribly specific; it would probably be nicer
    /// to find a different way to have the overlay activity
    /// communicate this information to its parent, known only to
    /// those two types, like an `Rc<RefCell>` containing the answer.
    /// Then we could get rid of this _and_ `got_search_expression()`.)
    GotSearchExpression(SearchDirection, String),

    /// Go to a new activity. This may involve pushing it on the
    /// activity stack, or in some situations popping the stack _back_
    /// to it if it was already there.
    Goto(Activity),

    /// After login or account registration concludes successfully, go
    /// to the Main Menu. This has to be distinct from `Goto` so that
    /// it can also signal [`PhysicalAction::MainSessionSetup`] to the
    /// `Tui`.
    FinishedLoggingIn,

    /// Terminate the `Tui` main loop and exit Mastodonochrome.
    Exit,

    /// Do nothing (except redraw the screen, which is implicit).
    Nothing,

    /// Put a non-fatal error into the interactive Error Log, and go
    /// to the Error Log viewing activity.
    Error(ClientError),

    /// Terminate Mastodonochrome immediately with a fatal error.
    Fatal(TuiError),

    /// After leaving the editor, go to the posting menu that follows
    /// it. This has to be distinct from `Goto` so that it can pass
    /// through the details of the composed [`Post`].
    PostComposed(Post),

    /// After leaving the posting menu, go back to the editor to
    /// re-edit the draft post. This has to be distinct from `Goto` so
    /// that it can pass through the details of the composed [`Post`].
    PostReEdit(Post),

    /// Give help on the current activity.
    Help,
}

/// Current position within a feed of data, stored by `TuiLogicalState`.
///
/// If the activity viewing this feed is popped from the activity
/// stack and freed, and another one is later created, it can resume
/// reading from the same place using this stored data. But the data
/// is also in a good state to be saved in the LDB file by
/// [`TuiLogicalState::save_ldb()`].
#[derive(Debug, Clone, Default)]
pub struct SavedFilePos {
    /// The current position we're _looking at_ within the file,
    /// within this run.
    ///
    /// This `FilePosition` is the same type used within the
    /// [`file`](super::file) module itself. But where that stores the
    /// high-level part of the position (which entire post?) as an
    /// integer index into `File`'s current vector of items, this
    /// stores it as a string id, which is also meaningful to the
    /// Mastodon server, so it continues to make sense even after
    /// `File` has thrown away that particular vector.
    pub file_pos: Option<FilePosition<String>>,

    /// The id of the latest item we've actually read, which can
    /// persist between runs in the LDB.
    ///
    /// This differs from the id in `file_pos` in that this is the
    /// high-water mark. If you've read up to Friday in a given feed,
    /// but then scrolled back to Tuesday to check something, then
    /// making a new instance of that feed's activity within the same
    /// run of Mastodonochrome will resume from `file_pos` (back at
    /// Tuesday where you were just looking), but after quitting and
    /// restarting, you'll be back to _this_ id (Friday, the first
    /// thing you haven't yet seen at all).
    pub latest_read_id: Option<String>,
}

/// Trait implemented by all activities managed by the TUI.
pub trait ActivityState {
    /// Notifies an activity that the terminal has been resized.
    /// Default implementation does nothing.
    fn resize(&mut self, _w: usize, _h: usize) {}

    /// Asks the activity to return what it would like to see on the
    /// screen, in the form of a vector of `[`ColouredString`]` giving
    /// the lines of the terminal, plus a [`CursorPosition`] saying
    /// where it wants the cursor to appear (if anywhere).
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition);

    /// Asks the activity to decide what to do about a keypress. A
    /// [`Client`] is provided, in case network requests are needed in
    /// response.
    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction;

    /// Asks the activity to decide what to do after some feed(s)
    /// receive updates from the Mastodon server. A [`Client`] is
    /// provided, in case network requests are needed in response.
    ///
    /// Many activities don't care at all, so a default implementation
    /// is provided that just returns [`LogicalAction::Nothing`] to
    /// have no effect.
    fn handle_feed_updates(
        &mut self,
        _feeds_updated: &HashSet<AnyFeedId>,
        _client: &mut Client,
    ) -> LogicalAction {
        LogicalAction::Nothing
    }

    /// Asks the activity if it would like to contribute a saved file
    /// position to the `TuiLogicalState` before being destroyed. If
    /// so, the return value says which feed the position relates to.
    fn save_file_position(&self) -> Option<(FeedId, SavedFilePos)> {
        None
    }

    /// Tells the activity that data held in the `Client`'s caches may
    /// have changed, so it should check whether anything it's
    /// displaying needs to be modified. (For example, a post was
    /// edited by its author, and maybe that's one of the posts
    /// currently being displayed.)
    ///
    /// Default implementation does nothing.
    fn refresh_content(&mut self, _client: &mut Client) {}

    /// Tells the activity to jump forwards to new (unread) content,
    /// if it was previously looking at stuff in the past.
    ///
    /// This is only called if [`TuiLogicalState::changed_activity()`]
    /// is called with `is_interrupt==true`, which only happens at all
    /// for particular activity types, like
    /// [`ErrorLog`](super::activity_stack::UtilityActivity::ErrorLog) and
    /// [`ReadMentions`](super::activity_stack::UtilityActivity::ReadMentions).
    ///
    /// Any activity which isn't ever called with `is_interrupt` can
    /// use the default implementation, which does nothing.
    fn show_new_content(&mut self, _client: &mut Client) {}

    /// Tells the activity that a prompt in the bottom line for a
    /// search expression has returned a result, in response to
    /// [`LogicalAction::GotSearchExpression`] generated by the prompt
    /// overlay activity.
    ///
    /// Default implementation panics, which is another thing that
    /// makes me think this mechanism is rather horrible.
    fn got_search_expression(
        &mut self,
        _dir: SearchDirection,
        _regex: String,
    ) -> LogicalAction {
        panic!("a trait returning GetSearchExpression should fill this in");
    }
}

/// The inner one of the two main types implementing the top-level TUI.
///
/// This type is responsible for managing the way the TUI is composed
/// of smaller 'activities' (most of which are full-screen, but a few
/// are 'overlays' which replace only the bottom line of the
/// full-screen activity they were invoked from). It maintains an
/// [`ActivityStack`] of the current activities, and each one also has
/// an object implementing the [`ActivityState`] trait.
///
/// Generally, events are sent here from the owning [`Tui`], and
/// passed on to whatever the current activity is. The activity's
/// response, in the form of [`LogicalAction`], may stop here and
/// cause changes in the activity stack, or may be passed on to the
/// `Tui` as a [`PhysicalAction`], or both.
///
/// A few event types are handled centrally here, so that they can
/// work the same everywhere. For example, the `[ESC]` keypress
/// _always_ goes to the Utilities Menu: Monochrome found that to be a
/// useful UI invariant allowing a lost user to easily get back to
/// somewhere known, and I've kept it. Similarly, `[F1]` always asks
/// for help (although that one is my own addition to Mono's UI).
struct TuiLogicalState {
    activity_stack: ActivityStack,
    activity_states: HashMap<Activity, Box<dyn ActivityState>>,
    last_area: Option<Rect>,
    file_positions: HashMap<AnyFeedId, SavedFilePos>,
    unfolded_posts: Rc<RefCell<HashSet<String>>>,
    cfgloc: ConfigLocation,
}

impl TuiLogicalState {
    /// Make a new `TuiLogicalState`.
    fn new(client: &mut Client, cfgloc: ConfigLocation) -> Self {
        let activity = if client.auth.is_logged_in() {
            NonUtilityActivity::MainMenu
        } else {
            NonUtilityActivity::LoginMenu
        };

        let mut st = TuiLogicalState {
            activity_stack: ActivityStack::new(activity),
            activity_states: HashMap::new(),
            last_area: None,
            file_positions: HashMap::new(),
            cfgloc,
            unfolded_posts: Rc::new(RefCell::new(HashSet::new())),
        };
        st.changed_activity(client, None, false);

        st
    }

    /// Draw the screen. We're given a `ratatui` [`Buffer`] to draw
    /// text into, and told its dimensions. We must ask our current
    /// activity (or more than one, if there's an overlay) for screen
    /// contents in the form of vectors of [`ColouredString`], and
    /// draw those on the `Buffer` via the helper function
    /// [`ratatui_set_string()`].
    ///
    /// Return value is the desired cursor location, or none if we
    /// want the cursor to be hidden. (This is passed on to the
    /// `Buffer`'s owning [`Frame`](ratatui::prelude::Frame) by our
    /// caller.)
    fn draw_frame(
        &mut self,
        area: Rect,
        buf: &mut Buffer,
    ) -> Option<(usize, usize)> {
        let (w, h) = (area.width as usize, area.height as usize);

        if self.last_area != Some(area) {
            self.last_area = Some(area);
            self.activity_state_mut().resize(w, h);
            if let Some(ref mut state) = self.overlay_activity_state_mut() {
                state.resize(w, h);
            }
        }

        let (lines, cursorpos) = self.activity_state().draw(w, h);
        let mut cursorpos = cursorpos;
        buf.reset();
        let mut last_x = 0;
        let mut last_y = 0;
        for (y, line) in lines.iter().enumerate() {
            if y >= h {
                break;
            }
            ratatui_set_string(buf, 0, y, line);
            last_y = y;
            last_x = line.width();
        }

        let mut cursorcoords = match cursorpos {
            CursorPosition::None => None,
            CursorPosition::At(x, y) => Some((x, y)),
            CursorPosition::End => Some((last_x, last_y)),
        };

        if let Some(state) = &self.overlay_activity_state() {
            let (lines, overlay_cursorpos) = state.draw(w, h);
            cursorpos = overlay_cursorpos;
            let ytop = h - min(h, lines.len());
            for (line, y) in lines.iter().zip(ytop..) {
                if y >= h {
                    break;
                }
                // Clear the whole previous line in case the new one is shorter
                for x in 0..area.width {
                    buf.get_mut(x, y as u16).reset();
                }
                ratatui_set_string(buf, 0, y, line);
                last_y = y;
                last_x = line.width();
            }
            cursorcoords = match cursorpos {
                CursorPosition::None => None,
                CursorPosition::At(x, y) => Some((x, y + ytop)),
                CursorPosition::End => Some((last_x, last_y)),
            };
        }

        cursorcoords
    }

    /// Notifies the `TuiLogicalState` that a new keyboard event is
    /// beginning.
    ///
    /// This is called just once for each keypress received from
    /// `crossterm`, even if that keypress translates into multiple
    /// [`OurKey`]. This allows those multi-event keypresses to be
    /// handled somewhat as a unit: for example, if the user presses
    /// Alt+E to examine a user, then it's nicer if the bottom-line
    /// prompt appears over whatever they were already doing, instead
    /// of the literal-minded handling in which the Alt+E is
    /// translated into ESC E, and the first ESC replaces the whole
    /// screen with the content-free Utilities Menu and _that's_ what
    /// they get to look at while typing the name of the user to
    /// examine.
    fn new_event(&mut self) {
        self.activity_stack.new_event();
    }

    /// Notifies the `TuiLogicalState` that an event has completed and
    /// the whole main loop is going to go back round to the top.
    ///
    /// This happens whether or not the event was a keypress causing a
    /// call to `new_event()`. It's used to clean up the state that
    /// `new_event()` saved, but also, it calls
    /// [`gc_activity_states()`](TuiLogicalState::gc_activity_states).
    fn completed_event(&mut self) {
        self.activity_stack.completed_event();
        self.gc_activity_states();
    }

    /// Called from the `Tui` main loop to handle a keypress.
    ///
    /// This function must handle centralised keypresses like `[ESC]`
    /// and `[F1]`; pass on other keystrokes to the current activity;
    /// and handle the [`LogicalAction`] responses as best it can,
    /// generating [`PhysicalAction`] responses in turn to its caller.
    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> PhysicalAction {
        let logact = match key {
            // Central handling of [ESC]: it _always_ goes to the
            // utilities menu, from any UI context at all.
            OurKey::Escape => {
                LogicalAction::Goto(UtilityActivity::UtilsMenu.into())
            }

            OurKey::FunKey(1) => LogicalAction::Help,

            // ^L forces a full screen redraw, in case your terminal
            // was corrupted by extraneous output. And again it should
            // do it absolutely anywhere.
            OurKey::Ctrl('L') => return PhysicalAction::Refresh,

            _ => {
                if let Some(state) = self.overlay_activity_state_mut() {
                    state.handle_keypress(key, client)
                } else {
                    self.activity_state_mut().handle_keypress(key, client)
                }
            }
        };

        self.handle_logact(logact, client)
    }

    /// Internal function to process a [`LogicalAction`] and turn it
    /// into a [`PhysicalAction`].
    fn handle_logact(
        &mut self,
        mut logact: LogicalAction,
        client: &mut Client,
    ) -> PhysicalAction {
        loop {
            logact = match logact {
                LogicalAction::Beep => break PhysicalAction::Beep,
                LogicalAction::Exit => break PhysicalAction::Exit,
                LogicalAction::Fatal(e) => break PhysicalAction::Error(e),
                LogicalAction::Nothing => break PhysicalAction::Nothing,
                LogicalAction::Goto(activity) => {
                    self.activity_stack.goto(activity);
                    self.changed_activity(client, None, false);
                    break PhysicalAction::Nothing;
                }
                LogicalAction::Help => {
                    self.activity_stack.goto(
                        HelpActivity::Help(Box::new(
                            self.activity_stack
                                .overlay()
                                .unwrap_or_else(|| self.activity_stack.top())
                                .clone(),
                        ))
                        .into(),
                    );
                    self.changed_activity(client, None, false);
                    break PhysicalAction::Nothing;
                }
                LogicalAction::FinishedLoggingIn => {
                    self.activity_stack
                        .goto(NonUtilityActivity::MainMenu.into());
                    self.changed_activity(client, None, false);
                    break PhysicalAction::MainSessionSetup;
                }
                LogicalAction::Pop => {
                    self.activity_stack.pop();
                    self.changed_activity(client, None, false);
                    break PhysicalAction::Nothing;
                }
                LogicalAction::PopOverlaySilent => {
                    self.pop_overlay_activity();
                    break PhysicalAction::Nothing;
                }
                LogicalAction::PopOverlayBeep => {
                    self.pop_overlay_activity();
                    break PhysicalAction::Beep;
                }
                LogicalAction::GotSearchExpression(dir, regex) => {
                    self.pop_overlay_activity();
                    self.activity_state_mut().got_search_expression(dir, regex)
                }
                LogicalAction::Error(err) => {
                    client.add_to_error_log(err);
                    self.activity_stack.goto(UtilityActivity::ErrorLog.into());
                    self.changed_activity(client, None, true);
                    break PhysicalAction::Beep;
                }
                LogicalAction::PostComposed(post) => {
                    let newact = match self.activity_stack.top() {
                        Activity::Compose(
                            ComposeActivity::ComposeToplevel,
                        ) => ComposeActivity::PostComposeMenu.into(),
                        Activity::Compose(ComposeActivity::ComposeReply(
                            id,
                        )) => ComposeActivity::PostReplyMenu(id).into(),
                        Activity::Compose(
                            ComposeActivity::EditExistingPost(src),
                        ) => ComposeActivity::PostEditMenu(src).into(),
                        act => panic!("can't postcompose {act:?}"),
                    };
                    self.activity_stack.chain_to(newact);
                    self.changed_activity(client, Some(post), false);
                    break PhysicalAction::Nothing;
                }
                LogicalAction::PostReEdit(post) => {
                    let newact = match self.activity_stack.top() {
                        Activity::Compose(
                            ComposeActivity::PostComposeMenu,
                        ) => ComposeActivity::ComposeToplevel.into(),
                        Activity::Compose(ComposeActivity::PostReplyMenu(
                            id,
                        )) => ComposeActivity::ComposeReply(id).into(),
                        Activity::Compose(ComposeActivity::PostEditMenu(
                            src,
                        )) => ComposeActivity::EditExistingPost(src).into(),
                        act => panic!("can't reedit {act:?}"),
                    };
                    self.activity_stack.chain_to(newact);
                    self.changed_activity(client, Some(post), false);
                    break PhysicalAction::Nothing;
                }
            };
        }
    }

    /// Called from the `Tui` main loop to handle updates to feeds on
    /// the Mastodon server.
    ///
    /// This returns an array of two [`PhysicalAction`], because there
    /// may need to be one from the current activity and another
    /// generated here (throwing the user into their mentions feed).
    /// The `Tui` will process them in sequence.
    fn handle_feed_updates(
        &mut self,
        feeds_updated: HashSet<AnyFeedId>,
        client: &mut Client,
    ) -> [PhysicalAction; 2] {
        let logact = self
            .activity_state_mut()
            .handle_feed_updates(&feeds_updated, client);
        let physact1 = self.handle_logact(logact, client);

        let physact2 = if feeds_updated.contains(&FeedId::Mentions.into()) {
            if self.activity_stack.top().throw_into_mentions() {
                self.activity_stack
                    .goto(UtilityActivity::ReadMentions.into());
                self.changed_activity(client, None, true);
            }

            // FIXME: we'd quite like a double-beep if you're in the composer
            PhysicalAction::Beep
        } else {
            PhysicalAction::Nothing
        };

        [physact1, physact2]
    }

    /// Called from [`Tui::main_session_setup()`] to see if there have
    /// been new mentions since the user last started Mastodonochrome.
    /// If so, we might want to throw the user straight into their
    /// mentions feed.
    fn check_startup_mentions(&mut self, client: &mut Client) -> bool {
        let feedid = FeedId::Mentions;
        let last_id = client.borrow_feed(&feedid).ids.back();
        let last_read_mention = self
            .file_positions
            .get(&feedid.clone().into())
            .and_then(|sfp| sfp.latest_read_id.clone());

        if let Some(read) = last_read_mention {
            if !last_id.is_some_and(|latest| latest > &read) {
                return false; // nothing new to see
            }
        } else {
            // If this client is being started for the very first
            // time, then we don't want to instantly beep the user
            // into their mentions. But we do want to save the fact
            // that _those_ mentions were the ones that existed before
            // they started running this client. So here we _make_ an
            // LDB entry.
            if let Some(last_id) = last_id {
                self.set_latest_read(feedid, last_id.to_owned());
            }
            return false;
        }

        self.activity_stack
            .goto(UtilityActivity::ReadMentions.into());
        self.changed_activity(client, None, true);
        true
    }

    /// Checkpoint the user's saved file positions into the LDB file
    /// on disk.
    fn checkpoint_ldb(&mut self) {
        let mut changed = false;
        for state in self.activity_states.values_mut() {
            if let Some((feed_id, saved_pos)) = state.save_file_position() {
                let feed_id = feed_id.into();
                let old_latest_read = self
                    .file_positions
                    .get(&feed_id)
                    .and_then(|sfp| sfp.latest_read_id.clone());
                let new_latest_read = saved_pos.latest_read_id.clone();
                self.file_positions.insert(feed_id, saved_pos);
                if old_latest_read != new_latest_read {
                    changed = true;
                }
            }
        }

        if changed {
            // FIXME: maybe suddenly change our mind and go to the
            // Error Log
            self.save_ldb().unwrap();
        }
    }

    /// Internal function to make sure an activity has an entry in the
    /// `activity_states` map, e.g. because we've just pushed it on
    /// the activity stack. Subroutine of `changed_activity()`.
    ///
    /// For the meanings of `post` and `is_interrupt`, see
    /// [`new_activity_state()`](TuiLogicalState::new_activity_state).
    fn ensure_activity_state(
        &mut self,
        act: Activity,
        client: &mut Client,
        post: Option<Post>,
        is_interrupt: bool,
    ) {
        if let Some(ref mut state) = self.activity_states.get_mut(&act) {
            if is_interrupt {
                // If we're resuming an activity that was already on
                // the stack _because it interrupted us_, then we
                // should jump it to the new content. Otherwise, let
                // it keep its existing position.
                state.show_new_content(client);
            } else {
                // If not that, then we should at least make the
                // activity check that it's up to date with the
                // current Client contents.
                state.refresh_content(client);
            }
        } else {
            self.activity_states.insert(
                act.clone(),
                self.new_activity_state(act, client, post, is_interrupt),
            );
        }
    }

    /// Internal function called after changing the activity stack so
    /// that a new activity is now the current one.
    ///
    /// For the meanings of `post` and `is_interrupt`, see
    /// [`new_activity_state()`](TuiLogicalState::new_activity_state).
    fn changed_activity(
        &mut self,
        client: &mut Client,
        post: Option<Post>,
        is_interrupt: bool,
    ) {
        self.checkpoint_ldb();

        self.ensure_activity_state(
            self.activity_stack.top(),
            client,
            post,
            is_interrupt,
        );
        if let Some(act) = self.activity_stack.overlay() {
            self.ensure_activity_state(act, client, None, false);
        }

        self.gc_activity_states();

        if let Some(area) = self.last_area {
            let (w, h) = (area.width as usize, area.height as usize);
            self.activity_state_mut().resize(w, h);
            if let Some(state) = self.overlay_activity_state_mut() {
                state.resize(w, h);
            }
        }
    }

    /// Garbage-collect our hashmap of activity states, leaving only
    /// the ones that correspond to an activity currently on the stack.
    fn gc_activity_states(&mut self) {
        let states: HashSet<_> = self.activity_stack.iter().collect();
        self.activity_states.retain(|k, _| states.contains(k));
    }

    /// Pop any current overlay activity from the stack.
    fn pop_overlay_activity(&mut self) {
        self.activity_stack.pop_overlay();
        self.gc_activity_states();
    }

    /// Create an activity state for a given activity.
    ///
    /// `post` is used to pass a current draft post between the
    /// composition activities (the post editor, and the posting
    /// menu). `is_interrupt` is `true` if the newly activated
    /// activity is one that can appear as an interrupt (the mentions
    /// feed or the Error Log) and is doing so on this occasion.
    ///
    /// Many subroutines to construct activities are fallible, because
    /// they use the provided [`Client`], hence can return a
    /// [`ClientError`]. But this function is _not_ fallible. So if it
    /// has a `ClientError` to deal with, it's responsible for storing
    /// it in the Error Log itself, and returning an activity state
    /// for the Error Log in place of whatever it had previously
    /// expected to return.
    fn new_activity_state(
        &self,
        activity: Activity,
        client: &mut Client,
        post: Option<Post>,
        is_interrupt: bool,
    ) -> Box<dyn ActivityState> {
        let result = match activity {
            Activity::Help(HelpActivity::Help(act)) => {
                Ok(help_file(client, &act))
            }
            Activity::NonUtil(NonUtilityActivity::MainMenu) => {
                Ok(main_menu(client))
            }
            Activity::NonUtil(NonUtilityActivity::LoginMenu) => {
                Ok(login_menu(self.cfgloc.clone()))
            }
            Activity::Util(UtilityActivity::UtilsMenu) => {
                Ok(utils_menu(client))
            }
            Activity::Util(UtilityActivity::ExitMenu) => Ok(exit_menu()),
            Activity::Util(UtilityActivity::LogsMenu1) => Ok(logs_menu_1()),
            Activity::Util(UtilityActivity::LogsMenu2) => Ok(logs_menu_2()),
            Activity::NonUtil(NonUtilityActivity::HomeTimelineFile) => {
                home_timeline(
                    &self.file_positions,
                    self.unfolded_posts.clone(),
                    client,
                )
            }
            Activity::NonUtil(NonUtilityActivity::PublicTimelineFile) => {
                public_timeline(
                    &self.file_positions,
                    self.unfolded_posts.clone(),
                    client,
                )
            }
            Activity::NonUtil(NonUtilityActivity::LocalTimelineFile) => {
                local_timeline(
                    &self.file_positions,
                    self.unfolded_posts.clone(),
                    client,
                )
            }
            Activity::NonUtil(NonUtilityActivity::HashtagTimeline(ref id)) => {
                hashtag_timeline(self.unfolded_posts.clone(), client, id)
            }
            Activity::Util(UtilityActivity::ReadMentions) => mentions(
                &self.file_positions,
                self.unfolded_posts.clone(),
                client,
                is_interrupt,
            ),
            Activity::Util(UtilityActivity::ErrorLog) => {
                Ok(error_log(&self.file_positions, client, is_interrupt))
            }
            Activity::Util(UtilityActivity::HttpLog) => Ok(http_log(client)),
            Activity::Util(UtilityActivity::EgoLog) => {
                ego_log(&self.file_positions, client)
            }
            Activity::Overlay(OverlayActivity::GetUserToExamine) => {
                Ok(get_user_to_examine())
            }
            Activity::Overlay(OverlayActivity::GetPostIdToRead) => {
                Ok(get_post_id_to_read())
            }
            Activity::Overlay(OverlayActivity::GetHashtagToRead) => {
                Ok(get_hashtag_to_read())
            }
            Activity::Overlay(OverlayActivity::GetSearchExpression(dir)) => {
                Ok(get_search_expression(dir))
            }
            Activity::Overlay(OverlayActivity::BottomLineError(msg)) => {
                Ok(bottom_line_error(&msg))
            }
            Activity::Util(UtilityActivity::ExamineUser(ref name)) => {
                examine_user(client, name)
            }
            Activity::Util(UtilityActivity::InfoStatus(ref id)) => {
                view_single_post(self.unfolded_posts.clone(), client, id)
            }
            Activity::Util(UtilityActivity::InfoHttp(ref id)) => {
                view_http_transaction(client, id)
            }
            Activity::Compose(ComposeActivity::ComposeToplevel) => (|| {
                let post = match post {
                    Some(post) => post,
                    None => Post::new(client)?,
                };
                compose_post(client, post)
            })(
            ),
            Activity::Compose(ComposeActivity::PostComposeMenu) => post_menu(
                client,
                post.expect("how did we get here without a Post?"),
            ),
            Activity::Compose(ComposeActivity::ComposeReply(ref id)) => {
                let post = match post {
                    Some(post) => Ok(post),
                    None => Post::reply_to(id, client),
                };
                match post {
                    Ok(post) => compose_post(client, post),
                    Err(e) => Err(e),
                }
            }
            Activity::Compose(ComposeActivity::PostReplyMenu(_)) => post_menu(
                client,
                post.expect("how did we get here without a Post?"),
            ),
            Activity::Compose(ComposeActivity::EditExistingPost(ref src)) => {
                let post = match post {
                    Some(post) => Ok(post),
                    None => Post::edit_existing(src, client),
                };
                match post {
                    Ok(post) => compose_post(client, post),
                    Err(e) => Err(e),
                }
            }
            Activity::Compose(ComposeActivity::PostEditMenu(_)) => post_menu(
                client,
                post.expect("how did we get here without a Post?"),
            ),
            Activity::NonUtil(NonUtilityActivity::ThreadFile(
                ref id,
                full,
            )) => view_thread(self.unfolded_posts.clone(), client, id, full),
            Activity::Util(UtilityActivity::ListStatusFavouriters(ref id)) => {
                list_status_favouriters(client, id)
            }
            Activity::Util(UtilityActivity::ListStatusBoosters(ref id)) => {
                list_status_boosters(client, id)
            }
            Activity::Util(UtilityActivity::ListUserFollowers(ref id)) => {
                list_user_followers(client, id)
            }
            Activity::Util(UtilityActivity::ListUserFollowees(ref id)) => {
                list_user_followees(client, id)
            }
            Activity::Util(UtilityActivity::ListFollowRequesters) => {
                list_follow_requesters(client)
            }
            Activity::NonUtil(NonUtilityActivity::UserPosts(
                ref user,
                boosts,
                replies,
            )) => user_posts(
                &self.file_positions,
                self.unfolded_posts.clone(),
                client,
                user,
                boosts,
                replies,
            ),
            Activity::Util(UtilityActivity::UserOptions(ref id)) => {
                user_options_menu(client, id)
            }
            Activity::Util(UtilityActivity::InstanceRules) => {
                view_instance_rules(client)
            }
        };

        match result {
            Ok(state) => state,
            Err(err) => {
                client.add_to_error_log(err);
                error_log(&self.file_positions, client, true)
            }
        }
    }

    /// Save the list of current file positions into the LDB file.
    fn save_ldb(&self) -> Result<(), TuiError> {
        let mut ldb = BTreeMap::new();
        for (key, value) in &self.file_positions {
            use AnyFeedId::*;
            let keystr = match key {
                RemoteFeed(FeedId::Home) => "home".to_owned(),
                RemoteFeed(FeedId::Mentions) => "mentions".to_owned(),
                RemoteFeed(FeedId::Ego) => "ego".to_owned(),

                // For all other feeds, we don't persist the last-read
                // position.
                _ => continue,
            };

            let valstr = match &value.latest_read_id {
                Some(s) => s.clone(),
                None => continue,
            };

            ldb.insert(keystr, valstr);
        }
        let mut json = serde_json::to_string_pretty(&ldb)?;
        json.push('\n');
        self.cfgloc.create_file("ldb", &json)?;
        Ok(())
    }

    /// Load the list of current file positions from the LDB file.
    fn load_ldb(&mut self) -> Result<(), TuiError> {
        let filename = self.cfgloc.get_path("ldb");
        let load_result = std::fs::read_to_string(&filename);

        if load_result
            .as_ref()
            .is_err_and(|e| e.kind() == std::io::ErrorKind::NotFound)
        {
            // Most errors are errors, but if the LDB file simply
            // doesn't exist, that's fine (we may be being run for the
            // first time!) and we just return without having loaded
            // anything.
            return Ok(());
        }

        let json = load_result?;
        let ldb: BTreeMap<String, String> = serde_json::from_str(&json)?;

        for (keystr, valstr) in &ldb {
            let key = match keystr as &str {
                "home" => FeedId::Home,
                "mentions" => FeedId::Mentions,
                "ego" => FeedId::Ego,

                // Tolerate extra keys in ldb, in case they were
                // written by a later version
                _ => continue,
            };

            self.set_latest_read(key.clone(), valstr.clone());
        }
        Ok(())
    }

    /// Insert an entry into the saved file positions.
    fn set_latest_read(&mut self, key: FeedId, val: String) {
        match self.file_positions.entry(key.into()) {
            hash_map::Entry::Vacant(e) => {
                e.insert(SavedFilePos {
                    file_pos: None,
                    latest_read_id: Some(val),
                });
            }
            hash_map::Entry::Occupied(mut e) => {
                e.get_mut().latest_read_id = Some(val)
            }
        }
    }

    /// Return a non-mutable reference to the current topmost activity state.
    fn activity_state(&self) -> &dyn ActivityState {
        self.activity_states
            .get(&self.activity_stack.top())
            .expect("every current activity ought to have an entry here")
            .as_ref()
    }

    /// Return a mutable reference to the current topmost activity state.
    fn activity_state_mut(&mut self) -> &mut dyn ActivityState {
        self.activity_states
            .get_mut(&self.activity_stack.top())
            .expect("every current activity ought to have an entry here")
            .as_mut()
    }

    /// Return a non-mutable reference to the current overlay activity
    /// state, if any.
    fn overlay_activity_state(&self) -> Option<&dyn ActivityState> {
        match self.activity_stack.overlay() {
            Some(activity) => Some(
                self.activity_states
                    .get(&activity)
                    .expect(
                        "every current activity ought to have an entry here",
                    )
                    .as_ref(),
            ),
            None => None,
        }
    }

    /// Return a mutable reference to the current overlay activity
    /// state, if any.
    fn overlay_activity_state_mut(
        &mut self,
    ) -> Option<&mut (dyn ActivityState + '_)> {
        match self.activity_stack.overlay() {
            Some(activity) => Some(
                self.activity_states
                    .get_mut(&activity)
                    .expect(
                        "every current activity ought to have an entry here",
                    )
                    .as_mut(),
            ),
            None => None,
        }
    }

    /// Cause the TUI to jump to the Error Log.
    fn throw_into_error_log(&mut self, client: &mut Client) {
        self.activity_stack.goto(UtilityActivity::ErrorLog.into());
        self.changed_activity(client, None, true);
    }
}
