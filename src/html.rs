use html2text::render::{TaggedLine, TaggedLineElement, TextDecorator};
pub use html2text::RenderTree;
use html2text::{config, Colour};
use std::cell::RefCell;

use super::coloured_string::*;

#[derive(Clone, Debug, Default)]
struct OurDecorator<'a> {
    urls: Option<&'a RefCell<Vec<String>>>,
    colours_pushed: usize,
    current_url: Option<String>,
}

impl<'a> OurDecorator<'a> {
    fn new() -> Self {
        Self::with_option_urls(None)
    }

    fn with_urls(urls: &'a RefCell<Vec<String>>) -> Self {
        Self::with_option_urls(Some(urls))
    }

    fn with_option_urls(urls: Option<&'a RefCell<Vec<String>>>) -> Self {
        OurDecorator {
            urls,
            colours_pushed: 0,
            current_url: None,
        }
    }
}

impl<'a> TextDecorator for OurDecorator<'a> {
    type Annotation = char;

    /// Return an annotation and rendering prefix for a link.
    fn decorate_link_start(
        &mut self,
        url: &str,
    ) -> (String, Self::Annotation) {
        if self.colours_pushed == 0 && self.urls.is_some() {
            self.current_url = Some(url.to_owned());
        }
        ("".to_string(), 'u')
    }

    /// Return a suffix for after a link.
    fn decorate_link_end(&mut self) -> String {
        if let Some(url) = self.current_url.take() {
            if let Some(rc) = self.urls {
                // This is safe because the borrow only lasts for the
                // duration of this Vec::push, and it's the only
                // borrow_mut of this RefCell anywhere, so nothing is
                // going to be trying to re-borrow it during
                // re-entrant code.
                rc.borrow_mut().push(url);
            }
        }
        "".to_string()
    }

    /// Return an annotation and rendering prefix for em
    fn decorate_em_start(&self) -> (String, Self::Annotation) {
        ("".to_string(), '_')
    }

    /// Return a suffix for after an em.
    fn decorate_em_end(&self) -> String {
        "".to_string()
    }

    /// Return an annotation and rendering prefix for strong
    fn decorate_strong_start(&self) -> (String, Self::Annotation) {
        ("".to_string(), 's')
    }

    /// Return a suffix for after a strong.
    fn decorate_strong_end(&self) -> String {
        "".to_string()
    }

    /// Return an annotation and rendering prefix for strikeout
    fn decorate_strikeout_start(&self) -> (String, Self::Annotation) {
        ("".to_string(), ' ')
    }

    /// Return a suffix for after a strikeout.
    fn decorate_strikeout_end(&self) -> String {
        "".to_string()
    }

    /// Return an annotation and rendering prefix for code
    fn decorate_code_start(&self) -> (String, Self::Annotation) {
        ("".to_string(), 'c')
    }

    /// Return a suffix for after a code.
    fn decorate_code_end(&self) -> String {
        "".to_string()
    }

    /// Return an annotation for the initial part of a preformatted line
    fn decorate_preformat_first(&self) -> Self::Annotation {
        'c'
    }

    /// Return an annotation for a continuation line when a preformatted
    /// line doesn't fit.
    fn decorate_preformat_cont(&self) -> Self::Annotation {
        'c'
    }

    /// Return an annotation and rendering prefix for a link.
    fn decorate_image(
        &mut self,
        _src: &str,
        _title: &str,
    ) -> (String, Self::Annotation) {
        ("".to_string(), 'm')
    }

    /// Return prefix string of header in specific level.
    fn header_prefix(&self, level: usize) -> String {
        "#".repeat(level) + " "
    }

    /// Return prefix string of quoted block.
    fn quote_prefix(&self) -> String {
        "> ".to_string()
    }

    /// Return prefix string of unordered list item.
    fn unordered_item_prefix(&self) -> String {
        " - ".to_string()
    }

    /// Return prefix string of ith ordered list item.
    fn ordered_item_prefix(&self, i: i64) -> String {
        format!(" {}. ", i)
    }

    /// Return a new decorator of the same type which can be used
    /// for sub blocks.
    fn make_subblock_decorator(&self) -> Self {
        OurDecorator::with_option_urls(self.urls)
    }

    /// Return an annotation corresponding to adding colour, or none.
    fn push_colour(&mut self, col: Colour) -> Option<Self::Annotation> {
        let annot = match col.r {
            1 => Some('@'),
            4 => Some('#'),
            _ => None,
        };

        self.colours_pushed += 1;
        if annot.is_some() {
            self.current_url = None;
        }

        annot
    }

    /// Pop the last colour pushed if we pushed one.
    fn pop_colour(&mut self) -> bool {
        self.colours_pushed -= 1;
        true
    }

    /// Finish with a document, and return extra lines (eg footnotes)
    /// to add to the rendered text.
    fn finalise(
        &mut self,
        _links: Vec<String>,
    ) -> Vec<TaggedLine<Self::Annotation>> {
        Vec::new()
    }
}

pub fn parse(html: &str) -> Result<RenderTree, html2text::Error> {
    let cfg = config::plain().add_css(
        r##"
.mention { color: #010203; }
.hashtag { color: #040506; }
p { white-space: pre-wrap; }
"##,
    )?;
    let dom = cfg.parse_html(html.as_bytes())?;
    cfg.dom_to_render_tree(&dom)
}

fn try_render(
    rt: &RenderTree,
    wrapwidth: usize,
    fullwidth: usize,
) -> Result<Vec<TaggedLine<Vec<char>>>, html2text::Error> {
    let cfg =
        config::with_decorator(OurDecorator::new()).max_wrap_width(wrapwidth);
    cfg.render_to_lines(rt.clone(), fullwidth)
}

fn render_tl(rt: &RenderTree, width: usize) -> Vec<TaggedLine<Vec<char>>> {
    if let Ok(lines) = try_render(rt, width, width) {
        return lines;
    }

    let mut wbad = width;
    let mut wgood = {
        let mut w = width;
        loop {
            w += w / 2;
            if try_render(rt, width, w).is_ok() {
                break w;
            }
        }
    };

    while wgood - wbad > 1 {
        let wmid = wbad + (wgood - wbad) / 2;
        if try_render(rt, width, wmid).is_ok() {
            wgood = wmid;
        } else {
            wbad = wmid;
        }
    }

    try_render(rt, width, wgood + wgood / 4)
        .expect("Shouldn't be too narrow now")
}

fn to_coloured_string(tl: &TaggedLine<Vec<char>>) -> ColouredString {
    let mut cs = ColouredString::plain("");
    for e in tl.iter() {
        if let TaggedLineElement::Str(ts) = e {
            let c: char = match ts.tag.first() {
                Some(c) => *c,
                None => ' ',
            };
            cs.push_str(ColouredString::uniform(&ts.s, c));
        }
    }
    cs
}

pub fn render(rt: &RenderTree, width: usize) -> Vec<ColouredString> {
    render_tl(rt, width)
        .iter()
        .map(to_coloured_string)
        .collect()
}

pub fn list_urls(rt: &RenderTree) -> Vec<String> {
    let mut width = 256;
    loop {
        let urls = RefCell::new(Vec::new());
        if config::with_decorator(OurDecorator::with_urls(&urls))
            .render_to_lines(rt.clone(), width)
            .is_ok()
        {
            break urls.into_inner();
        }
        width = width
            .checked_mul(2)
            .expect("Surely something else went wrong before we got this big");
    }
}
