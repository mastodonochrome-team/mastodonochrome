use std::fmt::Write as _;
use std::iter::once;
use sys_locale::get_locale;

use super::client::{Client, ClientError};
use super::coloured_string::{ColouredString, ColouredStringCommon};
use super::editor::{
    count_characters_in_post, ContentWarning, EditableMenuLine,
};
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
};
use super::types::{Account, InstanceStatusConfig, StatusSource, Visibility};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PostMetadata {
    pub replaces_id: Option<String>,
    pub in_reply_to_id: Option<String>,
    pub visibility: Visibility,
    pub content_warning: Option<String>,
    pub language: Option<String>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Post {
    pub text: String,
    pub m: PostMetadata,
}

fn default_language(ac: &Account) -> String {
    ac.source
        .as_ref()
        .and_then(|s| s.language.clone())
        .unwrap_or_else(|| {
            get_locale()
                .as_deref()
                .and_then(|s| s.split('-').next())
                .map(|s| if s.is_empty() { "en" } else { s })
                .unwrap_or("en")
                .to_owned()
        })
}

impl Post {
    pub fn new(client: &mut Client) -> Result<Self, ClientError> {
        let ac = client.account_by_id(&client.our_account_id())?;

        // Take the default visibility from your account settings
        let visibility =
            ac.source.as_ref().map_or(Visibility::Public, |s| s.privacy);

        // Set the 'sensitive' flag if the account is marked as
        // 'posts are sensitive by default'.
        //
        // I don't really approve of _just_ setting the sensitive flag
        // without also giving a textual content warning saying why,
        // so it would be nice here to encourage the user to write
        // one, or fill in a default one. But the former is horribly
        // intrusive (to the code structure _as well_ as to the UX!),
        // and the latter is language-dependent (software shouldn't
        // auto-fill an English message if the user is posting in
        // Korean). So if the user has set that as their defaults, we
        // sigh, and go with 'sensitive but no message'.
        let content_warning = ac.source.as_ref().and_then(|s| {
            if s.sensitive {
                Some("".to_owned())
            } else {
                None
            }
        });

        Ok(Post {
            text: "".to_owned(),
            m: PostMetadata {
                replaces_id: None,
                in_reply_to_id: None,
                visibility,
                content_warning,
                language: Some(default_language(&ac)),
            },
        })
    }

    pub fn edit_existing(
        src: &StatusSource,
        client: &mut Client,
    ) -> Result<Self, ClientError> {
        let st = client.status_by_id(&src.id)?;

        if st.reblog.is_some() {
            // Shouldn't even be possible to get here, we'd hope
            return Err(ClientError::Internal(
                "INTERNAL ERROR: Boosts cannot be edited".to_owned(),
            ));
        }
        if st.poll.is_some() {
            return Err(ClientError::Internal(
                "NOT YET IMPLEMENTED: Posts with polls can't be edited"
                    .to_owned(),
            ));
        }
        if !st.media_attachments.is_empty() {
            return Err(ClientError::Internal(
                "NOT YET IMPLEMENTED: Posts with media can't be edited"
                    .to_owned(),
            ));
        }

        // Keep all settings from the existing post
        let visibility = st.visibility;
        let content_warning = if st.sensitive {
            Some(src.spoiler_text.to_owned())
        } else {
            None
        };

        Ok(Post {
            text: src.text.to_owned(),
            m: PostMetadata {
                replaces_id: Some(src.id.clone()),
                in_reply_to_id: st.in_reply_to_id.clone(),
                visibility,
                content_warning,
                language: st.language.clone(),
            },
        })
    }

    #[cfg(test)]
    pub fn with_text(text: &str) -> Self {
        Post {
            text: text.to_owned(),
            m: PostMetadata {
                replaces_id: None,
                in_reply_to_id: None,
                visibility: Visibility::Public,
                content_warning: None,
                language: Some("dummy".to_owned()),
            },
        }
    }

    pub fn reply_to(
        id: &str,
        client: &mut Client,
    ) -> Result<Self, ClientError> {
        let ac = client.account_by_id(&client.our_account_id())?;
        let st = client.status_by_id(id)?.strip_boost();

        let ourself = client.our_account_fq();

        let userids = once(client.fq(&st.account.acct))
            .chain(st.mentions.iter().map(|m| client.fq(&m.acct)))
            .filter(|acct| acct != &ourself);

        let text = userids.fold(String::new(), |mut s, acct| {
            write!(s, "@{} ", acct).expect("fmt to string failed");
            s
        });

        // Set a default content warning of the same as the post we're
        // replying to.
        let content_warning = if !st.sensitive {
            None
        } else {
            Some(st.spoiler_text.clone())
        };

        Ok(Post {
            text,
            m: PostMetadata {
                replaces_id: None,
                in_reply_to_id: Some(id.to_owned()),
                visibility: st.visibility, // match the existing vis
                content_warning,
                language: Some(default_language(&ac)),
            },
        })
    }
}

struct PostMenu {
    post: Post,
    title: FileHeader,
    normal_status: FileStatusLineFinal,
    edit_status: FileStatusLineFinal,
    ml_post: MenuKeypressLine,
    ml_cancel: MenuKeypressLine,
    ml_edit: MenuKeypressLine,
    cl_vis: Option<CyclingMenuLine<Visibility>>,
    cl_sensitive: CyclingMenuLine<bool>,
    el_content_warning: EditableMenuLine<ContentWarning>,
    el_language: EditableMenuLine<Option<String>>,
    para_post_outcome: Paragraph,
}

impl PostMenu {
    fn new(post: Post, conf: &InstanceStatusConfig) -> Self {
        let title = match &post.m.in_reply_to_id {
            None => "Post a toot".to_owned(),
            Some(id) => format!("Reply to post id {id}"),
        };
        let title = FileHeader::new(ColouredString::uniform(&title, 'H'));

        let normal_status = FileStatusLine::new()
            .message("Select a menu option.")
            .add(Pr('?'), "Help", 101)
            .finalise();
        let edit_status = FileStatusLine::new()
            .message("Edit line and press Return")
            .finalise();

        let ml_post =
            MenuKeypressLine::new(Space, ColouredString::plain("Post"));
        let ml_cancel = MenuKeypressLine::new(
            Pr('Q'),
            ColouredString::plain("Cancel post"),
        );
        let ml_edit = MenuKeypressLine::new(
            Pr('A'),
            ColouredString::plain("Re-edit post"),
        );
        let cl_vis = if post.m.replaces_id.is_none() {
            Some(CyclingMenuLine::new(
                Pr('V'),
                ColouredString::plain("Visibility: "),
                &Visibility::long_descriptions(),
                post.m.visibility,
            ))
        } else {
            // Don't show a visibility selector if we're editing an
            // existing post, because the Mastodon edit API doesn't let us
            // change visibility
            None
        };
        let cl_sensitive = CyclingMenuLine::new(
            Pr('S'),
            ColouredString::plain("Mark post as sensitive: "),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            post.m.content_warning.is_some(),
        );
        let cw_char_limit = conf
            .max_characters
            .saturating_sub(count_characters_in_post(&post.text, conf));
        let el_content_warning = EditableMenuLine::new(
            Pr('W'),
            ColouredString::plain("  Content warning: "),
            ContentWarning::new(post.m.content_warning.clone(), cw_char_limit),
        );
        let el_language = EditableMenuLine::new(
            Pr('L'),
            ColouredString::plain("Language: "),
            post.m.language.clone(),
        );

        let para_post_outcome =
            Paragraph::new().set_centred(true).set_indent(10, 10);

        let mut pm = PostMenu {
            post,
            title,
            normal_status,
            edit_status,
            ml_post,
            ml_cancel,
            ml_edit,
            cl_vis,
            cl_sensitive,
            el_content_warning,
            el_language,
            para_post_outcome,
        };
        pm.fix_widths();
        pm
    }

    fn fix_widths(&mut self) -> (usize, usize) {
        let mut lmaxwid = 0;
        let mut rmaxwid = 0;
        self.ml_post.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_cancel.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_edit.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_vis.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_sensitive.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_content_warning
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_language.check_widths(&mut lmaxwid, &mut rmaxwid);

        self.ml_post.reset_widths();
        self.ml_cancel.reset_widths();
        self.ml_edit.reset_widths();
        self.cl_vis.reset_widths();
        self.cl_sensitive.reset_widths();
        self.el_content_warning.reset_widths();
        self.el_language.reset_widths();

        self.ml_post.ensure_widths(lmaxwid, rmaxwid);
        self.ml_cancel.ensure_widths(lmaxwid, rmaxwid);
        self.ml_edit.ensure_widths(lmaxwid, rmaxwid);
        self.cl_vis.ensure_widths(lmaxwid, rmaxwid);
        self.cl_sensitive.ensure_widths(lmaxwid, rmaxwid);
        self.el_content_warning.ensure_widths(lmaxwid, rmaxwid);
        self.el_language.ensure_widths(lmaxwid, rmaxwid);

        (lmaxwid, rmaxwid)
    }

    fn write_data_into_post(&mut self) {
        if let Some(cl_vis) = &self.cl_vis {
            self.post.m.visibility = cl_vis.get_value();
        }
        self.post.m.content_warning = if self.cl_sensitive.get_value() {
            self.el_content_warning
                .get_data()
                .get_text()
                .clone()
                .or_else(|| Some("".to_owned()))
        } else {
            None
        };
        self.post.m.language.clone_from(self.el_language.get_data());
    }

    fn post(&mut self, client: &mut Client) -> LogicalAction {
        self.write_data_into_post();

        match client.post_status(&self.post) {
            Ok(_) => LogicalAction::Pop,
            Err(
                err @ ClientError::UrlFetchHTTPRich(
                    _,
                    reqwest::StatusCode::UNPROCESSABLE_ENTITY,
                    _,
                ),
            ) => {
                self.para_post_outcome.clear();
                self.para_post_outcome.push_text(
                    ColouredString::uniform(
                        &format!("Posting error: {err}"),
                        'r',
                    ),
                    false,
                );
                LogicalAction::Beep
            }
            Err(err) => LogicalAction::Error(err),
        }
    }
}

impl ActivityState for PostMenu {
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let push_split_lines =
            |lines: &mut Vec<ColouredString>, output: Vec<ColouredString>| {
                for line in output {
                    for frag in line.split(w.saturating_sub(1)) {
                        lines.push(frag.into());
                    }
                }
            };

        let mut lines = Vec::new();
        let mut cursorpos = CursorPosition::End;
        lines.extend_from_slice(&self.title.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.ml_post.render(w));
        lines.extend_from_slice(&self.ml_cancel.render(w));
        lines.extend_from_slice(&self.ml_edit.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_vis.render(w));
        lines.extend_from_slice(&self.cl_sensitive.render(w));
        lines.push(self.el_content_warning.render(
            w,
            &mut cursorpos,
            lines.len(),
        ));
        lines.push(self.el_language.render(w, &mut cursorpos, lines.len()));

        lines.extend_from_slice(&BlankLine::render_static());
        push_split_lines(&mut lines, self.para_post_outcome.render(w));

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        if self.el_content_warning.is_editing()
            || self.el_language.is_editing()
        {
            lines.extend_from_slice(&self.edit_status.render(w));
        } else {
            lines.extend_from_slice(&self.normal_status.render(w));
        }

        (lines, cursorpos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        // Let editable menu lines have first crack at the keypress
        if self.el_content_warning.handle_keypress(key)
            || self.el_language.handle_keypress(key)
        {
            self.fix_widths();
            return LogicalAction::Nothing;
        }

        match key {
            Pr('?') => LogicalAction::Help,

            Space => self.post(client),
            Pr('q') | Pr('Q') => LogicalAction::Pop,
            Pr('a') | Pr('A') => {
                self.write_data_into_post();
                LogicalAction::PostReEdit(self.post.clone())
            }
            Pr('v') | Pr('V') => match &mut self.cl_vis {
                Some(cl_vis) => cl_vis.cycle(),
                None => LogicalAction::Nothing,
            },
            Pr('s') | Pr('S') => {
                let action = self.cl_sensitive.cycle();
                if self.cl_sensitive.get_value()
                    && self.el_content_warning.get_data().get_text().is_none()
                {
                    // Encourage the user to write a content warning,
                    // by automatically focusing into the content
                    // warning editor.
                    self.el_content_warning.start_editing()
                } else {
                    action
                }
            }
            Pr('w') | Pr('W') => {
                // If the user wants to write a content warning, mark
                // the post as sensitive too, to prevent accidents.
                if !self.cl_sensitive.get_value() {
                    self.cl_sensitive.cycle();
                }
                self.el_content_warning.start_editing()
            }
            Pr('l') | Pr('L') => self.el_language.start_editing(),
            _ => LogicalAction::Nothing,
        }
    }

    fn resize(&mut self, w: usize, _h: usize) {
        self.el_content_warning.resize(w);
        self.el_language.resize(w);
    }
}

pub fn post_menu(
    client: &mut Client,
    post: Post,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let conf = client.instance()?.configuration.statuses;
    Ok(Box::new(PostMenu::new(post, &conf)))
}
