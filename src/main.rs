// These allows should be in a [lints] in Cargo.toml,
// which was stabilised in Rust 1.74.  So maybe not do that yet.

// We don't mind this accumulating lints indefinitely.  Misspellings
// are unlikely because we put things here to suppress lints we're seeing,
// in which case a misspelling wouldn't be effective.  Eventually,
// we might run a CI check for *deprecated* lints with our MSRV,
// and *unknown* lints with nightly.  But this will definitely do:
#![allow(unknown_lints)]
// Non-minimal coding patterns we sometimes use for clarity
#![allow(clippy::collapsible_if)]
#![allow(clippy::single_match)]
#![allow(clippy::let_and_return)]
#![allow(clippy::redundant_closure_call)]
#![allow(clippy::needless_question_mark)]
#![allow(clippy::len_zero)]
#![allow(clippy::needless_borrows_for_generic_args)]
#![allow(clippy::wildcard_in_or_patterns)]
// Things we might do for uniformity/generality which the compiler will
// optimise, but which clippy wants to normalise into a less general case
#![allow(clippy::single_char_pattern)]
#![allow(clippy::useless_format)]
#![allow(clippy::needless_borrow)]
#![allow(clippy::redundant_closure)]

use clap::Parser;
use std::process::ExitCode;

pub mod activity_stack;
pub mod auth;
pub mod client;
pub mod coloured_string;
pub mod config;
pub mod editor;
pub mod file;
pub mod help;
pub mod html;
pub mod login;
pub mod menu;
pub mod options;
pub mod posting;
pub mod scan_re;
pub mod streamparse;
pub mod text;
pub mod top_level_error;
pub mod tui;
pub mod types;
pub mod unordered_merge;

use crate::config::ConfigLocation;
use crate::top_level_error::TopLevelError;
use crate::tui::Tui;

#[derive(Parser, Debug)]
struct Args {
    /// Directory containing configuration files.
    #[arg(short, long)]
    config: Option<std::path::PathBuf>,

    /// Read-only mode: the client prevents accidental posting.
    #[arg(short, long, action(clap::ArgAction::SetTrue))]
    readonly: bool,

    /// HTTP logging mode: the client logs all its transactions to a file.
    #[arg(long)]
    loghttp: Option<std::path::PathBuf>,
}

fn main_inner() -> Result<(), TopLevelError> {
    let cli = Args::try_parse()?;
    let cfgloc = match cli.config {
        None => ConfigLocation::default_loc()?,
        Some(dir) => ConfigLocation::from_pathbuf(dir),
    };
    let httplogfile = match cli.loghttp {
        None => None,
        Some(path) => Some(std::fs::File::create(path)?),
    };
    Ok(Tui::run(&cfgloc, cli.readonly, httplogfile)?)
}

fn main() -> ExitCode {
    match main_inner() {
        Ok(_) => ExitCode::from(0),
        Err(e) => {
            eprintln!("{}", e);
            ExitCode::from(1)
        }
    }
}
