use lazy_static::lazy_static;

use regex::Regex;

pub struct Findable {
    text: Regex,
    bad_pre: Option<Regex>,
}

impl Findable {
    pub fn get_span(
        &self,
        text: &str,
        start: usize,
    ) -> Option<(usize, usize)> {
        let mut start = start;
        loop {
            match self.text.find_at(text, start) {
                None => break None,
                Some(m) => {
                    let (ms, me) = (m.start(), m.end());
                    let find_bad_pre = match &self.bad_pre {
                        None => None,
                        Some(re) => re.find(&text[..ms]),
                    };
                    match find_bad_pre {
                        None => break Some((ms, me)),
                        Some(_) => start = ms + 1,
                    }
                }
            };
        }
    }
}

pub struct Scan {
    pub mention: Findable,
    pub hashtag: Findable,
    pub url: Findable,

    #[cfg(test)]
    domain: Findable, // for unit testing only
}

lazy_static! {
    static ref SCANNER: Scan = Scan::new();
}

impl Scan {
    fn new() -> Self {
        let word = "0-9A-Z_a-z\u{aa}\u{b5}\u{ba}\u{c0}-\u{d6}\u{d8}-\u{f6}\u{f8}-\u{2c1}\u{2c6}-\u{2d1}\u{2e0}-\u{2e4}\u{2ec}\u{2ee}\u{300}-\u{374}\u{376}-\u{377}\u{37a}-\u{37d}\u{37f}\u{386}\u{388}-\u{38a}\u{38c}\u{38e}-\u{3a1}\u{3a3}-\u{3f5}\u{3f7}-\u{481}\u{483}-\u{52f}\u{531}-\u{556}\u{559}\u{560}-\u{588}\u{591}-\u{5bd}\u{5bf}\u{5c1}-\u{5c2}\u{5c4}-\u{5c5}\u{5c7}\u{5d0}-\u{5ea}\u{5ef}-\u{5f2}\u{610}-\u{61a}\u{620}-\u{669}\u{66e}-\u{6d3}\u{6d5}-\u{6dc}\u{6df}-\u{6e8}\u{6ea}-\u{6fc}\u{6ff}\u{710}-\u{74a}\u{74d}-\u{7b1}\u{7c0}-\u{7f5}\u{7fa}\u{7fd}\u{800}-\u{82d}\u{840}-\u{85b}\u{860}-\u{86a}\u{8a0}-\u{8b4}\u{8b6}-\u{8bd}\u{8d3}-\u{8e1}\u{8e3}-\u{963}\u{966}-\u{96f}\u{971}-\u{983}\u{985}-\u{98c}\u{98f}-\u{990}\u{993}-\u{9a8}\u{9aa}-\u{9b0}\u{9b2}\u{9b6}-\u{9b9}\u{9bc}-\u{9c4}\u{9c7}-\u{9c8}\u{9cb}-\u{9ce}\u{9d7}\u{9dc}-\u{9dd}\u{9df}-\u{9e3}\u{9e6}-\u{9f1}\u{9fc}\u{9fe}\u{a01}-\u{a03}\u{a05}-\u{a0a}\u{a0f}-\u{a10}\u{a13}-\u{a28}\u{a2a}-\u{a30}\u{a32}-\u{a33}\u{a35}-\u{a36}\u{a38}-\u{a39}\u{a3c}\u{a3e}-\u{a42}\u{a47}-\u{a48}\u{a4b}-\u{a4d}\u{a51}\u{a59}-\u{a5c}\u{a5e}\u{a66}-\u{a75}\u{a81}-\u{a83}\u{a85}-\u{a8d}\u{a8f}-\u{a91}\u{a93}-\u{aa8}\u{aaa}-\u{ab0}\u{ab2}-\u{ab3}\u{ab5}-\u{ab9}\u{abc}-\u{ac5}\u{ac7}-\u{ac9}\u{acb}-\u{acd}\u{ad0}\u{ae0}-\u{ae3}\u{ae6}-\u{aef}\u{af9}-\u{aff}\u{b01}-\u{b03}\u{b05}-\u{b0c}\u{b0f}-\u{b10}\u{b13}-\u{b28}\u{b2a}-\u{b30}\u{b32}-\u{b33}\u{b35}-\u{b39}\u{b3c}-\u{b44}\u{b47}-\u{b48}\u{b4b}-\u{b4d}\u{b56}-\u{b57}\u{b5c}-\u{b5d}\u{b5f}-\u{b63}\u{b66}-\u{b6f}\u{b71}\u{b82}-\u{b83}\u{b85}-\u{b8a}\u{b8e}-\u{b90}\u{b92}-\u{b95}\u{b99}-\u{b9a}\u{b9c}\u{b9e}-\u{b9f}\u{ba3}-\u{ba4}\u{ba8}-\u{baa}\u{bae}-\u{bb9}\u{bbe}-\u{bc2}\u{bc6}-\u{bc8}\u{bca}-\u{bcd}\u{bd0}\u{bd7}\u{be6}-\u{bef}\u{c00}-\u{c0c}\u{c0e}-\u{c10}\u{c12}-\u{c28}\u{c2a}-\u{c39}\u{c3d}-\u{c44}\u{c46}-\u{c48}\u{c4a}-\u{c4d}\u{c55}-\u{c56}\u{c58}-\u{c5a}\u{c60}-\u{c63}\u{c66}-\u{c6f}\u{c80}-\u{c83}\u{c85}-\u{c8c}\u{c8e}-\u{c90}\u{c92}-\u{ca8}\u{caa}-\u{cb3}\u{cb5}-\u{cb9}\u{cbc}-\u{cc4}\u{cc6}-\u{cc8}\u{cca}-\u{ccd}\u{cd5}-\u{cd6}\u{cde}\u{ce0}-\u{ce3}\u{ce6}-\u{cef}\u{cf1}-\u{cf2}\u{d00}-\u{d03}\u{d05}-\u{d0c}\u{d0e}-\u{d10}\u{d12}-\u{d44}\u{d46}-\u{d48}\u{d4a}-\u{d4e}\u{d54}-\u{d57}\u{d5f}-\u{d63}\u{d66}-\u{d6f}\u{d7a}-\u{d7f}\u{d82}-\u{d83}\u{d85}-\u{d96}\u{d9a}-\u{db1}\u{db3}-\u{dbb}\u{dbd}\u{dc0}-\u{dc6}\u{dca}\u{dcf}-\u{dd4}\u{dd6}\u{dd8}-\u{ddf}\u{de6}-\u{def}\u{df2}-\u{df3}\u{e01}-\u{e3a}\u{e40}-\u{e4e}\u{e50}-\u{e59}\u{e81}-\u{e82}\u{e84}\u{e86}-\u{e8a}\u{e8c}-\u{ea3}\u{ea5}\u{ea7}-\u{ebd}\u{ec0}-\u{ec4}\u{ec6}\u{ec8}-\u{ecd}\u{ed0}-\u{ed9}\u{edc}-\u{edf}\u{f00}\u{f18}-\u{f19}\u{f20}-\u{f29}\u{f35}\u{f37}\u{f39}\u{f3e}-\u{f47}\u{f49}-\u{f6c}\u{f71}-\u{f84}\u{f86}-\u{f97}\u{f99}-\u{fbc}\u{fc6}\u{1000}-\u{1049}\u{1050}-\u{109d}\u{10a0}-\u{10c5}\u{10c7}\u{10cd}\u{10d0}-\u{10fa}\u{10fc}-\u{1248}\u{124a}-\u{124d}\u{1250}-\u{1256}\u{1258}\u{125a}-\u{125d}\u{1260}-\u{1288}\u{128a}-\u{128d}\u{1290}-\u{12b0}\u{12b2}-\u{12b5}\u{12b8}-\u{12be}\u{12c0}\u{12c2}-\u{12c5}\u{12c8}-\u{12d6}\u{12d8}-\u{1310}\u{1312}-\u{1315}\u{1318}-\u{135a}\u{135d}-\u{135f}\u{1380}-\u{138f}\u{13a0}-\u{13f5}\u{13f8}-\u{13fd}\u{1401}-\u{166c}\u{166f}-\u{167f}\u{1681}-\u{169a}\u{16a0}-\u{16ea}\u{16ee}-\u{16f8}\u{1700}-\u{170c}\u{170e}-\u{1714}\u{1720}-\u{1734}\u{1740}-\u{1753}\u{1760}-\u{176c}\u{176e}-\u{1770}\u{1772}-\u{1773}\u{1780}-\u{17d3}\u{17d7}\u{17dc}-\u{17dd}\u{17e0}-\u{17e9}\u{180b}-\u{180d}\u{1810}-\u{1819}\u{1820}-\u{1878}\u{1880}-\u{18aa}\u{18b0}-\u{18f5}\u{1900}-\u{191e}\u{1920}-\u{192b}\u{1930}-\u{193b}\u{1946}-\u{196d}\u{1970}-\u{1974}\u{1980}-\u{19ab}\u{19b0}-\u{19c9}\u{19d0}-\u{19d9}\u{1a00}-\u{1a1b}\u{1a20}-\u{1a5e}\u{1a60}-\u{1a7c}\u{1a7f}-\u{1a89}\u{1a90}-\u{1a99}\u{1aa7}\u{1ab0}-\u{1abe}\u{1b00}-\u{1b4b}\u{1b50}-\u{1b59}\u{1b6b}-\u{1b73}\u{1b80}-\u{1bf3}\u{1c00}-\u{1c37}\u{1c40}-\u{1c49}\u{1c4d}-\u{1c7d}\u{1c80}-\u{1c88}\u{1c90}-\u{1cba}\u{1cbd}-\u{1cbf}\u{1cd0}-\u{1cd2}\u{1cd4}-\u{1cfa}\u{1d00}-\u{1df9}\u{1dfb}-\u{1f15}\u{1f18}-\u{1f1d}\u{1f20}-\u{1f45}\u{1f48}-\u{1f4d}\u{1f50}-\u{1f57}\u{1f59}\u{1f5b}\u{1f5d}\u{1f5f}-\u{1f7d}\u{1f80}-\u{1fb4}\u{1fb6}-\u{1fbc}\u{1fbe}\u{1fc2}-\u{1fc4}\u{1fc6}-\u{1fcc}\u{1fd0}-\u{1fd3}\u{1fd6}-\u{1fdb}\u{1fe0}-\u{1fec}\u{1ff2}-\u{1ff4}\u{1ff6}-\u{1ffc}\u{203f}-\u{2040}\u{2054}\u{2071}\u{207f}\u{2090}-\u{209c}\u{20d0}-\u{20f0}\u{2102}\u{2107}\u{210a}-\u{2113}\u{2115}\u{2119}-\u{211d}\u{2124}\u{2126}\u{2128}\u{212a}-\u{212d}\u{212f}-\u{2139}\u{213c}-\u{213f}\u{2145}-\u{2149}\u{214e}\u{2160}-\u{2188}\u{24b6}-\u{24e9}\u{2c00}-\u{2c2e}\u{2c30}-\u{2c5e}\u{2c60}-\u{2ce4}\u{2ceb}-\u{2cf3}\u{2d00}-\u{2d25}\u{2d27}\u{2d2d}\u{2d30}-\u{2d67}\u{2d6f}\u{2d7f}-\u{2d96}\u{2da0}-\u{2da6}\u{2da8}-\u{2dae}\u{2db0}-\u{2db6}\u{2db8}-\u{2dbe}\u{2dc0}-\u{2dc6}\u{2dc8}-\u{2dce}\u{2dd0}-\u{2dd6}\u{2dd8}-\u{2dde}\u{2de0}-\u{2dff}\u{2e2f}\u{3005}-\u{3007}\u{3021}-\u{302f}\u{3031}-\u{3035}\u{3038}-\u{303c}\u{3041}-\u{3096}\u{3099}-\u{309a}\u{309d}-\u{309f}\u{30a1}-\u{30fa}\u{30fc}-\u{30ff}\u{3105}-\u{312f}\u{3131}-\u{318e}\u{31a0}-\u{31ba}\u{31f0}-\u{31ff}\u{3400}-\u{4db5}\u{4e00}-\u{9fef}\u{a000}-\u{a48c}\u{a4d0}-\u{a4fd}\u{a500}-\u{a60c}\u{a610}-\u{a62b}\u{a640}-\u{a672}\u{a674}-\u{a67d}\u{a67f}-\u{a6f1}\u{a717}-\u{a71f}\u{a722}-\u{a788}\u{a78b}-\u{a7bf}\u{a7c2}-\u{a7c6}\u{a7f7}-\u{a827}\u{a840}-\u{a873}\u{a880}-\u{a8c5}\u{a8d0}-\u{a8d9}\u{a8e0}-\u{a8f7}\u{a8fb}\u{a8fd}-\u{a92d}\u{a930}-\u{a953}\u{a960}-\u{a97c}\u{a980}-\u{a9c0}\u{a9cf}-\u{a9d9}\u{a9e0}-\u{a9fe}\u{aa00}-\u{aa36}\u{aa40}-\u{aa4d}\u{aa50}-\u{aa59}\u{aa60}-\u{aa76}\u{aa7a}-\u{aac2}\u{aadb}-\u{aadd}\u{aae0}-\u{aaef}\u{aaf2}-\u{aaf6}\u{ab01}-\u{ab06}\u{ab09}-\u{ab0e}\u{ab11}-\u{ab16}\u{ab20}-\u{ab26}\u{ab28}-\u{ab2e}\u{ab30}-\u{ab5a}\u{ab5c}-\u{ab67}\u{ab70}-\u{abea}\u{abec}-\u{abed}\u{abf0}-\u{abf9}\u{ac00}-\u{d7a3}\u{d7b0}-\u{d7c6}\u{d7cb}-\u{d7fb}\u{f900}-\u{fa6d}\u{fa70}-\u{fad9}\u{fb00}-\u{fb06}\u{fb13}-\u{fb17}\u{fb1d}-\u{fb28}\u{fb2a}-\u{fb36}\u{fb38}-\u{fb3c}\u{fb3e}\u{fb40}-\u{fb41}\u{fb43}-\u{fb44}\u{fb46}-\u{fbb1}\u{fbd3}-\u{fd3d}\u{fd50}-\u{fd8f}\u{fd92}-\u{fdc7}\u{fdf0}-\u{fdfb}\u{fe00}-\u{fe0f}\u{fe20}-\u{fe2f}\u{fe33}-\u{fe34}\u{fe4d}-\u{fe4f}\u{fe70}-\u{fe74}\u{fe76}-\u{fefc}\u{ff10}-\u{ff19}\u{ff21}-\u{ff3a}\u{ff3f}\u{ff41}-\u{ff5a}\u{ff66}-\u{ffbe}\u{ffc2}-\u{ffc7}\u{ffca}-\u{ffcf}\u{ffd2}-\u{ffd7}\u{ffda}-\u{ffdc}\u{10000}-\u{1000b}\u{1000d}-\u{10026}\u{10028}-\u{1003a}\u{1003c}-\u{1003d}\u{1003f}-\u{1004d}\u{10050}-\u{1005d}\u{10080}-\u{100fa}\u{10140}-\u{10174}\u{101fd}\u{10280}-\u{1029c}\u{102a0}-\u{102d0}\u{102e0}\u{10300}-\u{1031f}\u{1032d}-\u{1034a}\u{10350}-\u{1037a}\u{10380}-\u{1039d}\u{103a0}-\u{103c3}\u{103c8}-\u{103cf}\u{103d1}-\u{103d5}\u{10400}-\u{1049d}\u{104a0}-\u{104a9}\u{104b0}-\u{104d3}\u{104d8}-\u{104fb}\u{10500}-\u{10527}\u{10530}-\u{10563}\u{10600}-\u{10736}\u{10740}-\u{10755}\u{10760}-\u{10767}\u{10800}-\u{10805}\u{10808}\u{1080a}-\u{10835}\u{10837}-\u{10838}\u{1083c}\u{1083f}-\u{10855}\u{10860}-\u{10876}\u{10880}-\u{1089e}\u{108e0}-\u{108f2}\u{108f4}-\u{108f5}\u{10900}-\u{10915}\u{10920}-\u{10939}\u{10980}-\u{109b7}\u{109be}-\u{109bf}\u{10a00}-\u{10a03}\u{10a05}-\u{10a06}\u{10a0c}-\u{10a13}\u{10a15}-\u{10a17}\u{10a19}-\u{10a35}\u{10a38}-\u{10a3a}\u{10a3f}\u{10a60}-\u{10a7c}\u{10a80}-\u{10a9c}\u{10ac0}-\u{10ac7}\u{10ac9}-\u{10ae6}\u{10b00}-\u{10b35}\u{10b40}-\u{10b55}\u{10b60}-\u{10b72}\u{10b80}-\u{10b91}\u{10c00}-\u{10c48}\u{10c80}-\u{10cb2}\u{10cc0}-\u{10cf2}\u{10d00}-\u{10d27}\u{10d30}-\u{10d39}\u{10f00}-\u{10f1c}\u{10f27}\u{10f30}-\u{10f50}\u{10fe0}-\u{10ff6}\u{11000}-\u{11046}\u{11066}-\u{1106f}\u{1107f}-\u{110ba}\u{110d0}-\u{110e8}\u{110f0}-\u{110f9}\u{11100}-\u{11134}\u{11136}-\u{1113f}\u{11144}-\u{11146}\u{11150}-\u{11173}\u{11176}\u{11180}-\u{111c4}\u{111c9}-\u{111cc}\u{111d0}-\u{111da}\u{111dc}\u{11200}-\u{11211}\u{11213}-\u{11237}\u{1123e}\u{11280}-\u{11286}\u{11288}\u{1128a}-\u{1128d}\u{1128f}-\u{1129d}\u{1129f}-\u{112a8}\u{112b0}-\u{112ea}\u{112f0}-\u{112f9}\u{11300}-\u{11303}\u{11305}-\u{1130c}\u{1130f}-\u{11310}\u{11313}-\u{11328}\u{1132a}-\u{11330}\u{11332}-\u{11333}\u{11335}-\u{11339}\u{1133b}-\u{11344}\u{11347}-\u{11348}\u{1134b}-\u{1134d}\u{11350}\u{11357}\u{1135d}-\u{11363}\u{11366}-\u{1136c}\u{11370}-\u{11374}\u{11400}-\u{1144a}\u{11450}-\u{11459}\u{1145e}-\u{1145f}\u{11480}-\u{114c5}\u{114c7}\u{114d0}-\u{114d9}\u{11580}-\u{115b5}\u{115b8}-\u{115c0}\u{115d8}-\u{115dd}\u{11600}-\u{11640}\u{11644}\u{11650}-\u{11659}\u{11680}-\u{116b8}\u{116c0}-\u{116c9}\u{11700}-\u{1171a}\u{1171d}-\u{1172b}\u{11730}-\u{11739}\u{11800}-\u{1183a}\u{118a0}-\u{118e9}\u{118ff}\u{119a0}-\u{119a7}\u{119aa}-\u{119d7}\u{119da}-\u{119e1}\u{119e3}-\u{119e4}\u{11a00}-\u{11a3e}\u{11a47}\u{11a50}-\u{11a99}\u{11a9d}\u{11ac0}-\u{11af8}\u{11c00}-\u{11c08}\u{11c0a}-\u{11c36}\u{11c38}-\u{11c40}\u{11c50}-\u{11c59}\u{11c72}-\u{11c8f}\u{11c92}-\u{11ca7}\u{11ca9}-\u{11cb6}\u{11d00}-\u{11d06}\u{11d08}-\u{11d09}\u{11d0b}-\u{11d36}\u{11d3a}\u{11d3c}-\u{11d3d}\u{11d3f}-\u{11d47}\u{11d50}-\u{11d59}\u{11d60}-\u{11d65}\u{11d67}-\u{11d68}\u{11d6a}-\u{11d8e}\u{11d90}-\u{11d91}\u{11d93}-\u{11d98}\u{11da0}-\u{11da9}\u{11ee0}-\u{11ef6}\u{12000}-\u{12399}\u{12400}-\u{1246e}\u{12480}-\u{12543}\u{13000}-\u{1342e}\u{14400}-\u{14646}\u{16800}-\u{16a38}\u{16a40}-\u{16a5e}\u{16a60}-\u{16a69}\u{16ad0}-\u{16aed}\u{16af0}-\u{16af4}\u{16b00}-\u{16b36}\u{16b40}-\u{16b43}\u{16b50}-\u{16b59}\u{16b63}-\u{16b77}\u{16b7d}-\u{16b8f}\u{16e40}-\u{16e7f}\u{16f00}-\u{16f4a}\u{16f4f}-\u{16f87}\u{16f8f}-\u{16f9f}\u{16fe0}-\u{16fe1}\u{16fe3}\u{17000}-\u{187f7}\u{18800}-\u{18af2}\u{1b000}-\u{1b11e}\u{1b150}-\u{1b152}\u{1b164}-\u{1b167}\u{1b170}-\u{1b2fb}\u{1bc00}-\u{1bc6a}\u{1bc70}-\u{1bc7c}\u{1bc80}-\u{1bc88}\u{1bc90}-\u{1bc99}\u{1bc9d}-\u{1bc9e}\u{1d165}-\u{1d169}\u{1d16d}-\u{1d172}\u{1d17b}-\u{1d182}\u{1d185}-\u{1d18b}\u{1d1aa}-\u{1d1ad}\u{1d242}-\u{1d244}\u{1d400}-\u{1d454}\u{1d456}-\u{1d49c}\u{1d49e}-\u{1d49f}\u{1d4a2}\u{1d4a5}-\u{1d4a6}\u{1d4a9}-\u{1d4ac}\u{1d4ae}-\u{1d4b9}\u{1d4bb}\u{1d4bd}-\u{1d4c3}\u{1d4c5}-\u{1d505}\u{1d507}-\u{1d50a}\u{1d50d}-\u{1d514}\u{1d516}-\u{1d51c}\u{1d51e}-\u{1d539}\u{1d53b}-\u{1d53e}\u{1d540}-\u{1d544}\u{1d546}\u{1d54a}-\u{1d550}\u{1d552}-\u{1d6a5}\u{1d6a8}-\u{1d6c0}\u{1d6c2}-\u{1d6da}\u{1d6dc}-\u{1d6fa}\u{1d6fc}-\u{1d714}\u{1d716}-\u{1d734}\u{1d736}-\u{1d74e}\u{1d750}-\u{1d76e}\u{1d770}-\u{1d788}\u{1d78a}-\u{1d7a8}\u{1d7aa}-\u{1d7c2}\u{1d7c4}-\u{1d7cb}\u{1d7ce}-\u{1d7ff}\u{1da00}-\u{1da36}\u{1da3b}-\u{1da6c}\u{1da75}\u{1da84}\u{1da9b}-\u{1da9f}\u{1daa1}-\u{1daaf}\u{1e000}-\u{1e006}\u{1e008}-\u{1e018}\u{1e01b}-\u{1e021}\u{1e023}-\u{1e024}\u{1e026}-\u{1e02a}\u{1e100}-\u{1e12c}\u{1e130}-\u{1e13d}\u{1e140}-\u{1e149}\u{1e14e}\u{1e2c0}-\u{1e2f9}\u{1e800}-\u{1e8c4}\u{1e8d0}-\u{1e8d6}\u{1e900}-\u{1e94b}\u{1e950}-\u{1e959}\u{1ee00}-\u{1ee03}\u{1ee05}-\u{1ee1f}\u{1ee21}-\u{1ee22}\u{1ee24}\u{1ee27}\u{1ee29}-\u{1ee32}\u{1ee34}-\u{1ee37}\u{1ee39}\u{1ee3b}\u{1ee42}\u{1ee47}\u{1ee49}\u{1ee4b}\u{1ee4d}-\u{1ee4f}\u{1ee51}-\u{1ee52}\u{1ee54}\u{1ee57}\u{1ee59}\u{1ee5b}\u{1ee5d}\u{1ee5f}\u{1ee61}-\u{1ee62}\u{1ee64}\u{1ee67}-\u{1ee6a}\u{1ee6c}-\u{1ee72}\u{1ee74}-\u{1ee77}\u{1ee79}-\u{1ee7c}\u{1ee7e}\u{1ee80}-\u{1ee89}\u{1ee8b}-\u{1ee9b}\u{1eea1}-\u{1eea3}\u{1eea5}-\u{1eea9}\u{1eeab}-\u{1eebb}\u{1f130}-\u{1f149}\u{1f150}-\u{1f169}\u{1f170}-\u{1f189}\u{20000}-\u{2a6d6}\u{2a700}-\u{2b734}\u{2b740}-\u{2b81d}\u{2b820}-\u{2cea1}\u{2ceb0}-\u{2ebe0}\u{2f800}-\u{2fa1d}\u{e0100}-\u{e01ef}";

        let alpha = "A-Za-z\u{aa}\u{b5}\u{ba}\u{c0}-\u{d6}\u{d8}-\u{f6}\u{f8}-\u{2c1}\u{2c6}-\u{2d1}\u{2e0}-\u{2e4}\u{2ec}\u{2ee}\u{345}\u{370}-\u{374}\u{376}-\u{377}\u{37a}-\u{37d}\u{37f}\u{386}\u{388}-\u{38a}\u{38c}\u{38e}-\u{3a1}\u{3a3}-\u{3f5}\u{3f7}-\u{481}\u{48a}-\u{52f}\u{531}-\u{556}\u{559}\u{560}-\u{588}\u{5b0}-\u{5bd}\u{5bf}\u{5c1}-\u{5c2}\u{5c4}-\u{5c5}\u{5c7}\u{5d0}-\u{5ea}\u{5ef}-\u{5f2}\u{610}-\u{61a}\u{620}-\u{657}\u{659}-\u{65f}\u{66e}-\u{6d3}\u{6d5}-\u{6dc}\u{6e1}-\u{6e8}\u{6ed}-\u{6ef}\u{6fa}-\u{6fc}\u{6ff}\u{710}-\u{73f}\u{74d}-\u{7b1}\u{7ca}-\u{7ea}\u{7f4}-\u{7f5}\u{7fa}\u{800}-\u{817}\u{81a}-\u{82c}\u{840}-\u{858}\u{860}-\u{86a}\u{8a0}-\u{8b4}\u{8b6}-\u{8bd}\u{8d4}-\u{8df}\u{8e3}-\u{8e9}\u{8f0}-\u{93b}\u{93d}-\u{94c}\u{94e}-\u{950}\u{955}-\u{963}\u{971}-\u{983}\u{985}-\u{98c}\u{98f}-\u{990}\u{993}-\u{9a8}\u{9aa}-\u{9b0}\u{9b2}\u{9b6}-\u{9b9}\u{9bd}-\u{9c4}\u{9c7}-\u{9c8}\u{9cb}-\u{9cc}\u{9ce}\u{9d7}\u{9dc}-\u{9dd}\u{9df}-\u{9e3}\u{9f0}-\u{9f1}\u{9fc}\u{a01}-\u{a03}\u{a05}-\u{a0a}\u{a0f}-\u{a10}\u{a13}-\u{a28}\u{a2a}-\u{a30}\u{a32}-\u{a33}\u{a35}-\u{a36}\u{a38}-\u{a39}\u{a3e}-\u{a42}\u{a47}-\u{a48}\u{a4b}-\u{a4c}\u{a51}\u{a59}-\u{a5c}\u{a5e}\u{a70}-\u{a75}\u{a81}-\u{a83}\u{a85}-\u{a8d}\u{a8f}-\u{a91}\u{a93}-\u{aa8}\u{aaa}-\u{ab0}\u{ab2}-\u{ab3}\u{ab5}-\u{ab9}\u{abd}-\u{ac5}\u{ac7}-\u{ac9}\u{acb}-\u{acc}\u{ad0}\u{ae0}-\u{ae3}\u{af9}-\u{afc}\u{b01}-\u{b03}\u{b05}-\u{b0c}\u{b0f}-\u{b10}\u{b13}-\u{b28}\u{b2a}-\u{b30}\u{b32}-\u{b33}\u{b35}-\u{b39}\u{b3d}-\u{b44}\u{b47}-\u{b48}\u{b4b}-\u{b4c}\u{b56}-\u{b57}\u{b5c}-\u{b5d}\u{b5f}-\u{b63}\u{b71}\u{b82}-\u{b83}\u{b85}-\u{b8a}\u{b8e}-\u{b90}\u{b92}-\u{b95}\u{b99}-\u{b9a}\u{b9c}\u{b9e}-\u{b9f}\u{ba3}-\u{ba4}\u{ba8}-\u{baa}\u{bae}-\u{bb9}\u{bbe}-\u{bc2}\u{bc6}-\u{bc8}\u{bca}-\u{bcc}\u{bd0}\u{bd7}\u{c00}-\u{c03}\u{c05}-\u{c0c}\u{c0e}-\u{c10}\u{c12}-\u{c28}\u{c2a}-\u{c39}\u{c3d}-\u{c44}\u{c46}-\u{c48}\u{c4a}-\u{c4c}\u{c55}-\u{c56}\u{c58}-\u{c5a}\u{c60}-\u{c63}\u{c80}-\u{c83}\u{c85}-\u{c8c}\u{c8e}-\u{c90}\u{c92}-\u{ca8}\u{caa}-\u{cb3}\u{cb5}-\u{cb9}\u{cbd}-\u{cc4}\u{cc6}-\u{cc8}\u{cca}-\u{ccc}\u{cd5}-\u{cd6}\u{cde}\u{ce0}-\u{ce3}\u{cf1}-\u{cf2}\u{d00}-\u{d03}\u{d05}-\u{d0c}\u{d0e}-\u{d10}\u{d12}-\u{d3a}\u{d3d}-\u{d44}\u{d46}-\u{d48}\u{d4a}-\u{d4c}\u{d4e}\u{d54}-\u{d57}\u{d5f}-\u{d63}\u{d7a}-\u{d7f}\u{d82}-\u{d83}\u{d85}-\u{d96}\u{d9a}-\u{db1}\u{db3}-\u{dbb}\u{dbd}\u{dc0}-\u{dc6}\u{dcf}-\u{dd4}\u{dd6}\u{dd8}-\u{ddf}\u{df2}-\u{df3}\u{e01}-\u{e3a}\u{e40}-\u{e46}\u{e4d}\u{e81}-\u{e82}\u{e84}\u{e86}-\u{e8a}\u{e8c}-\u{ea3}\u{ea5}\u{ea7}-\u{eb9}\u{ebb}-\u{ebd}\u{ec0}-\u{ec4}\u{ec6}\u{ecd}\u{edc}-\u{edf}\u{f00}\u{f40}-\u{f47}\u{f49}-\u{f6c}\u{f71}-\u{f81}\u{f88}-\u{f97}\u{f99}-\u{fbc}\u{1000}-\u{1036}\u{1038}\u{103b}-\u{103f}\u{1050}-\u{108f}\u{109a}-\u{109d}\u{10a0}-\u{10c5}\u{10c7}\u{10cd}\u{10d0}-\u{10fa}\u{10fc}-\u{1248}\u{124a}-\u{124d}\u{1250}-\u{1256}\u{1258}\u{125a}-\u{125d}\u{1260}-\u{1288}\u{128a}-\u{128d}\u{1290}-\u{12b0}\u{12b2}-\u{12b5}\u{12b8}-\u{12be}\u{12c0}\u{12c2}-\u{12c5}\u{12c8}-\u{12d6}\u{12d8}-\u{1310}\u{1312}-\u{1315}\u{1318}-\u{135a}\u{1380}-\u{138f}\u{13a0}-\u{13f5}\u{13f8}-\u{13fd}\u{1401}-\u{166c}\u{166f}-\u{167f}\u{1681}-\u{169a}\u{16a0}-\u{16ea}\u{16ee}-\u{16f8}\u{1700}-\u{170c}\u{170e}-\u{1713}\u{1720}-\u{1733}\u{1740}-\u{1753}\u{1760}-\u{176c}\u{176e}-\u{1770}\u{1772}-\u{1773}\u{1780}-\u{17b3}\u{17b6}-\u{17c8}\u{17d7}\u{17dc}\u{1820}-\u{1878}\u{1880}-\u{18aa}\u{18b0}-\u{18f5}\u{1900}-\u{191e}\u{1920}-\u{192b}\u{1930}-\u{1938}\u{1950}-\u{196d}\u{1970}-\u{1974}\u{1980}-\u{19ab}\u{19b0}-\u{19c9}\u{1a00}-\u{1a1b}\u{1a20}-\u{1a5e}\u{1a61}-\u{1a74}\u{1aa7}\u{1b00}-\u{1b33}\u{1b35}-\u{1b43}\u{1b45}-\u{1b4b}\u{1b80}-\u{1ba9}\u{1bac}-\u{1baf}\u{1bba}-\u{1be5}\u{1be7}-\u{1bf1}\u{1c00}-\u{1c36}\u{1c4d}-\u{1c4f}\u{1c5a}-\u{1c7d}\u{1c80}-\u{1c88}\u{1c90}-\u{1cba}\u{1cbd}-\u{1cbf}\u{1ce9}-\u{1cec}\u{1cee}-\u{1cf3}\u{1cf5}-\u{1cf6}\u{1cfa}\u{1d00}-\u{1dbf}\u{1de7}-\u{1df4}\u{1e00}-\u{1f15}\u{1f18}-\u{1f1d}\u{1f20}-\u{1f45}\u{1f48}-\u{1f4d}\u{1f50}-\u{1f57}\u{1f59}\u{1f5b}\u{1f5d}\u{1f5f}-\u{1f7d}\u{1f80}-\u{1fb4}\u{1fb6}-\u{1fbc}\u{1fbe}\u{1fc2}-\u{1fc4}\u{1fc6}-\u{1fcc}\u{1fd0}-\u{1fd3}\u{1fd6}-\u{1fdb}\u{1fe0}-\u{1fec}\u{1ff2}-\u{1ff4}\u{1ff6}-\u{1ffc}\u{2071}\u{207f}\u{2090}-\u{209c}\u{2102}\u{2107}\u{210a}-\u{2113}\u{2115}\u{2119}-\u{211d}\u{2124}\u{2126}\u{2128}\u{212a}-\u{212d}\u{212f}-\u{2139}\u{213c}-\u{213f}\u{2145}-\u{2149}\u{214e}\u{2160}-\u{2188}\u{24b6}-\u{24e9}\u{2c00}-\u{2c2e}\u{2c30}-\u{2c5e}\u{2c60}-\u{2ce4}\u{2ceb}-\u{2cee}\u{2cf2}-\u{2cf3}\u{2d00}-\u{2d25}\u{2d27}\u{2d2d}\u{2d30}-\u{2d67}\u{2d6f}\u{2d80}-\u{2d96}\u{2da0}-\u{2da6}\u{2da8}-\u{2dae}\u{2db0}-\u{2db6}\u{2db8}-\u{2dbe}\u{2dc0}-\u{2dc6}\u{2dc8}-\u{2dce}\u{2dd0}-\u{2dd6}\u{2dd8}-\u{2dde}\u{2de0}-\u{2dff}\u{2e2f}\u{3005}-\u{3007}\u{3021}-\u{3029}\u{3031}-\u{3035}\u{3038}-\u{303c}\u{3041}-\u{3096}\u{309d}-\u{309f}\u{30a1}-\u{30fa}\u{30fc}-\u{30ff}\u{3105}-\u{312f}\u{3131}-\u{318e}\u{31a0}-\u{31ba}\u{31f0}-\u{31ff}\u{3400}-\u{4db5}\u{4e00}-\u{9fef}\u{a000}-\u{a48c}\u{a4d0}-\u{a4fd}\u{a500}-\u{a60c}\u{a610}-\u{a61f}\u{a62a}-\u{a62b}\u{a640}-\u{a66e}\u{a674}-\u{a67b}\u{a67f}-\u{a6ef}\u{a717}-\u{a71f}\u{a722}-\u{a788}\u{a78b}-\u{a7bf}\u{a7c2}-\u{a7c6}\u{a7f7}-\u{a805}\u{a807}-\u{a827}\u{a840}-\u{a873}\u{a880}-\u{a8c3}\u{a8c5}\u{a8f2}-\u{a8f7}\u{a8fb}\u{a8fd}-\u{a8ff}\u{a90a}-\u{a92a}\u{a930}-\u{a952}\u{a960}-\u{a97c}\u{a980}-\u{a9b2}\u{a9b4}-\u{a9bf}\u{a9cf}\u{a9e0}-\u{a9ef}\u{a9fa}-\u{a9fe}\u{aa00}-\u{aa36}\u{aa40}-\u{aa4d}\u{aa60}-\u{aa76}\u{aa7a}-\u{aabe}\u{aac0}\u{aac2}\u{aadb}-\u{aadd}\u{aae0}-\u{aaef}\u{aaf2}-\u{aaf5}\u{ab01}-\u{ab06}\u{ab09}-\u{ab0e}\u{ab11}-\u{ab16}\u{ab20}-\u{ab26}\u{ab28}-\u{ab2e}\u{ab30}-\u{ab5a}\u{ab5c}-\u{ab67}\u{ab70}-\u{abea}\u{ac00}-\u{d7a3}\u{d7b0}-\u{d7c6}\u{d7cb}-\u{d7fb}\u{f900}-\u{fa6d}\u{fa70}-\u{fad9}\u{fb00}-\u{fb06}\u{fb13}-\u{fb17}\u{fb1d}-\u{fb28}\u{fb2a}-\u{fb36}\u{fb38}-\u{fb3c}\u{fb3e}\u{fb40}-\u{fb41}\u{fb43}-\u{fb44}\u{fb46}-\u{fbb1}\u{fbd3}-\u{fd3d}\u{fd50}-\u{fd8f}\u{fd92}-\u{fdc7}\u{fdf0}-\u{fdfb}\u{fe70}-\u{fe74}\u{fe76}-\u{fefc}\u{ff21}-\u{ff3a}\u{ff41}-\u{ff5a}\u{ff66}-\u{ffbe}\u{ffc2}-\u{ffc7}\u{ffca}-\u{ffcf}\u{ffd2}-\u{ffd7}\u{ffda}-\u{ffdc}\u{10000}-\u{1000b}\u{1000d}-\u{10026}\u{10028}-\u{1003a}\u{1003c}-\u{1003d}\u{1003f}-\u{1004d}\u{10050}-\u{1005d}\u{10080}-\u{100fa}\u{10140}-\u{10174}\u{10280}-\u{1029c}\u{102a0}-\u{102d0}\u{10300}-\u{1031f}\u{1032d}-\u{1034a}\u{10350}-\u{1037a}\u{10380}-\u{1039d}\u{103a0}-\u{103c3}\u{103c8}-\u{103cf}\u{103d1}-\u{103d5}\u{10400}-\u{1049d}\u{104b0}-\u{104d3}\u{104d8}-\u{104fb}\u{10500}-\u{10527}\u{10530}-\u{10563}\u{10600}-\u{10736}\u{10740}-\u{10755}\u{10760}-\u{10767}\u{10800}-\u{10805}\u{10808}\u{1080a}-\u{10835}\u{10837}-\u{10838}\u{1083c}\u{1083f}-\u{10855}\u{10860}-\u{10876}\u{10880}-\u{1089e}\u{108e0}-\u{108f2}\u{108f4}-\u{108f5}\u{10900}-\u{10915}\u{10920}-\u{10939}\u{10980}-\u{109b7}\u{109be}-\u{109bf}\u{10a00}-\u{10a03}\u{10a05}-\u{10a06}\u{10a0c}-\u{10a13}\u{10a15}-\u{10a17}\u{10a19}-\u{10a35}\u{10a60}-\u{10a7c}\u{10a80}-\u{10a9c}\u{10ac0}-\u{10ac7}\u{10ac9}-\u{10ae4}\u{10b00}-\u{10b35}\u{10b40}-\u{10b55}\u{10b60}-\u{10b72}\u{10b80}-\u{10b91}\u{10c00}-\u{10c48}\u{10c80}-\u{10cb2}\u{10cc0}-\u{10cf2}\u{10d00}-\u{10d27}\u{10f00}-\u{10f1c}\u{10f27}\u{10f30}-\u{10f45}\u{10fe0}-\u{10ff6}\u{11000}-\u{11045}\u{11082}-\u{110b8}\u{110d0}-\u{110e8}\u{11100}-\u{11132}\u{11144}-\u{11146}\u{11150}-\u{11172}\u{11176}\u{11180}-\u{111bf}\u{111c1}-\u{111c4}\u{111da}\u{111dc}\u{11200}-\u{11211}\u{11213}-\u{11234}\u{11237}\u{1123e}\u{11280}-\u{11286}\u{11288}\u{1128a}-\u{1128d}\u{1128f}-\u{1129d}\u{1129f}-\u{112a8}\u{112b0}-\u{112e8}\u{11300}-\u{11303}\u{11305}-\u{1130c}\u{1130f}-\u{11310}\u{11313}-\u{11328}\u{1132a}-\u{11330}\u{11332}-\u{11333}\u{11335}-\u{11339}\u{1133d}-\u{11344}\u{11347}-\u{11348}\u{1134b}-\u{1134c}\u{11350}\u{11357}\u{1135d}-\u{11363}\u{11400}-\u{11441}\u{11443}-\u{11445}\u{11447}-\u{1144a}\u{1145f}\u{11480}-\u{114c1}\u{114c4}-\u{114c5}\u{114c7}\u{11580}-\u{115b5}\u{115b8}-\u{115be}\u{115d8}-\u{115dd}\u{11600}-\u{1163e}\u{11640}\u{11644}\u{11680}-\u{116b5}\u{116b8}\u{11700}-\u{1171a}\u{1171d}-\u{1172a}\u{11800}-\u{11838}\u{118a0}-\u{118df}\u{118ff}\u{119a0}-\u{119a7}\u{119aa}-\u{119d7}\u{119da}-\u{119df}\u{119e1}\u{119e3}-\u{119e4}\u{11a00}-\u{11a32}\u{11a35}-\u{11a3e}\u{11a50}-\u{11a97}\u{11a9d}\u{11ac0}-\u{11af8}\u{11c00}-\u{11c08}\u{11c0a}-\u{11c36}\u{11c38}-\u{11c3e}\u{11c40}\u{11c72}-\u{11c8f}\u{11c92}-\u{11ca7}\u{11ca9}-\u{11cb6}\u{11d00}-\u{11d06}\u{11d08}-\u{11d09}\u{11d0b}-\u{11d36}\u{11d3a}\u{11d3c}-\u{11d3d}\u{11d3f}-\u{11d41}\u{11d43}\u{11d46}-\u{11d47}\u{11d60}-\u{11d65}\u{11d67}-\u{11d68}\u{11d6a}-\u{11d8e}\u{11d90}-\u{11d91}\u{11d93}-\u{11d96}\u{11d98}\u{11ee0}-\u{11ef6}\u{12000}-\u{12399}\u{12400}-\u{1246e}\u{12480}-\u{12543}\u{13000}-\u{1342e}\u{14400}-\u{14646}\u{16800}-\u{16a38}\u{16a40}-\u{16a5e}\u{16ad0}-\u{16aed}\u{16b00}-\u{16b2f}\u{16b40}-\u{16b43}\u{16b63}-\u{16b77}\u{16b7d}-\u{16b8f}\u{16e40}-\u{16e7f}\u{16f00}-\u{16f4a}\u{16f4f}-\u{16f87}\u{16f8f}-\u{16f9f}\u{16fe0}-\u{16fe1}\u{16fe3}\u{17000}-\u{187f7}\u{18800}-\u{18af2}\u{1b000}-\u{1b11e}\u{1b150}-\u{1b152}\u{1b164}-\u{1b167}\u{1b170}-\u{1b2fb}\u{1bc00}-\u{1bc6a}\u{1bc70}-\u{1bc7c}\u{1bc80}-\u{1bc88}\u{1bc90}-\u{1bc99}\u{1bc9e}\u{1d400}-\u{1d454}\u{1d456}-\u{1d49c}\u{1d49e}-\u{1d49f}\u{1d4a2}\u{1d4a5}-\u{1d4a6}\u{1d4a9}-\u{1d4ac}\u{1d4ae}-\u{1d4b9}\u{1d4bb}\u{1d4bd}-\u{1d4c3}\u{1d4c5}-\u{1d505}\u{1d507}-\u{1d50a}\u{1d50d}-\u{1d514}\u{1d516}-\u{1d51c}\u{1d51e}-\u{1d539}\u{1d53b}-\u{1d53e}\u{1d540}-\u{1d544}\u{1d546}\u{1d54a}-\u{1d550}\u{1d552}-\u{1d6a5}\u{1d6a8}-\u{1d6c0}\u{1d6c2}-\u{1d6da}\u{1d6dc}-\u{1d6fa}\u{1d6fc}-\u{1d714}\u{1d716}-\u{1d734}\u{1d736}-\u{1d74e}\u{1d750}-\u{1d76e}\u{1d770}-\u{1d788}\u{1d78a}-\u{1d7a8}\u{1d7aa}-\u{1d7c2}\u{1d7c4}-\u{1d7cb}\u{1e000}-\u{1e006}\u{1e008}-\u{1e018}\u{1e01b}-\u{1e021}\u{1e023}-\u{1e024}\u{1e026}-\u{1e02a}\u{1e100}-\u{1e12c}\u{1e137}-\u{1e13d}\u{1e14e}\u{1e2c0}-\u{1e2eb}\u{1e800}-\u{1e8c4}\u{1e900}-\u{1e943}\u{1e947}\u{1e94b}\u{1ee00}-\u{1ee03}\u{1ee05}-\u{1ee1f}\u{1ee21}-\u{1ee22}\u{1ee24}\u{1ee27}\u{1ee29}-\u{1ee32}\u{1ee34}-\u{1ee37}\u{1ee39}\u{1ee3b}\u{1ee42}\u{1ee47}\u{1ee49}\u{1ee4b}\u{1ee4d}-\u{1ee4f}\u{1ee51}-\u{1ee52}\u{1ee54}\u{1ee57}\u{1ee59}\u{1ee5b}\u{1ee5d}\u{1ee5f}\u{1ee61}-\u{1ee62}\u{1ee64}\u{1ee67}-\u{1ee6a}\u{1ee6c}-\u{1ee72}\u{1ee74}-\u{1ee77}\u{1ee79}-\u{1ee7c}\u{1ee7e}\u{1ee80}-\u{1ee89}\u{1ee8b}-\u{1ee9b}\u{1eea1}-\u{1eea3}\u{1eea5}-\u{1eea9}\u{1eeab}-\u{1eebb}\u{1f130}-\u{1f149}\u{1f150}-\u{1f169}\u{1f170}-\u{1f189}\u{20000}-\u{2a6d6}\u{2a700}-\u{2b734}\u{2b740}-\u{2b81d}\u{2b820}-\u{2cea1}\u{2ceb0}-\u{2ebe0}\u{2f800}-\u{2fa1d}";

        let cyrillic = "\u{400}-\u{484}\u{487}-\u{52f}\u{1c80}-\u{1c88}\u{1d2b}\u{1d78}\u{2de0}-\u{2dff}\u{a640}-\u{a69f}\u{fe2e}-\u{fe2f}";

        let accented = "\u{C0}-\u{D6}\u{D8}-\u{F6}\u{F8}-\u{FF}\u{100}-\u{24F}\u{253}-\u{254}\u{256}-\u{257}\u{259}\u{25B}\u{263}\u{268}\u{26F}\u{272}\u{289}\u{28B}\u{2BB}\u{300}-\u{36F}\u{1E00}-\u{1EFF}";

        let pd = "\\-\u{58A}\u{5BE}\u{1400}\u{1806}\u{2010}-\u{2015}\u{2E17}\u{2E1A}\u{2E3A}\u{2E3B}\u{2E40}\u{2E5D}\u{301C}\u{3030}\u{30A0}\u{FE31}\u{FE32}\u{FE58}\u{FE63}\u{FF0D}\u{10EAD}";

        let directional = "\u{61C}\u{200E}\u{200F}\u{202A}\u{202B}\u{202C}\u{202D}\u{202E}\u{2066}\u{2067}\u{2068}\u{2069}";
        let ctrl = "\u{0}-\u{1F}\u{7F}";
        let space = "\u{9}-\u{D}\u{20}\u{85}\u{A0}\u{1680}\u{180E}\u{2000}-\u{200A}\u{2028}\u{2029}\u{202F}\u{205F}\u{3000}";

        let username = "(?i:[a-z0-9_]+([a-z0-9_.-]+[a-z0-9_]+)?)";

        let mention_bad_pre =
            Regex::new(&("[=/".to_owned() + word + "]$")).unwrap();
        let mention = Regex::new(
            &("(?i:@((".to_owned()
                + username
                + ")(?:@["
                + word
                + ".-]+["
                + word
                + "]+)?))"),
        )
        .unwrap();

        let hashtag_separators = "_\u{B7}\u{30FB}\u{200C}";
        let word_hash_sep = word.to_owned() + "#" + hashtag_separators;
        let alpha_hash_sep = alpha.to_owned() + "#" + hashtag_separators;

        let hashtag_bad_pre =
            Regex::new(&("[=/\\)".to_owned() + word + "]$")).unwrap();
        let hashtag = Regex::new(
            &("(?i:#([".to_owned()
                + word
                + "_]["
                + &word_hash_sep
                + "]*["
                + &alpha_hash_sep
                + "]["
                + &word_hash_sep
                + "]*["
                + word
                + "_]|(["
                + word
                + "_]*["
                + alpha
                + "]["
                + word
                + "_]*)))"),
        )
        .unwrap();

        let domain_invalid_middle_chars = directional.to_owned()
            + space
            + ctrl
            + "!\"#$%&\\'()*+,./:;<=>?@\\[\\]^\\`{|}~";
        let domain_invalid_end_chars =
            domain_invalid_middle_chars.to_owned() + "_-";
        let domain_component = "[^".to_owned()
            + &domain_invalid_end_chars
            + "](?:[^"
            + &domain_invalid_middle_chars
            + "]*"
            + "[^"
            + &domain_invalid_end_chars
            + "])?";

        // This is not quite the way the server does it, because the
        // server has a huge list of valid TLDs! I can't face that.
        // And I think it's only there so that it can match URLs
        // _without_ an http[s] prefix and avoid too many false
        // positives. So my compromise is to trust the user, when
        // composing a toot, to only enter URLs with sensible domains,
        // otherwise we'll mis-highlight them and get the character
        // counts wrong.
        let domain =
            domain_component.to_owned() + "(?:\\." + &domain_component + ")*";

        let path_end_chars =
            "a-z".to_owned() + cyrillic + accented + "0-9=_#/\\+\\-";
        let path_mid_chars = path_end_chars.to_owned()
            + pd
            + "!\\*\\';:\\,\\.\\$\\%\\[\\]~&\\|@";

        let path_bracketed_once =
            "\\([".to_owned() + &path_mid_chars + "]*\\)";
        let path_char_or_bracketed_once = "(?:[".to_owned()
            + &path_mid_chars
            + "]|"
            + &path_bracketed_once
            + ")";
        let path_bracketed =
            "\\(".to_owned() + &path_char_or_bracketed_once + "*\\)";

        let path = "(?:[".to_owned()
            + &path_mid_chars
            + "]|"
            + &path_bracketed
            + ")*"
            + "(?:["
            + &path_end_chars
            + "]|"
            + &path_bracketed
            + ")";

        let query_end_chars = "a-z0-9_&=#/\\-";
        let query_mid_chars = query_end_chars.to_owned()
            + "!?\\*\\'\\(\\);:\\+\\$%\\[\\]\\.,~|@";

        let url_bad_pre = Regex::new(
            &("[A-Z0-9@$#\u{FF20}\u{FF03}".to_owned() + directional + "]$"),
        )
        .unwrap();
        let url = Regex::new(
            &("(?i:".to_owned()
                + "https?://"
                + "(?:"
                + &domain
                + ")"
                + "(?::[0-9]+)?"
                + "(?:"
                + &path
                + ")*"
                + "(?:\\?["
                + &query_mid_chars
                + "]*["
                + query_end_chars
                + "])?"
                + ")"),
        )
        .unwrap();

        #[cfg(test)]
        let domain = Regex::new(&domain).unwrap();

        Self {
            mention: Findable {
                text: mention,
                bad_pre: Some(mention_bad_pre),
            },
            hashtag: Findable {
                text: hashtag,
                bad_pre: Some(hashtag_bad_pre),
            },
            url: Findable {
                text: url,
                bad_pre: Some(url_bad_pre),
            },
            #[cfg(test)]
            domain: Findable {
                text: domain,
                bad_pre: None,
            },
        }
    }

    pub fn get() -> &'static Self {
        &SCANNER
    }
}

#[test]
fn test_mention() {
    let scan = Scan::get();
    assert_eq!(scan.mention.get_span("hello @user", 0), Some((6, 11)));
    assert_eq!(
        scan.mention.get_span("hello @user@domain.foo", 0),
        Some((6, 22))
    );

    assert_eq!(scan.mention.get_span("hello a@user", 0), None);
    assert_eq!(scan.mention.get_span("hello =@user", 0), None);
    assert_eq!(scan.mention.get_span("hello /@user", 0), None);
    assert_eq!(scan.mention.get_span("hello )@user", 0), Some((7, 12)));

    assert_eq!(scan.mention.get_span("hello @user.name", 0), Some((6, 16)));
    assert_eq!(scan.mention.get_span("hello @user.name.", 0), Some((6, 16)));
    assert_eq!(scan.mention.get_span("hello @user-name", 0), Some((6, 16)));
    assert_eq!(scan.mention.get_span("hello @user-name-", 0), Some((6, 16)));
}

#[test]
fn test_hashtag() {
    let scan = Scan::get();
    assert_eq!(scan.hashtag.get_span("some #text here", 0), Some((5, 10)));
    assert_eq!(scan.hashtag.get_span("some # here", 0), None);
    assert_eq!(scan.hashtag.get_span("some #__a__ here", 0), Some((5, 11)));
    assert_eq!(scan.hashtag.get_span("some #_____ here", 0), Some((5, 11)));
    assert_eq!(scan.hashtag.get_span("some #_0_0_ here", 0), Some((5, 11)));

    assert_eq!(scan.hashtag.get_span("some a#text here", 0), None);
    assert_eq!(scan.hashtag.get_span("some )#text here", 0), None);
    assert_eq!(scan.hashtag.get_span("some (#text here", 0), Some((6, 11)));
}

#[test]
fn test_domain() {
    let scan = Scan::get();
    assert_eq!(scan.domain.get_span("foo.bar.baz", 0), Some((0, 11)));
    assert_eq!(scan.domain.get_span("foo.bar.baz.", 0), Some((0, 11)));
    assert_eq!(scan.domain.get_span("foo.b-r.baz", 0), Some((0, 11)));
    assert_eq!(scan.domain.get_span("foo.-br.baz", 0), Some((0, 3)));
    assert_eq!(scan.domain.get_span("foo.br-.baz", 0), Some((0, 6))); // matches foo.br
}

#[test]
fn test_url() {
    let scan = Scan::get();
    assert_eq!(
        scan.url.get_span("Look at https://example.com.", 0),
        Some((8, 27))
    );
    assert_eq!(
        scan.url.get_span(
            "Or https://en.wikipedia.org/wiki/Panda_(disambiguation).",
            0
        ),
        Some((3, 55))
    );
    assert_eq!(
        scan.url.get_span(
            "Or https://example.com/music/Track_(Thing_(Edited)).",
            0
        ),
        Some((3, 51))
    );
}
