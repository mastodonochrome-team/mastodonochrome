use super::client::{
    AccountDetails, AccountFlag, Boosts, Client, ClientError, Followness,
};
use super::coloured_string::ColouredString;
use super::editor::{EditableMenuLine, EditableMenuLineData};
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
};
use super::types::Visibility;

struct YourOptionsMenu {
    title: FileHeader,
    normal_status: FileStatusLineFinal,
    edit_status: FileStatusLineFinal,
    el_display_name: EditableMenuLine<String>, // N
    cl_default_vis: CyclingMenuLine<Visibility>,
    cl_default_sensitive: CyclingMenuLine<bool>,
    el_default_language: EditableMenuLine<Option<String>>,

    cl_locked: CyclingMenuLine<bool>,
    cl_bot: CyclingMenuLine<bool>,
    cl_discoverable: CyclingMenuLine<bool>,
    cl_hide_collections: CyclingMenuLine<bool>,
    cl_indexable: CyclingMenuLine<bool>,
    // fields (harder because potentially open-ended number of them)
    // note (bio) (harder because flip to an editor)
}

impl YourOptionsMenu {
    fn new(client: &mut Client) -> Result<Self, ClientError> {
        let ac = client.account_by_id(&client.our_account_id())?;
        let source = match ac.source {
            Some(source) => source,
            None => {
                return Err(ClientError::Consistency(
                    "server did not send 'source' details for our account"
                        .to_owned(),
                ))
            }
        };

        let title = FileHeader::new(ColouredString::general(
            "Your user options [ESC][Y][O]",
            "HHHHHHHHHHHHHHHHHHHKKKHHKHHKH",
        ));

        let normal_status = FileStatusLine::new()
            .add(Return, "Back", 10)
            .add(Space, "Submit Changes", 15)
            .add(Pr('?'), "Help", 101)
            .finalise();
        let edit_status = FileStatusLine::new()
            .message("Edit line and press Return")
            .finalise();

        let el_display_name = EditableMenuLine::new(
            Pr('N'),
            ColouredString::plain("Display name: "),
            ac.display_name.clone(),
        );
        let cl_default_vis = CyclingMenuLine::new(
            Pr('V'),
            ColouredString::plain("Default post visibility: "),
            &Visibility::long_descriptions(),
            source.privacy,
        );
        let el_default_language = EditableMenuLine::new(
            Pr('L'),
            ColouredString::plain("Default language: "),
            source.language.clone(),
        );
        let cl_default_sensitive = CyclingMenuLine::new(
            Pr('S'),
            ColouredString::plain("Posts marked sensitive by default: "),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            source.sensitive,
        );
        let cl_locked = CyclingMenuLine::new(
            Ctrl('K'),
            ColouredString::plain("Locked (you must approve followers): "),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            ac.locked,
        );
        let cl_hide_collections = CyclingMenuLine::new(
            Ctrl('F'),
            ColouredString::plain(
                "Hide your lists of followers and followed users: ",
            ),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            source.hide_collections == Some(true),
        );
        let cl_discoverable = CyclingMenuLine::new(
            Ctrl('D'),
            ColouredString::plain(
                "Discoverable (listed in profile directory): ",
            ),
            &[
                (false, ColouredString::uniform("no", 'r')),
                (true, ColouredString::uniform("yes", 'g')),
            ],
            source.discoverable == Some(true),
        );
        let cl_indexable = CyclingMenuLine::new(
            Ctrl('X'),
            ColouredString::plain(
                "Indexable (people can search for your posts): ",
            ),
            &[
                (false, ColouredString::uniform("no", 'r')),
                (true, ColouredString::uniform("yes", 'g')),
            ],
            source.indexable == Some(true),
        );
        let cl_bot = CyclingMenuLine::new(
            Ctrl('B'),
            ColouredString::plain("Bot (account identifies as automated): "),
            &[
                (false, ColouredString::uniform("no", 'g')),
                (true, ColouredString::uniform("yes", 'H')),
            ],
            ac.bot,
        );

        let mut menu = YourOptionsMenu {
            title,
            normal_status,
            edit_status,
            el_display_name,
            cl_default_vis,
            cl_default_sensitive,
            el_default_language,
            cl_locked,
            cl_bot,
            cl_discoverable,
            cl_hide_collections,
            cl_indexable,
        };
        menu.fix_widths();
        Ok(menu)
    }

    fn fix_widths(&mut self) -> (usize, usize) {
        let mut lmaxwid = 0;
        let mut rmaxwid = 0;
        self.el_display_name
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_default_vis.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_default_language
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_default_sensitive
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_locked.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_bot.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_discoverable
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_hide_collections
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_indexable.check_widths(&mut lmaxwid, &mut rmaxwid);

        self.el_display_name.reset_widths();
        self.cl_default_vis.reset_widths();
        self.el_default_language.reset_widths();
        self.cl_default_sensitive.reset_widths();
        self.cl_locked.reset_widths();
        self.cl_bot.reset_widths();
        self.cl_discoverable.reset_widths();
        self.cl_hide_collections.reset_widths();
        self.cl_indexable.reset_widths();

        self.el_display_name.ensure_widths(lmaxwid, rmaxwid);
        self.cl_default_vis.ensure_widths(lmaxwid, rmaxwid);
        self.el_default_language.ensure_widths(lmaxwid, rmaxwid);
        self.cl_default_sensitive.ensure_widths(lmaxwid, rmaxwid);
        self.cl_locked.ensure_widths(lmaxwid, rmaxwid);
        self.cl_bot.ensure_widths(lmaxwid, rmaxwid);
        self.cl_discoverable.ensure_widths(lmaxwid, rmaxwid);
        self.cl_hide_collections.ensure_widths(lmaxwid, rmaxwid);
        self.cl_indexable.ensure_widths(lmaxwid, rmaxwid);

        (lmaxwid, rmaxwid)
    }

    fn submit(&self, client: &mut Client) -> LogicalAction {
        let details = AccountDetails {
            display_name: self.el_display_name.get_data().clone(),
            default_visibility: self.cl_default_vis.get_value(),
            default_sensitive: self.cl_default_sensitive.get_value(),
            default_language: self.el_default_language.get_data().clone(),
            locked: self.cl_locked.get_value(),
            bot: self.cl_bot.get_value(),
            discoverable: self.cl_discoverable.get_value(),
            hide_collections: self.cl_hide_collections.get_value(),
            indexable: self.cl_indexable.get_value(),
        };

        match client.set_account_details(&client.our_account_id(), details) {
            Ok(..) => LogicalAction::Pop,
            Err(..) => LogicalAction::Beep, // FIXME: report the error!
        }
    }
}

impl ActivityState for YourOptionsMenu {
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let mut lines = Vec::new();
        let mut cursorpos = CursorPosition::End;
        lines.extend_from_slice(&self.title.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.push(self.el_display_name.render(
            w,
            &mut cursorpos,
            lines.len(),
        ));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_default_vis.render(w));
        lines.push(self.el_default_language.render(
            w,
            &mut cursorpos,
            lines.len(),
        ));
        lines.extend_from_slice(&self.cl_default_sensitive.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_locked.render(w));
        lines.extend_from_slice(&self.cl_hide_collections.render(w));
        lines.extend_from_slice(&self.cl_discoverable.render(w));
        lines.extend_from_slice(&self.cl_indexable.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_bot.render(w));

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        if self.el_display_name.is_editing()
            || self.el_default_language.is_editing()
        {
            lines.extend_from_slice(&self.edit_status.render(w));
        } else {
            lines.extend_from_slice(&self.normal_status.render(w));
        }

        (lines, cursorpos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        // Let editable menu lines have first crack at the keypress
        if self.el_display_name.handle_keypress(key)
            || self.el_default_language.handle_keypress(key)
        {
            self.fix_widths();
            return LogicalAction::Nothing;
        }

        match key {
            Pr('?') => LogicalAction::Help,

            Space => self.submit(client),
            Pr('q') | Pr('Q') | Return => LogicalAction::Pop,
            Pr('n') | Pr('N') => self.el_display_name.start_editing(),
            Pr('v') | Pr('V') => self.cl_default_vis.cycle(),
            Pr('l') | Pr('L') => self.el_default_language.start_editing(),
            Pr('s') | Pr('S') => self.cl_default_sensitive.cycle(),
            Ctrl('K') => self.cl_locked.cycle(),
            Ctrl('F') => self.cl_hide_collections.cycle(),
            Ctrl('D') => self.cl_discoverable.cycle(),
            Ctrl('X') => self.cl_indexable.cycle(),
            Ctrl('B') => self.cl_bot.cycle(),
            _ => LogicalAction::Nothing,
        }
    }

    fn resize(&mut self, w: usize, _h: usize) {
        self.el_display_name.resize(w);
        self.el_default_language.resize(w);
    }
}

struct LanguageVector(Vec<String>);
impl EditableMenuLineData for LanguageVector {
    fn display(&self) -> ColouredString {
        if self.0.is_empty() {
            ColouredString::uniform("any", '0')
        } else {
            // Make the comma separators appear in the 'boring' colour
            let mut s = ColouredString::plain("");
            let mut sep = ColouredString::plain("");
            for lang in &self.0 {
                s = s + &sep + ColouredString::plain(lang);
                sep = ColouredString::uniform(",", '0');
            }
            s
        }
    }

    fn to_text(&self) -> String {
        self.0.as_slice().join(",")
    }

    fn update(&mut self, text: &str) {
        *self = Self(
            text.split(|c| c == ' ' || c == ',')
                .filter(|s| !s.is_empty())
                .map(|s| s.to_owned())
                .collect(),
        )
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum FollowRequestResponse {
    Accept,
    Reject,
    Undecided,
}

struct OtherUserOptionsMenu {
    title: FileHeader,
    normal_status: FileStatusLineFinal,
    edit_status: FileStatusLineFinal,
    cl_follow: CyclingMenuLine<bool>,
    cl_boosts: CyclingMenuLine<Boosts>,
    el_languages: EditableMenuLine<LanguageVector>,
    cl_follow_request: Option<CyclingMenuLine<FollowRequestResponse>>,
    cl_block: CyclingMenuLine<bool>,
    cl_mute: CyclingMenuLine<bool>,
    id: String,
    prev_follow: Followness,
    prev_block: bool,
    prev_mute: bool,
}

impl OtherUserOptionsMenu {
    fn new(client: &mut Client, id: &str) -> Result<Self, ClientError> {
        let ac = client.account_by_id(id)?;
        let name = client.fq(&ac.acct);
        let rel = client.account_relationship_by_id(id)?;

        let title = FileHeader::new(ColouredString::uniform(
            &format!("Your options for user {name}"),
            'H',
        ));

        let normal_status = FileStatusLine::new()
            .add(Return, "Back", 10)
            .add(Space, "Submit Changes", 15)
            .add(Pr('?'), "Help", 101)
            .finalise();
        let edit_status = FileStatusLine::new()
            .message("Edit line and press Return")
            .finalise();

        let prev_follow = Followness::from_rel(&rel);

        // The 'follow this user' request should be renamed 'Request
        // to follow', if the account is locked and we're not already
        // following it. But in both other cases (if we aren't
        // following it but can do so unilaterally, or if we _are_ so
        // that we can unfollow unilaterally), call it just 'follow'.
        //
        // The 'request' wording should therefore also show up if
        // we've already requested to follow this user but it isn't
        // confirmed yet.
        let follow_wording = if ac.locked && !rel.following {
            "Request to follow this user: "
        } else {
            "Follow this user: "
        };
        let cl_follow = CyclingMenuLine::new(
            Pr('F'),
            ColouredString::plain(follow_wording),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'g')),
            ],
            prev_follow != Followness::NotFollowing,
        );

        let boosts = if rel.following {
            if rel.showing_reblogs {
                Boosts::Show
            } else {
                Boosts::Hide
            }
        } else {
            // Default, if we start off not following the user
            Boosts::Show
        };
        let cl_boosts = CyclingMenuLine::new(
            Pr('B'),
            ColouredString::plain("  Include their boosts: "),
            &[
                (Boosts::Hide, ColouredString::plain("no")),
                (Boosts::Show, ColouredString::uniform("yes", 'g')),
            ],
            boosts,
        );
        let el_languages = EditableMenuLine::new(
            Pr('L'),
            ColouredString::plain("  Include languages: "),
            LanguageVector(rel.languages.clone().unwrap_or_default()),
        );

        let cl_follow_request = if rel.requested_by {
            Some(CyclingMenuLine::new(
                Pr('R'),
                ColouredString::plain(
                    "Approve user's request to follow you: ",
                ),
                &[
                    (
                        FollowRequestResponse::Undecided,
                        ColouredString::plain("no action"),
                    ),
                    (
                        FollowRequestResponse::Accept,
                        ColouredString::uniform("accept", 'g'),
                    ),
                    (
                        FollowRequestResponse::Reject,
                        ColouredString::uniform("reject", 'r'),
                    ),
                ],
                FollowRequestResponse::Undecided,
            ))
        } else {
            None
        };

        let cl_block = CyclingMenuLine::new(
            Ctrl('B'),
            ColouredString::plain("Block this user: "),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            rel.blocking,
        );
        // Can't use the obvious ^M because it's also Return, of course!
        let cl_mute = CyclingMenuLine::new(
            Ctrl('U'),
            ColouredString::plain("Mute this user: "),
            &[
                (false, ColouredString::plain("no")),
                (true, ColouredString::uniform("yes", 'r')),
            ],
            rel.muting,
        );

        let mut menu = OtherUserOptionsMenu {
            title,
            normal_status,
            edit_status,
            cl_follow,
            cl_boosts,
            el_languages,
            cl_follow_request,
            cl_block,
            cl_mute,
            id: id.to_owned(),
            prev_follow,
            prev_block: rel.blocking,
            prev_mute: rel.muting,
        };
        menu.fix_widths();
        Ok(menu)
    }

    fn fix_widths(&mut self) -> (usize, usize) {
        let mut lmaxwid = 0;
        let mut rmaxwid = 0;
        self.cl_follow.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_boosts.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_languages.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_follow_request
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_block.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_mute.check_widths(&mut lmaxwid, &mut rmaxwid);

        self.cl_follow.reset_widths();
        self.cl_boosts.reset_widths();
        self.el_languages.reset_widths();
        self.cl_follow_request.reset_widths();
        self.cl_block.reset_widths();
        self.cl_mute.reset_widths();

        self.cl_follow.ensure_widths(lmaxwid, rmaxwid);
        self.cl_boosts.ensure_widths(lmaxwid, rmaxwid);
        self.el_languages.ensure_widths(lmaxwid, rmaxwid);
        self.cl_follow_request.ensure_widths(lmaxwid, rmaxwid);
        self.cl_block.ensure_widths(lmaxwid, rmaxwid);
        self.cl_mute.ensure_widths(lmaxwid, rmaxwid);

        (lmaxwid, rmaxwid)
    }

    fn submit(&self, client: &mut Client) -> LogicalAction {
        let new_follow = if self.cl_follow.get_value() {
            Followness::Following {
                boosts: self.cl_boosts.get_value(),
                languages: self.el_languages.get_data().0.clone(),
            }
        } else {
            Followness::NotFollowing
        };

        if new_follow != self.prev_follow {
            if client.set_following(&self.id, new_follow).is_err() {
                return LogicalAction::Beep; // FIXME: report the error!
            }
        }

        let new_block = self.cl_block.get_value();
        if new_block != self.prev_block {
            if client
                .set_account_flag(&self.id, AccountFlag::Block, new_block)
                .is_err()
            {
                return LogicalAction::Beep; // FIXME: report the error!
            }
        }

        let new_mute = self.cl_mute.get_value();
        if new_mute != self.prev_mute {
            if client
                .set_account_flag(&self.id, AccountFlag::Mute, new_mute)
                .is_err()
            {
                return LogicalAction::Beep; // FIXME: report the error!
            }
        }

        if let Some(ref cl) = self.cl_follow_request {
            let action = cl.get_value();
            let result = match action {
                FollowRequestResponse::Accept
                | FollowRequestResponse::Reject => client
                    .respond_to_follow_request(
                        &self.id,
                        action == FollowRequestResponse::Accept,
                    ),
                FollowRequestResponse::Undecided => Ok(()),
            };
            if result.is_err() {
                return LogicalAction::Beep; // FIXME: report the error!
            }
        }

        LogicalAction::Pop
    }
}

impl ActivityState for OtherUserOptionsMenu {
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let mut lines = Vec::new();
        let mut cursorpos = CursorPosition::End;
        lines.extend_from_slice(&self.title.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_follow.render(w));
        lines.extend_from_slice(&self.cl_boosts.render(w));
        lines.push(self.el_languages.render(w, &mut cursorpos, lines.len()));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.extend_from_slice(&self.cl_follow_request.render(w));
        lines.extend_from_slice(&self.cl_block.render(w));
        lines.extend_from_slice(&self.cl_mute.render(w));

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        if self.el_languages.is_editing() {
            lines.extend_from_slice(&self.edit_status.render(w));
        } else {
            lines.extend_from_slice(&self.normal_status.render(w));
        }

        (lines, cursorpos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        // Let editable menu lines have first crack at the keypress
        if self.el_languages.handle_keypress(key) {
            self.fix_widths();
            return LogicalAction::Nothing;
        }

        match key {
            Pr('?') => LogicalAction::Help,

            Space => self.submit(client),
            Pr('q') | Pr('Q') | Return => LogicalAction::Pop,
            Pr('f') | Pr('F') => self.cl_follow.cycle(),
            Pr('b') | Pr('B') => self.cl_boosts.cycle(),
            Pr('l') | Pr('L') => self.el_languages.start_editing(),
            Ctrl('B') => self.cl_block.cycle(),
            Ctrl('U') => self.cl_mute.cycle(),
            Pr('r') | Pr('R') => {
                if let Some(ref mut cl) = self.cl_follow_request {
                    cl.cycle()
                } else {
                    LogicalAction::Nothing
                }
            }
            _ => LogicalAction::Nothing,
        }
    }

    fn resize(&mut self, w: usize, _h: usize) {
        self.el_languages.resize(w);
    }
}

pub fn user_options_menu(
    client: &mut Client,
    id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    if id == client.our_account_id() {
        Ok(Box::new(YourOptionsMenu::new(client)?))
    } else {
        Ok(Box::new(OtherUserOptionsMenu::new(client, id)?))
    }
}
