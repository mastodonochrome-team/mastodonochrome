use std::cmp::{max, min};
use unicode_width::UnicodeWidthChar;

use super::activity_stack::{
    NonUtilityActivity, OverlayActivity, UtilityActivity,
};
use super::client::{Client, ClientError, ResolvedUrl};
use super::coloured_string::*;
use super::file::SearchDirection;
use super::posting::{Post, PostMetadata};
use super::scan_re::{Findable, Scan};
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
};
use super::types::InstanceStatusConfig;

#[derive(Debug, PartialEq, Eq, Clone)]
struct ColouredBufferRegion {
    start: usize,
    end: usize,
    colour: char,
}

trait TextBuffer {
    fn buffer(&self) -> &str;

    fn is_char_boundary(&self, pos: usize) -> bool {
        let text = self.buffer();
        if !text.is_char_boundary(pos) {
            false
        } else if pos == 0 {
            true // beginning of string
        } else {
            match text[pos..].chars().next() {
                None => true, // end of string

                // not just before a combining character
                Some(c) => {
                    c == '\n' || UnicodeWidthChar::width(c).unwrap_or(0) > 0
                }
            }
        }
    }

    fn is_word_sep(c: char) -> bool {
        c == ' '
    }

    fn is_word_boundary(&self, pos: usize) -> bool {
        if !self.is_char_boundary(pos) {
            false
        } else if let Some(prevpos) = self.prev_position(pos) {
            let mut chars = self.buffer()[prevpos..].chars();
            let prevchar = chars.next().expect("We're not at end of string");
            if let Some(thischar) = chars.next() {
                Self::is_word_sep(prevchar) && !Self::is_word_sep(thischar)
            } else {
                true // end of string
            }
        } else {
            true // start of string
        }
    }

    fn next_position(&self, pos: usize) -> Option<usize> {
        let len = self.buffer().len();
        if pos >= len {
            None
        } else {
            let mut pos = pos + 1;
            while !self.is_char_boundary(pos) {
                pos += 1;
            }
            Some(pos)
        }
    }

    fn prev_position(&self, pos: usize) -> Option<usize> {
        if pos == 0 {
            None
        } else {
            let mut pos = pos - 1;
            while !self.is_char_boundary(pos) {
                pos -= 1;
            }
            Some(pos)
        }
    }

    /// Decides how to highlight everything in the editor buffer. The
    /// returned `Vec<ColouredBufferRegion>` covers the entire buffer
    /// with regions indicating how to highlight each fragment of
    /// text.
    ///
    /// The returned `usize` counts the number of characters used by
    /// the text, for post length limiting purposes.
    fn make_coloured_regions_common(
        &self,
        scanners: &[(char, &Findable)],
        costfn: impl Fn(char, usize, &str) -> usize,
        max_characters: usize,
    ) -> (Vec<ColouredBufferRegion>, usize) {
        let mut regions: Vec<ColouredBufferRegion> = Vec::new();
        let mut pos = 0;
        let mut nchars = 0;
        let text = self.buffer();
        let full_len = text.len();
        // Newlines will be trimmed when posting a status, so we trim
        // them here too so as not to count them towards the character
        // limit. When we're done, we adjust the final region so that
        // it covers them.
        let text = text.trim_end_matches('\n');
        let trimmed_len = text.len();

        // Internal helper that we call whenever we decide what colour
        // a region of the buffer ought to be, _after_ checking the
        // character limit. Appends to 'regions', unless the region
        // can be merged with the previous one.
        let mut add_region_inner = |start: usize, end: usize, colour: char| {
            // Special case: if the function below generates a
            // zero-length region, don't bother to add it at all.
            if end == start {
                return;
            }

            // Check if there's a previous region with the same colour
            if let Some(ref mut prev) = regions.last_mut() {
                if prev.colour == colour {
                    assert_eq!(prev.end, start, "Regions should abut!");
                    prev.end = end;
                    return;
                }
            }

            // Otherwise, just push a new region
            regions.push(ColouredBufferRegion { start, end, colour });
        };

        // Internal helper that we call whenever we decide what colour
        // a region of the buffer ought to be, _irrespective_ of the
        // character limit. Inside here, we check the character limit
        // and may use it to adjust the colours we give to
        // add_region_inner.
        let mut add_region = |start: usize, end: usize, colour: char| {
            let region = &text[start..end];
            // Count the actual characters in the region.
            let region_chars = region.chars().count();
            // Determine the total cost of the current region.
            let cost = costfn(colour, region_chars, region);

            // Maybe recolour the region as 'out of bounds', or partly so.
            if nchars + cost <= max_characters {
                // Whole region is in bounds
                add_region_inner(start, end, colour);
            } else if nchars >= max_characters {
                // Whole region is out of bounds
                add_region_inner(start, end, '!');
            } else {
                // Break the region into an ok and a bad subsection
                let nbad_chars = nchars + cost - max_characters;
                let nok_chars = region_chars.saturating_sub(nbad_chars);
                let nok_bytes = {
                    let slice = &text[start..];
                    let mut char_iter = slice.char_indices();
                    for _ in 0..nok_chars {
                        char_iter.next();
                    }
                    let mut pos = char_iter
                        .next()
                        .map_or_else(|| slice.len(), |(i, _)| i);

                    // If we've landed on a Unicode char boundary but
                    // not on an _editor_ char boundary (i.e. between
                    // a printing character and a combining one), move
                    // the position backwards so that the printable
                    // character is marked as overlong.
                    while pos > start && !self.is_char_boundary(start + pos) {
                        pos -= 1;
                    }

                    pos
                };
                let midpoint = start + nok_bytes;
                add_region_inner(start, midpoint, colour);
                add_region_inner(midpoint, end, '!');
            }

            nchars += cost;
        };

        while pos < text.len() {
            // Try all three regex matchers and see which one picks up
            // something starting soonest (out of those that pick up
            // anything at all).
            let next_match = scanners
                .iter()
                .filter_map(|(colour, scanner)| {
                    scanner
                        .get_span(&text, pos)
                        .map(|(start, end)| (start, end, colour))
                })
                .min();

            match next_match {
                Some((start, end, colour)) => {
                    if pos < start {
                        add_region(pos, start, ' ');
                    }
                    if start < end {
                        add_region(start, end, *colour);
                    }
                    pos = end;
                }
                None => {
                    add_region(pos, text.len(), ' ');
                    break;
                }
            }
        }

        // Extend the final region to include trailing newlines, if
        // any. It doesn't really matter what colour we claim these
        // are highlighted: they don't give rise to actual characters
        // on the screen anyway.
        if let Some(region) = regions.last_mut() {
            region.end += full_len - trimmed_len;
        }

        (regions, nchars)
    }

    /// Wrapper on make_coloured_regions_common suitable for composing
    /// a post, counting URLs and mentions in interesting ways, and
    /// highlighting those and also hashtags.
    ///
    /// max_characters is specified separately from the
    /// InstanceStatusConfig, because the post's content warning might
    /// have consumed some of the character count already.
    fn make_coloured_regions_composer(
        &self,
        conf: &InstanceStatusConfig,
        max_characters: usize,
    ) -> (Vec<ColouredBufferRegion>, usize) {
        let scanner = Scan::get();
        let scanners = &[
            ('#', &scanner.hashtag),
            ('@', &scanner.mention),
            ('u', &scanner.url),
        ];

        let cost = |colour, nchars, text: &str| match colour {
            'u' => conf.characters_reserved_per_url,
            '@' => match text[1..].find('@') {
                // For "@foo@bar", just count the chars before the second @
                // (which lives at pos in text[1..], i.e. at pos+1 in text)
                Some(pos) => text[..pos + 1].chars().count(),
                // Otherwise everything counts
                None => nchars,
            },
            _ => nchars,
        };

        self.make_coloured_regions_common(scanners, cost, max_characters)
    }

    /// Wrapper on make_coloured_regions_common suitable for writing a
    /// post's content warning, doing no highlighting except 'too
    /// long', and counting the characters without special exceptions.
    fn make_coloured_regions_content_warning(
        &self,
        max_characters: usize,
    ) -> (Vec<ColouredBufferRegion>, usize) {
        self.make_coloured_regions_common(
            &[],
            |_colour, nchars, _text| nchars,
            max_characters,
        )
    }
}

/// Newtype on &str to allow counting characters as a free function.
struct RefTextBuffer<'a>(&'a str);
impl<'a> TextBuffer for RefTextBuffer<'a> {
    fn buffer(&self) -> &str {
        self.0
    }
}

pub fn count_characters_in_post(
    text: &str,
    conf: &InstanceStatusConfig,
) -> usize {
    // We can pass the character limit as any value we like, because
    // that only affects highlighting, and here we only care about the
    // returned char count.
    RefTextBuffer(text)
        .make_coloured_regions_composer(conf, 0)
        .1
}

pub fn count_characters_in_content_warning(text: &str) -> usize {
    // As above, character limit can be anything we like.
    RefTextBuffer(text)
        .make_coloured_regions_content_warning(0)
        .1
}

struct EditorCore {
    text: String,
    paste_buffer: String,
    point: usize,
    secret: bool,
}

impl TextBuffer for EditorCore {
    fn buffer(&self) -> &str {
        &self.text
    }
}

impl EditorCore {
    fn char_width_and_bytes(&self, pos: usize) -> Option<(usize, usize)> {
        match self.text[pos..].chars().next() {
            None => None,
            Some(c) => {
                let width = UnicodeWidthChar::width(c).unwrap_or(0);
                let width = if self.secret { min(width, 1) } else { width };
                let mut end = pos + 1;
                while !self.is_char_boundary(end) {
                    end += 1;
                }
                Some((width, end - pos))
            }
        }
    }

    fn char_at(&self, start: usize, end: usize) -> &str {
        if self.secret {
            "*"
        } else {
            &self.text[start..end]
        }
    }

    fn forward(&mut self) -> bool {
        match self.next_position(self.point) {
            Some(pos) => {
                self.point = pos;
                true
            }
            None => false,
        }
    }

    fn backward(&mut self) -> bool {
        match self.prev_position(self.point) {
            Some(pos) => {
                self.point = pos;
                true
            }
            None => false,
        }
    }

    fn delete_backward(&mut self) {
        let prev_point = self.point;
        if self.backward() {
            self.text =
                self.text[..self.point].to_owned() + &self.text[prev_point..];
        }
    }

    fn delete_forward(&mut self) {
        let prev_point = self.point;
        if self.forward() {
            self.text =
                self.text[..prev_point].to_owned() + &self.text[self.point..];
            self.point = prev_point;
        }
    }

    fn forward_word(&mut self) -> bool {
        let len = self.text.len();
        if self.point >= len {
            false
        } else {
            self.point += 1;
            while !self.is_word_boundary(self.point) {
                self.point += 1;
            }
            true
        }
    }

    fn backward_word(&mut self) -> bool {
        if self.point == 0 {
            false
        } else {
            self.point -= 1;
            while !self.is_word_boundary(self.point) {
                self.point -= 1;
            }
            true
        }
    }

    fn insert_after(&mut self, text: &str) {
        self.text = self.text[..self.point].to_owned()
            + text
            + &self.text[self.point..];
    }

    fn insert(&mut self, text: &str) {
        self.insert_after(text);
        self.point += text.len();
    }

    fn paste(&mut self) {
        self.text = self.text[..self.point].to_owned()
            + &self.paste_buffer
            + &self.text[self.point..];
        self.point += self.paste_buffer.len();
    }

    fn beginning_of_buffer(&mut self) {
        self.point = 0;
    }

    fn end_of_buffer(&mut self) {
        self.point = self.text.len();
    }

    fn delete(&mut self, start: usize, end: usize) -> String {
        assert!(self.is_char_boundary(start));
        assert!(self.is_char_boundary(end));
        assert!(end >= start);

        if end == start {
            return "".to_owned();
        }

        let new_point = if self.point <= start {
            self.point
        } else if self.point <= end {
            start
        } else {
            self.point + start - end
        };

        let deleted_text = self.text[start..end].to_owned();
        self.text = self.text[..start].to_owned() + &self.text[end..];
        self.point = new_point;
        deleted_text
    }

    fn cut(&mut self, start: usize, end: usize) {
        let deleted = self.delete(start, end);
        if !deleted.is_empty() {
            self.paste_buffer = deleted;
        }
    }

    fn handle_keypress(&mut self, key: OurKey) {
        match key {
            Left | Ctrl('B') => {
                self.backward();
            }
            Right | Ctrl('F') => {
                self.forward();
            }
            Backspace => {
                self.delete_backward();
            }
            Del | Ctrl('D') => {
                self.delete_forward();
            }
            Ctrl('W') => {
                self.backward_word();
            }
            Ctrl('T') => {
                self.forward_word();
            }
            Ctrl('Y') => {
                self.paste();
            }
            Pr(c) => {
                self.insert(&c.to_string());
            }
            Space => {
                self.insert(" ");
            }
            _ => (),
        }
    }
}

#[test]
fn test_forward_backward() {
    let mut ec = EditorCore {
        text: "héllo, wørld".to_owned(),
        point: 0,
        paste_buffer: "".to_owned(),
        secret: false,
    };

    assert!(ec.forward());
    assert_eq!(ec.point, 1);
    assert!(ec.forward());
    assert_eq!(ec.point, 3);
    assert!(ec.forward());
    assert_eq!(ec.point, 4);
    assert!(ec.backward());
    assert_eq!(ec.point, 3);
    assert!(ec.backward());
    assert_eq!(ec.point, 1);
    assert!(ec.backward());
    assert_eq!(ec.point, 0);
    assert!(!ec.backward());

    ec.point = ec.text.len() - 2;
    assert!(ec.forward());
    assert_eq!(ec.point, ec.text.len() - 1);
    assert!(ec.forward());
    assert_eq!(ec.point, ec.text.len());
    assert!(!ec.forward());
    assert_eq!(ec.point, ec.text.len());
    assert!(ec.backward());
    assert_eq!(ec.point, ec.text.len() - 1);
    assert!(ec.backward());
    assert_eq!(ec.point, ec.text.len() - 2);
    assert!(ec.backward());
    assert_eq!(ec.point, ec.text.len() - 3);
    assert!(ec.backward());
    assert_eq!(ec.point, ec.text.len() - 5);
    assert!(ec.backward());
}

#[test]
fn test_forward_backward_word() {
    let mut ec = EditorCore {
        text: "lorem ipsum dolor sit amet".to_owned(),
        point: 0,
        paste_buffer: "".to_owned(),
        secret: false,
    };

    assert!(ec.forward_word());
    assert_eq!(ec.point, 6); // ipsum
    assert!(ec.forward_word());
    assert_eq!(ec.point, 12); // dolor
    assert!(ec.forward_word());
    assert_eq!(ec.point, 18); // sit
    assert!(ec.forward_word());
    assert_eq!(ec.point, 22); // amet
    assert!(ec.forward_word());
    assert_eq!(ec.point, 26); // end of string
    assert!(!ec.forward_word());

    assert!(ec.backward_word());
    assert_eq!(ec.point, 22); // amet
    assert!(ec.backward_word());
    assert_eq!(ec.point, 18); // sit
    assert!(ec.backward_word());
    assert_eq!(ec.point, 12); // dolor
    assert!(ec.backward_word());
    assert_eq!(ec.point, 6); // ipsum
    assert!(ec.backward_word());
    assert_eq!(ec.point, 0); // lorem
    assert!(!ec.backward_word());
}

#[test]
fn test_delete() {
    let mut ec = EditorCore {
        text: "hélło".to_owned(),
        point: 3,
        paste_buffer: "".to_owned(),
        secret: false,
    };

    ec.delete_forward();
    assert_eq!(&ec.text, "héło");
    assert_eq!(ec.point, 3);
    ec.delete_forward();
    assert_eq!(&ec.text, "héo");
    assert_eq!(ec.point, 3);
    ec.delete_backward();
    assert_eq!(&ec.text, "ho");
    assert_eq!(ec.point, 1);
    ec.delete_backward();
    assert_eq!(&ec.text, "o");
    assert_eq!(ec.point, 0);
    ec.delete_backward();
    assert_eq!(&ec.text, "o");
    assert_eq!(ec.point, 0);
    ec.delete_forward();
    assert_eq!(&ec.text, "");
    assert_eq!(ec.point, 0);
    ec.delete_forward();
    assert_eq!(&ec.text, "");
    assert_eq!(ec.point, 0);
}

#[test]
fn test_insert() {
    let mut ec = EditorCore {
        text: "hélło".to_owned(),
        point: 3,
        paste_buffer: "".to_owned(),
        secret: false,
    };

    ec.insert("PÏNG");
    assert_eq!(&ec.text, "héPÏNGlło");
    assert_eq!(ec.point, 8);

    // We don't let you move _to_ a position before a combining char,
    // but you can insert one in a way that makes your previous position
    // no longer valid
    ec.text = "Beszel".to_string();
    ec.point = 4;
    ec.insert("\u{301}");
    assert_eq!(&ec.text, "Besźel");
    assert_eq!(ec.point, 6);

    // But then moving backwards one character goes back over the
    // combining char and the printing one before it
    assert!(ec.backward());
    assert_eq!(ec.point, 3);

    "PASTE".clone_into(&mut ec.paste_buffer);
    ec.paste();
    assert_eq!(&ec.text, "BesPASTEźel");
    assert_eq!(ec.point, 8);
}

pub struct SingleLineEditor {
    core: EditorCore,
    width: usize,
    first_visible: usize,
    prompt: ColouredString,
    promptwidth: usize,
    max_characters: Option<usize>,
}

impl SingleLineEditor {
    pub fn new(text: String) -> Self {
        Self::new_with_prompt(text, ColouredString::plain(""))
    }

    pub fn new_with_prompt(text: String, prompt: ColouredString) -> Self {
        let point = text.len();
        let promptwidth = prompt.width();
        SingleLineEditor {
            core: EditorCore {
                text,
                point,
                paste_buffer: "".to_owned(),
                secret: false,
            },
            width: 0,
            first_visible: 0,
            prompt,
            promptwidth,
            max_characters: None,
        }
    }

    pub fn set_prompt(&mut self, prompt: ColouredString) {
        self.prompt = prompt;
        self.promptwidth = self.prompt.width();
        self.update_first_visible();
    }

    pub fn set_max_characters(&mut self, max_characters: usize) {
        self.max_characters = Some(max_characters);
    }

    fn update_first_visible(&mut self) {
        if self.first_visible > self.core.point {
            self.first_visible = self.core.point;
        } else {
            let mut avail_width =
                self.width.saturating_sub(self.promptwidth + 1);
            let mut counted_initial_trunc_marker = false;
            if self.first_visible > 0 {
                counted_initial_trunc_marker = true;
                avail_width = avail_width.saturating_sub(1);
            }
            let mut head = self.first_visible;
            let mut tail = self.first_visible;
            let mut currwidth = 0;
            while head < self.core.point || currwidth > avail_width {
                if currwidth <= avail_width {
                    match self.core.char_width_and_bytes(head) {
                        None => break,
                        Some((w, b)) => {
                            head += b;
                            currwidth += w;
                        }
                    }
                } else {
                    match self.core.char_width_and_bytes(tail) {
                        None => panic!("tail should always be behind head"),
                        Some((w, b)) => {
                            tail += b;
                            currwidth -= w;
                            if !counted_initial_trunc_marker {
                                counted_initial_trunc_marker = true;
                                avail_width = avail_width.saturating_sub(1);
                            }
                        }
                    }
                }
            }
            self.first_visible = tail;
        }

        // Special case: if the < indicator hides a single character
        // at the start of the buffer which is the same width as it,
        // we can just reveal it, which is strictly better.
        if let Some((w, b)) = self.core.char_width_and_bytes(0) {
            if w == 1 && b == self.first_visible {
                self.first_visible = 0;
            }
        }
    }

    fn cut_to_end(&mut self) {
        self.core.cut(self.core.point, self.core.text.len());
    }

    pub fn handle_keypress(&mut self, key: OurKey) -> bool {
        match key {
            Ctrl('A') | Home => {
                self.core.beginning_of_buffer();
            }
            Ctrl('E') | End => {
                self.core.end_of_buffer();
            }
            Ctrl('K') => {
                self.cut_to_end();
            }
            Return => {
                return true;
            }
            _ => {
                self.core.handle_keypress(key);
            }
        }
        self.update_first_visible();
        false
    }

    pub fn borrow_text(&self) -> &str {
        &self.core.text
    }

    pub fn draw(&self, width: usize) -> (ColouredString, Option<usize>) {
        let mut s = self.prompt.clone();
        if self.first_visible > 0 {
            s.push_str(ColouredString::uniform("<", '>'));
        }
        let mut pos = self.first_visible;
        let mut cursor = None;

        // Decide what colour to display everything in the buffer,
        // depending on whether we're highlighting it for a length
        // limit or not.
        let regions = match self.max_characters {
            Some(n) => self.core.make_coloured_regions_content_warning(n).0,
            None => vec![ColouredBufferRegion {
                start: 0,
                end: self.core.text.len(),
                colour: ' ',
            }],
        };

        // Find the region in that list that corresponds to 'pos', in
        // the sense that r.start <= pos < r.end, so that the next
        // character after 'pos' is in region r.
        //
        // There may not be one, but that should only happen if pos is
        // at the end of the text, so that there _is_ no next
        // character after pos. In that situation we'll just set
        // curr_region to an out-of-range index (which is the same
        // thing that will happen in the loop below when we reach
        // EOB), and if that causes an array bounds check failure,
        // it's a bug that should be fixed.
        let mut curr_region = regions
            .binary_search_by(|r| {
                use std::cmp::Ordering::*;
                if r.start > pos {
                    Greater
                } else if r.end <= pos {
                    Less
                } else {
                    Equal
                }
            })
            .unwrap_or(regions.len());

        loop {
            let width_so_far = s.width();
            if pos == self.core.point {
                cursor = Some(width_so_far);
            }
            match self.core.char_width_and_bytes(pos) {
                None => break,
                Some((w, b)) => {
                    if width_so_far + w > width
                        || (width_so_far + w == width
                            && pos + b < self.core.text.len())
                    {
                        s.push_str(ColouredString::uniform(">", '>'));
                        break;
                    } else {
                        s.push_str(ColouredString::uniform(
                            &self.core.char_at(pos, pos + b),
                            regions[curr_region].colour,
                        ));
                        pos += b;
                        if pos >= regions[curr_region].end {
                            curr_region += 1;
                            if pos < self.core.text.len() {
                                assert_eq!(pos, regions[curr_region].start);
                            }
                        }
                    }
                }
            }
        }
        (s, cursor)
    }

    pub fn resize(&mut self, width: usize) {
        self.width = width;
        self.update_first_visible();
    }
}

#[test]
fn test_single_line_extra_ops() {
    let mut sle = SingleLineEditor {
        core: EditorCore {
            text: "hélło".to_owned(),
            point: 3,
            paste_buffer: "".to_owned(),
            secret: false,
        },
        width: 0,
        first_visible: 0,
        prompt: ColouredString::plain(""),
        promptwidth: 0,
        max_characters: None,
    };

    sle.cut_to_end();
    assert_eq!(&sle.core.text, "hé");
    assert_eq!(sle.core.point, 3);
    assert_eq!(sle.core.paste_buffer, "lło");

    sle.core.beginning_of_buffer();
    assert_eq!(sle.core.point, 0);

    sle.core.paste();
    assert_eq!(&sle.core.text, "lłohé");
    assert_eq!(sle.core.point, 4);

    sle.core.end_of_buffer();
    assert_eq!(sle.core.point, 7);
}

#[test]
fn test_single_line_visibility() {
    let mut sle = SingleLineEditor {
        core: EditorCore {
            text: "".to_owned(),
            point: 0,
            paste_buffer: "".to_owned(),
            secret: false,
        },
        width: 5,
        first_visible: 0,
        prompt: ColouredString::plain(""),
        promptwidth: 0,
        max_characters: None,
    };

    assert_eq!(sle.draw(sle.width), (ColouredString::plain(""), Some(0)));

    // Typing 'a' doesn't move first_visible away from the buffer start
    sle.core.insert("a");
    assert_eq!(sle.core.point, 1);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 0);
    assert_eq!(sle.draw(sle.width), (ColouredString::plain("a"), Some(1)));

    // Typing three more characters leaves the cursor in the last of
    // the 5 positions, so we're still good: we can print "abcd"
    // followed by an empty space containing the cursor.
    sle.core.insert("bcd");
    assert_eq!(sle.core.point, 4);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 0);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::plain("abcd"), Some(4))
    );

    // One more character and we overflow. Now we must print "<cde"
    // followed by the cursor.
    sle.core.insert("e");
    assert_eq!(sle.core.point, 5);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 2);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("<cde", ">   "), Some(4))
    );

    // And another two characters move that on in turn: "<efg" + cursor.
    sle.core.insert("fg");
    assert_eq!(sle.core.point, 7);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 4);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("<efg", ">   "), Some(4))
    );

    // Now start moving backwards. Three backwards movements leave the
    // cursor on the e, but nothing has changed.
    sle.core.backward();
    sle.core.backward();
    sle.core.backward();
    assert_eq!(sle.core.point, 4);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 4);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("<efg", ">   "), Some(1))
    );

    // Move backwards one more, so that we must scroll to get the d in view.
    sle.core.backward();
    assert_eq!(sle.core.point, 3);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 3);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("<defg", ">    "), Some(1))
    );

    // And on the _next_ backwards scroll, the end of the string also
    // becomes hidden.
    sle.core.backward();
    assert_eq!(sle.core.point, 2);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 2);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("<cde>", ">   >"), Some(1))
    );

    // The one after that would naively leave us at "<bcd>" with the
    // cursor on the b. But we can do better! In this case, the <
    // marker hides just one single-width character, so we can reveal
    // it and put first_visible right back to 0.
    sle.core.backward();
    assert_eq!(sle.core.point, 1);
    sle.update_first_visible();
    assert_eq!(sle.first_visible, 0);
    assert_eq!(
        sle.draw(sle.width),
        (ColouredString::general("abcd>", "    >"), Some(1))
    );
}

type BottomLineEditorOverlayResult =
    Box<dyn Fn(&str, &mut Client) -> LogicalAction>;

struct BottomLineEditorOverlay {
    ed: SingleLineEditor,
    result: BottomLineEditorOverlayResult,
}

impl BottomLineEditorOverlay {
    fn new(
        prompt: ColouredString,
        result: BottomLineEditorOverlayResult,
    ) -> Self {
        BottomLineEditorOverlay {
            ed: SingleLineEditor::new_with_prompt("".to_owned(), prompt),
            result,
        }
    }
}

impl ActivityState for BottomLineEditorOverlay {
    fn resize(&mut self, w: usize, _h: usize) {
        self.ed.resize(w);
    }

    fn draw(
        &self,
        w: usize,
        _h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let (text, cursorpos) = self.ed.draw(w);

        let cursorpos = match cursorpos {
            Some(x) => CursorPosition::At(x, 0),
            None => CursorPosition::None,
        };

        (vec![text], cursorpos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        if self.ed.handle_keypress(key) {
            (self.result)(&self.ed.core.text, client)
        } else {
            LogicalAction::Nothing
        }
    }
}

struct BottomLineErrorOverlay {
    line: CentredInfoLine,
}

impl BottomLineErrorOverlay {
    fn new(msg: &str) -> Self {
        let msg = ColouredString::uniform("Error: ", 'r')
            + ColouredString::plain(msg)
            + ColouredString::general("  [RET]", "   kkk ");
        BottomLineErrorOverlay {
            line: CentredInfoLine::new(msg),
        }
    }
}

impl ActivityState for BottomLineErrorOverlay {
    fn draw(
        &self,
        w: usize,
        _h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        (self.line.render(w), CursorPosition::End)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        _client: &mut Client,
    ) -> LogicalAction {
        match key {
            Return => LogicalAction::PopOverlaySilent,
            _ => LogicalAction::Nothing,
        }
    }
}

pub trait EditableMenuLineData {
    // If SECRET, then the implementor of this trait promises that
    // display() will show the text as ***** when it's not being
    // edited, and instructs the SingleLineEditor to do the same
    // during editing.
    //
    // display() can use the helper function count_edit_chars to help
    // it figure out how many * to display.
    const SECRET: bool = false;

    fn display(&self) -> ColouredString;
    fn to_text(&self) -> String;
    fn update(&mut self, text: &str);

    // If this is Some, then the implementor of this trait promises
    // that display() will show the text highlighted if it goes over
    // the character limit, and instructs the SingleLineEditor to do
    // the same during editing. It will be read at the start of each
    // editing session.
    fn max_characters(&self) -> Option<usize> {
        None
    }
}

impl EditableMenuLineData for String {
    fn display(&self) -> ColouredString {
        ColouredString::plain(self)
    }
    fn to_text(&self) -> String {
        self.clone()
    }
    fn update(&mut self, text: &str) {
        text.clone_into(self);
    }
}

impl EditableMenuLineData for Option<String> {
    fn display(&self) -> ColouredString {
        match self {
            None => ColouredString::uniform("none", '0'),
            Some(ref text) => ColouredString::plain(text),
        }
    }

    fn to_text(&self) -> String {
        match self {
            None => "".to_owned(),
            Some(ref text) => text.clone(),
        }
    }

    fn update(&mut self, text: &str) {
        match text {
            "" => *self = None,
            text => *self = Some(text.to_owned()),
        }
    }
}

/// EditableMenuLine data parameter type suitable for storing the
/// content warning for a Mastodon post. It stores an optional String
/// (there might not be a warning at all), plus a length limit that
/// will be used to highlight the string if the user makes it too
/// long.
///
/// It would be nice to do this in two independently reusable steps,
/// making a `StringWithLengthLimit` and separately arranging that if
/// `T` impls `EditableMenuLineData` then so does `Option<T>`. But it
/// can't be done, because the length limit has to be stored outside
/// the Option so that it can be remembered if the Option becomes None
/// and then Some again.
#[derive(Debug, Clone)]
pub struct ContentWarning {
    text: Option<String>,
    max_characters: usize,
}

impl ContentWarning {
    pub fn new(text: Option<String>, max_characters: usize) -> Self {
        ContentWarning {
            text,
            max_characters,
        }
    }

    pub fn get_text(&self) -> &Option<String> {
        &self.text
    }
}

// StringWithLengthLimit implements TextBuffer so that it can inherit
// make_coloured_regions
impl TextBuffer for ContentWarning {
    fn buffer(&self) -> &str {
        match &self.text {
            Some(text) => text,
            // We don't expect that this will need to be called when
            // text is None, but it's easy enough to provide a
            // sensible default anyway.
            None => "",
        }
    }
}

impl EditableMenuLineData for ContentWarning {
    fn display(&self) -> ColouredString {
        match self.text {
            None => ColouredString::uniform("none", '0'),
            Some(ref text) => {
                let mut cs = ColouredString::plain("");

                for region in self
                    .make_coloured_regions_content_warning(self.max_characters)
                    .0
                {
                    cs.push_str(ColouredString::uniform(
                        &text[region.start..region.end],
                        region.colour,
                    ));
                }

                cs
            }
        }
    }
    fn to_text(&self) -> String {
        match self.text {
            None => "".to_owned(),
            Some(ref text) => text.clone(),
        }
    }
    fn update(&mut self, text: &str) {
        match text {
            "" => self.text = None,
            text => self.text = Some(text.to_owned()),
        }
    }
    fn max_characters(&self) -> Option<usize> {
        Some(self.max_characters)
    }
}

pub struct EditableMenuLine<Data: EditableMenuLineData> {
    key: OurKey,
    description: ColouredString,
    data: Data,
    menuline: MenuKeypressLine,
    prompt: MenuKeypressLine,
    editor: Option<SingleLineEditor>,
    last_width: Option<usize>,
}

pub fn count_edit_chars(text: &str) -> usize {
    text.chars()
        .filter(|c| UnicodeWidthChar::width(*c).unwrap_or(0) > 0)
        .count()
}

impl<Data: EditableMenuLineData> EditableMenuLine<Data> {
    pub fn new(key: OurKey, description: ColouredString, data: Data) -> Self {
        let menuline = Self::make_menuline(key, &description, &data);
        let prompt = Self::make_prompt(key, &description);

        EditableMenuLine {
            key,
            description,
            data,
            menuline,
            prompt,
            editor: None,
            last_width: None,
        }
    }

    fn make_menuline(
        key: OurKey,
        description: &ColouredString,
        data: &Data,
    ) -> MenuKeypressLine {
        let desc = description + data.display();
        MenuKeypressLine::new(key, desc)
    }

    fn make_prompt(
        key: OurKey,
        description: &ColouredString,
    ) -> MenuKeypressLine {
        MenuKeypressLine::new(key, description.to_owned())
    }

    pub fn render(
        &self,
        width: usize,
        cursorpos: &mut CursorPosition,
        cy: usize,
    ) -> ColouredString {
        if let Some(ref editor) = self.editor {
            let (text, cx) = editor.draw(width);
            if let Some(cx) = cx {
                *cursorpos = CursorPosition::At(cx, cy);
            }
            text
        } else {
            self.menuline
                .render_oneline(width, None, &DefaultDisplayStyle)
        }
    }

    // Returns a LogicalAction just to make it more convenient to put
    // in matches on keypresses
    pub fn start_editing(&mut self) -> LogicalAction {
        let mut editor = SingleLineEditor::new(self.data.to_text());
        editor.core.secret = Data::SECRET;
        if let Some(n) = self.data.max_characters() {
            editor.set_max_characters(n);
        }
        self.editor = Some(editor);
        self.refresh_editor_prompt();
        LogicalAction::Nothing
    }

    pub fn resize(&mut self, w: usize) {
        self.last_width = Some(w);
        self.refresh_editor_prompt();
    }

    pub fn refresh_editor_prompt(&mut self) {
        if let Some(ref mut editor) = self.editor {
            if let Some(w) = self.last_width {
                let prompt =
                    self.prompt.render_oneline(w, None, &DefaultDisplayStyle);
                editor.resize(w);
                editor.set_prompt(prompt);
            }
        }
    }

    pub fn handle_keypress(&mut self, key: OurKey) -> bool {
        let (consumed, done) = if let Some(ref mut editor) = self.editor {
            if editor.handle_keypress(key) {
                self.data.update(editor.borrow_text());
                self.menuline = Self::make_menuline(
                    self.key,
                    &self.description,
                    &self.data,
                );
                (true, true)
            } else {
                (true, false)
            }
        } else {
            (false, false)
        };

        if done {
            self.editor = None;
        }

        consumed
    }

    pub fn get_data(&self) -> &Data {
        &self.data
    }
    pub fn is_editing(&self) -> bool {
        self.editor.is_some()
    }
    pub fn set_text(&mut self, text: &str) {
        self.data.update(text);
        self.menuline =
            Self::make_menuline(self.key, &self.description, &self.data);
    }
}

impl<Data: EditableMenuLineData> MenuKeypressLineGeneral
    for EditableMenuLine<Data>
{
    fn check_widths(&self, lmaxwid: &mut usize, rmaxwid: &mut usize) {
        self.menuline.check_widths(lmaxwid, rmaxwid);
        self.prompt.check_widths(lmaxwid, rmaxwid);
    }
    fn reset_widths(&mut self) {
        self.menuline.reset_widths();
        self.prompt.reset_widths();
    }
    fn ensure_widths(&mut self, lmaxwid: usize, rmaxwid: usize) {
        self.menuline.ensure_widths(lmaxwid, rmaxwid);
        self.prompt.ensure_widths(lmaxwid, rmaxwid);
    }
}

pub fn get_user_to_examine() -> Box<dyn ActivityState> {
    Box::new(BottomLineEditorOverlay::new(
        ColouredString::plain("Examine User: "),
        Box::new(move |s, client| {
            let s = s.trim();
            let s = s.strip_prefix('@').unwrap_or(s);
            if s.is_empty() {
                LogicalAction::PopOverlaySilent
            } else {
                let account_result = if s.contains("://") {
                    // If the user has entered a URL rather than a
                    // numeric id, try to resolve it.
                    if let Ok(Some(ResolvedUrl::Account(id))) =
                        client.resolve_mastodon_url(s)
                    {
                        client.account_by_id(&id)
                    } else {
                        client.account_by_name(s)
                    }
                } else {
                    client.account_by_name(s)
                };

                match account_result {
                    Ok(account) => LogicalAction::Goto(
                        UtilityActivity::ExamineUser(account.id).into(),
                    ),

                    Err(ClientError::UrlFetchHTTPRich(
                        _,
                        reqwest::StatusCode::NOT_FOUND,
                        _,
                    )) => LogicalAction::Goto(
                        OverlayActivity::BottomLineError(format!(
                            "User '{}' not found",
                            s
                        ))
                        .into(),
                    ),

                    Err(err) => LogicalAction::Error(err),
                }
            }
        }),
    ))
}

pub fn get_post_id_to_read() -> Box<dyn ActivityState> {
    Box::new(BottomLineEditorOverlay::new(
        ColouredString::plain("View post with id: "),
        Box::new(move |s, client| {
            let s = s.trim();
            if s.is_empty() {
                LogicalAction::PopOverlaySilent
            } else {
                let resolved_id;
                let s = if s.contains("://") {
                    // If the user has entered a URL rather than a
                    // numeric id, try to resolve it.
                    if let Ok(Some(ResolvedUrl::Status(id))) =
                        client.resolve_mastodon_url(s)
                    {
                        resolved_id = id.clone();
                        &resolved_id
                    } else {
                        s
                    }
                } else {
                    s
                };

                match client.status_by_id(&s) {
                    Ok(st) => LogicalAction::Goto(
                        UtilityActivity::InfoStatus(st.id).into(),
                    ),

                    Err(ClientError::UrlFetchHTTPRich(
                        _,
                        reqwest::StatusCode::NOT_FOUND,
                        _,
                    )) => LogicalAction::Goto(
                        OverlayActivity::BottomLineError(format!(
                            "Post id {} not found",
                            s
                        ))
                        .into(),
                    ),

                    Err(err) => LogicalAction::Error(err),
                }
            }
        }),
    ))
}

pub fn get_hashtag_to_read() -> Box<dyn ActivityState> {
    Box::new(BottomLineEditorOverlay::new(
        ColouredString::plain("View feed for hashtag: "),
        Box::new(move |s, _client| {
            let s = s.trim();
            let s = s.strip_prefix('#').unwrap_or(s);
            if s.is_empty() {
                LogicalAction::PopOverlaySilent
            } else {
                LogicalAction::Goto(
                    NonUtilityActivity::HashtagTimeline(s.to_owned()).into(),
                )
            }
        }),
    ))
}

pub fn get_search_expression(dir: SearchDirection) -> Box<dyn ActivityState> {
    let title = match dir {
        SearchDirection::Up => "Search back (blank = last): ",
        SearchDirection::Down => "Search (blank = last): ",
    };
    Box::new(BottomLineEditorOverlay::new(
        ColouredString::plain(title),
        Box::new(move |s, _client| {
            LogicalAction::GotSearchExpression(dir, s.to_owned())
        }),
    ))
}

pub fn bottom_line_error(msg: &str) -> Box<dyn ActivityState> {
    Box::new(BottomLineErrorOverlay::new(msg))
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct ComposeLayoutCell {
    pos: usize,
    x: usize,
    y: usize,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum ComposerKeyState {
    Start,
    CtrlO,
}

struct Composer {
    core: EditorCore,
    regions: Vec<ColouredBufferRegion>,
    layout: Vec<ComposeLayoutCell>,
    cursor_pos: Option<(usize, usize)>,
    ytop: usize,
    conf: InstanceStatusConfig,
    max_characters: usize, // conf.max_characters minus space consumed by CW
    last_size: Option<(usize, usize)>,
    page_len: usize,
    keystate: ComposerKeyState,
    goal_column: Option<usize>,
    header: FileHeader,
    irt: Option<InReplyToLine>,
    headersep: EditorHeaderSeparator,
    post_metadata: PostMetadata,
}

impl Composer {
    fn new(
        conf: InstanceStatusConfig,
        header: FileHeader,
        irt: Option<InReplyToLine>,
        post: Post,
    ) -> Self {
        let point = post.text.len();
        let max_characters = conf.max_characters.saturating_sub(
            post.m
                .content_warning
                .as_deref()
                .map_or(0, count_characters_in_content_warning),
        );
        Composer {
            core: EditorCore {
                text: post.text,
                point,
                paste_buffer: "".to_owned(),
                secret: false,
            },
            regions: Vec::new(),
            layout: Vec::new(),
            cursor_pos: None,
            ytop: 0,
            conf,
            max_characters,
            last_size: None,
            page_len: 0,
            keystate: ComposerKeyState::Start,
            goal_column: None,
            header,
            irt,
            headersep: EditorHeaderSeparator::new(),
            post_metadata: post.m,
        }
    }

    #[cfg(test)]
    fn test_new(conf: InstanceStatusConfig, text: &str) -> Self {
        Self::new(
            conf,
            FileHeader::new(ColouredString::plain("dummy")),
            None,
            Post::with_text(text),
        )
    }

    fn is_line_boundary(c: char) -> bool {
        // FIXME: for supporting multi-toot threads, perhaps introduce
        // a second thing that functions as a toot-break, say \0, and
        // regard it as a line boundary too for wrapping purposes
        c == '\n'
    }

    fn make_regions(&self) -> Vec<ColouredBufferRegion> {
        self.core
            .make_coloured_regions_composer(&self.conf, self.max_characters)
            .0
    }

    fn layout(&self, width: usize) -> Vec<ComposeLayoutCell> {
        // Leave the last column blank, if possible. Partly because of
        // Monochrome tradition, but also, that's a convenient place
        // to put _just the cursor_ when it's on the newline at the
        // end of an (n-1)-char line that hasn't wrapped yet.
        let width = width.saturating_sub(1);

        let mut cells = Vec::new();
        let mut pos = 0;
        let mut x = 0;
        let mut y = 0;
        let mut soft_wrap_pos = None;
        let mut hard_wrap_pos = None;

        loop {
            cells.push(ComposeLayoutCell { pos, x, y });
            match self.core.char_width_and_bytes(pos) {
                None => break, // we're done
                Some((w, b)) => {
                    let mut chars_iter = self.core.text[pos..].chars();
                    let c = chars_iter.next().expect(
                        "we just found out we're not at end of string",
                    );
                    if Self::is_line_boundary(c) {
                        // End of paragraph.
                        y += 1;
                        x = 0;
                        soft_wrap_pos = None;
                        hard_wrap_pos = None;
                        pos += b;
                    } else {
                        x += w;
                        if x <= width {
                            pos += b;
                            hard_wrap_pos = Some((pos, cells.len()));
                            if c == ' ' && chars_iter.next() != Some(' ') {
                                soft_wrap_pos = hard_wrap_pos;
                            }
                        } else {
                            let (wrap_pos, wrap_cell) = match soft_wrap_pos {
                                Some(p) => p,
                                None => hard_wrap_pos.expect(
                                    "We can't break the line _anywhere_?!",
                                ),
                            };

                            // Now rewind to the place we just broke
                            // the line, and keep going
                            pos = wrap_pos;
                            cells.truncate(wrap_cell);
                            y += 1;
                            x = 0;
                            soft_wrap_pos = None;
                            hard_wrap_pos = None;
                        }
                    }
                }
            }
        }

        cells
    }

    fn get_coloured_line(&self, y: usize) -> Option<ColouredString> {
        // Use self.layout to find the bounds of this line within the
        // buffer text.
        let start_cell_index = self.layout.partition_point(|cell| cell.y < y);
        if start_cell_index == self.layout.len() {
            return None; // y is after the end of the buffer
        }
        let start_pos = self.layout[start_cell_index].pos;

        let end_cell_index = self.layout.partition_point(|cell| cell.y <= y);
        let end_pos = if end_cell_index == self.layout.len() {
            self.core.text.len()
        } else {
            self.layout[end_cell_index].pos
        };

        // Trim the line-end character from the bounds if there is
        // one: we don't want to _draw_ it, under any circumstances.
        let last_char = if end_cell_index > start_cell_index {
            let last_cell = &self.layout[end_cell_index - 1];
            self.core.text[last_cell.pos..].chars().next()
        } else {
            None
        };
        let end_pos = if last_char.map_or(false, Self::is_line_boundary) {
            end_pos - 1
        } else {
            end_pos
        };

        // Now look up in self.regions to decide what colour to make
        // everything.
        let start_region_index = self
            .regions
            .partition_point(|region| region.end <= start_pos);

        let mut cs = ColouredString::plain("");

        for region in self.regions[start_region_index..].iter() {
            if end_pos <= region.start {
                break; // finished this line
            }
            let start = max(start_pos, region.start);
            let end = min(end_pos, region.end);
            cs.push_str(ColouredString::uniform(
                &self.core.text[start..end],
                region.colour,
            ));
        }

        Some(cs)
    }

    fn determine_cursor_pos(&self) -> Option<(usize, usize)> {
        let cursor_cell_index = self
            .layout
            .partition_point(|cell| cell.pos < self.core.point);
        if let Some(cell) = self.layout.get(cursor_cell_index) {
            if cell.pos == self.core.point {
                return Some((cell.x, cell.y));
            }
        }
        None // _probably_ shouldn't happen unless something is confused
    }

    fn post_update(&mut self) {
        self.regions = self.make_regions();
        if let Some((w, _h)) = self.last_size {
            self.layout = self.layout(w);
        }
        self.cursor_pos = self.determine_cursor_pos();

        if let Some((_cx, cy)) = self.cursor_pos {
            // Scroll to keep the cursor in view
            self.ytop = min(self.ytop, cy);
            self.ytop = max(self.ytop, (cy + 1).saturating_sub(self.page_len));
        }
    }

    fn goto_xy(&mut self, x: usize, y: usize) -> bool {
        // If we can't hit the desired x,y position exactly because
        // it's in the middle of a wide character, I think it's more
        // desirable to land on the wide character than to its right.
        //
        // This means we can't just search for the first layout cell
        // that is >= (x,y), because that _is_ the one to its right.
        // Instead we must search for the last one that is <= (x,y).

        let cell_index_after = self
            .layout
            .partition_point(|cell| (cell.y, cell.x) <= (y, x));
        let cell_index = cell_index_after
            .checked_sub(1)
            .expect("Cell 0 should be at (0,0) and always count as <= (y,x)");
        if let Some(cell) = self.layout.get(cell_index) {
            self.core.point = cell.pos;
            // Report failure if we didn't even get the right _line_
            cell.y == y
        } else {
            // overshoot to end of text
            self.core.point = self.core.text.len();
            false
        }
    }

    fn next_line(&mut self) {
        let (x, y) = self.cursor_pos.expect("post_update should have run");
        let x = self.goal_column.unwrap_or(x);
        if !self.goto_xy(x, y + 1) {
            self.goal_column = None;
        }
    }

    fn prev_line(&mut self) {
        let (x, y) = self.cursor_pos.expect("post_update should have run");
        let x = self.goal_column.unwrap_or(x);
        if let Some(newy) = y.checked_sub(1) {
            self.goto_xy(x, newy);
        } else {
            self.core.beginning_of_buffer();
            self.goal_column = None;
        }
    }

    fn page_down(&mut self) {
        let (_x, y) = self.cursor_pos.expect("post_update should have run");
        self.goto_xy(0, y + self.page_len);
    }

    fn page_up(&mut self) {
        let (_x, y) = self.cursor_pos.expect("post_update should have run");
        if let Some(newy) = y.checked_sub(self.page_len) {
            self.goto_xy(0, newy);
        } else {
            self.core.beginning_of_buffer();
        }
    }

    fn beginning_of_line(&mut self) {
        let (_x, y) = self.cursor_pos.expect("post_update should have run");
        self.goto_xy(0, y);
    }

    fn end_of_line(&mut self) {
        let (_x, y) = self.cursor_pos.expect("post_update should have run");
        self.goto_xy(usize::MAX, y);
    }

    fn cut_to_end_of_line(&mut self) {
        let (_x, y) = self.cursor_pos.expect("post_update should have run");
        let eol_index = self.layout.partition_point(|cell| cell.y <= y);
        let eol_pos = match self.layout.get(eol_index) {
            Some(cell) => cell.pos,
            None => self.core.text.len(),
        };

        let slice = &self.core.text[self.core.point..eol_pos];

        if slice == "\n" {
            // If the slice is _just_ \n, we delete it, so that you
            // get the emacs-like behaviour where hammering on ^K
            // alternates between deleting the contents of a line and
            // the empty line it left behind. But we don't put that
            // boring \n in the paste buffer.
            self.core.delete(self.core.point, eol_pos);
        } else if slice.ends_with('\n') {
            // If the slice goes up to but not including a \n,
            // literally cut everything up to but not including the
            // end of the line.
            self.core.cut(self.core.point, eol_pos - 1);
        } else {
            // Otherwise, the line we cut to the end of is a non-final
            // line of a wrapped paragraph. Counterintuitively, in
            // this case, I _insert_ a \n in place of the cut text,
            // because that way the text on later lines doesn't rewrap
            // confusingly.
            //
            // I'm not _sure_ this is the best thing, but the naïve
            // thing isn't great either, and I think on balance this
            // does more or less what my reflexes expect.
            //
            // Put another way: after ^K actually cuts some
            // non-newline text, we expect that the cursor should
            // always be on a newline or end-of-buffer.
            self.core.cut(self.core.point, eol_pos);
            self.core.insert_after("\n");
        }
    }

    fn insert_newline(&mut self) -> Option<bool> {
        self.core.insert("\n");

        let detect_magic_sequence = |seq: &str| {
            self.core.text[..self.core.point].ends_with(seq)
                && (self.core.point == seq.len()
                    || self.core.text[..self.core.point - seq.len()]
                        .ends_with('\n'))
        };

        if detect_magic_sequence(".\n") {
            // The magic sequence! This terminates the editor and
            // submits the buffer contents to whatever wanted it.
            self.core.delete(self.core.point - 2, self.core.point);
            Some(true)
        } else if detect_magic_sequence(".quit\n") {
            // The other magic sequence, which abandons the post.
            Some(false)
        } else {
            // We've just normally inserted a newline.
            None
        }
    }

    fn submit_post(&self) -> LogicalAction {
        LogicalAction::PostComposed(Post {
            text: self.core.text.clone(),
            m: self.post_metadata.clone(),
        })
    }
}

#[test]
fn test_regions() {
    let standard_conf = InstanceStatusConfig {
        max_characters: 500,
        max_media_attachments: 4,
        characters_reserved_per_url: 23,
    };
    let main_sample_text = "test a #hashtag and a @mention@domain.thingy and a https://url.url.url.example.com/whatnot.";

    // Scan the sample text and ensure we're spotting the hashtag,
    // mention and URL.
    let composer = Composer::test_new(standard_conf.clone(), main_sample_text);
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51, colour: ' ' },
            ColouredBufferRegion { start: 51, end: 90, colour: 'u' },
            ColouredBufferRegion { start: 90, end: 91, colour: ' ' },
        }
    );

    // When a hashtag and a mention directly abut, it should
    // disqualify the hashtag.
    let composer =
        Composer::test_new(standard_conf.clone(), "#hashtag@mention");
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 8, colour: '#' },
            ColouredBufferRegion { start: 8, end: 16, colour: ' ' },
        }
    );

    // But a space between them is enough to make them work.
    let composer =
        Composer::test_new(standard_conf.clone(), "#hashtag @mention");
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 8, colour: '#' },
            ColouredBufferRegion { start: 8, end: 9, colour: ' ' },
            ColouredBufferRegion { start: 9, end: 17, colour: '@' },
        }
    );

    // The total cost of main_sample_text is 61 (counting the mention
    // and the URL for less than their full lengths). So setting
    // max=60 highlights the final character as overflow.
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 60,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        main_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51, colour: ' ' },
            ColouredBufferRegion { start: 51, end: 90, colour: 'u' },
            ColouredBufferRegion { start: 90, end: 91, colour: '!' },
        }
    );

    // Dropping the limit by another 1 highlights the last character
    // of the URL.
    // them.)
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 59,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        main_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51, colour: ' ' },
            ColouredBufferRegion { start: 51, end: 90-1, colour: 'u' },
            ColouredBufferRegion { start: 90-1, end: 91, colour: '!' },
        }
    );

    // and dropping it by another 21 highlights the last 22 characters
    // of the URL ...
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 38,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        main_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51, colour: ' ' },
            ColouredBufferRegion { start: 51, end: 90-22, colour: 'u' },
            ColouredBufferRegion { start: 90-22, end: 91, colour: '!' },
        }
    );

    // but dropping it by _another_ one means that the entire URL
    // (since it costs 23 chars no matter what its length) is beyond
    // the limit, so now it all gets highlighted.
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 37,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        main_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51, colour: ' ' },
            ColouredBufferRegion { start: 51, end: 91, colour: '!' },
        }
    );

    // And just for good measure, drop the limit by one _more_, and show the ordinary character just before the URL being highlighted as well.
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 36,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        main_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            ColouredBufferRegion { start: 0, end: 7, colour: ' ' },
            ColouredBufferRegion { start: 7, end: 15, colour: '#' },
            ColouredBufferRegion { start: 15, end: 22, colour: ' ' },
            ColouredBufferRegion { start: 22, end: 44, colour: '@' },
            ColouredBufferRegion { start: 44, end: 51-1, colour: ' ' },
            ColouredBufferRegion { start: 51-1, end: 91, colour: '!' },
        }
    );

    // Test handling of non-single-byte Unicode characters. Note here
    // that ² and ³ take two bytes each in UTF-8 (they live in the ISO
    // 8859-1 top half), but all the other superscript digits need
    // three bytes (U+2070 onwards).
    let unicode_sample_text = "⁰ⁱ²³⁴⁵⁶⁷⁸⁹⁰ⁱ²³⁴⁵⁶⁷⁸⁹⁰ⁱ²³⁴⁵⁶⁷⁸⁹";
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 23,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        unicode_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            // 28 bytes for a full ⁰ⁱ²³⁴⁵⁶⁷⁸⁹, 3+3+2=8 bytes for ⁰ⁱ²
            ColouredBufferRegion { start: 0, end: 2*28+8, colour: ' ' },
            ColouredBufferRegion { start: 2*28+8, end: 3*28, colour: '!' },
        }
    );

    // An even more awkward case, where there's a combining mark at
    // the join.
    let unicode_sample_text = "Besźel";
    let composer = Composer::test_new(
        InstanceStatusConfig {
            max_characters: 4,
            max_media_attachments: 4,
            characters_reserved_per_url: 23,
        },
        unicode_sample_text,
    );
    assert_eq!(
        composer.make_regions(),
        vec! {
            // We expect only the "Bes" to be marked as ok, even though
            // the 'z' part of "ź" is within bounds in principle
            ColouredBufferRegion { start: 0, end: 3, colour: ' ' },
            ColouredBufferRegion { start: 3, end: 8, colour: '!' },
        }
    );
}

#[test]
fn test_layout() {
    let conf = InstanceStatusConfig {
        max_characters: 500,
        max_media_attachments: 4,
        characters_reserved_per_url: 23,
    };

    // The empty string, because it would be embarrassing if that didn't work
    let composer = Composer::test_new(conf.clone(), "");
    assert_eq!(
        composer.layout(10),
        vec! {
        ComposeLayoutCell { pos: 0, x: 0, y: 0 }}
    );

    // One line, just to check that we get a position assigned to
    // every character boundary
    let composer = Composer::test_new(conf.clone(), "abc");
    assert_eq!(
        composer.layout(10),
        (0..=3)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .collect::<Vec<_>>()
    );
    assert_eq!(
        composer.layout(4),
        (0..=3)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .collect::<Vec<_>>()
    );

    // Two lines, which wrap so that 'g' is first on the new line
    let composer = Composer::test_new(conf.clone(), "abc def ghi jkl");
    assert_eq!(
        composer.layout(10),
        (0..=7)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .chain((0..=7).map(|i| ComposeLayoutCell {
                pos: i + 8,
                x: i,
                y: 1
            }))
            .collect::<Vec<_>>()
    );

    // An overlong line, which has to wrap via the fallback
    // hard_wrap_pos system, so we get the full 10 characters (as
    // usual, omitting the last column) on the first line
    let composer = Composer::test_new(conf.clone(), "abcxdefxghixjkl");
    assert_eq!(
        composer.layout(11),
        (0..=9)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .chain((0..=5).map(|i| ComposeLayoutCell {
                pos: i + 10,
                x: i,
                y: 1
            }))
            .collect::<Vec<_>>()
    );

    // The most trivial case with a newline in: _just_ the newline
    let composer = Composer::test_new(conf.clone(), "\n");
    assert_eq!(
        composer.layout(10),
        vec! {
        ComposeLayoutCell { pos: 0, x: 0, y: 0 },
        ComposeLayoutCell { pos: 1, x: 0, y: 1 }}
    );

    // And now two newlines
    let composer = Composer::test_new(conf.clone(), "\n\n");
    assert_eq!(
        composer.layout(10),
        vec! {
        ComposeLayoutCell { pos: 0, x: 0, y: 0 },
        ComposeLayoutCell { pos: 1, x: 0, y: 1 },
        ComposeLayoutCell { pos: 2, x: 0, y: 2 }}
    );

    // Watch what happens just as we type text across a wrap boundary.
    // At 8 characters, this should be fine as it is, since the wrap
    // width is 1 less than the physical screen width and the cursor
    // is permitted to be in the final column if it's at end-of-buffer
    // rather than on a character.
    let composer = Composer::test_new(conf.clone(), "abc def ");
    assert_eq!(
        composer.layout(9),
        (0..=8)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .collect::<Vec<_>>()
    );
    // Now we type the next character, and it should wrap on to the
    // next line.
    let composer = Composer::test_new(conf.clone(), "abc def g");
    assert_eq!(
        composer.layout(9),
        (0..=7)
            .map(|i| ComposeLayoutCell { pos: i, x: i, y: 0 })
            .chain((0..=1).map(|i| ComposeLayoutCell {
                pos: i + 8,
                x: i,
                y: 1
            }))
            .collect::<Vec<_>>()
    );
}

impl ActivityState for Composer {
    fn resize(&mut self, w: usize, h: usize) {
        if self.last_size != Some((w, h)) {
            self.last_size = Some((w, h));
            self.page_len = h.saturating_sub(
                self.header.render(w).len()
                    + match self.irt {
                        None => 0,
                        Some(ref irt) => irt.render(w).len(),
                    }
                    + self.headersep.render(w).len(),
            );
            self.post_update();
        }
    }

    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let mut lines = Vec::new();
        lines.extend_from_slice(&self.header.render(w));
        if let Some(irt) = &self.irt {
            lines.extend_from_slice(&irt.render(w));
        }
        lines.extend_from_slice(&self.headersep.render(w));

        let ystart = lines.len();

        while lines.len() < h {
            let y = self.ytop + (lines.len() - ystart);
            match self.get_coloured_line(y) {
                Some(line) => lines.push(line),
                None => break, // ran out of lines in the buffer
            }
        }

        let mut cursor_pos = CursorPosition::None;
        if let Some((cx, cy)) = self.cursor_pos {
            if let Some(y) = cy.checked_sub(self.ytop) {
                let cy = y + ystart;
                if cy < h {
                    cursor_pos = CursorPosition::At(cx, cy);
                }
            }
        }

        (lines, cursor_pos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        _client: &mut Client,
    ) -> LogicalAction {
        use ComposerKeyState::*;

        // Start by identifying whether a keystroke is an up/down one,
        // so that we can consistently clear the goal column for all
        // keystrokes that are not
        match (self.keystate, key) {
            (Start, Ctrl('N'))
            | (Start, Down)
            | (Start, Ctrl('P'))
            | (Start, Up) => {
                if self.goal_column.is_none() {
                    self.goal_column = self.cursor_pos.map(|(x, _y)| x);
                }
            }
            _ => self.goal_column = None,
        }

        match (self.keystate, key) {
            (Start, Ctrl('N')) | (Start, Down) => self.next_line(),
            (Start, Ctrl('P')) | (Start, Up) => self.prev_line(),

            (Start, Ctrl('V')) | (Start, PgDn) => self.page_down(),
            (Start, Ctrl('Z')) | (Start, PgUp) => self.page_up(),

            (Start, Ctrl('A')) | (Start, Home) => self.beginning_of_line(),
            (Start, Ctrl('E')) | (Start, End) => self.end_of_line(),

            (Start, Ctrl('K')) => self.cut_to_end_of_line(),

            // Return is an ordinary self-inserting key, but we have
            // to handle it specially here, because EditorCore doesn't
            // handle it at all, because in BottomLineEditor it _is_
            // special
            (Start, Return) => match self.insert_newline() {
                Some(true) => return self.submit_post(),
                Some(false) => return LogicalAction::Pop,
                None => (),
            },

            // ^O is a prefix key that is followed by various less
            // common keystrokes
            (Start, Ctrl('O')) => {
                self.keystate = CtrlO;
                return LogicalAction::Nothing; // no need to post_update
            }
            (CtrlO, Ctrl('P')) | (CtrlO, Up) => {
                self.core.beginning_of_buffer();
                self.keystate = Start;
            }
            (CtrlO, Ctrl('N')) | (CtrlO, Down) => {
                self.core.end_of_buffer();
                self.keystate = Start;
            }
            (CtrlO, Pr('q')) | (CtrlO, Pr('Q')) => return LogicalAction::Pop,
            (CtrlO, Space) => return self.submit_post(),
            (CtrlO, _) => {
                self.keystate = Start;
                return LogicalAction::Beep;
            }

            // Anything else goes to the EditorCore
            (Start, _) => self.core.handle_keypress(key),
        }
        self.post_update();
        LogicalAction::Nothing
    }
}

pub fn compose_post(
    client: &mut Client,
    post: Post,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let inst = client.instance()?;
    let title = match post.m.in_reply_to_id {
        None => "Compose a post".to_owned(),
        Some(ref id) => format!("Reply to post {id}"),
    };
    let header = FileHeader::new(ColouredString::uniform(&title, 'H'));
    let irt = post
        .m
        .in_reply_to_id
        .as_ref()
        .map(|id| InReplyToLine::from_id(id, client));
    Ok(Box::new(Composer::new(
        inst.configuration.statuses,
        header,
        irt,
        post,
    )))
}
