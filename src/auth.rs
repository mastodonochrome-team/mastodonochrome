use serde::{Deserialize, Serialize};

use super::config::ConfigLocation;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum AuthError {
    Nonexistent(String),
    Bad(String),
    SaveFailed(String),
}

impl super::top_level_error::TopLevelErrorCandidate for AuthError {}

impl std::fmt::Display for AuthError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        match self {
            AuthError::Nonexistent(_) => {
                write!(f, "no saved authentication file")
            }
            AuthError::Bad(ref msg) => {
                write!(f, "unable to read authentication: {}", msg)
            }
            AuthError::SaveFailed(ref msg) => {
                write!(f, "unable to save authentication data: {}", msg)
            }
        }
    }
}

impl std::error::Error for AuthError {}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct AuthConfig {
    pub account_id: Option<String>,
    pub username: Option<String>,
    pub instance_url: Option<String>,
    pub instance_domain: Option<String>,
    pub client_id: Option<String>,
    pub client_secret: Option<String>,
    pub user_token: Option<String>,
}

impl AuthConfig {
    pub fn load(cfgloc: &ConfigLocation) -> Result<Self, AuthError> {
        let authfile = cfgloc.get_path("auth");
        let authdata = match std::fs::read_to_string(&authfile) {
            Err(e) => Err(AuthError::Nonexistent(format!(
                "unable to read config file '{}': {}",
                authfile.display(),
                e
            ))),
            Ok(d) => Ok(d),
        }?;
        let auth: Self = match serde_json::from_str(&authdata) {
            Err(e) => Err(AuthError::Bad(format!(
                "unable to parse config file '{}': {}",
                authfile.display(),
                e
            ))),
            Ok(d) => Ok(d),
        }?;
        Ok(auth)
    }

    pub fn save(&self, cfgloc: &ConfigLocation) -> Result<(), AuthError> {
        let mut json = serde_json::to_string_pretty(self)
            .expect("should never fail to format this at all");
        json.push('\n');
        cfgloc.create_file("auth", &json).map_err(|e| {
            AuthError::SaveFailed(format!(
                "unable to parse config file '{}': {}",
                cfgloc.get_path("auth").display(),
                e
            ))
        })
    }

    pub fn is_logged_in(&self) -> bool {
        self.account_id.is_some()
    }
}
