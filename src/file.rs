use derive_more::From;
use itertools::Itertools;
use regex::{Regex, RegexBuilder};
use std::cell::RefCell;
use std::cmp::{max, min};
use std::collections::{hash_map, HashMap, HashSet};
use std::rc::Rc;
use void::{ResultVoidExt, Void};

use super::activity_stack::{
    Activity, ComposeActivity, NonUtilityActivity, OverlayActivity,
    UtilityActivity,
};
use super::client::{
    AnyFeedId, Boosts, Client, ClientError, FeedExtend, FeedId, LocalFeedId,
    LogLock, Replies,
};
use super::coloured_string::*;
use super::help::HelpItem;
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
    SavedFilePos,
};
use super::unordered_merge::find_new_pos;

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
enum FileItemId {
    Header,
    Extender,
    Item(String),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Fraction {
    numerator: usize,
    denominator: usize,
}

impl Fraction {
    fn new(numerator: usize, denominator: usize) -> Self {
        assert!(numerator <= denominator);
        Self {
            numerator,
            denominator,
        }
    }
}

/// A file position indicates what line of the file is displayed on a
/// specified line of the display.
#[derive(Debug, Clone)]
pub struct FilePosition<Item = usize> {
    /// Identifies one of the items in the file.
    item: Item,

    /// Identifies how to place that item on the screen.
    placement: Placement,
}

/// The part of [`FilePosition`] that describes how to place an item
/// on the screen.
#[derive(Debug, Clone, Copy)]
pub enum Placement {
    /// Align the top of the item at the top of the screen.
    Top,

    /// Align a position within the item (given as a fraction of its
    /// total height) at the bottom of the screen.
    BotFrac(Fraction),

    /// Align a position within the item at the top of the screen.
    TopFrac(Fraction),

    /// Try to centre the item in the middle of the screen. If it's
    /// larger than the screen, align it to the top.
    Centre,
}

impl Placement {
    /// Less-verbose constructor for `Placement::BotFrac`.
    fn botfrac(numerator: usize, denominator: usize) -> Self {
        Placement::BotFrac(Fraction {
            numerator,
            denominator,
        })
    }

    /// Less-verbose constructor for `Placement::TopFrac`.
    fn topfrac(numerator: usize, denominator: usize) -> Self {
        Placement::TopFrac(Fraction {
            numerator,
            denominator,
        })
    }
}

impl<Item> FilePosition<Item> {
    /// A file position that aligns the top of a given item with the
    /// top of the screen.
    fn item_top(item: Item) -> Self {
        FilePosition {
            item,
            placement: Placement::Top,
        }
    }

    /// A file position that aligns the bottom of a given item with
    /// the bottom of the screen.
    fn item_bottom(item: Item) -> Self {
        FilePosition {
            item,
            placement: Placement::botfrac(1, 1),
        }
    }

    /// A file position that aligns a particular position within a
    /// given item with the bottom of the screen.
    fn item_pos(item: Item, numerator: usize, denominator: usize) -> Self {
        assert!(numerator > 0, "Don't place an item entirely off the bottom");
        FilePosition {
            item,
            placement: Placement::botfrac(numerator, denominator),
        }
    }

    /// A file position that reuses all the fields from another
    /// `FilePosition` except for the item, which is provided as a
    /// parameter. Used when turning a `FilePosition<T>` into a
    /// `FilePosition<U>`.
    fn convert<NewItem>(&self, new_item: NewItem) -> FilePosition<NewItem> {
        FilePosition {
            item: new_item,
            placement: self.placement,
        }
    }

    /// A file position that centres a given item.
    fn item_centre(item: Item) -> Self {
        FilePosition {
            item,
            placement: Placement::Centre,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ScreenLayout {
    width: usize,
    height: usize,
    topitem: usize,
    toppos: Fraction,
    botitem: usize,
    botpos: Fraction,
}

impl ScreenLayout {
    /// Compute a screen layout, given the number of lines on the
    /// screen (`h`), a [`FilePosition`] indicating how to place a
    /// particular item, the total number of items `nitems`, and a
    /// means of querying the length of an item (`get_item_length`).
    ///
    /// `w` is only needed in order to put it into the output. It
    /// doesn't affect the layout algorithm.
    fn new(
        w: usize,
        h: usize,
        pos: &FilePosition,
        nitems: usize,
        mut get_item_length: impl FnMut(usize) -> usize,
    ) -> Self {
        // Start by finding out the length of the item referred to in
        // our file position, and decide where on the screen to put
        // it, in terms of how many screen lines appear below its
        // topmost line.
        //
        // That measurement can sensibly be a usize, because we expect
        // it to be at least 1.
        //
        // (It would be 0 if we had a Placement::BotFrac that put the
        // specified item just off the bottom of the display, but we
        // don't want to do that: if you do want to say that, it's
        // better to specify the previous item instead, with a
        // Placement::BotFrac fraction of 1 rather than 0.)
        let screenlines = h.saturating_sub(1);
        let nbelow = {
            let itemlen = get_item_length(pos.item);
            match pos.placement {
                Placement::Top => screenlines,
                Placement::BotFrac(frac) => {
                    itemlen * frac.numerator / frac.denominator
                }
                Placement::TopFrac(frac) => {
                    screenlines + itemlen * frac.numerator / frac.denominator
                }
                Placement::Centre => {
                    screenlines - screenlines.saturating_sub(itemlen) / 2
                }
            }
        };

        // Now count forward from that to find the item on the bottom
        // line.
        let (botitem, botpos) = Self::count_forward(
            pos.item,
            nbelow,
            nitems,
            &mut get_item_length,
        );

        // Count backward from there to the item on the top line.
        let (topitem, toppos) = Self::count_backward(
            botitem,
            screenlines,
            botpos.numerator,
            &mut get_item_length,
        );

        // And then count forward one more time, so that if the
        // backward pass hit the top of the file, we still get a full
        // screenful.
        let (botitem, botpos) = Self::count_forward(
            topitem,
            screenlines + toppos.numerator,
            nitems,
            &mut get_item_length,
        );

        ScreenLayout {
            width: w,
            height: h,
            topitem,
            toppos,
            botitem,
            botpos,
        }
    }

    fn count_forward(
        startitem: usize,
        lines: usize,
        nitems: usize,
        mut get_item_length: impl FnMut(usize) -> usize,
    ) -> (usize, Fraction) {
        let mut lines = lines;
        let mut curritem = startitem;
        loop {
            let itemlen = get_item_length(curritem);
            if lines <= itemlen {
                break (curritem, Fraction::new(lines, itemlen));
            }
            lines -= itemlen;
            if curritem + 1 == nitems {
                // Ran out of items! We're at the end of the file.
                break (curritem, Fraction::new(itemlen, itemlen));
            }
            curritem += 1;
        }
    }

    fn count_backward(
        startitem: usize,
        lines: usize,
        startitemlen: usize,
        mut get_item_length: impl FnMut(usize) -> usize,
    ) -> (usize, Fraction) {
        let mut lines = lines;
        let mut curritem = startitem;
        let mut itemlen = startitemlen;
        loop {
            if lines <= itemlen {
                break (
                    curritem,
                    Fraction::new(itemlen - lines, get_item_length(curritem)),
                );
            }
            lines -= itemlen;
            if curritem == 0 {
                // Ran out of items! We're at the top of the file.
                let itemlen = get_item_length(curritem);
                break (curritem, Fraction::new(0, itemlen));
            }
            curritem -= 1;
            itemlen = get_item_length(curritem);
        }
    }

    fn at_top(&self) -> bool {
        self.topitem == 0 && self.toppos.numerator == 0
    }

    fn at_item_end(&self) -> bool {
        self.botpos.numerator == self.botpos.denominator
    }

    fn at_specific_item_end(&self, item: usize) -> bool {
        self.botitem == item && self.at_item_end()
    }

    fn file_pos(&self) -> FilePosition {
        if self.botpos.numerator > 0 {
            FilePosition::item_pos(
                self.botitem,
                self.botpos.numerator,
                self.botpos.denominator,
            )
        } else if self.botitem > 0 {
            FilePosition::item_bottom(self.botitem - 1)
        } else {
            FilePosition::item_top(0)
        }
    }
}

#[test]
fn test_screen_layout() {
    let item_lengths = &[2, 3, 10, 6, 11, 7, 3, 6];
    let get_item_length = |item| item_lengths[item];

    // Top of the file. 2 + 3 + 10 + 6 + (2 lines of the 11-line item 4)
    // comes to 23, one less than the screen height.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_top(0),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // Same screen layout specified via topfrac.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition {
                item: 0,
                placement: Placement::topfrac(0, 2)
            },
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // Same screen layout, but this time specified via its bottom line.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(4, 2, 11),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // Try to go up one line. It fails, and we get the same layout yet
    // again.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(4, 1, 11),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // And the same if we try to go all the way to the top (or rather,
    // one line off it, which is the best we can do with a legal
    // FilePosition).
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(0, 1, 2),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // Same again specified by trying to centre the top item.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_centre(0),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(0, 2),
            botitem: 4,
            botpos: Fraction::new(2, 11),
        }
    );

    // Go one line down via topfrac
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition {
                item: 0,
                placement: Placement::topfrac(1, 2)
            },
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 0,
            toppos: Fraction::new(1, 2),
            botitem: 4,
            botpos: Fraction::new(3, 11),
        }
    );

    // Bottom of the file. (7 lines of the 11-line item 4) + 7 + 3 + 6
    // again equals 23, so we start item 4 at position 11-7 = 4.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_bottom(7),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 4,
            toppos: Fraction::new(4, 11),
            botitem: 7,
            botpos: Fraction::new(6, 6),
        }
    );

    // Same by trying to centre the bottom item
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_centre(7),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 4,
            toppos: Fraction::new(4, 11),
            botitem: 7,
            botpos: Fraction::new(6, 6),
        }
    );

    // One line up from the bottom of the file
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(7, 5, 6),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 4,
            toppos: Fraction::new(3, 11),
            botitem: 7,
            botpos: Fraction::new(5, 6),
        }
    );

    // Go up another 3 lines so that the top of the screen is at the
    // top of item 4. FIXME: this can't decide whether to be 4.0/11 or 3.6/6 at the top
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(7, 2, 6),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 4,
            toppos: Fraction::new(0, 11),
            botitem: 7,
            botpos: Fraction::new(2, 6),
        }
    );

    // And go up one more so that we get one line of the previous item.
    assert_eq!(
        ScreenLayout::new(
            999,
            24,
            &FilePosition::item_pos(7, 1, 6),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 24,
            topitem: 3,
            toppos: Fraction::new(5, 6),
            botitem: 7,
            botpos: Fraction::new(1, 6),
        }
    );

    // Centre item 4, where there's one line around it on each side
    assert_eq!(
        ScreenLayout::new(
            999,
            14,
            &FilePosition::item_centre(4),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 14,
            topitem: 3,
            toppos: Fraction::new(5, 6),
            botitem: 5,
            botpos: Fraction::new(1, 7),
        }
    );

    // Centre item 4, where it exactly fits on the screen
    assert_eq!(
        ScreenLayout::new(
            999,
            12,
            &FilePosition::item_centre(4),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 12,
            topitem: 4,
            toppos: Fraction::new(0, 11),
            botitem: 4,
            botpos: Fraction::new(11, 11),
        }
    );

    // Centre item 4, where it doesn't fit on the screen and we have
    // to get only the top of it
    assert_eq!(
        ScreenLayout::new(
            999,
            11,
            &FilePosition::item_centre(4),
            item_lengths.len(),
            &get_item_length
        ),
        ScreenLayout {
            width: 999,
            height: 11,
            topitem: 4,
            toppos: Fraction::new(0, 11),
            botitem: 4,
            botpos: Fraction::new(10, 11),
        }
    );
}

#[derive(From)]
enum StartingPosition<'a> {
    Top,
    Bottom,
    Saved(&'a SavedFilePos),
}

trait FileDataSource {
    type Error: std::error::Error + Into<ClientError>;
    fn get(&self, client: &mut Client) -> Vec<String>;
    fn init(&self, client: &mut Client) -> Result<(), Self::Error>;
    fn try_extend(&self, client: &mut Client) -> Result<bool, Self::Error>;
    fn updated(&self, feeds_updated: &HashSet<AnyFeedId>) -> bool;
    fn extendable(&self) -> bool;

    fn predecessors(
        &self,
        _client: &mut Client,
        _id: &str,
    ) -> Result<Vec<String>, Self::Error> {
        Ok(Vec::new())
    }

    fn single_id(&self) -> String {
        panic!("Should only call this if the FileType sets can_list(), CAN_GET_POSTS or IS_EXAMINE_USER");
    }
}

struct FeedSource {
    id: FeedId,
}

impl FeedSource {
    fn new(id: FeedId) -> Self {
        FeedSource { id }
    }
}

impl FileDataSource for FeedSource {
    type Error = ClientError;
    fn get(&self, client: &mut Client) -> Vec<String> {
        let feed = client.borrow_feed(&self.id);
        let ids = feed.ids.iter().cloned().collect();
        ids
    }

    fn init(&self, client: &mut Client) -> Result<(), ClientError> {
        // If the feed doesn't exist at all, make sure we've fetched
        // it once.
        if !client.fetch_feed(&self.id, FeedExtend::Initial)? {
            // But if it did exist, try to extend it into the future,
            // in case new things have been added to it since we last
            // looked.
            client.fetch_feed(&self.id, FeedExtend::Future)?;
        }
        Ok(())
    }

    fn try_extend(&self, client: &mut Client) -> Result<bool, ClientError> {
        client.fetch_feed(&self.id, FeedExtend::Past)
    }

    fn updated(&self, feeds_updated: &HashSet<AnyFeedId>) -> bool {
        feeds_updated.contains(&self.id.clone().into())
    }

    fn extendable(&self) -> bool {
        true
    }

    fn predecessors(
        &self,
        client: &mut Client,
        id: &str,
    ) -> Result<Vec<String>, ClientError> {
        client.get_feed_predecessors(&self.id, id)
    }
}

struct LocalFeedSource {
    id: LocalFeedId,
}

impl LocalFeedSource {
    fn new(id: LocalFeedId) -> Self {
        LocalFeedSource { id }
    }
}

impl FileDataSource for LocalFeedSource {
    type Error = Void;
    fn get(&self, client: &mut Client) -> Vec<String> {
        let feed = client.borrow_local_feed(self.id);
        let ids = feed.ids.iter().cloned().collect();
        ids
    }

    fn init(&self, _client: &mut Client) -> Result<(), Void> {
        Ok(())
    }

    fn try_extend(&self, _client: &mut Client) -> Result<bool, Void> {
        Ok(false)
    }

    fn updated(&self, feeds_updated: &HashSet<AnyFeedId>) -> bool {
        feeds_updated.contains(&self.id.into())
    }

    fn extendable(&self) -> bool {
        false
    }
}

struct StaticSource {
    ids: Vec<String>,
}

impl StaticSource {
    fn singleton(id: String) -> Self {
        StaticSource { ids: vec![id] }
    }
    fn vector(ids: Vec<String>) -> Self {
        StaticSource { ids }
    }
}

impl FileDataSource for StaticSource {
    type Error = Void;
    fn get(&self, _client: &mut Client) -> Vec<String> {
        self.ids.clone()
    }
    fn init(&self, _client: &mut Client) -> Result<(), Void> {
        Ok(())
    }
    fn try_extend(&self, _client: &mut Client) -> Result<bool, Void> {
        Ok(false)
    }
    fn updated(&self, _feeds_updated: &HashSet<AnyFeedId>) -> bool {
        false
    }
    fn extendable(&self) -> bool {
        false
    }
    fn single_id(&self) -> String {
        self.ids
            .iter()
            .exactly_one()
            .expect("Should only call this on singleton StaticSources")
            .to_owned()
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum CanList {
    Nothing,
    ForPost,

    // true if the account is your own, and also locked, so that we
    // also need a third option to list follow _requesters_
    ForUser(bool),
}

trait FileType {
    type Item: TextFragment + Sized;
    const CAN_GET_POSTS: bool = false;
    const IS_EXAMINE_USER: bool = false;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError>;

    fn feed_id(&self) -> Option<&FeedId> {
        None
    }

    fn can_list(&self) -> CanList {
        CanList::Nothing
    }
}

struct StatusFeedType {
    id: Option<FeedId>,
}
impl StatusFeedType {
    fn with_feed(id: FeedId) -> Self {
        Self { id: Some(id) }
    }
    fn without_feed() -> Self {
        Self { id: None }
    }
}
impl FileType for StatusFeedType {
    type Item = StatusDisplay;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let st = client.status_by_id(id)?;
        Ok(StatusDisplay::new(st, client))
    }

    fn feed_id(&self) -> Option<&FeedId> {
        self.id.as_ref()
    }
}

struct NotificationStatusFeedType {
    id: FeedId,
}
impl NotificationStatusFeedType {
    fn with_feed(id: FeedId) -> Self {
        Self { id }
    }
}
impl FileType for NotificationStatusFeedType {
    type Item = StatusDisplay;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let not = client.notification_by_id(id)?;
        let st = &not.status.expect(
            "expected all notifications in this feed would have statuses",
        );
        Ok(StatusDisplay::new(st.clone(), client))
    }

    fn feed_id(&self) -> Option<&FeedId> {
        Some(&self.id)
    }
}

struct EgoNotificationFeedType {
    id: FeedId,
}
impl EgoNotificationFeedType {
    fn with_feed(id: FeedId) -> Self {
        Self { id }
    }
}
impl FileType for EgoNotificationFeedType {
    type Item = NotificationLog;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let not = client.notification_by_id(id)?;
        Ok(NotificationLog::from_notification(&not, client))
    }

    fn feed_id(&self) -> Option<&FeedId> {
        Some(&self.id)
    }
}

struct UserListFeedType {}
impl FileType for UserListFeedType {
    type Item = UserListEntry;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let ac = client.account_by_id(id)?;
        Ok(UserListEntry::from_account(&ac, client))
    }
}

struct FileContents<Type: FileType, Source: FileDataSource> {
    source: Source,
    header: FileHeader,
    extender: Option<ExtendableIndicator>,
    ids: Vec<FileItemId>,
    items: HashMap<String, Type::Item>,
}

impl<Type: FileType, Source: FileDataSource> FileContents<Type, Source> {
    fn update_items(&mut self, file_desc: &Type, client: &mut Client) {
        // FIXME: if the feed has been extended rather than created,
        // we should be able to make less effort than this. But we
        // still need to regenerate any item derived from something we
        // _know_ has changed on the server side - in particular, a
        // post we just changed fave/boost status on.

        let source_ids = self.source.get(client);

        let mut file_ids = Vec::new();
        self.items.clear();

        file_ids.push(FileItemId::Header);
        if self.extender.is_some() {
            file_ids.push(FileItemId::Extender);
        }

        for id in source_ids {
            let item = file_desc
                .get_item(&id, client)
                .expect("Any id stored in a Feed should also be cached");
            file_ids.push(FileItemId::Item(id.clone()));
            self.items.insert(id, item);
        }

        self.ids = file_ids;
    }

    fn extender_index(&self) -> Option<usize> {
        if self.extender.is_some() {
            Some(1)
        } else {
            None
        }
    }
    fn index_limit(&self) -> usize {
        self.ids.len()
    }
    fn last_index(&self) -> usize {
        self.index_limit()
            .checked_sub(1)
            .expect("There should always be at least a file header")
    }

    fn get(&self, index: usize) -> &dyn TextFragment {
        match &self.ids[index] {
            FileItemId::Header => &self.header,
            FileItemId::Extender => match self.extender {
                Some(ref ext) => ext,
                _ => panic!("We only put Extender in the list if this exists"),
            },
            FileItemId::Item(id) => self
                .items
                .get(id)
                .expect("Every item we put in ids has an entry in items"),
        }
    }

    fn id_at_index(&self, index: usize) -> Option<&str> {
        match self.ids.get(index) {
            Some(FileItemId::Item(id)) => Some(id),
            _ => None,
        }
    }

    fn index_of_id(&self, id: &str) -> Option<usize> {
        // We can't do anything efficient like binary search, because
        // our ids might not be in any sensible order. (If they're,
        // say, a list of users that took an action, the ids' natural
        // order would be user creation date, but here they'd be
        // ordered by when each user did the thing.)
        self.ids.iter().position(|item| {
            if let FileItemId::Item(rid) = item {
                rid == id
            } else {
                false
            }
        })
    }

    fn index_before_id(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Option<usize>, Source::Error> {
        // Some item we had previously stored the id of is longer
        // here; maybe it was deleted. Try fetching some of its
        // predecessors, and see if any of those is still around.
        for prev_id in self.source.predecessors(client, &id)? {
            let index = self.index_of_id(&prev_id);
            if index.is_some() {
                return Ok(index);
            }
        }

        Ok(None)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum SelectionPurpose {
    ExamineUser,
    StatusInfo,
    Favourite,
    Boost,
    Unfold,
    Thread,
    Reply,
    Vote,
    HttpInfo,
    Edit,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum UIMode {
    Normal,
    ListSubmenu,
    PostsSubmenu,
    Select(HighlightType, SelectionPurpose),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum SearchDirection {
    Up,
    Down,
}

struct FileDisplayStyles {
    selected_poll_id: Option<String>,
    selected_poll_options: HashSet<usize>,
    unfolded: Option<Rc<RefCell<HashSet<String>>>>,
}

impl FileDisplayStyles {
    fn new(unfolded: Option<Rc<RefCell<HashSet<String>>>>) -> Self {
        FileDisplayStyles {
            selected_poll_id: None,
            selected_poll_options: HashSet::new(),
            unfolded,
        }
    }
}

impl DisplayStyleGetter for FileDisplayStyles {
    fn poll_options(&self, id: &str) -> Option<HashSet<usize>> {
        self.selected_poll_id.as_ref().and_then(|s| {
            if s == id {
                Some(self.selected_poll_options.clone())
            } else {
                None
            }
        })
    }

    fn unfolded(&self, id: &str) -> bool {
        self.unfolded
            .as_ref()
            .is_some_and(|set| set.borrow().contains(id))
    }
}

struct File<Type: FileType, Source: FileDataSource> {
    contents: FileContents<Type, Source>,
    rendered: HashMap<FileItemId, Vec<ColouredString>>,
    pos: FilePosition,
    last_layout: Option<ScreenLayout>,
    ui_mode: UIMode,
    selection: Option<(usize, usize)>,
    selection_restricted: bool,
    select_aux: Option<bool>, // distinguishes fave from unfave, etc
    display_styles: FileDisplayStyles,
    file_desc: Type,
    search_direction: Option<SearchDirection>,
    last_search: Option<Regex>,
    latest_read_index: Option<usize>,
    our_account_id: Option<String>, // for reference when we don't have a Client
}

impl<Type: FileType, Source: FileDataSource> File<Type, Source> {
    fn new(
        client: &mut Client,
        source: Source,
        desc: ColouredString,
        file_desc: Type,
        starting_pos: Option<StartingPosition>,
        unfolded: Option<Rc<RefCell<HashSet<String>>>>,
        show_new: bool,
    ) -> Result<Self, Source::Error> {
        source.init(client)?;

        let extender = if source.extendable() {
            Some(ExtendableIndicator::new())
        } else {
            None
        };

        let mut contents = FileContents {
            source,
            header: FileHeader::new(desc),
            extender,
            ids: Vec::new(),
            items: HashMap::new(),
        };

        contents.update_items(&file_desc, client);

        // Start with the initial position at the file top
        let mut initial_pos = FilePosition::item_top(0);

        // If we have a 'latest read item' from the saved file
        // position, determine it, and override the initial position
        // with it
        let mut latest_read_index = None;
        if let Some(StartingPosition::Saved(saved_pos)) = starting_pos {
            if let Some(latest_read_id) = saved_pos.latest_read_id.as_ref() {
                latest_read_index = contents.index_of_id(latest_read_id);
                if latest_read_index.is_none() {
                    latest_read_index =
                        contents.index_before_id(latest_read_id, client)?;
                }
            }

            if let Some(latest_read_index) = latest_read_index {
                initial_pos = if show_new {
                    FilePosition::item_top(latest_read_index + 1)
                } else {
                    FilePosition::item_bottom(latest_read_index)
                };
            }
        }

        // But if we have an actual FilePosition in our SavedFilePos,
        // it's even better to use that ... unless we're supposed to
        // be showing a new thing eagerly.
        if !show_new {
            match starting_pos {
                Some(StartingPosition::Top) => {
                    initial_pos = FilePosition::item_top(0)
                }
                Some(StartingPosition::Bottom) => {
                    initial_pos =
                        FilePosition::item_bottom(contents.last_index())
                }
                Some(StartingPosition::Saved(saved_pos)) => {
                    if let Some(file_pos) = saved_pos.file_pos.as_ref() {
                        // Translate the string id from SavedFilePos into an index.
                        let mut index = contents.index_of_id(&file_pos.item);
                        if index.is_none() {
                            index = contents
                                .index_before_id(&file_pos.item, client)?;
                        }
                        if let Some(index) = index {
                            initial_pos = file_pos.convert(index);
                        }
                    }
                }
                _ => (),
            }
        }

        // No matter how we invented initial_pos.item, we expect it to
        // be within the array bounds, because none of our ways of
        // making it up involved receiving it in integer form from
        // anyone else.
        assert!(
            initial_pos.item < contents.index_limit(),
            "Calculated initial_pos out of range"
        );

        Ok(File {
            contents,
            rendered: HashMap::new(),
            pos: initial_pos,
            last_layout: None,
            ui_mode: UIMode::Normal,
            selection: None,
            selection_restricted: false,
            select_aux: None,
            display_styles: FileDisplayStyles::new(unfolded),
            file_desc,
            search_direction: None,
            last_search: None,
            latest_read_index,
            our_account_id: client.our_account_id_optional(),
        })
    }

    fn ensure_item_rendered(
        &mut self,
        index: usize,
        w: usize,
    ) -> &Vec<ColouredString> {
        let id = self
            .contents
            .ids
            .get(index)
            .expect("Don't expect to call this on an out-of-range index");
        if let hash_map::Entry::Vacant(e) = self.rendered.entry(id.clone()) {
            let mut lines = Vec::new();

            let highlight = match self.ui_mode {
                UIMode::Select(htype, _purpose) => match self.selection {
                    None => None,
                    Some((item, sub)) => {
                        if item == index {
                            Some(Highlight(htype, sub))
                        } else {
                            None
                        }
                    }
                },
                _ => None,
            };

            for line in self.contents.get(index).render_highlighted(
                w,
                highlight,
                &self.display_styles,
            ) {
                for frag in line.split(w) {
                    lines.push(frag.into());
                }
            }

            e.insert(lines);
        }

        self.rendered
            .get(id)
            .expect("We just made sure this was present")
    }

    fn get_rendered_item(&self, index: usize) -> Option<&Vec<ColouredString>> {
        self.contents
            .ids
            .get(index)
            .and_then(|id| self.rendered.get(&id))
    }

    fn unrender_item(&mut self, index: usize) {
        if let Some(id) = self.contents.ids.get(index) {
            self.rendered.remove(&id);
        }
    }

    fn last_size(&self) -> Option<(usize, usize)> {
        self.last_layout
            .as_ref()
            .map(|layout| (layout.width, layout.height))
    }

    fn make_screen_layout_for_size_and_pos(
        &mut self,
        w: usize,
        h: usize,
        pos: FilePosition,
    ) -> ScreenLayout {
        // If we're told to put an item at the top of the screen, and
        // it's the item immediately after the file header, change our
        // mind and put the file header at the top.
        let pos = match pos {
            FilePosition {
                item: 1,
                placement: Placement::Top,
            } => {
                assert_eq!(self.contents.ids[0], FileItemId::Header);
                FilePosition {
                    item: 0,
                    placement: Placement::Top,
                }
            }
            _ => pos,
        };
        let nitems = self.contents.index_limit();
        let layout = ScreenLayout::new(w, h, &pos, nitems, |item| {
            self.ensure_item_rendered(item, w).len()
        });
        self.last_layout = Some(layout);
        layout
    }

    fn make_screen_layout(&mut self) -> ScreenLayout {
        let (w, h) = self
            .last_size()
            .expect("make_screen_layout before setting pos");
        self.make_screen_layout_for_size_and_pos(w, h, self.pos.clone())
    }

    fn set_pos(&mut self, pos: FilePosition) {
        let (w, _h) = self.last_size().expect("set_pos before setting width");

        self.pos = pos;
        let layout = self.make_screen_layout();

        // After a deliberate user action, normalise FilePosition into
        // what's now shown on the screen, regardless of what the
        // previous intention was. This is what clips attempts to move
        // up when the top line of the screen is already at the top of
        // the file.
        self.pos = layout.file_pos();
        self.make_screen_layout();

        if let Some(ref mut ext) = &mut self.contents.extender {
            ext.set_primed(layout.at_top());
        }
        if let Some(ei) = self.contents.extender_index() {
            self.rendered.remove(&FileItemId::Extender);
            self.ensure_item_rendered(ei, w);
        }
    }

    fn update_latest_read_index(&mut self) {
        let layout = self.make_screen_layout();
        let latest_read_index = if layout.at_item_end() {
            layout.botitem
        } else {
            layout.botitem - 1
        };

        self.latest_read_index =
            max(self.latest_read_index, Some(latest_read_index));
    }

    fn move_up(&mut self, distance: usize) {
        let (w, _h) = self.last_size().expect("move_up before setting width");

        let layout = self.make_screen_layout();
        let mut item = layout.botitem;
        let mut itemline = layout.botpos.numerator;
        let mut itemlen = layout.botpos.denominator;

        let mut remaining = distance;
        while remaining > 0 {
            let this = min(itemline, remaining);
            remaining -= this;
            itemline -= this;
            if itemline == 0 {
                if item == 0 {
                    self.set_pos(FilePosition::item_top(item));
                    return;
                }
                item -= 1;
                itemlen = self.ensure_item_rendered(item, w).len();
                itemline = itemlen;
            }
        }
        self.set_pos(FilePosition::item_pos(item, itemline, itemlen));
    }

    fn move_down(&mut self, distance: usize) {
        let (w, _h) =
            self.last_size().expect("move_down before setting width");

        let layout = self.make_screen_layout();
        let mut item = layout.botitem;
        let mut itemline = layout.botpos.numerator;
        let mut itemlen = layout.botpos.denominator;

        let mut remaining = distance;
        while remaining > 0 {
            if itemline == itemlen {
                if item >= self.contents.last_index() {
                    break;
                }
                item += 1;
                itemlen = self.ensure_item_rendered(item, w).len();
                itemline = 0;
            }

            let this = min(remaining, itemlen - itemline);
            remaining -= this;
            itemline += this;
        }
        self.set_pos(FilePosition::item_pos(item, itemline, itemlen));
    }

    fn goto_top(&mut self) {
        self.set_pos(FilePosition::item_top(0));
    }

    fn goto_bottom(&mut self) {
        self.set_pos(FilePosition::item_pos(self.contents.last_index(), 1, 1));
    }

    fn at_top(&mut self) -> bool {
        let layout = self.make_screen_layout();
        layout.at_top()
    }

    fn layout_at_bottom(&self, layout: &ScreenLayout) -> bool {
        layout.at_specific_item_end(self.contents.last_index())
    }

    fn at_bottom(&mut self) -> bool {
        let layout = self.make_screen_layout();
        self.layout_at_bottom(&layout)
    }

    fn update_items(&mut self, client: &mut Client) -> LogicalAction {
        let mut logact = LogicalAction::Nothing;

        // Preserve a copy of the previous self.contents.ids, and make
        // the new one.
        let old_ids = std::mem::take(&mut self.contents.ids);
        self.contents.update_items(&self.file_desc, client);

        // Now update all our fields that refer to indices in that list.
        use super::unordered_merge::FindResult::*;

        // Make a new FilePosition that best approximates where we
        // were in the old one.
        let (item, placement) = if let Some(layout) = self.last_layout {
            (layout.botitem, Placement::BotFrac(layout.botpos))
        } else {
            // Just in case this is called with self.last_layout equal
            // to None, we can always use our logical file position
            (self.pos.item, self.pos.placement)
        };
        match find_new_pos(&old_ids, &self.contents.ids, item) {
            Index(item) => self.set_pos(FilePosition { item, placement }),
            Gap(i) => self.set_pos(FilePosition::item_bottom(
                i.checked_sub(1).expect("what, not even the header?!"),
            )),
        }

        if let Some(latest) = self.latest_read_index {
            match find_new_pos(&old_ids, &self.contents.ids, latest) {
                Index(i) => self.latest_read_index = Some(i),
                Gap(i) => self.latest_read_index = i.checked_sub(1),
            }
        }

        if let Some((old_item, old_index)) = self.selection {
            match find_new_pos(&old_ids, &self.contents.ids, old_item) {
                Index(i) => {
                    // The item we were selecting still exists, and we
                    // know its new index.
                    self.selection = Some((i, old_index));
                }
                Gap(i) => {
                    // The item our selection was in has vanished.
                    if self.selection_restricted {
                        // If the selection was _restricted_ to that
                        // item, we can't just move the cursor to
                        // another item. We have to terminate the
                        // selection operation and apologise to the
                        // user (by going beep).
                        logact = LogicalAction::Beep;
                        self.end_selection();
                    } else {
                        // Otherwise, look for a nearby thing to
                        // select instead.
                        let htype = match self.ui_mode {
                            UIMode::Select(htype, _) => htype,
                            _ => panic!("selection with no htype"),
                        };
                        self.selection = None;
                        if let Some(prev) = i.checked_sub(1) {
                            self.selection =
                                self.last_selectable_above(htype, prev);
                        }
                        if self.selection.is_none() {
                            self.selection =
                                self.first_selectable_below(htype, i);
                        }
                        if self.selection.is_none() {
                            // There's nothing at all left to select
                            // of the current type. End the selection,
                            // as in the previous case.
                            logact = LogicalAction::Beep;
                            self.end_selection();
                        }
                    }
                }
            }

            self.scroll_to_selection();
        }

        logact
    }

    fn try_extend(
        &mut self,
        client: &mut Client,
    ) -> Result<LogicalAction, ClientError> {
        let any_new = self
            .contents
            .source
            .try_extend(client)
            .map_err(Into::into)?;

        self.rendered.remove(&FileItemId::Header);
        self.rendered.remove(&FileItemId::Extender);

        if !any_new {
            // Can't extend this any further into the past
            self.contents.extender = None;
        }

        let logact = self.update_items(client);
        self.make_screen_layout();
        Ok(logact)
    }

    fn last_selectable_above(
        &self,
        htype: HighlightType,
        index: usize,
    ) -> Option<(usize, usize)> {
        for i in (0..=index).rev() {
            let n = self.contents.get(i).count_highlightables(htype);
            if n > 0 {
                return Some((i, n - 1));
            }
        }

        None
    }

    fn first_selectable_below(
        &self,
        htype: HighlightType,
        index: usize,
    ) -> Option<(usize, usize)> {
        for i in index..self.contents.index_limit() {
            let n = self.contents.get(i).count_highlightables(htype);
            if n > 0 {
                return Some((i, 0));
            }
        }

        None
    }

    fn rerender_selected_item(&mut self) {
        if let Some((index, _)) = self.selection {
            self.unrender_item(index);
        }
    }

    fn start_selection(
        &mut self,
        htype: HighlightType,
        purpose: SelectionPurpose,
        client: &mut Client,
    ) -> LogicalAction {
        let item = self.make_screen_layout().botitem;
        let selection = self
            .last_selectable_above(htype, item)
            .or_else(|| self.first_selectable_below(htype, item + 1));

        if selection.is_some() {
            self.ui_mode = UIMode::Select(htype, purpose);
            self.change_selection_to(selection, false, client);
            LogicalAction::Nothing
        } else {
            LogicalAction::Beep
        }
    }

    fn scroll_to_selection(&mut self) {
        // Ensure at least one line containing a visible selection
        // highlight is on the screen.
        if let Some((item, _sub)) = self.selection {
            let (w, _h) =
                self.last_size().expect("scroll_to_selection before resize");

            let rendered = self.ensure_item_rendered(item, w);
            let rendered_len = rendered.len();
            let mut min_line = None;
            let mut max_line = None;
            for (i, line) in rendered.iter().enumerate() {
                if line.has_colour('*') {
                    max_line = Some(i + 1);
                    min_line = min_line.or(Some(i));
                }
            }
            let (max_line, min_line) = match (max_line, min_line) {
                (Some(max), Some(min)) => (max, min),
                _ => return,
            };

            fn sortkey(layout: &ScreenLayout) -> (usize, usize) {
                (layout.botitem, layout.botpos.denominator)
            }

            let mut layout = self.make_screen_layout();

            let max = self.make_screen_layout_for_size_and_pos(
                layout.width,
                layout.height,
                FilePosition {
                    item,
                    placement: Placement::topfrac(min_line, rendered_len),
                },
            );
            if sortkey(&layout) > sortkey(&max) {
                layout = max;
            }

            let min = self.make_screen_layout_for_size_and_pos(
                layout.width,
                layout.height,
                FilePosition {
                    item,
                    placement: Placement::botfrac(max_line, rendered_len),
                },
            );
            if sortkey(&layout) < sortkey(&min) {
                layout = min;
            }

            self.set_pos(layout.file_pos());
        }
    }

    fn change_selection_to(
        &mut self,
        new_selection: Option<(usize, usize)>,
        none_ok: bool,
        client: &mut Client,
    ) -> LogicalAction {
        if self.selection_restricted {
            if let Some((restricted, _)) = self.selection {
                if new_selection.is_some_and(|(item, _)| item != restricted) {
                    return LogicalAction::Beep;
                }
            }
        }

        let result = if new_selection.is_some() {
            self.rerender_selected_item();
            self.selection = new_selection;
            self.rerender_selected_item();
            self.scroll_to_selection();
            self.make_screen_layout();
            LogicalAction::Nothing
        } else if none_ok {
            LogicalAction::Nothing
        } else {
            LogicalAction::Beep
        };

        self.display_styles.selected_poll_id = match self.ui_mode {
            UIMode::Select(HighlightType::PollOption, _) => self.selected_id(),
            _ => None,
        };

        self.select_aux = match self.ui_mode {
            UIMode::Select(_, SelectionPurpose::Favourite) => {
                match self.selected_id() {
                    Some(id) => match client.status_by_id(&id) {
                        Ok(st) => Some(st.favourited == Some(true)),
                        Err(_) => Some(false),
                    },
                    None => Some(false),
                }
            }
            UIMode::Select(_, SelectionPurpose::Boost) => {
                match self.selected_id() {
                    Some(id) => match client.status_by_id(&id) {
                        Ok(st) => Some(st.reblogged == Some(true)),
                        Err(_) => Some(false),
                    },
                    None => Some(false),
                }
            }
            _ => None,
        };

        result
    }

    fn selection_up(&mut self, client: &mut Client) -> LogicalAction {
        let htype = match self.ui_mode {
            UIMode::Select(htype, _purpose) => htype,
            _ => return LogicalAction::Beep,
        };

        let new_selection = match self.selection {
            None => None,
            Some((item, sub)) => {
                if sub > 0 {
                    Some((item, sub - 1))
                } else {
                    self.last_selectable_above(htype, item - 1)
                }
            }
        };

        self.change_selection_to(new_selection, false, client)
    }

    fn selection_down(&mut self, client: &mut Client) -> LogicalAction {
        let htype = match self.ui_mode {
            UIMode::Select(htype, _purpose) => htype,
            _ => return LogicalAction::Beep,
        };

        let new_selection = match self.selection {
            None => None,
            Some((item, sub)) => {
                let count =
                    self.contents.get(item).count_highlightables(htype);
                if sub + 1 < count {
                    Some((item, sub + 1))
                } else {
                    self.first_selectable_below(htype, item + 1)
                }
            }
        };

        self.change_selection_to(new_selection, false, client)
    }

    fn vote(&mut self) -> LogicalAction {
        let (item, sub) = self
            .selection
            .expect("we should only call this if we have a selection");
        self.selection_restricted = true;
        if self.contents.get(item).is_multiple_choice_poll() {
            if self.display_styles.selected_poll_options.contains(&sub) {
                self.display_styles.selected_poll_options.remove(&sub);
            } else {
                self.display_styles.selected_poll_options.insert(sub);
            }
        } else {
            self.display_styles.selected_poll_options.clear();
            self.display_styles.selected_poll_options.insert(sub);
        }
        self.rerender_selected_item();
        self.make_screen_layout();
        LogicalAction::Nothing
    }

    fn end_selection(&mut self) {
        if self.selection.is_some() {
            self.rerender_selected_item();
            self.selection = None;
            self.selection_restricted = false;
            self.make_screen_layout();
        }
        self.display_styles.selected_poll_id = None;
        self.display_styles.selected_poll_options.clear();
        self.ui_mode = UIMode::Normal;
    }

    fn abort_selection(&mut self) -> LogicalAction {
        self.end_selection();
        LogicalAction::Nothing
    }

    fn selected_id(&self) -> Option<String> {
        let htype = match self.ui_mode {
            UIMode::Select(htype, _purpose) => htype,
            _ => return None,
        };

        match self.selection {
            Some((item, sub)) => self
                .contents
                .get(item)
                .highlighted_id(Some(Highlight(htype, sub))),
            None => None,
        }
    }

    fn complete_selection(
        &mut self,
        client: &mut Client,
        alt: bool,
    ) -> LogicalAction {
        let (_htype, purpose) = match self.ui_mode {
            UIMode::Select(htype, purpose) => (htype, purpose),
            _ => return LogicalAction::Beep,
        };

        match purpose {
            SelectionPurpose::Favourite | SelectionPurpose::Boost => {
                // alt means unfave; select_aux = Some(already faved?)
                if self.select_aux == Some(!alt) {
                    return LogicalAction::Nothing;
                }
            }
            _ => (),
        }

        let result = if let Some(id) = self.selected_id() {
            match purpose {
                SelectionPurpose::ExamineUser => LogicalAction::Goto(
                    UtilityActivity::ExamineUser(id).into(),
                ),
                SelectionPurpose::StatusInfo => {
                    LogicalAction::Goto(UtilityActivity::InfoStatus(id).into())
                }
                SelectionPurpose::HttpInfo => {
                    LogicalAction::Goto(UtilityActivity::InfoHttp(id).into())
                }
                SelectionPurpose::Favourite => {
                    match client.favourite_post(&id, !alt) {
                        Ok(_) => self.update_items(client),
                        Err(_) => LogicalAction::Beep,
                    }
                }
                SelectionPurpose::Boost => {
                    match client.boost_post(&id, !alt) {
                        Ok(_) => self.update_items(client),
                        Err(_) => LogicalAction::Beep,
                    }
                }
                SelectionPurpose::Unfold => {
                    let did_something =
                        if let Some(ref rc) = self.display_styles.unfolded {
                            let mut unfolded = rc.borrow_mut();
                            if !unfolded.remove(&id) {
                                unfolded.insert(id);
                            }
                            true
                        } else {
                            false
                        };

                    if did_something {
                        self.rendered.clear();
                    }
                    LogicalAction::Nothing
                }
                SelectionPurpose::Reply => LogicalAction::Goto(
                    ComposeActivity::ComposeReply(id).into(),
                ),
                SelectionPurpose::Thread => LogicalAction::Goto(
                    NonUtilityActivity::ThreadFile(id, alt).into(),
                ),
                SelectionPurpose::Vote => {
                    match client.vote_in_poll(
                        &id,
                        self.display_styles
                            .selected_poll_options
                            .iter()
                            .copied(),
                    ) {
                        Ok(_) => self.update_items(client),
                        Err(_) => LogicalAction::Beep,
                    }
                }
                SelectionPurpose::Edit => match client.status_source(&id) {
                    Ok(src) => LogicalAction::Goto(
                        ComposeActivity::EditExistingPost(src).into(),
                    ),
                    Err(_) => LogicalAction::Beep,
                },
            }
        } else {
            LogicalAction::Beep
        };

        self.end_selection();

        result
    }

    fn search(&mut self) -> LogicalAction {
        let (w, _h) = self.last_size().expect("search before setting width");

        if let Some(dir) = self.search_direction {
            if self.last_search.is_none() {
                return LogicalAction::Beep;
            }

            let layout = self.make_screen_layout();
            let mut item = layout.botitem;
            let mut itemline = layout.botpos.numerator - 1;
            let mut itemlen = layout.botpos.denominator;

            loop {
                // Move to the next or previous line
                match dir {
                    SearchDirection::Up => {
                        if itemline > 0 {
                            itemline -= 1;
                        } else if item > 0 {
                            item -= 1;
                            itemlen = self.ensure_item_rendered(item, w).len();
                            itemline = itemlen - 1;
                        } else {
                            break LogicalAction::Beep;
                        }
                    }
                    SearchDirection::Down => {
                        itemline += 1;
                        if itemline == itemlen {
                            if item == self.contents.last_index() {
                                break LogicalAction::Beep;
                            } else {
                                item += 1;
                                itemline = 0;
                                itemlen =
                                    self.ensure_item_rendered(item, w).len();
                            }
                        }
                    }
                };

                let rendered = self
                    .get_rendered_item(item)
                    .expect("we should have just rendered it");
                if let Some(line) = rendered.get(itemline) {
                    if self
                        .last_search
                        .as_ref()
                        .expect("we just checked it above")
                        .find(line.text())
                        .is_some()
                    {
                        self.set_pos(FilePosition::item_pos(
                            item,
                            itemline + 1,
                            itemlen,
                        ));
                        break LogicalAction::Nothing;
                    }
                }
            }
        } else {
            LogicalAction::Beep
        }
    }
}

impl<Type: FileType, Source: FileDataSource> ActivityState
    for File<Type, Source>
{
    fn resize(&mut self, w: usize, h: usize) {
        if self.last_size() != Some((w, h)) {
            self.rendered.clear();
            self.make_screen_layout_for_size_and_pos(w, h, self.pos.clone());
        }
    }

    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let layout = self
            .last_layout
            .clone()
            .expect("draw() without a screen layout");
        assert_eq!(
            (layout.width, layout.height),
            (w, h),
            "last resize() inconsistent with draw()"
        );
        let mut lines = Vec::new();

        for item in layout.topitem..=layout.botitem {
            let rendered = self
                .get_rendered_item(item)
                .expect("unrendered item reached draw()");

            let firstline = if item == layout.topitem {
                layout.toppos.numerator
            } else {
                0
            };
            let linelimit = if item == layout.botitem {
                layout.botpos.numerator
            } else {
                rendered.len()
            };
            for line in &rendered[firstline..linelimit] {
                lines.push(line.clone());
            }
        }

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        let fs = FileStatusLine::new();
        let fs = match self.ui_mode {
            UIMode::Normal => {
                let fs = if self.layout_at_bottom(&layout) {
                    fs.add(Pr('-'), "Up", 99)
                } else {
                    fs.add(Space, "Down", 99)
                };
                let fs =
                    if Type::Item::can_highlight(HighlightType::WholeStatus) {
                        fs.add(Pr('s'), "Reply", 42)
                    } else {
                        fs
                    };
                let fs = if Type::Item::can_highlight(HighlightType::User) {
                    fs.add(Pr('e'), "Examine", 40)
                } else {
                    fs
                };
                let fs = if Type::CAN_GET_POSTS {
                    fs.add(Pr('p'), "Show Posts", 41)
                } else {
                    fs
                };
                let fs = if self.file_desc.can_list() != CanList::Nothing {
                    fs.add(Pr('l'), "List", 40)
                } else {
                    fs
                };
                let fs = if Type::Item::can_highlight(HighlightType::Status) {
                    fs.add(Pr('i'), "Post Info", 38)
                } else {
                    fs
                };
                let fs = if Type::Item::can_highlight(
                    HighlightType::HttpLogEntry,
                ) {
                    fs.add(Pr('i'), "More Info", 38)
                } else {
                    fs
                };
                let fs = if Type::Item::can_highlight(HighlightType::Status) {
                    fs.add(Pr('t'), "Thread", 35)
                } else {
                    fs
                };
                let fs = if Type::Item::can_highlight(HighlightType::Status)
                    && self.display_styles.unfolded.is_some()
                {
                    fs.add(Pr('u'), "Unfold", 39)
                } else {
                    fs
                };
                let fs =
                    if Type::Item::can_highlight(HighlightType::WholeStatus) {
                        fs.add(Pr('f'), "Fave", 41).add(Ctrl('B'), "Boost", 41)
                    } else {
                        fs
                    };
                let fs =
                    if Type::Item::can_highlight(HighlightType::PollOption) {
                        fs.add(Ctrl('V'), "Vote", 10)
                    } else {
                        fs
                    };
                let fs = if Type::Item::can_highlight(HighlightType::OurStatus)
                {
                    fs.add(Pr('.'), "Edit", 20)
                } else {
                    fs
                };
                let fs = if Type::IS_EXAMINE_USER {
                    let fs = fs.add(Pr('O'), "Options", 41);
                    if Some(self.contents.source.single_id())
                        == self.our_account_id
                    {
                        fs.add(Pr('V'), "View Errors", 1)
                    } else {
                        fs
                    }
                } else {
                    fs
                };
                let fs = fs
                    .add(Pr('/'), "Search Down", 20)
                    .add(Pr('\\'), "Search Up", 20)
                    .add(Pr('q'), "Exit", 100);

                // We calculate the percentage through the file in a
                // loose sort of way, by assuming all items are the
                // same size, and only calculating a partial item for
                // the one we're actually in the middle of. This isn't
                // how Mono did it, but Mono didn't have to
                // dynamically rewrap whenever the window resized.
                //
                // (A robust way to get precise line-based percentages
                // even so would be to eagerly rewrap the entire
                // collection of items on every resize, but I don't
                // think that's a sensible tradeoff!)
                fs.set_proportion(
                    layout.botitem * layout.botpos.denominator
                        + layout.botpos.numerator,
                    self.contents.index_limit() * layout.botpos.denominator,
                )
            }
            UIMode::ListSubmenu => {
                let fs =
                    match self.file_desc.can_list() {
                        CanList::ForUser(locked) => {
                            let fs = fs
                                .add(Pr('I'), "List Followers", 98)
                                .add(Pr('O'), "List Followed", 98);
                            if locked {
                                fs.add(Pr('R'), "List Requesters", 99)
                            } else {
                                fs
                            }
                        }
                        CanList::ForPost => fs
                            .add(Pr('F'), "List Favouriters", 99)
                            .add(Pr('B'), "List Boosters", 99),
                        CanList::Nothing => {
                            panic!("Then we shouldn't be in this submenu")
                        }
                    };
                fs.add(Pr('Q'), "Quit", 100)
            }
            UIMode::PostsSubmenu => {
                assert!(
                    Type::CAN_GET_POSTS,
                    "How did we get here if !CAN_GET_POSTS?"
                );
                fs.add(Pr('A'), "All", 99)
                    .add(Pr('O'), "Original", 97)
                    .add(Pr('T'), "Top-level", 98)
                    .add(Pr('Q'), "Quit", 100)
            }
            UIMode::Select(_htype, purpose) => {
                let fs = match purpose {
                    SelectionPurpose::ExamineUser => {
                        fs.add(Space, "Examine", 98)
                    }
                    SelectionPurpose::StatusInfo
                    | SelectionPurpose::HttpInfo => fs.add(Space, "Info", 98),
                    SelectionPurpose::Reply => fs.add(Space, "Reply", 98),
                    SelectionPurpose::Favourite => {
                        if self.select_aux == Some(true) {
                            fs.add(Pr('D'), "Unfave", 98)
                        } else {
                            fs.add(Space, "Fave", 98)
                        }
                    }
                    SelectionPurpose::Boost => {
                        if self.select_aux == Some(true) {
                            fs.add(Pr('D'), "Unboost", 98)
                        } else {
                            fs.add(Space, "Boost", 98)
                        }
                    }
                    SelectionPurpose::Thread => fs
                        .add(Space, "Thread Context", 98)
                        .add(Pr('F'), "Full Thread", 97),
                    SelectionPurpose::Vote => {
                        // Different verb for selecting items
                        // depending on whether the vote lets you
                        // select more than one
                        let (item, _sub) = self.selection.expect("UIMode::Select ought to mean selection is not None");
                        let verb = if self
                            .contents
                            .get(item)
                            .is_multiple_choice_poll()
                        {
                            "Toggle"
                        } else {
                            "Select"
                        };

                        // If you've selected nothing yet, prioritise
                        // the keypress for selecting something.
                        // Otherwise, prioritise the one for
                        // submitting your answer.
                        if self.display_styles.selected_poll_options.is_empty()
                        {
                            fs.add(Space, verb, 98).add(
                                Ctrl('V'),
                                "Submit Vote",
                                97,
                            )
                        } else {
                            fs.add(Space, verb, 97).add(
                                Ctrl('V'),
                                "Submit Vote",
                                98,
                            )
                        }
                    }
                    SelectionPurpose::Unfold => {
                        let (item, _sub) = self.selection.expect("UIMode::Select ought to mean selection is not None");
                        let unfolded = self
                            .contents
                            .id_at_index(item)
                            .map_or(false, |id| {
                                self.display_styles.unfolded(id)
                            });
                        let verb = if unfolded { "Fold" } else { "Unfold" };
                        fs.add(Space, verb, 98)
                    }
                    SelectionPurpose::Edit => fs.add(Space, "Edit", 98),
                };
                fs.add(Pr('+'), "Down", 99).add(Pr('-'), "Up", 99).add(
                    Pr('Q'),
                    "Quit",
                    100,
                )
            }
        };

        // Include [?]:Help everywhere
        let fs = fs.add(Pr('?'), "Help", 101).finalise();

        lines.extend_from_slice(&fs.render(w));

        (lines, CursorPosition::End)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        let (_w, h) = match self.last_size() {
            Some(size) => size,
            None => panic!("handle_keypress before resize"),
        };

        self.update_latest_read_index();

        let action = if key == Pr('?') {
            LogicalAction::Help
        } else {
            // This whole match is in a lambda which we immediately call,
            // so that if any clause needs to do an early return, it will
            // only return from this lambda and not the whole function.
            // That way we never miss the update_latest_read_index at the
            // end.
            (|| match self.ui_mode {
                UIMode::Normal => match key {
                    Pr('q') | Pr('Q') => LogicalAction::Pop,
                    Up => {
                        self.move_up(1);
                        LogicalAction::Nothing
                    }
                    Pr('-') | Pr('b') | Pr('B') | PgUp | Left => {
                        self.move_up(max(1, h - min(h, 3)));
                        LogicalAction::Nothing
                    }
                    Down => {
                        self.move_down(1);
                        LogicalAction::Nothing
                    }
                    Return => {
                        if self.at_bottom() {
                            LogicalAction::Pop
                        } else {
                            self.move_down(1);
                            LogicalAction::Nothing
                        }
                    }
                    Space | PgDn | Right => {
                        self.move_down(max(1, h - min(h, 3)));
                        LogicalAction::Nothing
                    }
                    Pr('0') | Home => {
                        if self.at_top() && self.contents.extender.is_some() {
                            match self.try_extend(client) {
                                Ok(logact) => logact,
                                Err(e) => LogicalAction::Error(e),
                            }
                        } else {
                            self.goto_top();
                            LogicalAction::Nothing
                        }
                    }
                    Pr('z') | Pr('Z') | End => {
                        self.goto_bottom();
                        LogicalAction::Nothing
                    }

                    Pr('e') | Pr('E') => {
                        if Type::Item::can_highlight(HighlightType::User) {
                            self.start_selection(
                                HighlightType::User,
                                SelectionPurpose::ExamineUser,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('i') | Pr('I') => {
                        if Type::Item::can_highlight(HighlightType::Status) {
                            self.start_selection(
                                HighlightType::Status,
                                SelectionPurpose::StatusInfo,
                                client,
                            )
                        } else if Type::Item::can_highlight(
                            HighlightType::HttpLogEntry,
                        ) {
                            self.start_selection(
                                HighlightType::HttpLogEntry,
                                SelectionPurpose::HttpInfo,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('u') | Pr('U') => {
                        if Type::Item::can_highlight(
                            HighlightType::FoldableStatus,
                        ) && self.display_styles.unfolded.is_some()
                        {
                            self.start_selection(
                                HighlightType::FoldableStatus,
                                SelectionPurpose::Unfold,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('f') | Pr('F') => {
                        if Type::Item::can_highlight(
                            HighlightType::WholeStatus,
                        ) {
                            self.start_selection(
                                HighlightType::WholeStatus,
                                SelectionPurpose::Favourite,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Ctrl('B') => {
                        if Type::Item::can_highlight(
                            HighlightType::WholeStatus,
                        ) {
                            self.start_selection(
                                HighlightType::WholeStatus,
                                SelectionPurpose::Boost,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('s') | Pr('S') => {
                        if Type::Item::can_highlight(
                            HighlightType::WholeStatus,
                        ) {
                            self.start_selection(
                                HighlightType::WholeStatus,
                                SelectionPurpose::Reply,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('t') | Pr('T') => {
                        if Type::Item::can_highlight(HighlightType::Status) {
                            self.start_selection(
                                HighlightType::Status,
                                SelectionPurpose::Thread,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Ctrl('V') => {
                        if Type::Item::can_highlight(HighlightType::PollOption)
                        {
                            self.start_selection(
                                HighlightType::PollOption,
                                SelectionPurpose::Vote,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('.') => {
                        if Type::Item::can_highlight(HighlightType::OurStatus)
                        {
                            self.start_selection(
                                HighlightType::OurStatus,
                                SelectionPurpose::Edit,
                                client,
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('l') | Pr('L') => {
                        if self.file_desc.can_list() != CanList::Nothing {
                            self.ui_mode = UIMode::ListSubmenu;
                        }
                        LogicalAction::Nothing
                    }

                    Pr('/') | Pr('\\') => {
                        let search_direction = match key {
                            Pr('/') => SearchDirection::Down,
                            Pr('\\') => SearchDirection::Up,
                            _ => panic!("how are we in this arm anyway?"),
                        };
                        self.search_direction = Some(search_direction);
                        LogicalAction::Goto(
                            OverlayActivity::GetSearchExpression(
                                search_direction,
                            )
                            .into(),
                        )
                    }

                    Pr('n') | Pr('N') => self.search(),

                    Pr('p') | Pr('P') => {
                        if Type::CAN_GET_POSTS {
                            self.ui_mode = UIMode::PostsSubmenu;
                        }
                        LogicalAction::Nothing
                    }

                    Pr('o') | Pr('O') => {
                        if Type::IS_EXAMINE_USER {
                            LogicalAction::Goto(
                                UtilityActivity::UserOptions(
                                    self.contents.source.single_id(),
                                )
                                .into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('v') | Pr('V') => {
                        if Type::IS_EXAMINE_USER
                            && Some(self.contents.source.single_id())
                                == self.our_account_id
                        {
                            LogicalAction::Goto(
                                UtilityActivity::ErrorLog.into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    _ => LogicalAction::Nothing,
                },
                UIMode::ListSubmenu => match key {
                    Pr('f') | Pr('F') => {
                        if self.file_desc.can_list() == CanList::ForPost {
                            LogicalAction::Goto(
                                UtilityActivity::ListStatusFavouriters(
                                    self.contents.source.single_id(),
                                )
                                .into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('b') | Pr('B') => {
                        if self.file_desc.can_list() == CanList::ForPost {
                            LogicalAction::Goto(
                                UtilityActivity::ListStatusBoosters(
                                    self.contents.source.single_id(),
                                )
                                .into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('i') | Pr('I') => {
                        if let CanList::ForUser(_) = self.file_desc.can_list()
                        {
                            LogicalAction::Goto(
                                UtilityActivity::ListUserFollowers(
                                    self.contents.source.single_id(),
                                )
                                .into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('o') | Pr('O') => {
                        if let CanList::ForUser(_) = self.file_desc.can_list()
                        {
                            LogicalAction::Goto(
                                UtilityActivity::ListUserFollowees(
                                    self.contents.source.single_id(),
                                )
                                .into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('r') | Pr('R') => {
                        if self.file_desc.can_list() == CanList::ForUser(true)
                        {
                            LogicalAction::Goto(
                                UtilityActivity::ListFollowRequesters.into(),
                            )
                        } else {
                            LogicalAction::Nothing
                        }
                    }

                    Pr('q') | Pr('Q') => {
                        self.ui_mode = UIMode::Normal;
                        LogicalAction::Nothing
                    }
                    _ => LogicalAction::Nothing,
                },
                UIMode::PostsSubmenu => match key {
                    Pr('a') | Pr('A') => LogicalAction::Goto(
                        NonUtilityActivity::UserPosts(
                            self.contents.source.single_id(),
                            Boosts::Show,
                            Replies::Show,
                        )
                        .into(),
                    ),
                    Pr('o') | Pr('O') => LogicalAction::Goto(
                        NonUtilityActivity::UserPosts(
                            self.contents.source.single_id(),
                            Boosts::Hide,
                            Replies::Show,
                        )
                        .into(),
                    ),
                    Pr('t') | Pr('T') => LogicalAction::Goto(
                        NonUtilityActivity::UserPosts(
                            self.contents.source.single_id(),
                            Boosts::Hide,
                            Replies::Hide,
                        )
                        .into(),
                    ),
                    Pr('q') | Pr('Q') => {
                        self.ui_mode = UIMode::Normal;
                        LogicalAction::Nothing
                    }
                    _ => LogicalAction::Nothing,
                },
                UIMode::Select(_, purpose) => match key {
                    Space => match purpose {
                        SelectionPurpose::Vote => self.vote(),
                        _ => self.complete_selection(client, false),
                    },
                    Pr('d') | Pr('D') => match purpose {
                        SelectionPurpose::Favourite
                        | SelectionPurpose::Boost => {
                            self.complete_selection(client, true)
                        }
                        _ => LogicalAction::Nothing,
                    },
                    Pr('f') | Pr('F') => match purpose {
                        SelectionPurpose::Thread => {
                            self.complete_selection(client, true)
                        }
                        _ => LogicalAction::Nothing,
                    },
                    Ctrl('V') => match purpose {
                        SelectionPurpose::Vote => {
                            self.complete_selection(client, false)
                        }
                        _ => LogicalAction::Nothing,
                    },
                    Pr('-') | Up => self.selection_up(client),
                    Pr('+') | Down => self.selection_down(client),
                    Pr('q') | Pr('Q') => self.abort_selection(),
                    _ => LogicalAction::Nothing,
                },
            })()
        };

        self.update_latest_read_index();

        action
    }

    fn handle_feed_updates(
        &mut self,
        feeds_updated: &HashSet<AnyFeedId>,
        client: &mut Client,
    ) -> LogicalAction {
        if self.contents.source.updated(feeds_updated) {
            let logact = self.update_items(client);
            self.rendered.clear();
            self.make_screen_layout();
            logact
        } else {
            LogicalAction::Nothing
        }
    }

    fn save_file_position(&self) -> Option<(FeedId, SavedFilePos)> {
        self.file_desc.feed_id().map(|id| {
            let sfp = SavedFilePos {
                file_pos: self
                    .contents
                    .id_at_index(self.pos.item)
                    .map(|id| self.pos.convert(id.to_owned())),
                latest_read_id: self
                    .latest_read_index
                    .and_then(|i| self.contents.id_at_index(i))
                    .map(|s| s.to_owned()),
            };
            (id.clone(), sfp)
        })
    }

    fn got_search_expression(
        &mut self,
        dir: SearchDirection,
        regex: String,
    ) -> LogicalAction {
        if regex.is_empty() {
            if self.last_search.is_some() {
                self.search_direction = Some(dir);
                self.search()
            } else {
                LogicalAction::Goto(
                    OverlayActivity::BottomLineError(
                        "No previous search expression".to_owned(),
                    )
                    .into(),
                )
            }
        } else {
            match RegexBuilder::new(&regex).case_insensitive(true).build() {
                Ok(re) => {
                    self.search_direction = Some(dir);
                    self.last_search = Some(re);
                    self.search()
                }

                // FIXME: it would be nice to report the full syntax error
                // from regex::Error here, but that involves work to
                // format it nicely, and may also require BottomLineError
                // to learn how to span more than one line at the bottom
                // of the screen.
                Err(_) => LogicalAction::Goto(
                    OverlayActivity::BottomLineError(
                        "Invalid regular expression".to_owned(),
                    )
                    .into(),
                ),
            }
        }
    }

    fn refresh_content(&mut self, client: &mut Client) {
        self.contents.update_items(&self.file_desc, client);
        self.make_screen_layout();
    }

    fn show_new_content(&mut self, client: &mut Client) {
        self.contents.update_items(&self.file_desc, client);
        if let Some(index) = self.latest_read_index {
            if self.contents.last_index() > index {
                self.pos = FilePosition::item_top(index + 1);
                self.make_screen_layout();
            }
        }
    }
}

pub fn home_timeline(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::Home;
    let pos = file_positions.get(&feed.clone().into());
    let desc = StatusFeedType::with_feed(feed.clone());
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::general("Home timeline   <H>", "HHHHHHHHHHHHHHHHHKH"),
        desc,
        pos.map(Into::into),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

pub fn local_timeline(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::Local;
    let pos = file_positions.get(&feed.clone().into());
    let desc = StatusFeedType::with_feed(feed.clone());
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::general(
            "Local public timeline   <L>",
            "HHHHHHHHHHHHHHHHHHHHHHHHHKH",
        ),
        desc,
        pos.map(Into::into),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

pub fn public_timeline(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::Public;
    let pos = file_positions.get(&feed.clone().into());
    let desc = StatusFeedType::with_feed(feed.clone());
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::general(
            "Public timeline   <P>",
            "HHHHHHHHHHHHHHHHHHHKH",
        ),
        desc,
        pos.map(Into::into),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

pub fn mentions(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
    is_interrupt: bool,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::Mentions;
    let pos = file_positions.get(&feed.clone().into());
    let desc = NotificationStatusFeedType::with_feed(feed.clone());
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::general("Mentions   [ESC][R]", "HHHHHHHHHHHHKKKHHKH"),
        desc,
        pos.map(Into::into),
        Some(unfolded),
        is_interrupt,
    )?;
    Ok(Box::new(file))
}

pub fn ego_log(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::Ego;
    let pos = file_positions.get(&feed.clone().into());
    let desc = EgoNotificationFeedType::with_feed(feed.clone());
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::general(
            "Ego Log   [ESC][L][L][E]",
            "HHHHHHHHHHHKKKHHKHHKHHKH",
        ),
        desc,
        pos.map(Into::into),
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn user_posts(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
    user: &str,
    boosts: Boosts,
    replies: Replies,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let feed = FeedId::User(user.to_owned(), boosts, replies);
    let pos = file_positions.get(&feed.clone().into());
    let desc = StatusFeedType::with_feed(feed.clone());

    let ac = client.account_by_id(user)?;
    let username = client.fq(&ac.acct);

    let title = match (boosts, replies) {
        (Boosts::Hide, Replies::Hide) => {
            format!("Top-level posts by {username}")
        }
        (Boosts::Hide, Replies::Show) => {
            format!("Original posts by {username}")
        }
        (Boosts::Show, Replies::Show) => format!("All posts by {username}"),
        // We don't currently have a UI for asking for this
        // combination, but we can't leave it out of the match or Rust
        // will complain, so we might as well write a title in case we
        // ever decide to add it for some reason
        (Boosts::Show, Replies::Hide) => {
            format!("Boosts and top-level posts by {username}")
        }
    };
    let file = File::new(
        client,
        FeedSource::new(feed),
        ColouredString::uniform(&title, 'H'),
        desc,
        pos.map(Into::into),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

pub fn list_status_favouriters(
    client: &mut Client,
    id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let file = File::new(
        client,
        FeedSource::new(FeedId::Favouriters(id.to_owned())),
        ColouredString::uniform(
            &format!("Users who favourited post {id}"),
            'H',
        ),
        UserListFeedType {},
        None,
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn list_status_boosters(
    client: &mut Client,
    id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let file = File::new(
        client,
        FeedSource::new(FeedId::Boosters(id.to_owned())),
        ColouredString::uniform(&format!("Users who boosted post {id}"), 'H'),
        UserListFeedType {},
        None,
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn list_user_followers(
    client: &mut Client,
    id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let ac = client.account_by_id(id)?;
    let name = client.fq(&ac.acct);

    let file = File::new(
        client,
        FeedSource::new(FeedId::Followers(id.to_owned())),
        ColouredString::uniform(&format!("Users who follow {name}"), 'H'),
        UserListFeedType {},
        None,
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn list_user_followees(
    client: &mut Client,
    id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let ac = client.account_by_id(id)?;
    let name = client.fq(&ac.acct);

    let file = File::new(
        client,
        FeedSource::new(FeedId::Followees(id.to_owned())),
        ColouredString::uniform(&format!("Users who {name} follows"), 'H'),
        UserListFeedType {},
        None,
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn list_follow_requesters(
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let name = client.our_account_fq();

    let file = File::new(
        client,
        FeedSource::new(FeedId::YourFollowRequesters),
        ColouredString::uniform(
            &format!("Users who have requested to follow {name}"),
            'H',
        ),
        UserListFeedType {},
        None,
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn hashtag_timeline(
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
    tag: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let title = ColouredString::uniform(
        &format!("Posts mentioning hashtag #{tag}"),
        'H',
    );
    let feed = FeedId::Hashtag(tag.to_owned());
    let desc = StatusFeedType::with_feed(feed);
    let file = File::new(
        client,
        FeedSource::new(FeedId::Hashtag(tag.to_owned())),
        title,
        desc,
        None,
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

struct ExamineUserFileType {
    locked: bool,
}
impl FileType for ExamineUserFileType {
    type Item = ExamineUserDisplay;
    const CAN_GET_POSTS: bool = true;
    const IS_EXAMINE_USER: bool = true;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let ac = client.account_by_id(id)?;
        ExamineUserDisplay::new(ac, client)
    }

    fn can_list(&self) -> CanList {
        CanList::ForUser(self.locked)
    }
}

pub fn examine_user(
    client: &mut Client,
    account_id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let ac = client.account_by_id(account_id)?;
    let locked = ac.id == client.our_account_id() && ac.locked;
    let username = client.fq(&ac.acct);
    let title = ColouredString::uniform(
        &format!("Information about user {username}"),
        'H',
    );

    let file = File::new(
        client,
        StaticSource::singleton(ac.id),
        title,
        ExamineUserFileType { locked },
        Some(StartingPosition::Top),
        None,
        false,
    )?;
    Ok(Box::new(file))
}

struct DetailedStatusFileType {}
impl FileType for DetailedStatusFileType {
    type Item = DetailedStatusDisplay;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let st = client.status_by_id(id)?;
        Ok(DetailedStatusDisplay::new(st, client))
    }

    fn can_list(&self) -> CanList {
        CanList::ForPost
    }
}

pub fn view_single_post(
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
    status_id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let st = client.status_by_id(status_id)?;
    let title = ColouredString::uniform(
        &format!("Information about post {}", st.id),
        'H',
    );

    let file = File::new(
        client,
        StaticSource::singleton(st.id),
        title,
        DetailedStatusFileType {},
        Some(StartingPosition::Top),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

struct InstanceRulesFileType;
impl FileType for InstanceRulesFileType {
    type Item = InstanceRulesDisplay;

    fn get_item(
        &self,
        _id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let inst = client.instance()?;
        Ok(InstanceRulesDisplay::new(inst))
    }
}

pub fn view_instance_rules(
    client: &mut Client,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let inst = client.instance()?;
    let title = ColouredString::uniform(
        &format!("Rules for Mastodon instance {}", inst.domain),
        'H',
    );

    let file = File::new(
        client,
        StaticSource::singleton("unused".to_owned()),
        title,
        InstanceRulesFileType,
        Some(StartingPosition::Top),
        None,
        false,
    )?;
    Ok(Box::new(file))
}

pub fn view_thread(
    unfolded: Rc<RefCell<HashSet<String>>>,
    client: &mut Client,
    start_id: &str,
    full: bool,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let mut make_vec = |id: &str| -> Result<Vec<String>, ClientError> {
        let ctx = client.status_context(id)?;
        let mut v = Vec::new();
        for st in &ctx.ancestors {
            v.push(st.id.to_owned());
        }
        v.push(id.to_owned());
        for st in &ctx.descendants {
            v.push(st.id.to_owned());
        }
        Ok(v)
    };

    let ids = make_vec(start_id)?;
    let is_full = ids[0] == start_id;
    let (ids, is_full) = if full && !is_full {
        (make_vec(&ids[0])?, true)
    } else {
        (ids, is_full)
    };

    let title = if is_full {
        format!("Full thread of post {}", start_id)
    } else {
        format!("Thread context of post {}", start_id)
    };
    let title = ColouredString::uniform(&title, 'H');

    let file = File::new(
        client,
        StaticSource::vector(ids),
        title,
        StatusFeedType::without_feed(),
        Some(StartingPosition::Saved(&SavedFilePos {
            file_pos: Some(FilePosition::item_centre(start_id.to_owned())),
            latest_read_id: None,
        })),
        Some(unfolded),
        false,
    )?;
    Ok(Box::new(file))
}

struct ErrorLogFileType {
    #[allow(unused)] // this does its job by merely existing
    log_lock: LogLock,
}
impl FileType for ErrorLogFileType {
    type Item = ErrorLogEntry;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let (err, time) = client.get_error_log_entry(id);
        Ok(ErrorLogEntry::new(err, time))
    }
}

pub fn error_log(
    file_positions: &HashMap<AnyFeedId, SavedFilePos>,
    client: &mut Client,
    is_interrupt: bool,
) -> Box<dyn ActivityState> {
    let feed = LocalFeedId::Errors;
    let pos = file_positions.get(&feed.into());
    let title = ColouredString::general(
        "Error Log   [ESC][Y][V]",
        "HHHHHHHHHHHHHKKKHHKHHKH",
    );

    let file = File::new(
        client,
        LocalFeedSource::new(feed),
        title,
        ErrorLogFileType {
            log_lock: client.error_log_lock(),
        },
        pos.map(Into::into),
        None,
        is_interrupt,
    )
    .void_unwrap();
    Box::new(file)
}

struct HttpLogFileType {
    #[allow(unused)] // this does its job by merely existing
    log_lock: LogLock,
}
impl FileType for HttpLogFileType {
    type Item = HttpLogEntry;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        Ok(HttpLogEntry::new(
            id.to_owned(),
            client.get_http_log_entry(id),
            client,
        ))
    }
}

pub fn http_log(client: &mut Client) -> Box<dyn ActivityState> {
    let feed = LocalFeedId::HttpTransactions;
    let title = ColouredString::general(
        "HTTP Log   [ESC][L][H]",
        "HHHHHHHHHHHHKKKHHKHHKH",
    );

    let file = File::new(
        client,
        LocalFeedSource::new(feed),
        title,
        HttpLogFileType {
            log_lock: client.http_log_lock(),
        },
        Some(StartingPosition::Bottom),
        None,
        false,
    )
    .void_unwrap();
    Box::new(file)
}

struct HttpTransactionFileType {}
impl FileType for HttpTransactionFileType {
    type Item = HttpTransactionDisplay;

    fn get_item(
        &self,
        id: &str,
        client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        let tl = client.get_http_log_entry(id);
        Ok(HttpTransactionDisplay::new(tl))
    }
}

pub fn view_http_transaction(
    client: &mut Client,
    transaction_id: &str,
) -> Result<Box<dyn ActivityState>, ClientError> {
    let title =
        ColouredString::uniform("Information about an HTTP transaction", 'H');

    let file = File::new(
        client,
        StaticSource::singleton(transaction_id.to_owned()),
        title,
        HttpTransactionFileType {},
        Some(StartingPosition::Top),
        None,
        false,
    )?;
    Ok(Box::new(file))
}

struct HelpFileType {
    items: HashMap<String, HelpItem>,
}

impl FileType for HelpFileType {
    type Item = HelpItem;

    fn get_item(
        &self,
        id: &str,
        _client: &mut Client,
    ) -> Result<Self::Item, ClientError> {
        Ok(self.items[id].clone())
    }
}

pub fn help_file(
    client: &mut Client,
    activity: &Activity,
) -> Box<dyn ActivityState> {
    let help = super::help::help_for_activity(client, activity);

    let title = ColouredString::uniform(&help.title, 'H');
    let helpsource = StaticSource::vector(help.ids);
    let helpdesc = HelpFileType { items: help.items };

    let file = File::new(
        client,
        helpsource,
        title,
        helpdesc,
        Some(StartingPosition::Top),
        None,
        false,
    )
    .void_unwrap();
    Box::new(file)
}
