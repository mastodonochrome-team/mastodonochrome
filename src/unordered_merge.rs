use std::cmp::min;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum FindResult {
    Index(usize),
    Gap(usize),
}

pub fn find_new_pos<Item: Eq + Hash + Clone + Debug>(
    old_ids: &[Item],
    new_ids: &[Item],
    old_pos: usize,
) -> FindResult {
    use FindResult::*;

    // Map each new id to its index in the list
    let new_indices = HashMap::<Item, usize>::from_iter(
        new_ids.iter().enumerate().map(|(i, k)| (k.clone(), i)),
    );

    // If the specified old id is in the new list, just return its index
    if let Some(i) = new_indices.get(&old_ids[old_pos]) {
        return Index(*i);
    }

    // Find the smallest new id that appears in the old list after it,
    // and the largest one that appears before it.
    let smallest_new_after = old_ids[old_pos + 1..]
        .iter()
        .filter_map(|id| new_indices.get(id))
        .min();
    let largest_new_before = old_ids[..old_pos]
        .iter()
        .filter_map(|id| new_indices.get(id))
        .max();

    // One reasonable strategy is to insert the element just after
    // largest_new_before, or at the very start if that is None
    // (meaning no elements of the new list come before the old
    // element).
    let lnb_pos = largest_new_before.map_or(0, |pos| pos + 1);

    // Symmetrically, another possibility is to put it just before
    // smallest_new_after, or at the very end.
    let sna_pos = smallest_new_after.map_or_else(|| new_ids.len(), |pos| *pos);

    // Go with the min of those, which inserts the old element as
    // early as possible, on the general theory that lists move
    // forward in time, and in case of any doubt, things in the old
    // list are probably older.
    Gap(min(lnb_pos, sna_pos))
}

#[test]
fn test_find_new_pos() {
    use FindResult::*;

    // Test case where there's a consistent order between the lists
    let old_ids =
        vec!["surely", "the", "quick", "brown", "fox", "jumps", "quietly"];
    let new_ids = vec!["the", "fox", "jumps", "over", "things"];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Gap(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 1), Index(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 2), Gap(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 3), Gap(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 4), Index(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 5), Index(2));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 6), Gap(3));

    // Test the case where an old id comes after all the new ones. (We
    // couldn't test that _and_ the converse, in the same test case!)
    let old_ids = vec!["foo", "bar", "baz", "quux"];
    let new_ids = vec!["foo", "bar", "baz"];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Index(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 1), Index(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 2), Index(2));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 3), Gap(3));

    // Test cases where an old element doesn't have a new item on both
    // sides to anchor it. We expect oldstart to be sequenced before
    // newstart, and oldend before newend.
    let old_ids = vec!["oldstart", "common", "oldend"];
    let new_ids = vec!["newstart", "common", "newend"];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Gap(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 1), Index(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 2), Gap(2));

    // Test that _something_ vaguely plausible happens if items are
    // reordered between lists. "b" can't go both before new0 and
    // after new1, and we have to pick one. We put it before new0, on
    // the general principle of presuming things in the older list are
    // older rather than newer, if in any doubt.
    let old_ids = vec!["a", "new1", "b", "new0", "c"];
    let new_ids = vec!["new0", "new1"];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Gap(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 1), Index(1));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 2), Gap(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 3), Index(0));
    assert_eq!(find_new_pos(&old_ids, &new_ids, 4), Gap(2));

    // Degenerate case when the lists have nothing in common. We
    // expect the old item to be sequenced before the new one.
    let old_ids = vec!["a"];
    let new_ids = vec!["b"];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Gap(0));

    // Even more degenerate case when the new list is completely
    // empty. (The _old_ list can't be empty, or we wouldn't have any
    // element of it to search for in the first place.)
    let old_ids = vec!["a"];
    let new_ids = vec![];
    assert_eq!(find_new_pos(&old_ids, &new_ids, 0), Gap(0));
}
