# Mastodonochrome

Mastodonochrome is a terminal-based Mastodon client, with a user
interface modelled after [Monochrome BBS](https://www.mono.org/).

## Features

The reason I wrote Mastodonochrome is that I didn't know of any
existing Mastodon client with all of the following features:

* Runs in a terminal, so that I can use it over SSH.
* Can be run continuously, and shows updates to your home timeline as
  they appear.
* Shows your notifications and your home timeline integrated into the
  same user interface.
* One-column interface. In particular, when showing URLs, display them
  without any unrelated text to the left or right, so that they're
  easy to paste from the terminal into a browser.

I chose to mimic Monochrome in particular because it was a UI that I'm
already familiar with and have well established finger-macros for,
with all of those properties.

If you're a past Mono user yourself, maybe you'll feel at home with
this client. If you're not, but you still want the features in the
bullet list above, maybe it will still be useful to you.

## Status

At present, Mastodonochrome is **SIGNIFICANTLY UNFINISHED**. I'm using
it myself to read and post to Mastodon, but it lacks support for some
important Mastodon features (in particular, locked accounts and media
posting), and has UI deficiencies (draft posts, line-editing recall).

Also, it has no online help yet, and no separate documentation either.
So if you download and use it, be prepared to figure out what's going
on by reading the source code.

`TODO.md` contains a list of things I know aren't there yet.

## Licence

Mastodonochrome is free software, under the MIT licence. See the file
`LICENCE` for the full licence text.

## Source code note

Mastodonochrome is my first ever Rust program. It started off in
Python, but it was too slow, so I rewrote it in Rust. So the quality
of the Rust is probably not great.

Patches and help are welcome, but fair warning, it's probably less
easy to work with than the average reasonably polished Rust code you
might find on the Internet.
