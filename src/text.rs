#[cfg(test)]
use chrono::NaiveDateTime;
use chrono::{DateTime, Local, Utc};
use core::cmp::{max, min};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeMap, HashSet};
use std::ops::Add;
use unicode_width::UnicodeWidthStr;

use super::client::{
    possible_mastodon_url, Client, ClientError, ResolvedUrl,
    TransactionLogEntry,
};
use super::coloured_string::*;
use super::html;
use super::tui::{LogicalAction, OurKey};
use super::types::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum HighlightType {
    User,
    Status,
    WholeStatus,
    OurStatus,
    FoldableStatus,
    PollOption,
    HttpLogEntry,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Highlight(pub HighlightType, pub usize);

trait ConsumableHighlight {
    fn consume(&mut self, htype: HighlightType, n: usize) -> Option<usize>;
}

impl ConsumableHighlight for Option<Highlight> {
    fn consume(&mut self, htype: HighlightType, n: usize) -> Option<usize> {
        let (answer, new_self) = match *self {
            Some(hl) => {
                if hl.0 != htype {
                    (None, Some(hl))
                } else if hl.1 < n {
                    (Some(hl.1), None)
                } else {
                    (None, Some(Highlight(hl.0, hl.1 - n)))
                }
            }
            None => (None, None),
        };
        *self = new_self;
        answer
    }
}

// Trait that can be passed by the caller of render_highlighted(),
// providing information to modify how things are displayed
pub trait DisplayStyleGetter {
    fn poll_options(&self, id: &str) -> Option<HashSet<usize>>;
    fn unfolded(&self, id: &str) -> bool;
}
pub struct DefaultDisplayStyle;
impl DisplayStyleGetter for DefaultDisplayStyle {
    fn poll_options(&self, _id: &str) -> Option<HashSet<usize>> {
        None
    }
    fn unfolded(&self, _id: &str) -> bool {
        true
    }
}

pub trait TextFragment {
    fn render(&self, width: usize) -> Vec<ColouredString> {
        self.render_highlighted(width, None, &DefaultDisplayStyle)
    }

    fn can_highlight(_htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        false
    }

    fn count_highlightables(&self, _htype: HighlightType) -> usize {
        0
    }
    fn highlighted_id(&self, _highlight: Option<Highlight>) -> Option<String> {
        None
    }
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString>;

    fn is_multiple_choice_poll(&self) -> bool {
        false
    }

    fn render_highlighted_update(
        &self,
        width: usize,
        highlight: &mut Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let (new_highlight, text) = match *highlight {
            Some(Highlight(htype, index)) => {
                let count = self.count_highlightables(htype);
                if index < count {
                    (None, self.render_highlighted(width, *highlight, style))
                } else {
                    (
                        Some(Highlight(htype, index - count)),
                        self.render_highlighted(width, None, style),
                    )
                }
            }
            None => (None, self.render_highlighted(width, None, style)),
        };
        *highlight = new_highlight;
        text
    }

    fn highlighted_id_update(
        &self,
        highlight: &mut Option<Highlight>,
    ) -> Option<String> {
        let (answer, new_highlight) = match *highlight {
            Some(Highlight(htype, index)) => {
                let count = self.count_highlightables(htype);
                if index < count {
                    (self.highlighted_id(Some(Highlight(htype, index))), None)
                } else {
                    (None, Some(Highlight(htype, index - count)))
                }
            }
            None => (None, None),
        };
        *highlight = new_highlight;
        answer
    }
}

pub trait TextFragmentOneLine {
    // A more specific trait for fragments always producing exactly one line
    fn render_oneline(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> ColouredString;
}

impl<T: TextFragment> TextFragment for Option<T> {
    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        T::can_highlight(htype)
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match self {
            Some(ref inner) => inner.count_highlightables(htype),
            None => 0,
        }
    }
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        match self {
            Some(ref inner) => {
                inner.render_highlighted(width, highlight, style)
            }
            None => Vec::new(),
        }
    }
    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match self {
            Some(ref inner) => inner.highlighted_id(highlight),
            None => None,
        }
    }
}

impl<T: TextFragment> TextFragment for Vec<T> {
    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        T::can_highlight(htype)
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        self.iter().map(|x| x.count_highlightables(htype)).sum()
    }
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut highlight = highlight;
        itertools::concat(self.iter().map(|x| {
            x.render_highlighted_update(width, &mut highlight, style)
        }))
    }
    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        let mut highlight = highlight;
        for item in self {
            if let result @ Some(..) =
                item.highlighted_id_update(&mut highlight)
            {
                return result;
            }
        }
        None
    }
}

#[derive(Default)]
pub struct BlankLine {}

impl BlankLine {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn render_static() -> Vec<ColouredString> {
        vec![ColouredString::plain("")]
    }
}

impl TextFragment for BlankLine {
    fn render_highlighted(
        &self,
        _width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        Self::render_static()
    }
}

#[test]
fn test_blank() {
    assert_eq!(
        BlankLine::new().render(40),
        vec! {
            ColouredString::plain("")
        }
    );
}

pub struct SeparatorLine {
    timestamp: Option<DateTime<Utc>>,
    favourited: bool,
    boosted: bool,
}

impl SeparatorLine {
    pub fn new(
        timestamp: Option<DateTime<Utc>>,
        favourited: bool,
        boosted: bool,
    ) -> Self {
        SeparatorLine {
            timestamp,
            favourited,
            boosted,
        }
    }
}

fn format_date(date: DateTime<Utc>) -> String {
    date.with_timezone(&Local)
        .format("%a %b %e %H:%M:%S %Y")
        .to_string()
}

impl TextFragment for SeparatorLine {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut suffix = ColouredString::plain("");
        let display_pre = ColouredString::uniform("[", 'S');
        let display_post = ColouredString::uniform("]--", 'S');
        if let Some(date) = self.timestamp {
            let datestr = format_date(date);
            suffix = &display_pre
                + ColouredString::uniform(&datestr, 'D')
                + &display_post
                + suffix;
        }
        if self.boosted {
            suffix = &display_pre
                + ColouredString::uniform("B", 'D')
                + &display_post
                + suffix;
        }
        if self.favourited {
            suffix = &display_pre
                + ColouredString::uniform("F", 'D')
                + &display_post
                + suffix;
        }
        let w = suffix.width();
        if w < width - 1 {
            suffix = ColouredString::uniform("-", 'S').repeat(width - 1 - w)
                + suffix;
        }
        vec![suffix.truncate(width).into()]
    }
}

#[test]
fn test_separator() {
    let t = NaiveDateTime::parse_from_str(
        "2001-08-03 04:05:06",
        "%Y-%m-%d %H:%M:%S",
    )
    .unwrap()
    .and_local_timezone(Local)
    .unwrap()
    .with_timezone(&Utc);
    assert_eq!(
        SeparatorLine::new(Some(t), true, false).render(60),
        vec! {
            ColouredString::general(
                "--------------------------[F]--[Fri Aug  3 04:05:06 2001]--",
                "SSSSSSSSSSSSSSSSSSSSSSSSSSSDSSSSDDDDDDDDDDDDDDDDDDDDDDDDSSS",
                )
        }
    );
}

#[derive(Default)]
pub struct EditorHeaderSeparator {}

impl EditorHeaderSeparator {
    pub fn new() -> Self {
        Self::default()
    }
}

impl TextFragment for EditorHeaderSeparator {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![ColouredString::uniform(
            &("-".repeat(width - min(2, width)) + "|"),
            '-',
        )
        .truncate(width)
        .into()]
    }
}

#[test]
fn test_editorsep() {
    assert_eq!(
        EditorHeaderSeparator::new().render(5),
        vec! {
            ColouredString::general(
                "---|",
                "----",
                )
        }
    );
}

pub struct UsernameHeader {
    header: String,
    colour: char,
    account: String,
    nameline: String,
    id: String,
}

impl UsernameHeader {
    pub fn from(account: &str, nameline: &str, id: &str) -> Self {
        UsernameHeader {
            header: "From".to_owned(),
            colour: 'F',
            account: account.to_owned(),
            nameline: nameline.to_owned(),
            id: id.to_owned(),
        }
    }

    pub fn via(account: &str, nameline: &str, id: &str) -> Self {
        UsernameHeader {
            header: "Via".to_owned(),
            colour: 'f',
            account: account.to_owned(),
            nameline: nameline.to_owned(),
            id: id.to_owned(),
        }
    }
}

impl TextFragment for UsernameHeader {
    fn render_highlighted(
        &self,
        _width: usize,
        highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let header = ColouredString::plain(&format!("{}: ", self.header));
        let colour = match highlight {
            Some(Highlight(HighlightType::User, 0)) => '*',
            _ => self.colour,
        };
        let body = ColouredString::uniform(
            &format!("{} ({})", self.nameline, self.account),
            colour,
        );
        vec![header + body]
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::User
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match htype {
            HighlightType::User => 1,
            _ => 0,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::User, 0)) => Some(self.id.clone()),
            _ => None,
        }
    }
}

#[test]
fn test_userheader() {
    assert_eq!(
        UsernameHeader::from("stoat@example.com", "Some Person", "123")
            .render(80),
        vec! {
            ColouredString::general(
                "From: Some Person (stoat@example.com)",
                "      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
                )
        }
    );
    assert_eq!(
        UsernameHeader::via("stoat@example.com", "Some Person", "123")
            .render(80),
        vec! {
            ColouredString::general(
                "Via: Some Person (stoat@example.com)",
                "     fffffffffffffffffffffffffffffff",
                )
        }
    );
}

#[derive(PartialEq, Eq, Debug)]
pub struct Paragraph {
    words: Vec<ColouredString>,
    firstindent: usize,
    laterindent: usize,
    wrap: bool,
    centred: bool,
}

impl Default for Paragraph {
    fn default() -> Self {
        Self::new()
    }
}

impl Paragraph {
    pub fn new() -> Self {
        Paragraph {
            words: Vec::new(),
            firstindent: 0,
            laterindent: 0,
            wrap: true,
            centred: false,
        }
    }

    pub fn set_wrap(mut self, wrap: bool) -> Self {
        self.wrap = wrap;
        self
    }

    pub fn set_centred(mut self, centred: bool) -> Self {
        self.centred = centred;
        self
    }

    pub fn set_indent(
        mut self,
        firstindent: usize,
        laterindent: usize,
    ) -> Self {
        self.firstindent = firstindent;
        self.laterindent = laterindent;
        self
    }

    pub fn push_text(
        &mut self,
        text: impl ColouredStringCommon,
        squash_spaces: bool,
    ) {
        for ch in text.chars() {
            if let Some(curr_word) = self.words.last_mut() {
                let is_space = ch.is_space();
                if is_space == curr_word.is_space() {
                    if !(is_space && squash_spaces) {
                        curr_word.push_str(&ch);
                    }
                    continue;
                }
            }
            self.words.push(ch.into());
        }
    }

    pub fn push(&mut self, text: impl ColouredStringCommon) {
        self.push_text(text, false);
    }

    pub fn clear(&mut self) {
        self.words.clear();
    }

    pub fn end_word(&mut self) {
        if let Some(word) = self.words.last() {
            if !word.is_space() {
                self.push_text(&ColouredString::plain(" "), false);
            }
        }
    }

    pub fn push_para(&mut self, para: &Paragraph) {
        self.end_word();
        self.words.extend_from_slice(&para.words);
    }

    pub fn push_para_recoloured(&mut self, para: &Paragraph, colour: char) {
        self.end_word();
        for word in &para.words {
            self.words.push(word.recolour(colour));
        }
    }

    pub fn delete_mention_words_from(&mut self, start: usize) {
        let first_non_mention = start
            + self.words[start..]
                .iter()
                .position(|word| !(word.is_space() || word.is_colour('@')))
                .unwrap_or(self.words.len() - start);
        self.words.splice(start..first_non_mention, vec![]);
    }

    pub fn is_empty(&self) -> bool {
        match self.words.first() {
            None => true,
            Some(word) => word.is_empty(),
        }
    }
}

impl<T: ColouredStringCommon> Add<T> for Paragraph {
    type Output = Paragraph;

    fn add(mut self, text: T) -> Self {
        self.push(text);
        self
    }
}

#[test]
fn test_para_build() {
    assert_eq!(
        Paragraph::new(),
        Paragraph {
            words: Vec::new(),
            firstindent: 0,
            laterindent: 0,
            centred: false,
            wrap: true,
        }
    );
    assert_eq!(
        Paragraph::new().set_wrap(false),
        Paragraph {
            words: Vec::new(),
            firstindent: 0,
            laterindent: 0,
            centred: false,
            wrap: false,
        }
    );
    assert_eq!(
        Paragraph::new().set_indent(3, 4),
        Paragraph {
            words: Vec::new(),
            firstindent: 3,
            laterindent: 4,
            centred: false,
            wrap: true,
        }
    );
    assert_eq!(
        Paragraph::new().add(ColouredString::plain("foo bar baz")),
        Paragraph {
            words: vec! {
                ColouredString::plain("foo"),
                ColouredString::plain(" "),
                ColouredString::plain("bar"),
                ColouredString::plain(" "),
                ColouredString::plain("baz"),
            },
            firstindent: 0,
            laterindent: 0,
            centred: false,
            wrap: true,
        }
    );
    assert_eq!(
        Paragraph::new()
            .add(ColouredString::plain("foo ba"))
            .add(ColouredString::plain("r baz")),
        Paragraph {
            words: vec! {
                ColouredString::plain("foo"),
                ColouredString::plain(" "),
                ColouredString::plain("bar"),
                ColouredString::plain(" "),
                ColouredString::plain("baz"),
            },
            firstindent: 0,
            laterindent: 0,
            centred: false,
            wrap: true,
        }
    );
    assert_eq!(
        Paragraph::new().add(ColouredString::general(
            "  foo  bar  baz  ",
            "abcdefghijklmnopq"
        )),
        Paragraph {
            words: vec! {
                ColouredString::general("  ", "ab"),
                ColouredString::general("foo", "cde"),
                ColouredString::general("  ", "fg"),
                ColouredString::general("bar", "hij"),
                ColouredString::general("  ", "kl"),
                ColouredString::general("baz", "mno"),
                ColouredString::general("  ", "pq"),
            },
            firstindent: 0,
            laterindent: 0,
            centred: false,
            wrap: true,
        }
    );
}

impl TextFragment for Paragraph {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();
        let mut curr_width = 0;
        let mut curr_pos;

        let mut start_pos = 0;
        let mut start_width = 0;
        let mut break_pos = 0;
        let mut break_width = 0;
        let mut curr_indent = self.firstindent;

        let twidth = width.saturating_sub(1);
        let mut push_line = |line: ColouredString| {
            let line = if self.centred {
                let tspace = twidth.saturating_sub(line.width());
                let tleft = tspace / 2;
                ColouredString::plain(" ").repeat(tleft) + line
            } else {
                let indent = if lines.len() > 0 {
                    self.laterindent
                } else {
                    self.firstindent
                };
                ColouredString::plain(" ").repeat(indent) + line
            };
            lines.push(line);
        };

        for (i, word) in self.words.iter().enumerate() {
            curr_width += word.width();
            curr_pos = i + 1;

            if !word.is_space() {
                if self.wrap
                    && break_pos > start_pos
                    && curr_width - start_width + curr_indent >= width
                {
                    let mut line = ColouredString::plain("");
                    for i in start_pos..break_pos {
                        line.push_str(&self.words[i]);
                    }
                    push_line(line);
                    start_pos = break_pos;
                    start_width = break_width;
                    if self.words[start_pos].is_space() {
                        start_width += self.words[start_pos].width();
                        start_pos += 1;
                    }
                    curr_indent = self.laterindent;
                }

                break_pos = curr_pos;
                break_width = curr_width;
            }
        }

        let mut line = ColouredString::plain("");
        for i in start_pos..break_pos {
            line.push_str(&self.words[i]);
        }
        push_line(line);

        lines
    }
}

#[test]
fn test_para_wrap() {
    let p = Paragraph::new().add(ColouredString::plain(
        "the quick brown fox  jumps over  the lazy dog",
    ));
    assert_eq!(
        p.render(16),
        vec! {
            ColouredString::plain("the quick brown"),
            ColouredString::plain("fox  jumps over"),
            ColouredString::plain("the lazy dog"),
        }
    );

    let p = Paragraph::new().add(ColouredString::plain(
        "  one supercalifragilisticexpialidocious word",
    ));
    assert_eq!(
        p.render(15),
        vec! {
            ColouredString::plain("  one"),
            ColouredString::plain("supercalifragilisticexpialidocious"),
            ColouredString::plain("word"),
        }
    );

    let p = Paragraph::new().add(ColouredString::plain(
        "  supercalifragilisticexpialidocious word",
    ));
    assert_eq!(
        p.render(15),
        vec! {
            ColouredString::plain("  supercalifragilisticexpialidocious"),
            ColouredString::plain("word"),
        }
    );

    let p = Paragraph::new()
        .add(ColouredString::plain(
            "the quick brown fox  jumps over  the lazy dog",
        ))
        .set_wrap(false);
    assert_eq!(
        p.render(15),
        vec! {
            ColouredString::plain("the quick brown fox  jumps over  the lazy dog"),
        }
    );

    let p = Paragraph::new()
        .add(ColouredString::plain(
            "the quick brown fox jumps over the lazy dog",
        ))
        .set_indent(4, 2);
    assert_eq!(
        p.render(15),
        vec! {
            ColouredString::plain("    the quick"),
            ColouredString::plain("  brown fox"),
            ColouredString::plain("  jumps over"),
            ColouredString::plain("  the lazy dog"),
        }
    );
}

pub struct FileHeader {
    text: ColouredString,
}

impl FileHeader {
    pub fn new(text: ColouredString) -> Self {
        FileHeader { text }
    }
}

impl TextFragment for FileHeader {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let elephants = width >= 16;
        let twidth = if elephants { width - 9 } else { width - 1 };
        let title = self.text.truncate(twidth);
        let tspace = twidth - title.width();
        let tleft = tspace / 2;
        let tright = tspace - tleft;
        let titlepad = ColouredString::plain(" ").repeat(tleft)
            + title
            + ColouredString::plain(" ").repeat(tright);
        let underline = ColouredString::uniform("~", '~').repeat(twidth);
        if elephants {
            vec![
                (ColouredString::general("(o) ", "JJJ ")
                    + titlepad
                    + ColouredString::general(" (o)", " JJJ")),
                (ColouredString::general("/J\\ ", "JJJ ")
                    + underline
                    + ColouredString::general(" /J\\", " JJJ")),
            ]
        } else {
            vec![titlepad, underline]
        }
    }
}

#[test]
fn test_fileheader() {
    let fh = FileHeader::new(ColouredString::uniform("hello, world", 'H'));
    assert_eq!(
        fh.render(40),
        vec! {
            ColouredString::general("(o)          hello, world           (o)",
                                    "JJJ          HHHHHHHHHHHH           JJJ"),
            ColouredString::general("/J\\ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ /J\\",
                                    "JJJ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ JJJ"),
        }
    );

    assert_eq!(
        fh.render(21),
        vec! {
            ColouredString::general("(o) hello, world (o)",
                                    "JJJ HHHHHHHHHHHH JJJ"),
            ColouredString::general("/J\\ ~~~~~~~~~~~~ /J\\",
                                    "JJJ ~~~~~~~~~~~~ JJJ"),
        }
    );

    assert_eq!(
        fh.render(20),
        vec! {
            ColouredString::general("(o) hello, worl (o)",
                                    "JJJ HHHHHHHHHHH JJJ"),
            ColouredString::general("/J\\ ~~~~~~~~~~~ /J\\",
                                    "JJJ ~~~~~~~~~~~ JJJ"),
        }
    );

    assert_eq!(
        fh.render(10),
        vec! {
            ColouredString::general("hello, wo",
                                    "HHHHHHHHH"),
            ColouredString::general("~~~~~~~~~",
                                    "~~~~~~~~~"),
        }
    );
}

fn trim_para_list(paras: &mut Vec<Paragraph>) {
    let first_nonempty = paras
        .iter()
        .position(|p| !p.is_empty())
        .unwrap_or(paras.len());
    paras.splice(..first_nonempty, vec![]);

    while match paras.last() {
        Some(p) => p.is_empty(),
        None => false,
    } {
        paras.pop();
    }
}

pub enum Html {
    Rt(html::RenderTree),
    Bad(String),
}

impl Html {
    pub fn new(html: &str) -> Self {
        match html::parse(html) {
            Ok(rt) => Html::Rt(rt),
            Err(e) => Html::Bad(e.to_string()),
        }
    }

    pub fn to_para(&self) -> Paragraph {
        let mut para = Paragraph::new();

        // With ordinary wrapping paragraphs it shouldn't matter what
        // width we pick here. I pick a nice big one _just_ in case of
        // rogue table cells.
        for line in self.render(1024) {
            para.end_word();
            para.push_text(&line, true);
        }

        para
    }

    pub fn render_indented(
        &self,
        width: usize,
        indent: usize,
    ) -> Vec<ColouredString> {
        let prefix = ColouredString::plain(" ").repeat(indent);
        self.render(width.saturating_sub(indent))
            .into_iter()
            .map(|line| &prefix + line)
            .collect()
    }

    pub fn list_urls(&self) -> Vec<String> {
        match self {
            Html::Rt(tree) => html::list_urls(tree),
            Html::Bad(..) => Vec::new(),
        }
    }
}

impl TextFragment for Html {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        match self {
            Html::Rt(ref rt) => html::render(rt, width - min(width, 1)),
            Html::Bad(e) => vec![ColouredString::uniform(e, '!')],
        }
    }
}

#[cfg(test)]
fn render_html(html: &str, width: usize) -> Vec<ColouredString> {
    Html::new(html).render(width)
}

#[test]
fn test_html() {
    assert_eq!(
        render_html("<p>Testing, testing, 1, 2, 3</p>", 50),
        vec! {
            ColouredString::plain("Testing, testing, 1, 2, 3"),
        }
    );

    assert_eq!(
        render_html("<p>First para</p><p>Second para</p>", 50),
        vec! {
            ColouredString::plain("First para"),
            ColouredString::plain(""),
            ColouredString::plain("Second para"),
        }
    );

    assert_eq!(
        render_html("<p>First line<br>Second line</p>", 50),
        vec! {
            ColouredString::plain("First line"),
            ColouredString::plain("Second line"),
        }
    );

    assert_eq!(render_html("<p>Pease porridge hot, pease porridge cold, pease porridge in the pot, nine days old</p>", 50),
               vec! {
            ColouredString::plain("Pease porridge hot, pease porridge cold, pease"),
            ColouredString::plain("porridge in the pot, nine days old"),
        });

    assert_eq!(
        render_html("<p>Test of some <code>literal code</code></p>", 50),
        vec! {
            ColouredString::general("Test of some literal code",
                                    "             cccccccccccc"),
        }
    );

    assert_eq!(
        render_html("<p>Test of some <strong>strong text</strong></p>", 50),
        vec! {
            ColouredString::general("Test of some strong text",
                                    "             sssssssssss"),
        }
    );

    assert_eq!(render_html("<p>Test of a <a href=\"https://some.instance/tags/hashtag\" class=\"mention hashtag\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">#<span>hashtag</span></a></p>", 50),
               vec! {
            ColouredString::general("Test of a #hashtag",
                                    "          ########"),
        });

    assert_eq!(render_html("<p>Test of a <span class=\"h-card\" translate=\"no\"><a href=\"https://some.instance/@username\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>username</span></a></span></p>", 50),
               vec! {
            ColouredString::general("Test of a @username",
                                    "          @@@@@@@@@"),
        });

    assert_eq!(render_html("<p>Test of a <span class=\"h-card\" translate=\"no\"><a href=\"https://some.instance/@username\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>username</span></a></span></p>", 50),
               vec! {
            ColouredString::general("Test of a @username",
                                    "          @@@@@@@@@"),
        });

    assert_eq!(render_html("<p>URL to <a href=\"https://www.example.com/stuff/\" target=\"_blank\" rel=\"nofollow noopener noreferrer\" translate=\"no\"><span class=\"invisible\">https://www.</span><span class=\"ellipsis\">example.com/st</span><span class=\"invisible\">uff/</span></a>.</p>", 50),
               vec! {
            ColouredString::general("URL to https://www.example.com/stuff/.",
                                    "       uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu "),
        });
}

#[derive(Default)]
pub struct ExtendableIndicator {
    primed: bool,
}

impl ExtendableIndicator {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn set_primed(&mut self, primed: bool) {
        self.primed = primed;
    }
}

impl TextFragment for ExtendableIndicator {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let message = if self.primed {
            ColouredString::general(
                "Press [0] to extend",
                "HHHHHHHKHHHHHHHHHHH",
            )
        } else {
            ColouredString::general(
                "Press [0] twice to extend",
                "HHHHHHHKHHHHHHHHHHHHHHHHH",
            )
        };
        let msgtrunc = message.truncate(width);
        let space = width - min(msgtrunc.width() + 1, width);
        let left = space / 2;
        let msgpad = ColouredString::plain(" ").repeat(left) + msgtrunc;
        vec![ColouredString::plain(""), msgpad, ColouredString::plain("")]
    }
}

#[test]
fn test_extendable() {
    let mut ei = ExtendableIndicator::new();

    assert_eq!(
        ei.render(40),
        vec! {
            ColouredString::plain(""),
            ColouredString::general(
                "       Press [0] twice to extend",
                "       HHHHHHHKHHHHHHHHHHHHHHHHH"),
            ColouredString::plain(""),
        }
    );

    ei.set_primed(true);

    assert_eq!(
        ei.render(40),
        vec! {
            ColouredString::plain(""),
            ColouredString::general(
                "          Press [0] to extend",
                "          HHHHHHHKHHHHHHHHHHH"),
            ColouredString::plain(""),
        }
    );
}

pub struct InReplyToLine {
    para: Paragraph,
    warning: Option<Paragraph>,
    id: String,
}

impl InReplyToLine {
    pub fn new(post: Paragraph, warning: Option<String>, id: &str) -> Self {
        let mut para = Paragraph::new().add(ColouredString::plain("Re: "));
        let currlen = para.words.len();
        para.push_para(&post);
        para.delete_mention_words_from(currlen);

        let warning = warning.map(|text| {
            let mut para = Paragraph::new()
                .add(ColouredString::plain("Re: "))
                .add(ColouredString::uniform("[-]", 'r'));
            para.end_word();
            para.add(ColouredString::plain(&text))
        });

        InReplyToLine {
            para,
            warning,
            id: id.to_owned(),
        }
    }

    pub fn from_id(id: &str, client: &mut Client) -> Self {
        let st = client.status_by_id(id);
        let parent_text = match &st {
            Ok(st) => Html::new(&st.content).to_para(),
            Err(e) => Paragraph::new()
                .add(ColouredString::plain(&format!("[unavailable: {}]", e))),
        };

        let warning = match &st {
            Ok(st) => {
                if st.sensitive {
                    Some(st.spoiler_text.clone())
                } else {
                    None
                }
            }
            Err(..) => None,
        };

        Self::new(parent_text, warning, id)
    }
}

impl TextFragment for InReplyToLine {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut highlight = highlight;
        let highlighting =
            highlight.consume(HighlightType::FoldableStatus, 1) == Some(0);
        let which_para = match self.warning.as_ref() {
            Some(folded) => {
                if !style.unfolded(&self.id) {
                    folded
                } else {
                    &self.para
                }
            }
            None => &self.para,
        };

        let rendered_para = which_para.render(width - min(width, 3));
        let mut it = rendered_para.iter();
        // "Re:" guarantees the first line must exist at least
        let first_line = it.next().unwrap();
        let result = if it.next().is_some() {
            first_line + ColouredString::plain("...")
        } else {
            first_line.clone()
        };
        let result = result.truncate(width);
        let result = if highlighting {
            result.recolour('*')
        } else {
            result.into()
        };
        vec![result]
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::FoldableStatus
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match htype {
            HighlightType::FoldableStatus => {
                if self.warning.is_some() {
                    1
                } else {
                    0
                }
            }
            _ => 0,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::FoldableStatus, 0)) => {
                if self.warning.is_some() {
                    Some(self.id.clone())
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}

#[test]
fn test_in_reply_to() {
    let post = Html::new(
        "<p><span class=\"h-card\" translate=\"no\"><a href=\"https://some.instance/@stoat\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>stoat</span></a></span> <span class=\"h-card\" translate=\"no\"><a href=\"https://some.instance/@weasel\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>weasel</span></a></span> take a look at this otter!</p><p><span class=\"h-card\" translate=\"no\"><a href=\"https://some.instance/@badger\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>badger</span></a></span> might also like it</p>");

    let irt = InReplyToLine::new(post.to_para(), None, "123");
    assert_eq!(
        irt.render(48),
        vec! {
        ColouredString::general(
            "Re: take a look at this otter! @badger might...",
            "                               @@@@@@@         "),
        }
    );
    assert_eq!(
        irt.render(47),
        vec! {
        ColouredString::general(
            "Re: take a look at this otter! @badger...",
            "                               @@@@@@@   "),
        }
    );
    assert_eq!(
        irt.render(80),
        vec! {
        ColouredString::general(
            "Re: take a look at this otter! @badger might also like it",
            "                               @@@@@@@                   "),
        }
    );
}

pub struct VisibilityLine {
    vis: Visibility,
}

impl VisibilityLine {
    pub fn new(vis: Visibility) -> Self {
        VisibilityLine { vis }
    }
}

impl TextFragment for VisibilityLine {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let line = match self.vis {
            Visibility::Public => ColouredString::general(
                "Visibility: public",
                "            gggggg",
            ),
            Visibility::Unlisted => {
                ColouredString::plain("Visibility: unlisted")
            }
            Visibility::Private => ColouredString::general(
                "Visibility: private",
                "            rrrrrrr",
            ),
            Visibility::Direct => ColouredString::general(
                "Visibility: direct",
                "            rrrrrr",
            ),
        };
        vec![line.truncate(width).into()]
    }
}

pub struct NotificationLog {
    timestamp: DateTime<Utc>,
    account_desc: String,
    account_id: String,
    status_id: Option<String>,
    para: Paragraph,
}

impl NotificationLog {
    pub fn new(
        timestamp: DateTime<Utc>,
        account: &str,
        nameline: &str,
        account_id: &str,
        ntype: NotificationType,
        post: Option<&Paragraph>,
        status_id: Option<&str>,
    ) -> Self {
        let mut para = Paragraph::new();

        let verb = match ntype {
            NotificationType::Favourite => "favourited: ",
            NotificationType::Reblog => "boosted: ",
            NotificationType::Follow => "followed you",
            _ => panic!("bad type {:?} in NotificationLog::new", ntype),
        };
        para.push_text(&ColouredString::plain(verb), false);

        if let Some(cpara) = post {
            let currlen = para.words.len();
            para.push_para(cpara);
            para.delete_mention_words_from(currlen);
        }

        let status_id = status_id.map(|s| s.to_owned());

        NotificationLog {
            timestamp,
            account_desc: format!("{} ({})", nameline, account),
            account_id: account_id.to_owned(),
            status_id,
            para,
        }
    }

    pub fn from_notification(not: &Notification, client: &mut Client) -> Self {
        let para = not
            .status
            .as_ref()
            .map(|st| Html::new(&st.content).to_para());
        let status_id = not.status.as_ref().map(|st| &st.id as &str);
        Self::new(
            not.created_at,
            &client.fq(&not.account.acct),
            &not.account.display_name,
            &not.account.id,
            not.ntype,
            para.as_ref(),
            status_id,
        )
    }
}

impl TextFragment for NotificationLog {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut full_para = Paragraph::new().set_indent(0, 2);
        let datestr = format_date(self.timestamp);
        full_para.push_text(&ColouredString::uniform(&datestr, ' '), false);
        full_para.end_word();

        let user_colour = match highlight {
            Some(Highlight(HighlightType::User, 0)) => '*',
            _ => ' ',
        };
        full_para.push_text(
            &ColouredString::uniform(&self.account_desc, user_colour),
            false,
        );

        match (highlight, &self.status_id) {
            (Some(Highlight(HighlightType::Status, 0)), Some(..)) => {
                full_para.push_para_recoloured(&self.para, '*')
            }
            _ => full_para.push_para(&self.para),
        };

        let rendered_para = full_para.render(width);
        if rendered_para.len() > 2 {
            vec![
                rendered_para[0].clone(),
                rendered_para[1].truncate(width - 3)
                    + ColouredString::plain("..."),
            ]
        } else {
            rendered_para
        }
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::User || htype == HighlightType::Status
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match htype {
            HighlightType::User => 1,
            HighlightType::Status => {
                if self.status_id.is_some() {
                    1
                } else {
                    0
                }
            }
            _ => 0,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::User, 0)) => {
                Some(self.account_id.clone())
            }
            Some(Highlight(HighlightType::Status, 0)) => {
                self.status_id.clone()
            }
            _ => None,
        }
    }
}

#[test]
fn test_notification_log() {
    let t = NaiveDateTime::parse_from_str(
        "2001-08-03 04:05:06",
        "%Y-%m-%d %H:%M:%S",
    )
    .unwrap()
    .and_local_timezone(Local)
    .unwrap()
    .with_timezone(&Utc);

    let post = Paragraph::new().add(ColouredString::general(
        "@stoat @weasel take a look at this otter! @badger might also like it",
        "@@@@@@ @@@@@@@                            @@@@@@@                   ",
    ));

    assert_eq!(
        NotificationLog::new(
            t,
            "foo@example.com",
            "Foo Bar",
            "123",
            NotificationType::Reblog,
            Some(&post),
            None
        )
        .render(80),
        vec! {
            ColouredString::general("Fri Aug  3 04:05:06 2001 Foo Bar (foo@example.com) boosted: take a look at this",
                                    "                                                                               "),
            ColouredString::general("  otter! @badger might also like it",
                                    "         @@@@@@@                   "),
        }
    );

    assert_eq!(
        NotificationLog::new(
            t,
            "foo@example.com",
            "Foo Bar",
            "123",
            NotificationType::Favourite,
            Some(&post),
            None
        )
        .render(51),
        vec! {
            ColouredString::general("Fri Aug  3 04:05:06 2001 Foo Bar (foo@example.com)",
                                    "                                                  "),
            ColouredString::general("  favourited: take a look at this otter! @badger...",
                                    "                                         @@@@@@@   "),
        }
    );

    assert_eq!(
        NotificationLog::new(
            t,
            "foo@example.com",
            "Foo Bar",
            "123",
            NotificationType::Follow,
            None,
            None
        )
        .render(80),
        vec! {
            ColouredString::general("Fri Aug  3 04:05:06 2001 Foo Bar (foo@example.com) followed you",
                                    "                                                               "),
        }
    );
}

pub struct UserListEntry {
    account_desc: String,
    account_id: String,
}

impl UserListEntry {
    pub fn new(account: &str, nameline: &str, account_id: &str) -> Self {
        UserListEntry {
            account_desc: format!("{} ({})", nameline, account),
            account_id: account_id.to_owned(),
        }
    }

    pub fn from_account(ac: &Account, client: &mut Client) -> Self {
        Self::new(&client.fq(&ac.acct), &ac.display_name, &ac.id)
    }
}

impl TextFragment for UserListEntry {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut para = Paragraph::new().set_indent(0, 2);
        let colour = match highlight {
            Some(Highlight(HighlightType::User, 0)) => '*',
            _ => ' ',
        };
        para.push_text(
            &ColouredString::uniform(&self.account_desc, colour),
            false,
        );
        para.render(width)
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::User
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match htype {
            HighlightType::User => 1,
            _ => 0,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::User, 0)) => {
                Some(self.account_id.clone())
            }
            _ => None,
        }
    }
}

#[test]
fn test_user_list_entry() {
    assert_eq!(
        UserListEntry::new("foo@example.com", "Foo Bar", "123").render(80),
        vec! {
            ColouredString::general("Foo Bar (foo@example.com)",
                                    "                         "),
        }
    );
}

pub struct Media {
    url: String,
    description: Vec<Paragraph>,
}

impl Media {
    pub fn new(url: &str, description: Option<&str>) -> Self {
        let paras = match description {
            None => Vec::new(),
            Some(description) => {
                let mut paras = description
                    .split('\n')
                    .map(|x| {
                        Paragraph::new()
                            .set_indent(2, 2)
                            .add(ColouredString::uniform(x, 'm'))
                    })
                    .collect();
                trim_para_list(&mut paras);
                paras
            }
        };

        Media {
            url: url.to_owned(),
            description: paras,
        }
    }
}

impl TextFragment for Media {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines: Vec<_> = ColouredString::uniform(&self.url, 'M')
            .split(width.saturating_sub(1))
            .map(|x| x.into())
            .collect();
        for para in &self.description {
            lines.extend_from_slice(&para.render(width));
        }
        lines
    }
}

#[test]
fn test_media() {
    assert_eq!(Media::new("https://example.com/example.png",
                          Some("A picture of an example, sitting on an example, with examples dotted around the example landscape.\n\nThis doesn't really make sense, but whatever.")).render(50), vec! {
            ColouredString::uniform("https://example.com/example.png", 'M'),
            ColouredString::general("  A picture of an example, sitting on an example,",
                                    "  mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"),
            ColouredString::general("  with examples dotted around the example",
                                    "  mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"),
            ColouredString::general("  landscape.",
                                    "  mmmmmmmmmm"),
            ColouredString::plain("  "),
            ColouredString::general("  This doesn't really make sense, but whatever.",
                                    "  mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm"),
        });
}

pub fn key_to_string(key: OurKey) -> String {
    match key {
        // Menu and file keypresses are case insensitive, and the UI shows
        // them in uppercase always
        OurKey::Pr(c) => c.to_uppercase().to_string(),
        OurKey::Ctrl(c) => format!("^{}", c),
        OurKey::FunKey(n) => format!("F{}", n),
        OurKey::Backspace => "BS".to_string(), // likely won't come up
        OurKey::Return => "RET".to_string(),
        OurKey::Escape => "ESC".to_string(),
        OurKey::Space => "SPACE".to_string(),
        OurKey::Up => "Up".to_string(),
        OurKey::Down => "Down".to_string(),
        OurKey::Left => "Left".to_string(),
        OurKey::Right => "Right".to_string(),
        OurKey::PgUp => "PgUp".to_string(),
        OurKey::PgDn => "PgDn".to_string(),
        OurKey::Home => "Home".to_string(),
        OurKey::End => "End".to_string(),
        OurKey::Ins => "Ins".to_string(),
        OurKey::Del => "Del".to_string(),
    }
}

struct Keypress {
    key: OurKey,
    description: ColouredString,
}

#[derive(Default)]
pub struct FileStatusLine {
    keypresses: Vec<(Keypress, usize)>, // each with display priority
    proportion: Option<u32>,
    message: Option<String>,
}

pub struct FileStatusLineFinal {
    fs: FileStatusLine,
    priwidth: Vec<(usize, usize)>, // min priority, total width of those keys
}

impl FileStatusLine {
    const SPACING: usize = 2;

    pub fn new() -> FileStatusLine {
        Default::default()
    }

    pub fn set_proportion(mut self, numer: usize, denom: usize) -> Self {
        self.proportion = Some((100 * numer / denom) as u32);
        self
    }

    pub fn message(mut self, msg: &str) -> Self {
        self.message = Some(msg.to_owned());
        self
    }

    pub fn add(
        mut self,
        key: OurKey,
        description: &str,
        priority: usize,
    ) -> Self {
        self.keypresses.push((
            Keypress {
                key,
                description: ColouredString::plain(description),
            },
            priority,
        ));
        self
    }

    pub fn finalise(self) -> FileStatusLineFinal {
        let mut bypri = BTreeMap::new();
        for (kp, pri) in &self.keypresses {
            // [key]:desc
            let key = key_to_string(kp.key);
            let width = UnicodeWidthStr::width(&key as &str)
                + kp.description.width()
                + 3;

            if let Some(priwidth) = bypri.get_mut(pri) {
                *priwidth += Self::SPACING + width;
            } else {
                bypri.insert(pri, width);
            }
        }

        // If the keypresses are the only thing on this status line,
        // then we don't need an extra SPACING to separate them from
        // other stuff.
        let initial = if self.proportion.is_some() || self.message.is_some() {
            Self::SPACING
        } else {
            0
        };

        let mut cumulative = initial;
        let mut priwidth = Vec::new();
        for (minpri, thiswidth) in bypri.iter().rev() {
            cumulative += thiswidth
                + if cumulative == initial {
                    0
                } else {
                    Self::SPACING
                };
            priwidth.push((**minpri, cumulative));
        }

        FileStatusLineFinal { fs: self, priwidth }
    }
}

#[test]
fn test_filestatus_build() {
    let fs = FileStatusLine::new().set_proportion(34, 55);
    assert_eq!(fs.proportion, Some(61));

    let fs = FileStatusLine::new().message("hello, world");
    assert_eq!(fs.message, Some("hello, world".to_owned()));

    let fsf = FileStatusLine::new()
        .add(OurKey::Pr('A'), "Annoy", 10)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9),
        }
    );

    let fsf = FileStatusLine::new()
        .add(OurKey::Pr('A'), "Annoy", 10)
        .add(OurKey::Pr('B'), "Badger", 10)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9 + 10 + FileStatusLine::SPACING),
        }
    );

    let fsf = FileStatusLine::new()
        .add(OurKey::Pr('A'), "Annoy", 10)
        .add(OurKey::Pr('B'), "Badger", 5)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9),
            (5, 9 + 10 + FileStatusLine::SPACING),
        }
    );

    let fsf = FileStatusLine::new()
        .add(OurKey::Pr('A'), "Annoy", 10)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9),
        }
    );

    let fsf = FileStatusLine::new()
        .set_proportion(2, 3)
        .add(OurKey::Pr('A'), "Annoy", 10)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9 + FileStatusLine::SPACING),
        }
    );

    let fsf = FileStatusLine::new()
        .message("aha")
        .add(OurKey::Pr('A'), "Annoy", 10)
        .finalise();
    assert_eq!(
        fsf.priwidth,
        vec! {
            (10, 9 + FileStatusLine::SPACING),
        }
    );
}

impl TextFragment for FileStatusLineFinal {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut line = ColouredString::plain("");
        let space = ColouredString::plain(" ").repeat(FileStatusLine::SPACING);
        let push = |line: &mut ColouredString, s: ColouredStringSlice<'_>| {
            if !line.is_empty() {
                line.push_str(&space);
            }
            line.push_str(&s);
        };

        if let Some(msg) = &self.fs.message {
            push(&mut line, ColouredString::plain(msg).slice());
        }

        let cprop = self
            .fs
            .proportion
            .as_ref()
            .map(|prop| ColouredString::plain(&format!("({}%)", prop)));
        let cpropwidth = if let Some(cprop) = &cprop {
            cprop.width()
        } else {
            0
        };
        let extraspace = if !line.is_empty() && cpropwidth > 0 {
            FileStatusLine::SPACING
        } else if cprop.is_none() {
            1 // trailing '.' if no proportion
        } else {
            0
        };

        let avail =
            width - min(width, line.width() + cpropwidth + extraspace + 1);
        let minindex = self.priwidth.partition_point(|(_, w)| *w <= avail);
        if minindex > 0 {
            let minpri = self.priwidth[minindex - 1].0;

            for (kp, pri) in &self.fs.keypresses {
                if *pri >= minpri {
                    let mut ckey = ColouredString::plain("[");
                    let ckp =
                        ColouredString::uniform(&key_to_string(kp.key), 'k');
                    ckey.push_str(&ckp);
                    ckey.push_str(ColouredString::plain("]:"));
                    ckey.push_str(&kp.description);
                    push(&mut line, ckey.slice());
                }
            }
        }

        if let Some(cprop) = cprop {
            push(&mut line, cprop.slice());
        } else {
            line.push_str(ColouredString::plain("."));
        }

        // Done. Now centre it in the available space.
        let line = line.truncate(width - 1);
        let space = width - min(line.width() + 1, width);
        let left = space / 2;
        let linepad = ColouredString::plain(" ").repeat(left) + line;
        vec![linepad]
    }
}

#[test]
fn test_filestatus_render() {
    let fs = FileStatusLine::new()
        .message("stoat")
        .set_proportion(34, 55)
        .add(OurKey::Pr('a'), "Annoy", 10)
        .add(OurKey::Pr('b'), "Badger", 10)
        .add(OurKey::Pr('c'), "Critical", 1000)
        .add(OurKey::Pr('d'), "Dull", 1)
        .finalise();

    assert_eq!(
        fs.render(80),
        vec! {
            ColouredString::general(
                "          stoat  [A]:Annoy  [B]:Badger  [C]:Critical  [D]:Dull  (61%)",
                "                  k          k           k             k             "),
        }
    );

    assert_eq!(
        fs.render(60),
        vec! {
            ColouredString::general(
                "stoat  [A]:Annoy  [B]:Badger  [C]:Critical  [D]:Dull  (61%)",
                "        k          k           k             k             "),
        }
    );

    assert_eq!(
        fs.render(59),
        vec! {
            ColouredString::general(
                "    stoat  [A]:Annoy  [B]:Badger  [C]:Critical  (61%)",
                "            k          k           k                 "),
        }
    );

    assert_eq!(
        fs.render(50),
        vec! {
            ColouredString::general(
                "stoat  [A]:Annoy  [B]:Badger  [C]:Critical  (61%)",
                "        k          k           k                 "),
        }
    );

    assert_eq!(
        fs.render(49),
        vec! {
            ColouredString::general(
                "           stoat  [C]:Critical  (61%)",
                "                   k                 "),
        }
    );

    assert_eq!(
        fs.render(27),
        vec! {
            ColouredString::general(
                "stoat  [C]:Critical  (61%)",
                "        k                 "),
        }
    );

    assert_eq!(
        fs.render(26),
        vec! {
            ColouredString::plain("      stoat  (61%)"),
        }
    );

    let fs = FileStatusLine::new()
        .set_proportion(1, 11)
        .add(OurKey::Pr('K'), "Keypress", 10)
        .finalise();

    assert_eq!(
        fs.render(19),
        vec! {
            ColouredString::general(
                "[K]:Keypress  (9%)",
                " k                "),
        }
    );

    assert_eq!(
        fs.render(18),
        vec! {
            ColouredString::general(
                "      (9%)",
                "          "),
        }
    );

    let fs = FileStatusLine::new()
        .message("weasel")
        .add(OurKey::Pr('K'), "Keypress", 10)
        .finalise();

    assert_eq!(
        fs.render(22),
        vec! {
            ColouredString::general(
                "weasel  [K]:Keypress.",
                "         k           "),
        }
    );

    assert_eq!(
        fs.render(21),
        vec! {
            ColouredString::general(
                "      weasel.",
                "             "),
        }
    );
}

pub struct MenuKeypressLine {
    keypress: Keypress,
    lwid: usize,
    rwid: usize,
    lmaxwid: usize,
    rmaxwid: usize,
}

pub trait MenuKeypressLineGeneral {
    fn check_widths(&self, lmaxwid: &mut usize, rmaxwid: &mut usize);
    fn reset_widths(&mut self);
    fn ensure_widths(&mut self, lmaxwid: usize, rmaxwid: usize);
}

impl<T: MenuKeypressLineGeneral> MenuKeypressLineGeneral for Option<T> {
    fn check_widths(&self, lmaxwid: &mut usize, rmaxwid: &mut usize) {
        if let Some(inner) = self {
            inner.check_widths(lmaxwid, rmaxwid);
        }
    }
    fn reset_widths(&mut self) {
        if let Some(inner) = self {
            inner.reset_widths();
        }
    }
    fn ensure_widths(&mut self, lmaxwid: usize, rmaxwid: usize) {
        if let Some(inner) = self {
            inner.ensure_widths(lmaxwid, rmaxwid);
        }
    }
}

impl MenuKeypressLine {
    pub fn new(key: OurKey, description: ColouredString) -> Self {
        // +2 for [] around the key name
        let lwid = UnicodeWidthStr::width(&key_to_string(key) as &str) + 2;

        let rwid = description.width();
        MenuKeypressLine {
            keypress: Keypress { key, description },
            lwid,
            rwid,
            lmaxwid: lwid,
            rmaxwid: rwid,
        }
    }
}

impl MenuKeypressLineGeneral for MenuKeypressLine {
    fn check_widths(&self, lmaxwid: &mut usize, rmaxwid: &mut usize) {
        *lmaxwid = max(*lmaxwid, self.lwid);
        *rmaxwid = max(*rmaxwid, self.rwid);
    }
    fn reset_widths(&mut self) {
        self.lmaxwid = self.lwid;
        self.rmaxwid = self.rwid;
    }
    fn ensure_widths(&mut self, lmaxwid: usize, rmaxwid: usize) {
        self.lmaxwid = max(self.lmaxwid, lmaxwid);
        self.rmaxwid = max(self.rmaxwid, rmaxwid);
    }
}

impl TextFragmentOneLine for MenuKeypressLine {
    fn render_oneline(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> ColouredString {
        let ourwidth = self.lmaxwid + self.rmaxwid + 3; // " = " in the middle
        let space = width - min(width, ourwidth + 1);
        let leftpad = min(space * 3 / 4, width - min(width, self.lmaxwid + 2));
        let leftpad = min(leftpad, (width - min(width, self.lmaxwid + 8)) / 2);

        let lspace = self.lmaxwid - self.lwid;
        let lrspace = lspace / 2;
        let llspace = lspace - lrspace;

        let keydesc = key_to_string(self.keypress.key);
        let line = ColouredString::plain(" ").repeat(leftpad + llspace)
            + ColouredString::plain("[")
            + ColouredString::uniform(&keydesc, 'k')
            + ColouredString::plain("]")
            + ColouredString::plain(" ").repeat(lrspace)
            + ColouredString::plain(" = ")
            + &self.keypress.description;

        line.truncate(width).into()
    }
}

impl TextFragment for MenuKeypressLine {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![self.render_oneline(width, highlight, style)]
    }
}

pub struct CyclingMenuLine<Value: Eq + Copy> {
    menulines: Vec<(Value, MenuKeypressLine)>,
    index: usize,
}

impl<Value: Eq + Copy> CyclingMenuLine<Value> {
    pub fn new(
        key: OurKey,
        description: ColouredString,
        values: &[(Value, ColouredString)],
        value: Value,
    ) -> Self {
        let menulines = values
            .iter()
            .map(|(val, desc)| {
                (*val, MenuKeypressLine::new(key, &description + desc))
            })
            .collect();

        let index = values
            .iter()
            .position(|(val, _desc)| *val == value)
            .expect("Input value must match one of the provided options");

        CyclingMenuLine { menulines, index }
    }

    // Returns a LogicalAction just to make it more convenient to put
    // in matches on keypresses
    pub fn cycle(&mut self) -> LogicalAction {
        self.index += 1;
        if self.index >= self.menulines.len() {
            self.index = 0;
        }
        LogicalAction::Nothing
    }

    pub fn get_value(&self) -> Value {
        self.menulines[self.index].0
    }
}

impl<Value: Eq + Copy> MenuKeypressLineGeneral for CyclingMenuLine<Value> {
    fn check_widths(&self, lmaxwid: &mut usize, rmaxwid: &mut usize) {
        for (_value, ml) in self.menulines.iter() {
            ml.check_widths(lmaxwid, rmaxwid);
        }
    }
    fn reset_widths(&mut self) {
        for (_value, ml) in self.menulines.iter_mut() {
            ml.reset_widths();
        }
    }
    fn ensure_widths(&mut self, lmaxwid: usize, rmaxwid: usize) {
        for (_value, ml) in self.menulines.iter_mut() {
            ml.ensure_widths(lmaxwid, rmaxwid);
        }
    }
}

impl<Value: Eq + Copy> TextFragmentOneLine for CyclingMenuLine<Value> {
    fn render_oneline(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> ColouredString {
        self.menulines[self.index]
            .1
            .render_oneline(width, highlight, style)
    }
}

impl<Value: Eq + Copy> TextFragment for CyclingMenuLine<Value> {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![self.render_oneline(width, highlight, style)]
    }
}

#[test]
fn test_menu_keypress() {
    let mut mk = MenuKeypressLine::new(
        OurKey::Pr('S'),
        ColouredString::general("Something or other", "K                 "),
    );

    assert_eq!(
        mk.render(80),
        vec! {
        ColouredString::general(
            "                                  [S] = Something or other",
            "                                   k    K                 "),
        }
    );

    assert_eq!(
        mk.render(40),
        vec! {
        ColouredString::general(
            "           [S] = Something or other",
            "            k    K                 "),
        }
    );

    assert_eq!(
        mk.render(29),
        vec! {
        ColouredString::general(
            "   [S] = Something or other",
            "    k    K                 "),
        }
    );

    mk.ensure_widths(5, 0);

    assert_eq!(
        mk.render(40),
        vec! {
        ColouredString::general(
            "          [S]  = Something or other",
            "           k     K                 "),
        }
    );
}

pub struct CentredInfoLine {
    text: ColouredString,
}

impl CentredInfoLine {
    pub fn new(text: ColouredString) -> Self {
        CentredInfoLine { text }
    }
}

impl TextFragmentOneLine for CentredInfoLine {
    fn render_oneline(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> ColouredString {
        let twidth = width.saturating_sub(1);
        let title = self.text.truncate(twidth);
        let tspace = twidth - title.width();
        let tleft = tspace / 2;
        ColouredString::plain(" ").repeat(tleft) + &self.text
    }
}

impl TextFragment for CentredInfoLine {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![self.render_oneline(width, highlight, style)]
    }
}

struct Poll {
    title: Paragraph,
    options: Vec<(bool, ColouredString)>,
    id: String,
    eligible: bool,
    multiple: bool,
}

pub struct StatusDisplay {
    sep: SeparatorLine,
    from: UsernameHeader,
    via: Option<UsernameHeader>,
    irt: Option<InReplyToLine>,
    vis: Option<VisibilityLine>,
    warning: Option<String>,
    content: Html,
    media: Vec<Media>,
    poll: Option<Poll>,
    blank: BlankLine,
    id: String,
    ours: bool,
}

impl StatusDisplay {
    pub fn new(st: Status, client: &mut Client) -> Self {
        let (st, via) = match st.reblog {
            Some(b) => (*b, Some(st.account)),
            None => (st, None),
        };

        let sep = SeparatorLine::new(
            Some(st.created_at),
            st.favourited == Some(true),
            st.reblogged == Some(true),
        );

        let from = UsernameHeader::from(
            &client.fq(&st.account.acct),
            &st.account.display_name,
            &st.account.id,
        );

        let via = match via {
            None => None,
            Some(booster) => Some(UsernameHeader::via(
                &client.fq(&booster.acct),
                &booster.display_name,
                &booster.id,
            )),
        };

        let irt = st
            .in_reply_to_id
            .as_ref()
            .map(|id| InReplyToLine::from_id(id, client));

        let vis = match st.visibility {
            Visibility::Public => None,
            vis => Some(VisibilityLine::new(vis)),
        };

        let warning = if st.sensitive {
            Some(st.spoiler_text.clone())
        } else {
            None
        };

        let content = Html::new(&st.content);

        let media = st
            .media_attachments
            .iter()
            .map(|m| {
                let desc_ref = m.description.as_ref().map(|s| s as &str);
                Media::new(&m.url, desc_ref)
            })
            .collect();

        let poll = st.poll.map(|poll| {
            let mut extras = Vec::new();
            let voters = poll.voters_count.unwrap_or(poll.votes_count);
            if poll.multiple {
                extras.push(ColouredString::uniform("multiple choice", 'K'));
            }
            if poll.expired {
                extras.push(ColouredString::uniform("expired", 'H'));
                extras.push(ColouredString::uniform(
                    &format!("{} voters", voters),
                    'H',
                ));
            } else {
                if let Some(date) = poll.expires_at {
                    extras.push(ColouredString::uniform(
                        &format!("expires {}", format_date(date)),
                        'H',
                    ));
                } else {
                    extras.push(ColouredString::uniform("no expiry", 'H'));
                }
                extras.push(ColouredString::uniform(
                    &format!("{} voters so far", voters),
                    'H',
                ));
            }
            let mut desc = ColouredString::uniform("Poll: ", 'H');
            for (i, extra) in extras.iter().enumerate() {
                if i > 0 {
                    desc.push_str(ColouredString::uniform(", ", 'H'));
                }
                desc.push_str(extra);
            }

            let title = Paragraph::new().set_indent(0, 2).add(&desc);

            let mut options = Vec::new();
            for (thisindex, opt) in poll.options.iter().enumerate() {
                let voted = poll.own_votes.as_ref().map_or(false, |votes| {
                    votes.iter().any(|i| *i == thisindex)
                });
                let mut desc = ColouredString::plain(" ");
                desc.push_str(ColouredString::plain(&opt.title));
                if let Some(n) = opt.votes_count {
                    desc.push_str(ColouredString::uniform(
                        &format!(" ({})", n),
                        'H',
                    ));
                }
                options.push((voted, desc));
            }

            Poll {
                title,
                options,
                id: poll.id.clone(),
                eligible: poll.voted != Some(true) && !poll.expired,
                multiple: poll.multiple,
            }
        });

        StatusDisplay {
            sep,
            from,
            via,
            irt,
            vis,
            warning,
            content,
            media,
            poll,
            blank: BlankLine::new(),
            id: st.id,
            ours: st.account.id == client.our_account_id(),
        }
    }

    pub fn list_urls(&self) -> Vec<String> {
        self.content.list_urls()
    }
}

fn push_fragment(lines: &mut Vec<ColouredString>, frag: Vec<ColouredString>) {
    lines.extend(frag.iter().map(|line| line.to_owned()));
}
fn push_fragment_highlighted(
    lines: &mut Vec<ColouredString>,
    frag: Vec<ColouredString>,
) {
    lines.extend(frag.iter().map(|line| line.recolour('*')));
}

impl TextFragment for StatusDisplay {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();
        let mut highlight = highlight;

        push_fragment(&mut lines, self.sep.render(width));
        push_fragment(
            &mut lines,
            self.from
                .render_highlighted_update(width, &mut highlight, style),
        );
        push_fragment(
            &mut lines,
            self.via
                .render_highlighted_update(width, &mut highlight, style),
        );
        push_fragment(&mut lines, self.vis.render(width));
        push_fragment(
            &mut lines,
            self.irt
                .render_highlighted_update(width, &mut highlight, style),
        );
        push_fragment(&mut lines, self.blank.render(width));

        let highlighting_this_status = highlight
            .consume(HighlightType::Status, 1)
            == Some(0)
            || highlight.consume(HighlightType::WholeStatus, 1) == Some(0)
            || (self.ours
                && highlight.consume(HighlightType::OurStatus, 1) == Some(0))
            || (self.warning.is_some()
                && highlight.consume(HighlightType::FoldableStatus, 1)
                    == Some(0));
        let push_fragment_opt_highlight = if highlighting_this_status {
            push_fragment_highlighted
        } else {
            push_fragment
        };

        let folded = self.warning.is_some() && !style.unfolded(&self.id);
        if let Some(warning_text) = &self.warning {
            let mut para = Paragraph::new();
            para = para.add(&ColouredString::uniform(
                if folded { "[-]" } else { "[+]" },
                'r',
            ));
            para.end_word();
            para = para.add(&ColouredString::plain(&warning_text));
            push_fragment_opt_highlight(&mut lines, para.render(width));
        }

        if folded {
            push_fragment(&mut lines, self.blank.render(width));
        } else {
            let rendered_content = self.content.render(width);
            let content_empty = rendered_content.is_empty();
            push_fragment_opt_highlight(&mut lines, rendered_content);
            if !content_empty {
                push_fragment(&mut lines, self.blank.render(width));
            }
            for m in &self.media {
                push_fragment_opt_highlight(&mut lines, m.render(width));
                push_fragment_opt_highlight(
                    &mut lines,
                    self.blank.render(width),
                );
            }
        }

        // Even if we're folded, display the fact of there being a
        // poll, because otherwise it's just too painful to try to
        // conditionalise selecting the poll options.
        if let Some(poll) = &self.poll {
            push_fragment_opt_highlight(&mut lines, poll.title.render(width));
            let poll_options_selected = style.poll_options(&poll.id);
            for (i, (voted, desc)) in poll.options.iter().enumerate() {
                let highlighting_this_option =
                    highlight.consume(HighlightType::PollOption, 1) == Some(0);
                let voted = poll_options_selected
                    .as_ref()
                    .map_or(*voted, |opts| opts.contains(&i));
                let option = Paragraph::new().set_indent(0, 2).add(
                    &ColouredString::uniform(
                        if voted { "[X]" } else { "[ ]" },
                        'H',
                    ),
                );
                let option = if folded { option } else { option.add(desc) };
                let push_fragment_opt_highlight = if highlighting_this_option {
                    push_fragment_highlighted
                } else {
                    push_fragment_opt_highlight
                };
                push_fragment_opt_highlight(&mut lines, option.render(width));
            }
            push_fragment_opt_highlight(&mut lines, self.blank.render(width));
        }

        lines
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::User
            || htype == HighlightType::Status
            || htype == HighlightType::WholeStatus
            || htype == HighlightType::OurStatus
            || htype == HighlightType::FoldableStatus
            || htype == HighlightType::PollOption
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        match htype {
            HighlightType::User => {
                self.from.count_highlightables(htype)
                    + self.via.count_highlightables(htype)
            }
            HighlightType::Status => 1,
            HighlightType::WholeStatus => 1,
            HighlightType::OurStatus => {
                if self.ours {
                    1
                } else {
                    0
                }
            }
            HighlightType::FoldableStatus => {
                self.irt.count_highlightables(htype)
                    + if self.warning.is_some() { 1 } else { 0 }
            }

            HighlightType::PollOption => self
                .poll
                .as_ref()
                .filter(|poll| poll.eligible)
                .map_or(0, |poll| poll.options.len()),
            HighlightType::HttpLogEntry => 0,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::User, _)) => {
                let mut highlight = highlight;
                if let result @ Some(..) =
                    self.from.highlighted_id_update(&mut highlight)
                {
                    return result;
                }
                if let result @ Some(..) =
                    self.via.highlighted_id_update(&mut highlight)
                {
                    return result;
                }
                None
            }
            Some(Highlight(HighlightType::WholeStatus, 0))
            | Some(Highlight(HighlightType::OurStatus, 0))
            | Some(Highlight(HighlightType::Status, 0)) => {
                Some(self.id.clone())
            }

            Some(Highlight(HighlightType::FoldableStatus, _)) => {
                let mut highlight = highlight;
                if let result @ Some(..) =
                    self.irt.highlighted_id_update(&mut highlight)
                {
                    return result;
                }

                if self.warning.is_some()
                    && highlight.consume(HighlightType::FoldableStatus, 1)
                        == Some(0)
                {
                    return Some(self.id.clone());
                }

                None
            }

            Some(Highlight(HighlightType::PollOption, i)) => self
                .poll
                .as_ref()
                .filter(|poll| poll.eligible)
                .filter(|poll| i < poll.options.len())
                .map(|poll| poll.id.clone()),
            _ => None,
        }
    }

    fn is_multiple_choice_poll(&self) -> bool {
        self.poll.as_ref().map_or(false, |poll| poll.multiple)
    }
}

pub struct DetailedStatusDisplay {
    sd: StatusDisplay,
    id: Paragraph,
    webstatus: Option<Paragraph>,
    creation: Paragraph,
    lastedit: Paragraph,
    reply_to: Paragraph,
    reply_to_id: Option<String>,
    reply_to_user: Paragraph,
    reply_to_user_id: Option<String>,
    language: Paragraph,
    visibility: VisibilityLine,
    sensitive: Paragraph,
    spoiler: Paragraph,
    replies: Paragraph,
    boosts: Paragraph,
    favourites: Paragraph,
    mentions_header: Option<Paragraph>,
    mentions: Vec<(Paragraph, String)>,
    urls_header: Option<Paragraph>,
    urls: Vec<(Paragraph, Option<ResolvedUrl>)>,
    client_name: Paragraph,
    client_url: Paragraph,
    sep: SeparatorLine,
    blank: BlankLine,
}

impl DetailedStatusDisplay {
    pub fn new(st: Status, client: &mut Client) -> Self {
        let id = Paragraph::new()
            .add(ColouredString::plain("Post id: "))
            .add(ColouredString::plain(&st.id));
        let webstatus = st.url.as_ref().map(|s| {
            Paragraph::new()
                .add(ColouredString::plain("On the web: "))
                .add(ColouredString::uniform(s, 'u'))
        });

        let creation = Paragraph::new()
            .add(ColouredString::plain("Creation time: "))
            .add(ColouredString::plain(&format_date(st.created_at)));
        let lastedit = Paragraph::new()
            .add(ColouredString::plain("Last edit time: "))
            .add(&st.edited_at.map_or_else(
                || ColouredString::uniform("none", '0'),
                |date| ColouredString::plain(&format_date(date)),
            ));
        let reply_to = Paragraph::new()
            .add(ColouredString::plain("Reply to post: "))
            .add(&st.in_reply_to_id.as_ref().map_or_else(
                || ColouredString::uniform("none", '0'),
                |s| ColouredString::plain(s),
            ));
        let reply_to_id = st.in_reply_to_id.clone();
        let reply_to_user = Paragraph::new()
            .add(ColouredString::plain("Reply to account: "))
            .add(&st.in_reply_to_account_id.as_ref().map_or_else(
                || ColouredString::uniform("none", '0'),
                |s| ColouredString::plain(s),
            ));
        let reply_to_user_id = st.in_reply_to_account_id.clone();

        let language = Paragraph::new()
            .add(ColouredString::plain("Language: "))
            .add(&st.language.as_ref().map_or_else(
                || ColouredString::uniform("none", '0'),
                |s| ColouredString::plain(s),
            ));
        let visibility = VisibilityLine::new(st.visibility);
        let sens_str = match st.sensitive {
            false => ColouredString::uniform("no", 'g'),
            true => ColouredString::uniform("yes", 'r'),
        };
        let sensitive = Paragraph::new()
            .add(ColouredString::plain("Sensitive: "))
            .add(&sens_str);
        let opt_spoiler = if st.spoiler_text.is_empty() {
            None
        } else {
            Some(&st.spoiler_text)
        };
        let spoiler = Paragraph::new()
            .set_indent(0, 2)
            .add(ColouredString::plain("Spoiler text: "))
            .add(&opt_spoiler.as_ref().map_or_else(
                || ColouredString::uniform("none", '0'),
                |s| ColouredString::plain(s),
            ));

        let replies = Paragraph::new().add(ColouredString::plain(&format!(
            "Replies: {}",
            st.replies_count
        )));
        let boosts = Paragraph::new().add(ColouredString::plain(&format!(
            "Boosts: {}",
            st.reblogs_count
        )));
        let favourites = Paragraph::new().add(ColouredString::plain(
            &format!("Favourites: {}", st.favourites_count),
        ));

        let mentions: Vec<_> = st
            .mentions
            .iter()
            .map(|m| {
                let para = Paragraph::new()
                    .set_indent(2, 2)
                    .add(ColouredString::uniform(&client.fq(&m.acct), 'f'));
                (para, m.id.to_owned())
            })
            .collect();
        let mentions_header = if mentions.is_empty() {
            None
        } else {
            Some(
                Paragraph::new()
                    .add(&ColouredString::plain("Mentioned users:")),
            )
        };

        let client_name = Paragraph::new()
            .add(ColouredString::plain("Client name: "))
            .add(&st.application.as_ref().map_or_else(
                || ColouredString::uniform("none", '0'),
                |app| ColouredString::plain(&app.name),
            ));
        let client_url = Paragraph::new()
            .add(ColouredString::plain("Client website: "))
            .add(
                &st.application
                    .as_ref()
                    .and_then(|app| app.website.as_ref())
                    .map_or_else(
                        || ColouredString::uniform("none", '0'),
                        |url| ColouredString::uniform(url, 'u'),
                    ),
            );

        let sd = StatusDisplay::new(st, client);
        let urls: Vec<_> = sd
            .list_urls()
            .iter()
            .map(|u| {
                let r = if possible_mastodon_url(u) {
                    match client.resolve_mastodon_url(u) {
                        Ok(r) => r,
                        Err(..) => None,
                    }
                } else {
                    None
                };
                (
                    Paragraph::new()
                        .set_indent(2, 4)
                        .add(ColouredString::uniform(u, 'u')),
                    r,
                )
            })
            .collect();
        let urls_header = if urls.is_empty() {
            None
        } else {
            Some(
                Paragraph::new()
                    .add(&ColouredString::plain("URLs in hyperlinks:")),
            )
        };

        DetailedStatusDisplay {
            sd,
            id,
            webstatus,
            creation,
            lastedit,
            reply_to,
            reply_to_id,
            reply_to_user,
            reply_to_user_id,
            language,
            visibility,
            sensitive,
            spoiler,
            replies,
            boosts,
            favourites,
            mentions,
            mentions_header,
            urls,
            urls_header,
            client_name,
            client_url,
            sep: SeparatorLine::new(None, false, false),
            blank: BlankLine::new(),
        }
    }
}

impl TextFragment for DetailedStatusDisplay {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();
        let mut highlight = highlight;

        push_fragment(
            &mut lines,
            self.sd
                .render_highlighted_update(width, &mut highlight, style),
        );
        push_fragment(&mut lines, self.sep.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.id.render(width));
        push_fragment(&mut lines, self.webstatus.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.creation.render(width));
        push_fragment(&mut lines, self.lastedit.render(width));

        let mut reply_to = self.reply_to.render(width);
        if self.reply_to_id.is_some()
            && highlight.consume(HighlightType::Status, 1) == Some(0)
        {
            reply_to = reply_to.iter().map(|s| s.recolour('*')).collect();
        }
        push_fragment(&mut lines, reply_to);

        let mut reply_to_user = self.reply_to_user.render(width);
        if self.reply_to_user_id.is_some()
            && highlight.consume(HighlightType::User, 1) == Some(0)
        {
            reply_to_user =
                reply_to_user.iter().map(|s| s.recolour('*')).collect();
        }
        push_fragment(&mut lines, reply_to_user);

        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.language.render(width));
        push_fragment(&mut lines, self.visibility.render(width));
        push_fragment(&mut lines, self.sensitive.render(width));
        push_fragment(&mut lines, self.spoiler.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.replies.render(width));
        push_fragment(&mut lines, self.boosts.render(width));
        push_fragment(&mut lines, self.favourites.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        if !self.mentions.is_empty() {
            push_fragment(&mut lines, self.mentions_header.render(width));
            for (para, _id) in &self.mentions {
                let mut rendered = para.render(width);
                if highlight.consume(HighlightType::User, 1) == Some(0) {
                    rendered =
                        rendered.iter().map(|s| s.recolour('*')).collect();
                }
                push_fragment(&mut lines, rendered);
            }
            push_fragment(&mut lines, self.blank.render(width));
        }

        if !self.urls.is_empty() {
            push_fragment(&mut lines, self.urls_header.render(width));
            for (para, resolved) in &self.urls {
                let highlight_this = match resolved {
                    Some(ResolvedUrl::Account(..)) => {
                        highlight.consume(HighlightType::User, 1) == Some(0)
                    }
                    Some(ResolvedUrl::Status(..)) => {
                        highlight.consume(HighlightType::Status, 1) == Some(0)
                    }
                    _ => false,
                };
                let mut rendered = para.render(width);
                if highlight_this {
                    rendered =
                        rendered.iter().map(|s| s.recolour('*')).collect();
                }
                push_fragment(&mut lines, rendered);
            }
            push_fragment(&mut lines, self.blank.render(width));
        }

        push_fragment(&mut lines, self.client_name.render(width));
        push_fragment(&mut lines, self.client_url.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        lines
    }

    fn can_highlight(htype: HighlightType) -> bool
    where
        Self: Sized,
    {
        htype == HighlightType::User
            || htype == HighlightType::Status
            || htype == HighlightType::PollOption
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        let base = self.sd.count_highlightables(htype);
        match htype {
            HighlightType::User => {
                let nreply = usize::from(self.reply_to_user_id.is_some());
                let nmentions = self.mentions.len();
                let nurls = self
                    .urls
                    .iter()
                    .filter(|(_para, resolved)| {
                        matches!(resolved, Some(ResolvedUrl::Account(..)))
                    })
                    .count();
                base + nreply + nmentions + nurls
            }
            HighlightType::Status => {
                let nreply = usize::from(self.reply_to_user_id.is_some());
                let nurls = self
                    .urls
                    .iter()
                    .filter(|(_para, resolved)| {
                        matches!(resolved, Some(ResolvedUrl::Status(..)))
                    })
                    .count();
                base + nreply + nurls
            }
            _ => base,
        }
    }

    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        let mut highlight = highlight;
        if let result @ Some(..) =
            self.sd.highlighted_id_update(&mut highlight)
        {
            return result;
        }

        match highlight {
            Some(Highlight(HighlightType::User, mut index)) => {
                if let Some(ref id) = self.reply_to_user_id {
                    if index == 0 {
                        return Some(id.clone());
                    } else {
                        index -= 1;
                    }
                }
                if index < self.mentions.len() {
                    return Some(self.mentions[index].1.clone());
                } else {
                    index -= self.mentions.len();
                }
                for (_para, resolved) in &self.urls {
                    if let Some(ResolvedUrl::Account(id)) = resolved {
                        if index == 0 {
                            return Some(id.clone());
                        } else {
                            index -= 1;
                        }
                    }
                }
            }
            Some(Highlight(HighlightType::Status, mut index)) => {
                if let Some(ref id) = self.reply_to_id {
                    if index == 0 {
                        return Some(id.clone());
                    } else {
                        index -= 1;
                    }
                }
                for (_para, resolved) in &self.urls {
                    if let Some(ResolvedUrl::Status(id)) = resolved {
                        if index == 0 {
                            return Some(id.clone());
                        } else {
                            index -= 1;
                        }
                    }
                }
            }
            _ => (),
        }
        None
    }

    fn is_multiple_choice_poll(&self) -> bool {
        self.sd.is_multiple_choice_poll()
    }
}

pub struct ExamineUserDisplay {
    name: Paragraph,
    webaccount: Paragraph,
    dispname: Paragraph,
    bio_header: Paragraph,
    bio: Html,
    info_header: Paragraph,
    info_fields: Vec<(Paragraph, Html)>,
    id: Paragraph,
    creation: Paragraph,
    last_post: Paragraph,
    post_count: Paragraph,
    followers_count: Paragraph,
    following_count: Paragraph,
    flags: Vec<Paragraph>,
    relationships: Vec<Paragraph>,
    blank: BlankLine,
}

impl ExamineUserDisplay {
    pub fn new(ac: Account, client: &mut Client) -> Result<Self, ClientError> {
        let name = Paragraph::new()
            .add(ColouredString::plain("Account name: "))
            .add(ColouredString::uniform(&client.fq(&ac.acct), 'f'));
        let webaccount = Paragraph::new()
            .add(ColouredString::plain("On the web: "))
            .add(ColouredString::uniform(&ac.url, 'u'));

        let dispname = Paragraph::new()
            .add(ColouredString::plain("Display name: "))
            .add(ColouredString::plain(&ac.display_name));
        let bio_header = Paragraph::new().add(ColouredString::plain("Bio:"));
        let bio = Html::new(&ac.note);

        let info_header =
            Paragraph::new().add(ColouredString::plain("Information:"));
        let info_fields = ac
            .fields
            .iter()
            .map(|field| {
                let colour = if field.verified_at.is_some() {
                    'g'
                } else {
                    ' '
                };
                let title_text = field.name.trim();
                let title_text =
                    title_text.strip_suffix(':').unwrap_or(title_text);
                let title_text = title_text.to_owned() + ":";
                let title = Paragraph::new()
                    .add(ColouredString::uniform(&title_text, colour))
                    .set_indent(2, 2);
                let content = Html::new(&field.value);
                (title, content)
            })
            .collect();

        let id = Paragraph::new()
            .add(ColouredString::plain("Account id: "))
            .add(ColouredString::plain(&ac.id));
        let creation = Paragraph::new()
            .add(ColouredString::plain("Account created: "))
            .add(&Self::format_option_approx_date(Some(ac.created_at)));
        let last_post = Paragraph::new()
            .add(ColouredString::plain("Latest post: "))
            .add(&Self::format_option_approx_date(ac.last_status_at));
        let post_count = Paragraph::new().add(ColouredString::plain(
            &format!("Number of posts: {}", ac.statuses_count),
        ));
        let followers_count = Paragraph::new().add(ColouredString::plain(
            &format!("Number of followers: {}", ac.followers_count),
        ));
        let following_count = Paragraph::new().add(ColouredString::plain(
            &format!("Number of users followed: {}", ac.following_count),
        ));

        let mut flags = Vec::new();
        if ac.locked {
            flags.push(
                Paragraph::new()
                    .add(ColouredString::plain("This account is "))
                    .add(ColouredString::uniform("locked", 'r'))
                    .add(ColouredString::plain(
                        " (you can't follow it without its permission).",
                    )),
            );
        }
        if ac.suspended.unwrap_or(false) {
            flags.push(Paragraph::new().add(&ColouredString::uniform(
                "This account is suspended.",
                'r',
            )));
        }
        if ac.limited.unwrap_or(false) {
            flags.push(Paragraph::new().add(&ColouredString::uniform(
                "This account is silenced.",
                'r',
            )));
        }
        if ac.bot {
            flags.push(Paragraph::new().add(&ColouredString::plain(
                "This account identifies as a bot.",
            )));
        }
        if ac.group {
            flags.push(Paragraph::new().add(&ColouredString::plain(
                "This account identifies as a group.",
            )));
        }
        if let Some(moved_to) = ac.moved {
            flags.push(
                Paragraph::new()
                    .add(ColouredString::uniform(
                        "This account has moved to:",
                        'r',
                    ))
                    .add(ColouredString::plain(&format!(
                        " {}",
                        client.fq(&moved_to.acct)
                    ))),
            );
        }

        let mut relationships = Vec::new();
        if ac.id == client.our_account_id() {
            relationships.push(Paragraph::new().set_indent(2, 2).add(
                &ColouredString::general(
                    "You are this user!",
                    "    ___           ",
                ),
            ));
        }
        match client.account_relationship_by_id(&ac.id) {
            Ok(rs) => {
                if rs.following && rs.showing_reblogs {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform("You follow this user.", 'g'),
                    ));
                } else if rs.following {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You follow this user (but without boosts).",
                            'g',
                        ),
                    ));
                }
                if rs.followed_by {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "This user follows you.",
                            'g',
                        ),
                    ));
                }
                if rs.requested {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You have requested to follow this user",
                            'r',
                        ),
                    ));
                }
                if rs.requested_by {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "This user has requested to follow you!",
                            'F',
                        ),
                    ));
                }
                if rs.notifying {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::plain(
                            "You have enabled notifications for this user.",
                        ),
                    ));
                }
                if rs.blocking {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You have blocked this user.",
                            'r',
                        ),
                    ));
                }
                if rs.blocked_by {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "This user has blocked you.",
                            'r',
                        ),
                    ));
                }
                if rs.muting {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You have muted this user.",
                            'r',
                        ),
                    ));
                }
                if rs.muting_notifications {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You have muted notifications from this user.",
                            'r',
                        ),
                    ));
                }
                if rs.domain_blocking {
                    relationships.push(Paragraph::new().set_indent(2, 2).add(
                        &ColouredString::uniform(
                            "You have blocked this user's domain.",
                            'r',
                        ),
                    ));
                }
            }
            Err(e) => {
                relationships.push(Paragraph::new().set_indent(2, 2).add(
                    &ColouredString::uniform(
                        &format!("Unable to retrieve relationships: {}", e),
                        '!',
                    ),
                ))
            }
        }
        if !relationships.is_empty() {
            relationships.insert(
                0,
                Paragraph::new().add(&ColouredString::plain(
                    "Relationships to this user:",
                )),
            );
        }

        Ok(ExamineUserDisplay {
            name,
            webaccount,
            dispname,
            bio_header,
            bio,
            info_header,
            info_fields,
            id,
            creation,
            last_post,
            post_count,
            followers_count,
            following_count,
            flags,
            relationships,
            blank: BlankLine::new(),
        })
    }

    fn format_option_approx_date(date: Option<ApproxDate>) -> ColouredString {
        // Used for account creation dates and last-post times, which
        // don't seem to bother having precise timestamps
        match date {
            None => ColouredString::uniform("none", '0'),
            Some(ApproxDate(date)) => {
                ColouredString::plain(&date.format("%a %b %e %Y").to_string())
            }
        }
    }
}

impl TextFragment for ExamineUserDisplay {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();

        push_fragment(&mut lines, self.name.render(width));
        push_fragment(&mut lines, self.webaccount.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.dispname.render(width));
        push_fragment(&mut lines, self.bio_header.render(width));
        push_fragment(&mut lines, self.bio.render_indented(width, 2));
        push_fragment(&mut lines, self.blank.render(width));

        if !self.info_fields.is_empty() {
            push_fragment(&mut lines, self.info_header.render(width));
            for (key, value) in &self.info_fields {
                // Display a single line 'key: value' if it fits.
                // Otherwise, let the value go to the next line,
                // indented further.
                let rkey = key.render(width);
                let rval = value.render_indented(width, 4);
                if rkey.len() == 1
                    && rval.len() == 1
                    && rkey[0].width() + rval[0].width() + 2 <= width
                {
                    let rval = &rval[0];
                    assert!(rval.text().starts_with("    "));
                    // Trim 3 spaces off, leaving one after the colon
                    let rval = ColouredString::general(
                        &rval.text()[3..],
                        &rval.colours()[3..],
                    );
                    lines.push(&rkey[0] + &rval);
                } else {
                    push_fragment(&mut lines, rkey);
                    push_fragment(&mut lines, rval);
                }
            }
            push_fragment(&mut lines, self.blank.render(width));
        }

        if !self.flags.is_empty() {
            push_fragment(&mut lines, self.flags.render(width));
            push_fragment(&mut lines, self.blank.render(width));
        }

        if !self.relationships.is_empty() {
            push_fragment(&mut lines, self.relationships.render(width));
            push_fragment(&mut lines, self.blank.render(width));
        }

        push_fragment(&mut lines, self.id.render(width));
        push_fragment(&mut lines, self.creation.render(width));
        push_fragment(&mut lines, self.last_post.render(width));
        push_fragment(&mut lines, self.post_count.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        push_fragment(&mut lines, self.followers_count.render(width));
        push_fragment(&mut lines, self.following_count.render(width));
        push_fragment(&mut lines, self.blank.render(width));

        lines
    }
}

pub struct InstanceRulesDisplay {
    desc: Paragraph,
    rules: Vec<Paragraph>,
    blank: BlankLine,
}

impl InstanceRulesDisplay {
    pub fn new(inst: Instance) -> Self {
        let desc =
            Paragraph::new().add(ColouredString::plain(&inst.description));

        let rules = inst
            .rules
            .iter()
            .map(|r| {
                Paragraph::new().add(ColouredString::plain(&format!(
                    "{}. {}",
                    &r.id, &r.text
                )))
            })
            .collect();

        InstanceRulesDisplay {
            desc,
            rules,
            blank: BlankLine::new(),
        }
    }
}

impl TextFragment for InstanceRulesDisplay {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();

        push_fragment(&mut lines, self.desc.render(width));
        for rule in self.rules.iter() {
            push_fragment(&mut lines, self.blank.render(width));
            push_fragment(&mut lines, rule.render(width));
        }
        lines
    }
}

pub struct ErrorLogEntry {
    sep: SeparatorLine,
    title: Paragraph,
    paras: Vec<Paragraph>,
    blank: BlankLine,
}

impl ErrorLogEntry {
    pub fn new(err: ClientError, date: DateTime<Utc>) -> Self {
        let sep = SeparatorLine::new(Some(date), false, false);
        let blank = BlankLine::new();
        let mut paras = Vec::new();
        let mut title = Paragraph::new().add(ColouredString::plain("Type: "));

        fn log_msg(paras: &mut Vec<Paragraph>, msg: String) {
            paras.push(Paragraph::new().add(ColouredString::plain(&msg)));
        }
        fn log_status(
            paras: &mut Vec<Paragraph>,
            status: reqwest::StatusCode,
        ) {
            paras.push(Paragraph::new().add(ColouredString::plain(&format!(
                "HTTP status: {}",
                status
            ))))
        }
        fn log_url(paras: &mut Vec<Paragraph>, url: String) {
            paras.push(
                Paragraph::new()
                    .add(ColouredString::plain("while fetching URL: "))
                    .add(ColouredString::uniform(&url, 'u')),
            )
        }

        match err {
            ClientError::Internal(msg) => {
                title.push_text(
                    ColouredString::uniform("internal error", 'r'),
                    false,
                );
                log_msg(&mut paras, msg);
            }
            ClientError::UrlParse(url, msg) => {
                title.push_text(
                    ColouredString::plain("URL parsing error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::UrlFetchNet(url, msg) => {
                title.push_text(ColouredString::plain("network error"), false);
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::UrlFetchHTTP(url, status) => {
                title.push_text(ColouredString::plain("HTTP error"), false);
                log_status(&mut paras, status);
                log_url(&mut paras, url);
            }
            ClientError::UrlFetchHTTPRich(url, status, msg) => {
                title.push_text(
                    ColouredString::plain("error from Mastodon server"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_status(&mut paras, status);
                log_url(&mut paras, url);
            }
            ClientError::JSONParse(url, msg) => {
                title.push_text(
                    ColouredString::plain("JSON parsing error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::InvalidJSONSyntax(url, msg) => {
                title.push_text(
                    ColouredString::plain("JSON syntax error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::UnexpectedJSONContent(url, msg, json) => {
                title.push_text(
                    ColouredString::plain("JSON content error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
                paras.push(
                    Paragraph::new()
                        .add(ColouredString::plain("response JSON: "))
                        .add(ColouredString::uniform(&json, 'u')),
                );
            }
            ClientError::LinkParse(url, msg) => {
                title.push_text(
                    ColouredString::plain("Link header parsing error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::UrlConsistency(url, msg) => {
                title.push_text(
                    ColouredString::plain("server data consistency error"),
                    false,
                );
                log_msg(&mut paras, msg);
                log_url(&mut paras, url);
            }
            ClientError::Consistency(msg) => {
                title.push_text(
                    ColouredString::plain("server data consistency error"),
                    false,
                );
                log_msg(&mut paras, msg);
            }
        }

        ErrorLogEntry {
            sep,
            title,
            paras,
            blank,
        }
    }
}

impl TextFragment for ErrorLogEntry {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();
        push_fragment(&mut lines, self.sep.render(width));
        push_fragment(&mut lines, self.title.render(width));
        push_fragment(&mut lines, self.blank.render(width));
        push_fragment(&mut lines, self.paras.render(width));
        push_fragment(&mut lines, self.blank.render(width));
        lines
    }
}

pub struct UnwrappedLine {
    line: ColouredString,
}

impl UnwrappedLine {
    pub fn new(line: ColouredString) -> Self {
        UnwrappedLine { line }
    }
}

impl TextFragmentOneLine for UnwrappedLine {
    fn render_oneline(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> ColouredString {
        if self.line.width() > width {
            self.line.split(width.saturating_sub(1)).next().expect(
                "If this string was too wide then it must be non-empty",
            ) + ColouredString::uniform(">", '>')
        } else {
            self.line.clone()
        }
    }
}

impl TextFragment for UnwrappedLine {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![self.render_oneline(width, highlight, style)]
    }
}

pub struct SplitLine {
    line: ColouredString,
}

impl SplitLine {
    pub fn new(line: ColouredString) -> Self {
        SplitLine { line }
    }
}

impl TextFragment for SplitLine {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        self.line
            .split(width.saturating_sub(1))
            .map(|x| x.into())
            .collect()
    }
}

pub struct HttpLogEntry {
    id: String,
    log_line: UnwrappedLine,
}

impl HttpLogEntry {
    pub fn new(id: String, tl: TransactionLogEntry, client: &Client) -> Self {
        // Here, we're trying to squeeze a lot of info on to one line,
        // so we only print the time, not the date. We expect that the
        // user is most likely interested in very recent transactions
        // anyway.
        let timestamp =
            tl.end_time.with_timezone(&Local).format("%H:%M:%S%.3f");

        // Similarly, we discard a lot of the URL, because we expect
        // that the scheme and host parts will be fixed (we're always
        // talking to the same Mastodon server).
        //
        // We also discard the very common /api/v1/ or /api/v2/ at the
        // start of the URL. It _is_ sometimes relevant (the API
        // methods have different versions, and e.g. /api/v1/instance
        // and /api/v2/instance deliver different results), but I
        // think not often _enough_ to put it in this brief summary
        // line. The user can go to the full page for a transaction if
        // they need to check that detail.
        let url_text = tl.url_str();
        let url_text = if url_text.starts_with(client.instance_url_ref()) {
            &url_text[client.instance_url_ref().len()..]
        } else {
            url_text
        };
        lazy_static! {
            static ref API_REGEX: Regex =
                Regex::new(r#"^/api/v[0-9]+/(.*)$"#).unwrap();
        }
        let url_text = match API_REGEX.captures(url_text) {
            Some(caps) => caps.get(1).unwrap().as_str(),
            None => url_text,
        };

        let method_text = tl.method_str();
        let status_text = tl.status_str();
        let status_colour = match status_text.chars().next() {
            Some('2') | Some('s') => 'g', // "stream event" also counts
            Some('3') => 'a',
            _ => 'r',
        };
        let status_text = match &tl.note {
            Some(note) => format!("{status_text} ({note})"),
            None => status_text.to_owned(),
        };

        let log_text =
            ColouredString::plain(&format!("{timestamp} {method_text} "))
                + ColouredString::uniform(&url_text, 'u')
                + ColouredString::plain(" -> ")
                + ColouredString::uniform(&status_text, status_colour);
        let log_line = UnwrappedLine::new(log_text);

        HttpLogEntry { id, log_line }
    }
}

impl TextFragmentOneLine for HttpLogEntry {
    fn render_oneline(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> ColouredString {
        let mut highlight = highlight;
        let rendered =
            self.log_line
                .render_oneline(width, None, &DefaultDisplayStyle);
        if highlight.consume(HighlightType::HttpLogEntry, 1) == Some(0) {
            rendered.recolour('*')
        } else {
            rendered
        }
    }
}

impl TextFragment for HttpLogEntry {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        vec![self.render_oneline(width, highlight, style)]
    }

    fn can_highlight(htype: HighlightType) -> bool {
        htype == HighlightType::HttpLogEntry
    }

    fn count_highlightables(&self, htype: HighlightType) -> usize {
        if htype == HighlightType::HttpLogEntry {
            1
        } else {
            0
        }
    }
    fn highlighted_id(&self, highlight: Option<Highlight>) -> Option<String> {
        match highlight {
            Some(Highlight(HighlightType::HttpLogEntry, 0)) => {
                Some(self.id.clone())
            }
            _ => None,
        }
    }
}

pub struct HttpTransactionDisplay {
    url: Paragraph,
    rsp: Paragraph,
    req_headers: Vec<Paragraph>,
    rsp_headers: Vec<Paragraph>,
    start_time: Paragraph,
    end_time: Paragraph,
    req_body_header: Option<Paragraph>,
    req_body: Option<Vec<SplitLine>>,
    rsp_body_header: Option<Paragraph>,
    rsp_body_prefix: Option<Vec<UnwrappedLine>>,
    rsp_body: Option<Vec<UnwrappedLine>>,
    blank: BlankLine,
}

impl HttpTransactionDisplay {
    pub fn new(tl: TransactionLogEntry) -> Self {
        let start_time = Paragraph::new()
            .set_indent(0, 4)
            .add(ColouredString::plain("Request sent: "))
            .add(ColouredString::plain(
                &tl.start_time
                    .with_timezone(&Local)
                    .format("%Y-%m-%d %H:%M:%S%.f")
                    .to_string(),
            ));
        let end_time = Paragraph::new()
            .set_indent(0, 4)
            .add(ColouredString::plain("Response received: "))
            .add(ColouredString::plain(
                &tl.end_time
                    .with_timezone(&Local)
                    .format("%Y-%m-%d %H:%M:%S%.f")
                    .to_string(),
            ));

        let url = Paragraph::new()
            .set_indent(0, 4)
            .add(ColouredString::plain("HTTP request: "))
            .add(ColouredString::plain(tl.method_str()))
            .add(ColouredString::plain(" "))
            .add(ColouredString::uniform(tl.url_str(), 'u'));

        let rsp = Paragraph::new().set_indent(0, 4);
        let rsp = match tl.reason() {
            Some(reason) => {
                let status_text = format!("{} {}", tl.status_str(), reason);
                let status_colour = match status_text.chars().next() {
                    Some('2') => 'g',
                    Some('3') => 'a',
                    _ => 'r',
                };
                rsp.add(ColouredString::plain("HTTP response: "))
                    .add(ColouredString::uniform(&status_text, status_colour))
            }
            None => rsp
                .add(ColouredString::plain("New event received from stream")),
        };

        let req_headers = tl
            .req_headers
            .iter()
            .map(|(k, v)| {
                Paragraph::new()
                    .set_indent(2, 4)
                    .add(ColouredString::uniform(k, 'H'))
                    .add(ColouredString::uniform(": ", 'H'))
                    .add(ColouredString::plain(v))
            })
            .collect();
        let rsp_headers = tl
            .rsp_headers
            .iter()
            .map(|(k, v)| {
                Paragraph::new()
                    .set_indent(2, 4)
                    .add(ColouredString::uniform(k, 'H'))
                    .add(ColouredString::uniform(": ", 'H'))
                    .add(ColouredString::plain(v))
            })
            .collect();

        let mut rsp_body = None;
        let mut rsp_body_header = None;
        if let Some(text) = &tl.body {
            if let Ok(text) = serde_json::from_str::<serde_json::Value>(text)
                .and_then(|v| serde_json::to_string_pretty(&v))
            {
                rsp_body = Some(
                    text.lines()
                        .map(|line| {
                            UnwrappedLine::new(ColouredString::plain(line))
                        })
                        .collect(),
                );
                rsp_body_header = Some(
                    Paragraph::new()
                        .set_indent(0, 4)
                        .add(ColouredString::plain("Response body:")),
                );
            }
        }
        let rsp_body_prefix = tl.body_prefix.as_ref().map(|text| {
            text.lines()
                .map(|line| UnwrappedLine::new(ColouredString::plain(line)))
                .collect()
        });

        let mut req_body = None;
        let mut req_body_header = None;
        if let Some(text) = &tl.reqbody {
            req_body = Some(
                text.lines()
                    .map(|line| SplitLine::new(ColouredString::plain(line)))
                    .collect(),
            );

            req_body_header = Some(
                Paragraph::new()
                    .set_indent(0, 4)
                    .add(ColouredString::plain("Request body:")),
            );
        }

        HttpTransactionDisplay {
            url,
            rsp,
            start_time,
            end_time,
            req_headers,
            rsp_headers,
            rsp_body,
            rsp_body_prefix,
            rsp_body_header,
            req_body,
            req_body_header,
            blank: BlankLine::new(),
        }
    }
}

impl TextFragment for HttpTransactionDisplay {
    fn render_highlighted(
        &self,
        width: usize,
        _highlight: Option<Highlight>,
        _style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        let mut lines = Vec::new();
        push_fragment(&mut lines, self.start_time.render(width));
        push_fragment(&mut lines, self.url.render(width));
        push_fragment(&mut lines, self.req_headers.render(width));
        if self.req_body.is_some() {
            push_fragment(&mut lines, self.req_body_header.render(width));
            push_fragment(&mut lines, self.req_body.render(width));
        }
        push_fragment(&mut lines, self.blank.render(width));
        push_fragment(&mut lines, self.end_time.render(width));
        push_fragment(&mut lines, self.rsp.render(width));
        push_fragment(&mut lines, self.rsp_headers.render(width));
        push_fragment(&mut lines, self.blank.render(width));
        if self.rsp_body.is_some() {
            push_fragment(&mut lines, self.rsp_body_header.render(width));
            push_fragment(&mut lines, self.rsp_body_prefix.render(width));
            push_fragment(&mut lines, self.rsp_body.render(width));
            push_fragment(&mut lines, self.blank.render(width));
        }
        lines
    }
}
