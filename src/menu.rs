use itertools::Itertools;
use std::collections::HashMap;

use super::activity_stack::{
    ComposeActivity, NonUtilityActivity, OverlayActivity, UtilityActivity,
};
use super::client::Client;
use super::coloured_string::ColouredString;
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
};

enum MenuLine {
    Blank,
    Key(MenuKeypressLine),
    Info(CentredInfoLine),
}

fn find_substring(
    haystack: &str,
    needle: &str,
) -> Option<(usize, usize, usize)> {
    if let Some(pos) = haystack.find(needle) {
        let (pre, post) = haystack.split_at(pos);
        let pre_nchars = pre.chars().count();
        let needle_nchars = needle.chars().count();
        let post_nchars = post.chars().count();
        Some((pre_nchars, needle_nchars, post_nchars - needle_nchars))
    } else {
        None
    }
}

fn find_highlight_char(desc: &str, c: char) -> Option<(usize, usize, usize)> {
    let found = find_substring(desc, &c.to_uppercase().to_string());
    if found.is_some() {
        found
    } else {
        find_substring(desc, &c.to_lowercase().to_string())
    }
}

struct Menu {
    title: FileHeader,
    status: FileStatusLineFinal,
    lines: Vec<MenuLine>,
    bottom_lines: Vec<MenuLine>,
    actions: HashMap<OurKey, LogicalAction>,
}

impl Menu {
    fn new(title: ColouredString, is_main: bool) -> Self {
        let status = FileStatusLine::new();
        let status = if is_main {
            status.message("Select an option.")
        } else {
            status.add(Return, "Back", 10)
        };
        let status = status.add(Pr('?'), "Help", 101);

        let mut menu = Menu {
            title: FileHeader::new(title),
            status: status.finalise(),
            lines: Vec::new(),
            bottom_lines: Vec::new(),
            actions: HashMap::new(),
        };

        if !is_main {
            menu.actions.insert(Return, LogicalAction::Pop);
        }

        menu
    }

    fn add_action_coloured(
        &mut self,
        key: OurKey,
        desc: ColouredString,
        action: LogicalAction,
    ) {
        self.lines
            .push(MenuLine::Key(MenuKeypressLine::new(key, desc)));

        if action != LogicalAction::Nothing {
            if let Pr(c) = key {
                if let Ok(c) =
                    c.to_lowercase().to_string().chars().exactly_one()
                {
                    self.actions.insert(Pr(c), action.clone());
                }
                if let Ok(c) =
                    c.to_uppercase().to_string().chars().exactly_one()
                {
                    self.actions.insert(Pr(c), action.clone());
                }
            }

            self.actions.insert(key, action);
        }
    }

    fn add_action(&mut self, key: OurKey, desc: &str, action: LogicalAction) {
        let highlight_char = match key {
            Pr(c) => Some(c),
            Ctrl(c) => Some(c),
            _ => None,
        };

        let highlight = if let Some(c) = highlight_char {
            find_highlight_char(desc, c)
        } else {
            None
        };

        let desc_coloured = if let Some((before, during, after)) = highlight {
            ColouredString::general(
                desc,
                &("H".repeat(before)
                    + &"K".repeat(during)
                    + &"H".repeat(after)),
            )
        } else {
            ColouredString::uniform(desc, 'H')
        };

        self.add_action_coloured(key, desc_coloured, action)
    }

    fn add_blank_line(&mut self) {
        self.lines.push(MenuLine::Blank);
    }

    fn add_info_coloured(&mut self, desc: ColouredString) {
        self.bottom_lines
            .push(MenuLine::Info(CentredInfoLine::new(desc)));
    }
    fn add_info(&mut self, desc: &str) {
        self.add_info_coloured(ColouredString::uniform(desc, 'H'));
    }

    fn finalise(mut self) -> Self {
        let mut lmaxwid = 0;
        let mut rmaxwid = 0;
        for line in &self.lines {
            if let MenuLine::Key(mk) = line {
                mk.check_widths(&mut lmaxwid, &mut rmaxwid);
            }
        }
        for line in &mut self.lines {
            if let MenuLine::Key(mk) = line {
                mk.ensure_widths(lmaxwid, rmaxwid);
            }
        }
        self
    }
}

impl ActivityState for Menu {
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let mut lines = Vec::new();
        lines.extend_from_slice(&self.title.render(w));
        lines.extend_from_slice(&BlankLine::render_static());

        // FIXME: once menus get too big, we'll need to keep a current
        // starting position, and be able to scroll up and down the
        // menu with [<] and [>]
        for line in &self.lines {
            lines.extend_from_slice(&match line {
                MenuLine::Blank => BlankLine::render_static(),
                MenuLine::Key(mk) => mk.render(w),
                MenuLine::Info(cl) => cl.render(w),
            });
        }

        while lines.len() + 2 + self.bottom_lines.len() < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        for line in &self.bottom_lines {
            lines.extend_from_slice(&match line {
                MenuLine::Blank => BlankLine::render_static(),
                MenuLine::Key(mk) => mk.render(w),
                MenuLine::Info(cl) => cl.render(w),
            });
        }

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        lines.extend_from_slice(&self.status.render(w));

        (lines, CursorPosition::End)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        _client: &mut Client,
    ) -> LogicalAction {
        match key {
            Pr('?') => LogicalAction::Help,
            key => match self.actions.get(&key) {
                Some(action) => action.clone(),
                None => LogicalAction::Nothing,
            },
        }
    }
}

pub fn main_menu(client: &mut Client) -> Box<dyn ActivityState> {
    let mut menu = Menu::new(
        ColouredString::uniform("Mastodonochrome Main Menu", 'H'),
        true,
    );

    menu.add_action(
        Pr('H'),
        "Home timeline",
        LogicalAction::Goto(NonUtilityActivity::HomeTimelineFile.into()),
    );
    menu.add_blank_line();
    menu.add_action(
        Pr('P'),
        "Public timeline (all servers)",
        LogicalAction::Goto(NonUtilityActivity::PublicTimelineFile.into()),
    );
    menu.add_action(
        Pr('L'),
        "Local public timeline (this server)",
        LogicalAction::Goto(NonUtilityActivity::LocalTimelineFile.into()),
    );
    menu.add_action(
        Pr('#'),
        "Timeline for a #hashtag",
        LogicalAction::Goto(OverlayActivity::GetHashtagToRead.into()),
    );
    menu.add_blank_line();
    menu.add_action(
        Pr('I'),
        "View a post by its ID",
        LogicalAction::Goto(OverlayActivity::GetPostIdToRead.into()),
    );
    menu.add_blank_line();
    menu.add_action(
        Pr('C'),
        "Compose a post",
        LogicalAction::Goto(ComposeActivity::ComposeToplevel.into()),
    );
    menu.add_blank_line();

    // We don't need to provide a LogicalAction for this keystroke,
    // because it's only here as documentation. The actual handler is
    // centralised into TuiLogicalState, so that you can press [ESC]
    // anywhere.
    menu.add_action(Escape, "Utilities and Exit", LogicalAction::Nothing);

    menu.add_info(&format!("Logged in as {}", &client.our_account_fq()));
    if !client.is_writable() {
        menu.add_info_coloured(ColouredString::uniform(
            "Mastodonochrome was run in readonly mode",
            'r',
        ));
    }
    if let Some(n) = client
        .account_by_id(&client.our_account_id())
        .ok()
        .and_then(|ac| ac.source)
        .map(|s| s.follow_requests_count)
    {
        if n > 0 {
            menu.add_info_coloured(
                ColouredString::uniform(
                    &format!(
                        "You have {} pending follow request{}!",
                        n,
                        if n == 1 { "" } else { "s" },
                    ),
                    'F',
                ) + ColouredString::general(
                    " View with [ESC][Y][L][R]",
                    " HHHHHHHHHHHKKKHHKHHKHHKH",
                ),
            );
        }
    }

    Box::new(menu.finalise())
}

pub fn utils_menu(client: &Client) -> Box<dyn ActivityState> {
    let mut menu = Menu::new(
        ColouredString::general("Utilities [ESC]", "HHHHHHHHHHHKKKH"),
        false,
    );

    let logged_in = client.auth.is_logged_in();
    if logged_in {
        let our_account_id = client.our_account_id();

        menu.add_action(
            Pr('R'),
            "Read Mentions",
            LogicalAction::Goto(UtilityActivity::ReadMentions.into()),
        );
        menu.add_blank_line();
        menu.add_action(
            Pr('E'),
            "Examine User",
            LogicalAction::Goto(OverlayActivity::GetUserToExamine.into()),
        );
        menu.add_action(
            Pr('Y'),
            "Examine Yourself",
            LogicalAction::Goto(
                UtilityActivity::ExamineUser(our_account_id).into(),
            ),
        );
        menu.add_blank_line();

        menu.add_action(
            Pr('L'),
            "Logs menu",
            LogicalAction::Goto(UtilityActivity::LogsMenu1.into()),
        );
        menu.add_blank_line();

        menu.add_action(
            Pr('G'),
            "Go to Main Menu",
            LogicalAction::Goto(NonUtilityActivity::MainMenu.into()),
        );
        menu.add_blank_line();
    }

    menu.add_action(
        Pr('X'),
        "Exit Mastodonochrome",
        LogicalAction::Goto(UtilityActivity::ExitMenu.into()),
    );

    Box::new(menu.finalise())
}

pub fn exit_menu() -> Box<dyn ActivityState> {
    let mut menu = Menu::new(
        ColouredString::general(
            "Exit Mastodonochrome [ESC][X]",
            "HHHHHHHHHHHHHHHHHHHHHHKKKHHKH",
        ),
        false,
    );

    menu.add_action(Pr('X'), "Confirm exit", LogicalAction::Exit);

    Box::new(menu.finalise())
}

pub fn logs_menu_1() -> Box<dyn ActivityState> {
    let mut menu = Menu::new(
        ColouredString::general(
            "Client Logs [ESC][L]",
            "HHHHHHHHHHHHHKKKHHKH",
        ),
        false,
    );

    menu.add_action(
        Pr('H'),
        "HTTP Log",
        LogicalAction::Goto(UtilityActivity::HttpLog.into()),
    );

    menu.add_action(
        Pr('L'),
        "Server Logs",
        LogicalAction::Goto(UtilityActivity::LogsMenu2.into()),
    );

    Box::new(menu.finalise())
}

pub fn logs_menu_2() -> Box<dyn ActivityState> {
    let mut menu = Menu::new(
        ColouredString::general(
            "Server Logs [ESC][L][L]",
            "HHHHHHHHHHHHHKKKHHKHHKH",
        ),
        false,
    );

    menu.add_action(
        Pr('E'),
        "Ego Log (Boosts, Follows and Faves)",
        LogicalAction::Goto(UtilityActivity::EgoLog.into()),
    );

    Box::new(menu.finalise())
}
