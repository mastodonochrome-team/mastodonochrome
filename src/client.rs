use chrono::{DateTime, Utc};
use derive_more::From;
use lazy_static::lazy_static;
use regex::Regex;
use reqwest::Url;
use serde::de::DeserializeOwned;
use std::collections::{HashMap, HashSet, VecDeque};
use std::fs::File;
use std::io::{IoSlice, Write};
use std::ops::{Index, IndexMut};
use std::rc::Rc;
use strum::EnumCount;
use void::Void;

use super::auth::AuthConfig;
use super::posting::Post;
use super::streamparse::{stream_parser, StreamEvent, StreamResponse};
use super::tui::TuiBusyIndicator;
use super::types::*;

#[derive(Hash, Debug, PartialEq, Eq, Clone, Copy)]
pub enum Boosts {
    Show,
    Hide,
}

#[derive(Hash, Debug, PartialEq, Eq, Clone, Copy)]
pub enum Replies {
    Show,
    Hide,
}

#[derive(Hash, Debug, PartialEq, Eq, Clone)]
pub enum FeedId {
    Home,
    Local,
    Public,
    Hashtag(String),
    User(String, Boosts, Replies),
    Mentions,
    Ego,
    YourFollowRequesters,
    Favouriters(String),
    Boosters(String),
    Followers(String),
    Followees(String),
}

#[derive(Hash, Debug, PartialEq, Eq, Clone, Copy, EnumCount)]
pub enum LocalFeedId {
    Errors,
    HttpTransactions,
}

#[derive(PartialEq, Eq, Debug, Clone, Hash, From)]
pub enum AnyFeedId {
    RemoteFeed(FeedId),
    LocalFeed(LocalFeedId),
}

#[derive(Default, Clone, Debug)]
struct LocalFeedVec<T>([T; LocalFeedId::COUNT]);

impl<T> Index<LocalFeedId> for LocalFeedVec<T> {
    type Output = T;
    fn index(&self, i: LocalFeedId) -> &T {
        &self.0[i as usize]
    }
}
impl<T> IndexMut<LocalFeedId> for LocalFeedVec<T> {
    fn index_mut(&mut self, i: LocalFeedId) -> &mut T {
        &mut self.0[i as usize]
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum StreamId {
    User,
}

impl StreamId {
    /// Returns a set of all the FeedIds that this StreamId might ever
    /// deliver updates for. Used when we restart a stream after it
    /// died: after the stream is up and running, we poll all of those
    /// ids, to make sure we didn't miss any updates while the stream
    /// was down.
    pub fn feeds_affected(&self) -> Vec<FeedId> {
        match self {
            StreamId::User => {
                vec![FeedId::Home, FeedId::Mentions, FeedId::Ego]
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct StreamUpdate {
    pub id: StreamId,
    pub response: Option<StreamResponse>,
    start_time: DateTime<Utc>,
    response_time: DateTime<Utc>,
    method: reqwest::Method,
    url: reqwest::Url,
}

#[derive(Debug, Clone, Default)]
pub struct StreamUpdateResult {
    pub feeds_updated: HashSet<AnyFeedId>,
    pub stream_ended: Option<StreamId>,
}

#[derive(Debug, Default)]
pub struct Feed {
    pub ids: VecDeque<String>, // ids, whether of statuses, accounts or what

    extend_past: Option<HashMap<String, String>>,
    extend_future: Option<HashMap<String, String>>,
}

impl Feed {
    fn delete(&mut self, id: &str) {
        self.ids.retain(|x| x != id);
    }
}

#[derive(Debug, Default)]
pub struct LocalFeed {
    pub ids: VecDeque<String>, // ids, whether of statuses, accounts or what
}

impl LocalFeed {
    fn pop_front(&mut self) {
        self.ids.pop_front();
    }
}

/// Type that lets you find out whether you're the only owner of it.
///
/// The idea is: client logs like the Error Log and HTTP Log should be
/// pruned constantly at the back, because storing a permanent ongoing
/// log in RAM means you gradually eat all of memory. But you don't
/// want to prune a log while a user is actually looking at it,
/// because that's confusing, and likely annoying too (hey, that thing
/// I was trying to read just vanished!). So you want to temporarily
/// suspend pruning when an activity exists that's looking at the log.
///
/// So each log stores one of these, and a clone of it is handed out
/// to activities that have a FileDataSource for the log. Then, if the
/// log type itself is the only holder of a reference to its lock, the
/// log is prunable.
struct LogLockInner;

pub struct LogLock(Rc<LogLockInner>);

impl std::fmt::Debug for LogLock {
    fn fmt(
        &self,
        fmt: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        write!(
            fmt,
            "LogLock(ptr={:?}, count={})",
            Rc::as_ptr(&self.0),
            Rc::strong_count(&self.0)
        )
    }
}
impl Default for LogLock {
    fn default() -> Self {
        LogLock(Rc::new(LogLockInner))
    }
}
impl Clone for LogLock {
    fn clone(&self) -> Self {
        LogLock(Rc::clone(&self.0))
    }
}
impl LogLock {
    fn only(&self) -> bool {
        Rc::strong_count(&self.0) == 1
    }
}

#[derive(Debug)]
pub struct ErrorLog {
    items: VecDeque<(ClientError, DateTime<Utc>)>,
    origin: isize,
    lock: LogLock,
}

impl ErrorLog {
    fn new() -> Self {
        ErrorLog {
            items: VecDeque::new(),
            origin: 0,
            lock: LogLock::default(),
        }
    }

    fn prune(&mut self, feed: &mut LocalFeed) {
        if !self.lock.only() {
            return;
        }

        // FIXME: make cutoff time configurable
        let cutoff = Utc::now() - std::time::Duration::from_secs(86400);

        while self.items.front().is_some_and(|(_, time)| *time < cutoff) {
            self.items.pop_front();
            self.origin += 1;
            feed.pop_front();
        }
    }

    pub fn push_now(&mut self, error: ClientError) {
        self.items.push_back((error, Utc::now()));
    }

    pub fn get_bounds(&self) -> (isize, isize) {
        (self.origin, self.origin + self.items.len() as isize)
    }

    pub fn get(&self, index: isize) -> (ClientError, DateTime<Utc>) {
        self.items[(index - self.origin) as usize].clone()
    }
}

#[derive(Debug)]
pub struct HttpLog {
    items: VecDeque<TransactionLogEntry>,
    origin: isize,
    lock: LogLock,
}

impl HttpLog {
    fn new() -> Self {
        HttpLog {
            items: VecDeque::new(),
            origin: 0,
            lock: LogLock::default(),
        }
    }

    fn prune(&mut self, feed: &mut LocalFeed) {
        if !self.lock.only() {
            return;
        }

        // FIXME: make cutoff time configurable
        let cutoff = Utc::now() - std::time::Duration::from_secs(3600);

        while self.items.front().is_some_and(|tl| tl.end_time < cutoff) {
            self.items.pop_front();
            self.origin += 1;
            feed.pop_front();
        }
    }

    pub fn push(&mut self, entry: TransactionLogEntry) {
        self.items.push_back(entry);
    }

    pub fn get_bounds(&self) -> (isize, isize) {
        (self.origin, self.origin + self.items.len() as isize)
    }

    pub fn get(&self, index: isize) -> TransactionLogEntry {
        self.items[(index - self.origin) as usize].clone()
    }

    fn add_body(&mut self, index: HttpLogId, body: &str) {
        self.items[(index - self.origin) as usize].body =
            Some(body.to_owned());
    }

    fn add_note(&mut self, index: HttpLogId, note: &str) {
        self.items[(index - self.origin) as usize].note =
            Some(note.to_owned());
    }
}

/// The details of how you are following a user, or attempting to.
///
/// If the user's account is locked, then there's a third possible
/// state, which is that you have _requested_ to follow them and they
/// haven't yet given or refused permission. That's represented here
/// just as 'Following', because the same API request is used to get
/// into that state.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Followness {
    NotFollowing,
    Following {
        boosts: Boosts,
        languages: Vec<String>,
    },
}

impl Followness {
    pub fn from_rel(rel: &Relationship) -> Followness {
        if !rel.following && !rel.requested {
            Followness::NotFollowing
        } else {
            let boosts = if rel.showing_reblogs {
                Boosts::Show
            } else {
                Boosts::Hide
            };
            let languages = rel.languages.clone().unwrap_or_default();
            Followness::Following { boosts, languages }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct AccountDetails {
    pub display_name: String,
    // pub bio: String,
    pub default_visibility: Visibility,
    pub default_sensitive: bool,
    pub default_language: Option<String>,
    pub locked: bool,
    pub bot: bool,
    pub discoverable: bool,
    pub hide_collections: bool,
    pub indexable: bool,
    // pub fields: Vec<(String, String)>,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum AccountFlag {
    Block,
    Mute,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ResolvedUrl {
    Account(String),
    Status(String),
}

pub struct Client {
    pub auth: AuthConfig,
    client: reqwest::blocking::Client,
    accounts: HashMap<String, Account>,
    statuses: HashMap<String, Status>,
    notifications: HashMap<String, Notification>,
    polls: HashMap<String, Poll>,
    resolved_urls: HashMap<String, ResolvedUrl>,
    feeds: HashMap<FeedId, Feed>,
    local_feeds: LocalFeedVec<LocalFeed>,
    instance: Option<Instance>,
    permit_write: bool,
    logfile: Option<File>,
    error_log: ErrorLog,
    http_log: HttpLog,
    busy: Option<TuiBusyIndicator>,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ClientError {
    Internal(String),                          // message
    UrlParse(String, String),                  // url, parsing error message
    UrlFetchNet(String, String),               // url, error message
    UrlFetchHTTP(String, reqwest::StatusCode), // url, status

    // url, status, error text from the JSON error document
    UrlFetchHTTPRich(String, reqwest::StatusCode, String),

    LinkParse(String, String), // url, parsing error message
    JSONParse(String, String), // url, parsing error message
    InvalidJSONSyntax(String, String), // url, parsing error message
    UnexpectedJSONContent(String, String, String), // url, deser msg, json
    UrlConsistency(String, String), // url, error message
    Consistency(String),       // just error message
}

impl super::top_level_error::TopLevelErrorCandidate for ClientError {}

impl From<reqwest::Error> for ClientError {
    fn from(err: reqwest::Error) -> Self {
        match err.url() {
            Some(url) => {
                ClientError::UrlFetchNet(url.to_string(), err.to_string())
            }
            None => ClientError::Internal(err.to_string()),
        }
    }
}

impl From<Void> for ClientError {
    fn from(err: Void) -> Self {
        void::unreachable(err)
    }
}

impl ClientError {
    fn from_response(
        url: &str,
        rsp: reqwest::blocking::Response,
        log: &mut HttpLog,
        log_id: HttpLogId,
    ) -> Self {
        let rspstatus = rsp.status();
        if let Ok(text) = rsp.text() {
            log.add_body(log_id, &text);
            if let Ok(err) = serde_json::from_str::<ServerError>(&text) {
                ClientError::UrlFetchHTTPRich(
                    url.to_owned(),
                    rspstatus,
                    err.error,
                )
            } else {
                ClientError::UrlFetchHTTP(url.to_owned(), rspstatus)
            }
        } else {
            ClientError::UrlFetchHTTP(url.to_owned(), rspstatus)
        }
    }
}

impl std::fmt::Display for ClientError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        match self {
            ClientError::Internal(ref msg) => {
                write!(f, "internal failure: {}", msg)
            }
            ClientError::UrlParse(ref url, ref msg) => {
                write!(f, "Parse failure {} (retrieving URL: {})", msg, url)
            }
            ClientError::UrlFetchNet(ref url, ref msg) => {
                write!(f, "{} (retrieving URL: {})", msg, url)
            }
            ClientError::UrlFetchHTTP(ref url, status) => {
                write!(f, "{} (retrieving URL: {})", status, url)
            }
            ClientError::UrlFetchHTTPRich(ref url, status, ref msg) => {
                write!(
                    f,
                    "{} (HTTP status {}; retrieving URL: {})",
                    msg, status, url
                )
            }
            ClientError::JSONParse(ref url, ref msg) => {
                write!(f, "{} (parsing JSON returned from URL: {})", msg, url)
            }
            ClientError::InvalidJSONSyntax(url, msg) => {
                write!(f, "{msg} (bad JSON syntax from URL: {url})")
            }
            ClientError::UnexpectedJSONContent(url, msg, json) => {
                write!(
                    f,
            "{msg} (unexpected JSON value from URL: {url}, received {json})",
                )
            }
            ClientError::LinkParse(ref url, ref msg) => {
                write!(
                    f,
                    "{} (parsing Link header returned from URL: {})",
                    msg, url
                )
            }
            ClientError::UrlConsistency(ref url, ref msg) => {
                write!(f, "{} (after fetching URL: {})", msg, url)
            }
            ClientError::Consistency(ref msg) => {
                write!(f, "{}", msg)
            }
        }
    }
}

impl std::error::Error for ClientError {}

// Our own struct to collect the pieces of an HTTP request before we
// pass it on to reqwests. Allows incremental adding of request parameters.
pub struct Req {
    method: reqwest::Method,
    url_suffix: String,
    parameters: Vec<(String, String)>,
}

impl Req {
    pub fn get(url_suffix: &str) -> Self {
        Req {
            method: reqwest::Method::GET,
            url_suffix: url_suffix.to_owned(),
            parameters: Vec::new(),
        }
    }

    pub fn post(url_suffix: &str) -> Self {
        Req {
            method: reqwest::Method::POST,
            url_suffix: url_suffix.to_owned(),
            parameters: Vec::new(),
        }
    }

    pub fn patch(url_suffix: &str) -> Self {
        Req {
            method: reqwest::Method::PATCH,
            url_suffix: url_suffix.to_owned(),
            parameters: Vec::new(),
        }
    }

    pub fn put(url_suffix: &str) -> Self {
        Req {
            method: reqwest::Method::PUT,
            url_suffix: url_suffix.to_owned(),
            parameters: Vec::new(),
        }
    }

    pub fn param<T>(mut self, key: &str, value: T) -> Self
    where
        T: ReqParam,
    {
        self.parameters.push((key.to_owned(), value.param_value()));
        self
    }

    pub fn url(&self, base_url: &str) -> Result<(String, Url), ClientError> {
        let urlstr = base_url.to_owned() + &self.url_suffix;
        let parsed = if self.method == reqwest::Method::GET
            && !self.parameters.is_empty()
        {
            Url::parse_with_params(&urlstr, self.parameters.iter())
        } else {
            Url::parse(&urlstr)
        };
        let url = match parsed {
            Ok(url) => Ok(url),
            Err(e) => {
                Err(ClientError::UrlParse(urlstr.clone(), e.to_string()))
            }
        }?;
        Ok((urlstr, url))
    }

    pub fn build(
        self,
        base_url: &str,
        client: &reqwest::blocking::Client,
        bearer_token: Option<&str>,
    ) -> Result<(String, reqwest::blocking::RequestBuilder), ClientError> {
        let (urlstr, url) = self.url(base_url)?;
        let is_get = self.method == reqwest::Method::GET;
        let req = client.request(self.method, url);
        let req = match bearer_token {
            Some(tok) => req.bearer_auth(tok),
            None => req,
        };
        let req = if !is_get && !self.parameters.is_empty() {
            req.form(&self.parameters)
        } else {
            req
        };
        Ok((urlstr, req))
    }
}

pub trait ReqParam {
    fn param_value(self) -> String;
}

impl ReqParam for &str {
    fn param_value(self) -> String {
        self.to_owned()
    }
}
impl ReqParam for String {
    fn param_value(self) -> String {
        self
    }
}
impl ReqParam for &String {
    fn param_value(self) -> String {
        self.clone()
    }
}
impl ReqParam for i32 {
    fn param_value(self) -> String {
        self.to_string()
    }
}
impl ReqParam for usize {
    fn param_value(self) -> String {
        self.to_string()
    }
}
impl ReqParam for bool {
    fn param_value(self) -> String {
        match self {
            false => "false",
            true => "true",
        }
        .to_owned()
    }
}
impl ReqParam for Visibility {
    fn param_value(self) -> String {
        // A bit roundabout, but means we get to reuse the 'rename'
        // strings defined in types.rs
        let encoded = serde_json::to_string(&self).expect("can't fail");
        let decoded: String =
            serde_json::from_str(&encoded).expect("can't fail either");
        decoded
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum FeedExtend {
    Initial,
    Past,
    Future,
}

pub fn reqwest_client() -> Result<reqwest::blocking::Client, ClientError> {
    // We turn off cross-site HTTP redirections in our client
    // objects. That's because in general it doesn't do any good
    // for a Mastodon API endpoint to redirect to another host:
    // most requests have to be authenticated, and if you're
    // redirected to another host, that host (by normal reqwest
    // policy for cross-site redirections) won't receive your auth
    // header. So we might as well reject the redirection in the
    // first place.
    //
    // An exception is when setting up streaming connections,
    // because those _can_ redirect to another host - but in that
    // case, the target host can be known in advance (via the
    // streaming URL in the instance configuration), and also, we
    // still can't let _reqwest_ quietly follow the redirection,
    // because it will strip off the auth. So we follow it
    // ourselves manually, vet the target URL, and put the auth
    // back on.
    let no_xsite = reqwest::redirect::Policy::custom(|attempt| {
        if attempt.previous().len() > 10 {
            attempt.error("too many redirects")
        } else if let Some(prev_url) = attempt.previous().last() {
            let next_url = attempt.url();
            if (prev_url.host(), prev_url.port())
                != (next_url.host(), next_url.port())
            {
                // Stop and pass the 3xx response back to the
                // caller, rather than throwing a fatal error.
                // That way, the streaming setup can implement its
                // special case.
                attempt.stop()
            } else {
                attempt.follow()
            }
        } else {
            panic!("confusing redirect with no previous URLs!");
        }
    });

    let client = reqwest::blocking::Client::builder()
        .redirect(no_xsite)
        .build()?;
    Ok(client)
}

// An index into the HttpLog identifying a particular transaction
type HttpLogId = isize;

#[derive(Debug, Clone)]
enum TransactionResult {
    HTTPStatus(reqwest::StatusCode),
    StreamEvent,
}

#[derive(Debug, Clone)]
pub struct TransactionLogEntry {
    pub start_time: DateTime<Utc>,
    pub end_time: DateTime<Utc>,
    method: reqwest::Method,
    url: reqwest::Url,
    pub req_headers: Vec<(String, String)>,
    result: TransactionResult,
    pub rsp_headers: Vec<(String, String)>,
    id: HttpLogId,
    pub body_prefix: Option<String>,
    pub body: Option<String>,
    pub reqbody: Option<String>,
    pub note: Option<String>,
}

impl TransactionLogEntry {
    fn translate_header(
        h: (&reqwest::header::HeaderName, &reqwest::header::HeaderValue),
    ) -> (String, String) {
        let (key, value) = h;
        let value = if key == reqwest::header::AUTHORIZATION {
            "[elided]".to_owned()
        } else {
            match value.to_str() {
                Ok(s) => s.to_owned(),
                Err(..) => format!("invalid UTF-8: {:?}", value.as_bytes()),
            }
        };
        (key.as_str().to_owned(), value)
    }

    pub fn reason(&self) -> Option<&'static str> {
        match self.result {
            TransactionResult::HTTPStatus(status) => {
                Some(status.canonical_reason().unwrap_or("[untranslatable]"))
            }
            TransactionResult::StreamEvent => None,
        }
    }

    pub fn format_short(&self) -> String {
        match self.result {
            TransactionResult::HTTPStatus(status) => format!(
                "{0} {1} -> {2} {3}",
                self.method.as_str(),
                self.url.as_str(),
                status.as_str(),
                self.reason().unwrap(),
            ),
            TransactionResult::StreamEvent => format!(
                "{0} {1} -> stream event",
                self.method.as_str(),
                self.url.as_str()
            ),
        }
    }

    pub fn format_full(&self) -> Vec<String> {
        let mut lines = Vec::new();

        lines.push(format!(
            "Request: {0} {1}",
            self.method.as_str(),
            self.url.as_str()
        ));
        for (key, value) in &self.req_headers {
            lines.push(format!("  {0}: {1}", key, value));
        }
        match self.result {
            TransactionResult::HTTPStatus(status) => lines.push(format!(
                "Response: {0} {1}",
                status.as_str(),
                self.reason().unwrap()
            )),
            TransactionResult::StreamEvent => {
                lines.push("Response: stream event".to_owned())
            }
        }
        for (key, value) in &self.rsp_headers {
            lines.push(format!("  {0}: {1}", key, value));
        }
        lines.push("".to_owned());

        lines
    }

    pub fn method_str(&self) -> &str {
        self.method.as_str()
    }
    pub fn url_str(&self) -> &str {
        self.url.as_str()
    }
    pub fn status_str(&self) -> &str {
        match &self.result {
            TransactionResult::HTTPStatus(status) => status.as_str(),
            TransactionResult::StreamEvent => "stream event",
        }
    }

    pub fn write_to(&self, logfile: &mut Option<File>) {
        if let Some(ref mut file) = logfile {
            for line in self.format_full() {
                // Deliberately squash any error from writing to the
                // log file. If the disk fills up, it's a shame to
                // lose the logs, but we don't _also_ want to
                // terminate the client in a panic.
                let _ = file.write_vectored(&[
                    IoSlice::new(line.as_bytes()),
                    IoSlice::new(b"\n"),
                ]);
            }
        }
    }
}

const REDIRECT_MAGIC_STRING: &str = "urn:ietf:wg:oauth:2.0:oob";
const WEBSITE: &str =
    "https://www.chiark.greenend.org.uk/~sgtatham/mastodonochrome/";
const WEBSITE_REGISTERED: &str = "https://www.chiark.greenend.org.uk/~sgtatham/mastodonochrome/registered.html";

pub enum AppTokenType<'a> {
    ClientCredentialsLogin,
    ClientCredentialsRegister,
    AuthorizationCode(&'a str),
}

impl Client {
    pub fn new(auth: AuthConfig) -> Result<Self, ClientError> {
        Ok(Client {
            auth,
            client: reqwest_client()?,
            accounts: HashMap::new(),
            statuses: HashMap::new(),
            notifications: HashMap::new(),
            polls: HashMap::new(),
            resolved_urls: HashMap::new(),
            feeds: HashMap::new(),
            local_feeds: LocalFeedVec::default(),
            instance: None,
            permit_write: false,
            logfile: None,
            error_log: ErrorLog::new(),
            http_log: HttpLog::new(),
            busy: None,
        })
    }

    pub fn set_writable(&mut self, permit: bool) {
        self.permit_write = permit;
    }

    pub fn is_writable(&self) -> bool {
        self.permit_write
    }

    pub fn set_logfile(&mut self, file: Option<File>) {
        self.logfile = file;
    }

    pub fn set_busy_indicator(&mut self, busy: TuiBusyIndicator) {
        self.busy = Some(busy);
    }

    fn set_busy(&self) {
        if let Some(busy) = &self.busy {
            busy.set_busy();
        }
    }

    pub fn clear_caches(&mut self) {
        self.accounts.clear();
        self.statuses.clear();
        self.notifications.clear();
        self.polls.clear();
        self.feeds.clear();
        self.instance = None;
    }

    pub fn fq(&self, acct: &str) -> String {
        match acct.contains('@') {
            true => acct.to_owned(),
            false => {
                acct.to_owned()
                    + "@"
                    + self
                        .auth
                        .instance_domain
                        .as_deref()
                        .expect("Should never call fq in pre-login UI states")
            }
        }
    }

    pub fn our_account_id_optional(&self) -> Option<String> {
        self.auth.account_id.clone()
    }

    pub fn our_account_id(&self) -> String {
        self.auth
            .account_id
            .as_ref()
            .expect("Should never call our_account_id in pre-login UI states")
            .clone()
    }

    pub fn our_account_fq(&self) -> String {
        self.fq(self
            .auth
            .username
            .as_deref()
            .expect("Should never call our_account_fq in pre-login UI states"))
    }

    pub fn instance_url_ref(&self) -> &str {
        self.auth
            .instance_url
            .as_ref()
            .expect("Should have set up an instance URL before calling")
    }

    fn consume_transaction_log(&mut self, log: TransactionLogEntry) {
        log.write_to(&mut self.logfile)
    }

    fn api_request_cl(
        &self,
        client: &reqwest::blocking::Client,
        req: Req,
    ) -> Result<(String, reqwest::blocking::RequestBuilder), ClientError> {
        if req.method != reqwest::Method::GET && !self.permit_write {
            return Err(ClientError::Internal(
                "Non-GET request attempted in readonly mode".to_string(),
            ));
        }

        let base_url = self
            .auth
            .instance_url
            .as_ref()
            .expect("Should have set up an instance URL before calling")
            .clone();
        let base_url = if !base_url.ends_with("/") {
            base_url + "/"
        } else {
            base_url
        };
        req.build(&base_url, client, self.auth.user_token.as_deref())
    }

    fn execute_and_log_request(
        &mut self,
        req: reqwest::blocking::Request,
    ) -> Result<(reqwest::blocking::Response, TransactionLogEntry), ClientError>
    {
        let method = req.method().clone();
        let url = req.url().clone();
        let mut req_headers = Vec::new();
        for h in req.headers() {
            req_headers.push(TransactionLogEntry::translate_header(h));
        }

        let start_time = Utc::now();
        self.set_busy();
        let reqbody = req
            .body()
            .and_then(|b| b.as_bytes())
            .and_then(|bytes| std::str::from_utf8(bytes).ok())
            .map(|s| s.to_owned());
        let rsp = self.client.execute(req)?;
        let end_time = Utc::now();
        let result = TransactionResult::HTTPStatus(rsp.status());
        let mut rsp_headers = Vec::new();
        for h in rsp.headers() {
            rsp_headers.push(TransactionLogEntry::translate_header(h));
        }

        let new_id =
            self.http_log.origin + (self.http_log.items.len() as isize);

        let log = TransactionLogEntry {
            start_time,
            end_time,
            method,
            url,
            req_headers,
            result,
            rsp_headers,
            id: new_id,
            body_prefix: None,
            body: None,
            reqbody,
            note: None,
        };

        self.http_log.push(log.clone());
        self.local_feeds[LocalFeedId::HttpTransactions]
            .ids
            .push_back(new_id.to_string());

        Ok((rsp, log))
    }

    /// Sends and logs, but doesn't do any checking on the response
    ///
    /// You must call `.is_success()` (or equivalent) on the response
    /// yourself.
    fn api_request_raw(
        &mut self,
        req: Req,
    ) -> Result<(String, reqwest::blocking::Response, HttpLogId), ClientError>
    {
        let (urlstr, req) = self.api_request_cl(&self.client, req)?;
        let (rsp, log) = self.execute_and_log_request(req.build()?)?;
        let log_id = log.id;
        self.consume_transaction_log(log);
        Ok((urlstr, rsp, log_id))
    }

    /// Makes a request and insists it was a plain success
    ///
    /// Calls `.is_success()` for you.
    fn api_request_ok(
        &mut self,
        req: Req,
    ) -> Result<(String, reqwest::blocking::Response, HttpLogId), ClientError>
    {
        let (url, rsp, log_id) = self.api_request_raw(req)?;
        let rspstatus = rsp.status();
        if !rspstatus.is_success() {
            return Err(ClientError::from_response(
                &url,
                rsp,
                &mut self.http_log,
                log_id,
            ));
        }
        Ok((url, rsp, log_id))
    }

    /// Makes a request and deserialises the returned JSON
    ///
    /// Insists that the response is a success.
    /// Returns the `T` and the URL.
    fn api_request_parse<T: DeserializeOwned>(
        &mut self,
        req: Req,
    ) -> Result<(T, String), ClientError> {
        // There's a lot of code here that doesn't depend on T, but
        // the generics will monomorphise it for each T.  If we cared enough
        // about binary size, we could make a non-generic inner function.
        let (url, rsp, log_id) = self.api_request_ok(req)?;
        let text = rsp.text()?;
        self.http_log.add_body(log_id, &text);
        let t: T = serde_json::from_str(&text).map_err(|e0| {
            let url = url.clone();
            let val = match serde_json::from_str::<serde_json::Value>(&text) {
                Ok(y) => y,
                Err(e) => {
                    return ClientError::InvalidJSONSyntax(url, e.to_string())
                }
            };
            let val_restring = serde_json::to_string(&val)
                .unwrap_or_else(|e| format!("failed to regenerate json! {e}"));
            let e = match serde_json::from_value::<T>(val) {
                Err(e) => e.to_string(),
                Ok(_wat) => format!(
           "unexpectedly parsed Ok from Value! (earlier, from_str gave {e0}",
                ),
            };
            ClientError::UnexpectedJSONContent(url, e, val_restring)
        })?;
        Ok((t, url))
    }

    pub fn instance(&mut self) -> Result<Instance, ClientError> {
        if let Some(ref inst) = self.instance {
            return Ok(inst.clone());
        }

        let inst: Instance =
            self.api_request_parse(Req::get("api/v2/instance"))?.0;

        self.instance = Some(inst.clone());
        Ok(inst)
    }

    fn cache_account(&mut self, ac: &Account) {
        let mut ac = ac.clone();
        // Don't overwrite a cached account with a 'source' to one
        // without.
        if ac.source.is_none() {
            if let Some(source) =
                self.accounts.get(&ac.id).and_then(|ac| ac.source.as_ref())
            {
                ac.source = Some(source.clone());
            }
        }
        self.accounts.insert(ac.id.to_string(), ac);
    }

    fn cache_status(&mut self, st: &Status) {
        self.cache_account(&st.account);
        if let Some(poll) = &st.poll {
            self.cache_poll(poll);
        }
        self.statuses.insert(st.id.to_string(), st.clone());
        if let Some(ref sub) = st.reblog {
            self.statuses.insert(sub.id.to_string(), *sub.clone());
        }
    }

    fn cache_notification(&mut self, n: &Notification) {
        self.cache_account(&n.account);
        if let Some(st) = &n.status {
            self.cache_status(st);
        }
        self.notifications.insert(n.id.to_string(), n.clone());
    }

    fn cache_poll(&mut self, poll: &Poll) {
        self.polls.insert(poll.id.to_string(), poll.clone());
    }

    fn cache_search_results(&mut self, sr: &SearchResults) {
        for ac in &sr.accounts {
            self.cache_account(ac);
        }
        for st in &sr.statuses {
            self.cache_status(st);
        }
    }

    fn account_by_id_internal(
        &mut self,
        id: Option<&str>,
    ) -> Result<Account, ClientError> {
        // If we're fetching our own account, or we don't know our
        // account id yet because we're still logging in, we use the
        // verify_credentials request, which gets the extra
        // information. This also means we must repeat the request if
        // we've already received a copy of our own account details
        // via some other API and it doesn't contain the extra
        // information.

        if let Some(ac) = id.and_then(|id| self.accounts.get(id)) {
            if Some(&ac.id) != self.auth.account_id.as_ref()
                || ac.source.is_some()
            {
                return Ok(ac.clone());
            }
        }

        let req = if id == self.auth.account_id.as_deref() {
            Req::get(&format!("api/v1/accounts/verify_credentials"))
        } else {
            Req::get(&format!("api/v1/accounts/{}", id.unwrap()))
        };
        let (ac, url) = self.api_request_parse::<Account>(req)?;
        if id.is_some_and(|id| ac.id != id) {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong account id {}", &ac.id),
            ));
        }
        self.cache_account(&ac);
        Ok(ac)
    }

    pub fn account_by_id(&mut self, id: &str) -> Result<Account, ClientError> {
        self.account_by_id_internal(Some(id))
    }

    pub fn verify_account_credentials(
        &mut self,
    ) -> Result<Account, ClientError> {
        self.account_by_id_internal(None)
    }

    pub fn poll_by_id(&mut self, id: &str) -> Result<Poll, ClientError> {
        if let Some(st) = self.polls.get(id) {
            return Ok(st.clone());
        }

        let req = Req::get(&("api/v1/polls/".to_owned() + id));
        let (poll, url) = self.api_request_parse::<Poll>(req)?;
        if poll.id != id {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong poll id {}", &poll.id),
            ));
        }
        self.cache_poll(&poll);
        Ok(poll)
    }

    pub fn account_relationship_by_id(
        &mut self,
        id: &str,
    ) -> Result<Relationship, ClientError> {
        let req = Req::get("api/v1/accounts/relationships").param("id", id);
        let (rels, url) = self.api_request_parse::<Vec<Relationship>>(req)?;
        for rel in rels {
            if rel.id == id {
                return Ok(rel);
            }
        }
        Err(ClientError::UrlConsistency(
            url,
            format!("request did not return expected account id {}", id),
        ))
    }

    pub fn status_by_id(&mut self, id: &str) -> Result<Status, ClientError> {
        if let Some(st) = self.statuses.get(id) {
            let mut st = st.clone();
            if let Some(ac) = self.accounts.get(&st.account.id) {
                // Update the account details with the latest version
                // we had cached
                st.account = ac.clone();
            }
            if let Some(poll) =
                st.poll.as_ref().and_then(|poll| self.polls.get(&poll.id))
            {
                // Ditto with the poll, if any
                st.poll = Some(poll.clone());
            }
            if let Some(reblog_id) = st.reblog.clone().map(|st| st.id) {
                if let Ok(reblog_st) = self.status_by_id(&reblog_id) {
                    // Update the reblogged status with the latest version too
                    st.reblog = Some(Box::new(reblog_st));
                }
            }
            return Ok(st);
        }

        let req = Req::get(&("api/v1/statuses/".to_owned() + id));
        let (st, url) = self.api_request_parse::<Status>(req)?;
        if st.id != id {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong status id {}", &st.id),
            ));
        }
        self.cache_status(&st);
        Ok(st)
    }

    pub fn status_source(
        &mut self,
        id: &str,
    ) -> Result<StatusSource, ClientError> {
        let req = Req::get(&format!("api/v1/statuses/{}/source", id));
        let (ss, url) = self.api_request_parse::<StatusSource>(req)?;
        if ss.id != id {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong status id {}", &ss.id),
            ));
        }
        Ok(ss)
    }

    pub fn notification_by_id(
        &mut self,
        id: &str,
    ) -> Result<Notification, ClientError> {
        if let Some(not) = self.notifications.get(id) {
            let mut not = not.clone();
            if let Some(ac) = self.accounts.get(&not.account.id) {
                // Update the account details with the latest version
                // we had cached
                not.account = ac.clone();
            }
            let status_id = not.status.as_ref().map(|st| &st.id);
            if let Some(status_id) = status_id {
                if let Some(st) = self.statuses.get(status_id) {
                    not.status = Some(st.clone());
                }
            }
            return Ok(not);
        }

        let req = Req::get(&("api/v1/notifications/".to_owned() + id));
        let (not, url) = self.api_request_parse::<Notification>(req)?;
        if not.id != id {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong notification id {}", &not.id),
            ));
        }
        self.cache_notification(&not);
        Ok(not)
    }

    fn base_feed_req(id: &FeedId) -> Req {
        match id {
            FeedId::Home => Req::get("api/v1/timelines/home"),
            FeedId::Local => {
                Req::get("api/v1/timelines/public").param("local", true)
            }
            FeedId::Public => {
                Req::get("api/v1/timelines/public").param("local", false)
            }
            FeedId::Hashtag(ref tag) => {
                Req::get(&format!("api/v1/timelines/tag/{}", &tag))
            }
            FeedId::User(id, boosts, replies) => {
                Req::get(&format!("api/v1/accounts/{}/statuses", id))
                    .param("exclude_reblogs", *boosts == Boosts::Hide)
                    .param("exclude_replies", *replies == Replies::Hide)
            }
            FeedId::Mentions => {
                Req::get("api/v1/notifications").param("types[]", "mention")
            }
            FeedId::Ego => Req::get("api/v1/notifications")
                .param("types[]", "reblog")
                .param("types[]", "follow")
                .param("types[]", "favourite"),
            FeedId::Favouriters(id) => {
                Req::get(&format!("api/v1/statuses/{id}/favourited_by"))
            }
            FeedId::Boosters(id) => {
                Req::get(&format!("api/v1/statuses/{id}/reblogged_by"))
            }
            FeedId::Followers(id) => {
                Req::get(&format!("api/v1/accounts/{id}/followers"))
            }
            FeedId::Followees(id) => {
                Req::get(&format!("api/v1/accounts/{id}/following"))
            }
            FeedId::YourFollowRequesters => {
                Req::get(&format!("api/v1/follow_requests"))
            }
        }
    }

    fn decode_feed_response<T>(
        &mut self,
        feed_id: &FeedId,
        url: &str,
        body: &str,
    ) -> Result<T, ClientError>
    where
        T: FromIterator<String>,
    {
        // Decode the JSON response as a different kind of type
        // depending on the feed. But in all cases we expect to end up
        // with a list of ids.
        match feed_id {
            FeedId::Home
            | FeedId::Local
            | FeedId::Public
            | FeedId::Hashtag(..)
            | FeedId::User(..) => {
                let sts: Vec<Status> = match serde_json::from_str(&body) {
                    Ok(sts) => Ok(sts),
                    Err(e) => Err(ClientError::JSONParse(
                        url.to_owned(),
                        e.to_string(),
                    )),
                }?;
                for st in &sts {
                    self.cache_status(st);
                }
                Ok(sts.iter().rev().map(|st| st.id.clone()).collect())
            }
            FeedId::Mentions | FeedId::Ego => {
                let mut nots: Vec<Notification> =
                    match serde_json::from_str(&body) {
                        Ok(nots) => Ok(nots),
                        Err(e) => Err(ClientError::JSONParse(
                            url.to_owned(),
                            e.to_string(),
                        )),
                    }?;

                match feed_id {
                    FeedId::Mentions => {
                        // According to the protocol spec, all notifications
                        // of type Mention should have a status in them. We
                        // double-check that here, so that the rest of our
                        // code can safely .unwrap() or .expect() the status
                        // from notifications they get via this feed.
                        nots.retain(|not| {
                            not.ntype == NotificationType::Mention
                                && not.status.is_some()
                        });
                    }
                    FeedId::Ego => {
                        nots.retain(|not| {
                            not.ntype == NotificationType::Reblog
                                || not.ntype == NotificationType::Follow
                                || not.ntype == NotificationType::Favourite
                        });
                    }
                    _ => panic!("outer match passed us {:?}", feed_id),
                }
                for not in &nots {
                    self.cache_notification(not);
                }
                Ok(nots.iter().rev().map(|not| not.id.clone()).collect())
            }
            FeedId::Favouriters(..)
            | FeedId::Boosters(..)
            | FeedId::Followers(..)
            | FeedId::Followees(..)
            | FeedId::YourFollowRequesters => {
                let acs: Vec<Account> = match serde_json::from_str(&body) {
                    Ok(acs) => Ok(acs),
                    Err(e) => Err(ClientError::JSONParse(
                        url.to_owned(),
                        e.to_string(),
                    )),
                }?;
                for ac in &acs {
                    self.cache_account(ac);
                }
                Ok(acs.iter().rev().map(|ac| ac.id.clone()).collect())
            }
        }
    }

    // Ok(bool) tells you whether any new items were in fact retrieved
    pub fn fetch_feed(
        &mut self,
        id: &FeedId,
        ext: FeedExtend,
    ) -> Result<bool, ClientError> {
        if ext == FeedExtend::Initial {
            if self.feeds.contains_key(id) {
                // No need to fetch the initial contents - we already
                // have some.
                //
                // In this situation the boolean return value is not
                // expected to very useful: its main purpose is when
                // finding out if extending an _existing_ feed did
                // anything. But we might as well return false in the
                // case where nothing changed, and true in the case
                // below where we did end up retrieving something.
                return Ok(false);
            }
        } else if !self.feeds.contains_key(id) {
            // We might be called on to extend a feed that we've
            // not yet fetched for the first time, if a streaming
            // notification comes in before the user has yet taken
            // us into the activity reading that feed. If so,
            // ignore the event; the _actual_ initial fetch will
            // deal with it later.
            return Ok(false);
        }

        let req = Self::base_feed_req(id);

        fn initial(req: Req) -> Req {
            req.param("limit", 32)
        }
        let req = match ext {
            FeedExtend::Initial => initial(req),
            FeedExtend::Past => {
                if let Some(feed) = self.feeds.get(id) {
                    match feed.extend_past {
                        None => return Ok(false),
                        Some(ref params) => {
                            let mut req = req;
                            for (key, value) in params {
                                req = req.param(key, value);
                            }
                            req
                        }
                    }
                } else {
                    req
                }
            }
            FeedExtend::Future => {
                if let Some(feed) = self.feeds.get(id) {
                    match feed.extend_future {
                        // If we don't have an extend_future link for
                        // the feed yet, fetch it as if for the first
                        // time. This only occurs in the edge case
                        // where the initial fetch of the feed
                        // returned nothing at all - but that edge
                        // case *does* come up, if you're testing on a
                        // fresh instance of the dev server.
                        None => initial(req),
                        Some(ref params) => {
                            let mut req = req;
                            for (key, value) in params {
                                req = req.param(key, value);
                            }
                            req
                        }
                    }
                } else {
                    req
                }
            }
        };

        let (url, rsp, log_id) = self.api_request_ok(req)?;

        // Keep the Link: headers after we consume the response, for
        // use later once we've constructed a Feed
        let link_headers: Vec<_> = rsp
            .headers()
            .get_all(reqwest::header::LINK)
            .iter()
            .cloned()
            .collect();

        let body = rsp.text()?;
        self.http_log.add_body(log_id, &body);

        let ids: VecDeque<String> =
            self.decode_feed_response(id, &url, &body)?;
        let any_new = !ids.is_empty();

        match ext {
            FeedExtend::Initial => {
                self.feeds.insert(
                    id.clone(),
                    Feed {
                        ids,
                        extend_past: None,
                        extend_future: None,
                    },
                );
            }
            FeedExtend::Future => {
                let feed = self.feeds.get_mut(id).unwrap();
                for id in ids.iter() {
                    feed.ids.push_back(id.to_string());
                }
            }
            FeedExtend::Past => {
                let feed = self.feeds.get_mut(id).unwrap();
                for id in ids.iter().rev() {
                    feed.ids.push_front(id.to_string());
                }
            }
        }

        let feed = self.feeds.get_mut(id).unwrap();
        for linkhdr in link_headers {
            let linkhdr_str = match linkhdr.to_str() {
                Ok(s) => Ok(s),
                Err(e) => {
                    Err(ClientError::LinkParse(url.clone(), e.to_string()))
                }
            }?;
            let links = match parse_link_header::parse(linkhdr_str) {
                Ok(links) => Ok(links),
                Err(e) => {
                    Err(ClientError::LinkParse(url.clone(), e.to_string()))
                }
            }?;

            // If we've just used our existing 'extend into the past'
            // link, discard it, so that we don't accidentally reuse
            // it. Then if the server doesn't send us a replacement,
            // we know it's because we've reached the beginning of the
            // feed.
            //
            // We don't do the same for extension into the future,
            // because in that direction, new stuff can come into
            // existence since we last checked.
            if ext == FeedExtend::Past {
                feed.extend_past = None;
            }

            for (rel, link) in links {
                match rel.as_deref() {
                    // Oh, you think time flows _that_ way?
                    // Confusingly, the Mastodon protocol considers
                    // "next" to be heading into the past and "prev"
                    // the future.
                    //
                    // We only keep the extension link for directions
                    // that are the current frontier. If we're
                    // extending an existing feed further into the
                    // past, then the future link we have already is
                    // better than the new one (which will cause us to
                    // re-fetch stuff we already had). And vice versa.
                    Some("next") => {
                        if ext != FeedExtend::Future {
                            feed.extend_past = Some(link.queries);
                        }
                    }
                    Some("prev") => {
                        if ext != FeedExtend::Past {
                            feed.extend_future = Some(link.queries);
                        }
                    }
                    _ => (),
                }
            }
        }

        Ok(any_new)
    }

    pub fn get_feed_predecessors(
        &mut self,
        feed_id: &FeedId,
        item_id: &str,
    ) -> Result<Vec<String>, ClientError> {
        // These feeds are ineligible for this technique of finding
        // predecessors, because the id they return in each JSON
        // record is not the same id that goes in the max_id
        // parameter. Fortunately, it's OK, because we also don't
        // store a last-read position for any of these anyway.
        if matches!(
            feed_id,
            FeedId::Favouriters(..)
                | FeedId::Boosters(..)
                | FeedId::Followers(..)
                | FeedId::Followees(..)
                | FeedId::YourFollowRequesters
        ) {
            return Ok(Vec::new());
        }

        let req = Self::base_feed_req(feed_id)
            .param("limit", 32)
            .param("max_id", item_id);

        let (url, rsp, log_id) = self.api_request_ok(req)?;

        let body = rsp.text()?;
        self.http_log.add_body(log_id, &body);

        let mut ids: Vec<String> =
            self.decode_feed_response(feed_id, &url, &body)?;
        ids.reverse();
        Ok(ids)
    }

    pub fn borrow_feed(&self, id: &FeedId) -> &Feed {
        self.feeds
            .get(id)
            .expect("should only ever borrow feeds that have been fetched")
    }

    pub fn borrow_local_feed(&self, id: LocalFeedId) -> &LocalFeed {
        &self.local_feeds[id]
    }

    pub fn start_streaming_thread<Recv: Fn(StreamUpdate) + Send + 'static>(
        &mut self,
        id: &StreamId,
        receiver: Box<Recv>,
    ) -> Result<(), ClientError> {
        let req = match id {
            StreamId::User => Req::get("api/v1/streaming/user"),
        };
        let method = req.method.clone(); // to reuse for redirects and logging

        let client = reqwest_client()?;
        let (url, mut req) = self.api_request_cl(&client, req)?;
        let (rsp, log) = self.execute_and_log_request(req.build()?)?;
        let mut parsed_url = log.url.clone();
        let mut start_time = log.start_time;
        let mut log_id = log.id;
        self.consume_transaction_log(log);
        let mut rsp = rsp;
        if rsp.status().is_redirection() {
            // We follow one redirection here, and we permit it to be
            // to precisely the (host, port) pair that was specified
            // in the instance's configuration as the streaming API
            // endpoint. And when we follow that redirection, we do it
            // manually, and send the same bearer auth token that we'd
            // send for the normal API.
            //
            // An example server that needs this is the Mastodon
            // development test instance (the one you can set up in a
            // VM by running 'vagrant up' in the source checkout).
            // That runs on http://mastodon.local (via Vagrant
            // inserting that hostname in your /etc/hosts
            // temporarily), but its streaming APIs run on a separate
            // HTTP server, so you get redirected to
            // http://mastodon.local:4000.

            match rsp.headers().get(reqwest::header::LOCATION) {
                None => {
                    return Err(ClientError::UrlConsistency(
                        url,
                        "received redirection without a Location header"
                            .to_owned(),
                    ));
                }
                Some(hval) => {
                    let bval = hval.as_bytes();
                    let sval = match std::str::from_utf8(bval) {
                        Ok(s) => s,
                        Err(_) => {
                            return Err(ClientError::UrlConsistency(
                                url,
                                "HTTP redirect URL was invalid UTF-8"
                                    .to_owned(),
                            ))
                        }
                    };
                    let newurl = match rsp.url().join(sval) {
                        Ok(u) => u,
                        Err(e) => {
                            return Err(ClientError::UrlConsistency(
                                url,
                                format!("processing redirection: {}", e),
                            ))
                        }
                    };

                    let instance = self.instance()?;
                    let ok = match &instance.configuration.urls.streaming {
                        None => false,
                        Some(s) => {
                            if let Ok(goodurl) = Url::parse(s) {
                                (goodurl.host(), goodurl.port())
                                    == (newurl.host(), newurl.port())
                            } else {
                                false
                            }
                        }
                    };
                    if !ok {
                        return Err(ClientError::UrlConsistency(
                            url,
                            format!("redirection to suspicious URL {}", sval),
                        ));
                    }
                    req = client.request(method.clone(), newurl);
                    if let Some(ref token) = self.auth.user_token {
                        req = req.bearer_auth(token);
                    }
                    let (newrsp, log) =
                        self.execute_and_log_request(req.build()?)?;
                    parsed_url = log.url.clone();
                    start_time = log.start_time;
                    log_id = log.id;
                    self.consume_transaction_log(log);
                    rsp = newrsp;
                }
            }
        };

        let rspstatus = rsp.status();
        if !rspstatus.is_success() {
            return Err(ClientError::from_response(
                &url,
                rsp,
                &mut self.http_log,
                log_id,
            ));
        }
        self.http_log.add_note(log_id, "stream started");

        let id = id.clone();

        let _joinhandle = std::thread::spawn(move || {
            for response in stream_parser(rsp) {
                receiver(StreamUpdate {
                    id: id.clone(),
                    start_time,
                    method: method.clone(),
                    response_time: Utc::now(),
                    url: parsed_url.clone(),
                    response: Some(response),
                });
            }

            receiver(StreamUpdate {
                id: id.clone(),
                start_time,
                method,
                response_time: Utc::now(),
                url: parsed_url.clone(),
                response: None,
            });
        });

        Ok(())
    }

    pub fn process_stream_update(
        &mut self,
        up: StreamUpdate,
    ) -> Result<StreamUpdateResult, ClientError> {
        let mut result = StreamUpdateResult::default();

        {
            let new_id =
                self.http_log.origin + (self.http_log.items.len() as isize);

            let (body_prefix, body, note) = match &up.response {
                Some(response) => (
                    Some(format!("event:{}\ndata:", response.event_raw)),
                    Some(response.data.to_owned()),
                    None,
                ),
                None => (None, None, Some("EOF on stream".to_owned())),
            };

            let log = TransactionLogEntry {
                start_time: up.start_time,
                end_time: up.response_time,
                method: up.method,
                url: up.url,
                req_headers: Vec::new(),
                result: TransactionResult::StreamEvent,
                rsp_headers: Vec::new(),
                id: new_id,
                body_prefix,
                body,
                note,
                reqbody: None,
            };

            self.http_log.push(log.clone());
            self.local_feeds[LocalFeedId::HttpTransactions]
                .ids
                .push_back(new_id.to_string());
        }

        if let Some(response) = up.response {
            // Decide which of our FeedIds corresponds to this kind
            // event from this StreamId, and insert it into the set of
            // updated feeds we're returning.
            //
            // Also, while we're here, if the payload of the stream
            // event was any kind of object we keep in our caches,
            // cache it on general principles. (Particularly useful
            // for StatusUpdate, i.e. "some status was edited",
            // because that way we get the updated version
            // immediately.)
            let feed = response.event.and_then(|event| match (up.id, event) {
                (StreamId::User, StreamEvent::Update(st)) => {
                    self.cache_status(&st);
                    Some(FeedId::Home)
                }
                (StreamId::User, StreamEvent::StatusUpdate(st)) => {
                    self.cache_status(&st);
                    // A status on this feed has changed, so it will
                    // need refreshing, even though it hasn't been
                    // extended.
                    result.feeds_updated.insert(FeedId::Home.into());
                    None
                }
                (StreamId::User, StreamEvent::Delete(id)) => {
                    // A status on this feed has been deleted.
                    if let Some(f) = self.feeds.get_mut(&FeedId::Home) {
                        f.delete(&id);
                    }
                    result.feeds_updated.insert(FeedId::Home.into());
                    None
                }
                (StreamId::User, StreamEvent::Notification(not)) => {
                    self.cache_notification(&not);
                    match not.ntype {
                        NotificationType::Mention => Some(FeedId::Mentions),
                        NotificationType::Reblog
                        | NotificationType::Follow
                        | NotificationType::Favourite => Some(FeedId::Ego),
                        _ => None,
                    }
                }
            });

            if let Some(id) = feed {
                if self.fetch_feed(&id, FeedExtend::Future)? {
                    result.feeds_updated.insert(id.into());
                }
            }

            // Also, we've had a stream event _at all_, which means
            // the HTTP Log has also been updated.
            result
                .feeds_updated
                .insert(LocalFeedId::HttpTransactions.into());
        } else {
            // A stream has terminated. Return the terminated stream
            // id to the user.
            result.stream_ended = Some(up.id);
        }

        Ok(result)
    }

    pub fn account_by_name(
        &mut self,
        name: &str,
    ) -> Result<Account, ClientError> {
        let req = Req::get("api/v1/accounts/lookup").param("acct", name);
        let ac = self.api_request_parse::<Account>(req)?.0;
        self.cache_account(&ac);
        Ok(ac)
    }

    pub fn post_status(&mut self, post: &Post) -> Result<(), ClientError> {
        // FIXME: separate Post from a single status, so we can post threads
        let req = match &post.m.replaces_id {
            None => {
                // Making a new post, so we get to set visibility and
                // in_reply_to_id
                let req = Req::post("api/v1/statuses")
                    .param("visibility", post.m.visibility);
                let req = match &post.m.in_reply_to_id {
                    None => req,
                    Some(id) => req.param("in_reply_to_id", id),
                };
                req
            }
            Some(id) => {
                // Editing an existing post, so those aren't modifiable
                Req::put(&format!("api/v1/statuses/{}", id))
            }
        };
        let req = req.param("status", post.text.trim_end_matches('\n'));
        let req = match &post.m.language {
            None => req,
            Some(lang) => req.param("language", lang),
        };
        let req = match &post.m.content_warning {
            None => req,
            Some(text) => {
                req.param("sensitive", true).param("spoiler_text", text)
            }
        };

        self.api_request_ok(req)?;
        Ok(())
    }

    pub fn fave_boost_post(
        &mut self,
        id: &str,
        verb: &str,
    ) -> Result<(), ClientError> {
        let req = Req::post(&format!("api/v1/statuses/{id}/{verb}"));
        let st = self.api_request_parse::<Status>(req)?.0;
        // Cache the returned status so as to update its faved/boosted flags
        self.cache_status(&st);
        Ok(())
    }

    pub fn favourite_post(
        &mut self,
        id: &str,
        enable: bool,
    ) -> Result<(), ClientError> {
        let verb = if enable { "favourite" } else { "unfavourite" };
        self.fave_boost_post(id, verb)
    }

    pub fn boost_post(
        &mut self,
        id: &str,
        enable: bool,
    ) -> Result<(), ClientError> {
        let verb = if enable { "reblog" } else { "unreblog" };
        self.fave_boost_post(id, verb)
    }

    pub fn status_context(
        &mut self,
        id: &str,
    ) -> Result<Context, ClientError> {
        let req = Req::get(&format!("api/v1/statuses/{id}/context"));
        let ctx = self.api_request_parse::<Context>(req)?.0;
        for st in &ctx.ancestors {
            self.cache_status(st);
        }
        for st in &ctx.descendants {
            self.cache_status(st);
        }
        Ok(ctx)
    }

    pub fn vote_in_poll(
        &mut self,
        id: &str,
        choices: impl Iterator<Item = usize>,
    ) -> Result<(), ClientError> {
        let choices: Vec<_> = choices.collect();
        let mut req = Req::post(&format!("api/v1/polls/{id}/votes"));
        for choice in choices {
            req = req.param("choices[]", choice);
        }
        let poll = self.api_request_parse::<Poll>(req)?.0;
        // Cache the returned poll so as to update its faved/boosted flags
        self.cache_poll(&poll);
        Ok(())
    }

    pub fn set_following(
        &mut self,
        id: &str,
        follow: Followness,
    ) -> Result<(), ClientError> {
        let req = match follow {
            Followness::NotFollowing => {
                Req::post(&format!("api/v1/accounts/{id}/unfollow"))
            }
            Followness::Following { boosts, languages } => {
                let mut req =
                    Req::post(&format!("api/v1/accounts/{id}/follow"))
                        .param("reblogs", boosts == Boosts::Show);
                for language in languages {
                    req = req.param("languages[]", &language);
                }
                req
            }
        };
        self.api_request_ok(req)?;
        Ok(())
    }

    pub fn respond_to_follow_request(
        &mut self,
        id: &str,
        accept: bool,
    ) -> Result<(), ClientError> {
        let verb = if accept { "authorize" } else { "reject" };
        let req = Req::post(&format!("api/v1/follow_requests/{id}/{verb}"));
        self.api_request_ok(req)?;
        Ok(())
    }

    pub fn set_account_flag(
        &mut self,
        id: &str,
        flag: AccountFlag,
        enable: bool,
    ) -> Result<(), ClientError> {
        let req = match (flag, enable) {
            (AccountFlag::Block, true) => {
                Req::post(&format!("api/v1/accounts/{id}/block"))
            }
            (AccountFlag::Block, false) => {
                Req::post(&format!("api/v1/accounts/{id}/unblock"))
            }
            (AccountFlag::Mute, true) => {
                Req::post(&format!("api/v1/accounts/{id}/mute"))
            }
            (AccountFlag::Mute, false) => {
                Req::post(&format!("api/v1/accounts/{id}/unmute"))
            }
        };
        self.api_request_ok(req)?;
        Ok(())
    }

    pub fn set_account_details(
        &mut self,
        id: &str,
        details: AccountDetails,
    ) -> Result<(), ClientError> {
        // TODO: add "note" with details.bio, and "fields_attributes"
        // for the variable info fields
        let req = Req::patch("api/v1/accounts/update_credentials")
            .param("display_name", &details.display_name)
            .param("locked", details.locked)
            .param("bot", details.bot)
            .param("hide_collections", details.hide_collections)
            .param("indexable", details.indexable)
            .param("discoverable", details.discoverable)
            .param("source[privacy]", details.default_visibility)
            .param("source[sensitive]", details.default_sensitive);
        let req = if let Some(ref lang) = details.default_language {
            req.param("source[language]", lang)
        } else {
            req
        };

        let (ac, url) = self.api_request_parse::<Account>(req)?;
        if ac.id != id {
            return Err(ClientError::UrlConsistency(
                url,
                format!("request returned wrong account id {}", &ac.id),
            ));
        }
        self.cache_account(&ac);
        Ok(())
    }

    pub fn register_client(
        &mut self,
        creating_account: bool,
    ) -> Result<Application, ClientError> {
        // If we're logging in to an existing account, we set
        // redirect_uris to the magic string that causes the Mastodon
        // server's emailed link to display the code visibly to the
        // user, so that they can paste it back into our TUI to let it
        // retrieve its user token.
        //
        // If we're _creating_ an account, we instead set it to
        // redirect to a Mastodonochrome web page, because in that
        // situation, the browser only needs to say 'ok' - and what
        // happens if a browser tries to redirect to that urn:... URI
        // is confusing and looks more like an error than a success.
        //
        // The fact that "redirect_uris" is plural in this request and
        // singular in the followup ones suggests that we _ought_ to
        // be able to permit both possibilities here, and in the next
        // request, pick one of them. But I don't know how to do that,
        // so instead, we specify only the right one of them here.
        let redirect_uri = if creating_account {
            WEBSITE_REGISTERED
        } else {
            REDIRECT_MAGIC_STRING
        };
        let req = Req::post("/api/v1/apps")
            .param("redirect_uris", redirect_uri)
            .param("client_name", "Mastodonochrome")
            .param("scopes", "read write push")
            .param("website", WEBSITE);
        let app = self.api_request_parse::<Application>(req)?.0;
        Ok(app)
    }

    pub fn get_app_token(
        &mut self,
        app: &Application,
        toktype: AppTokenType,
    ) -> Result<Token, ClientError> {
        let client_id = match &app.client_id {
            Some(id) => Ok(id),
            None => Err(ClientError::Internal(
                "registering application did not return a client id"
                    .to_owned(),
            )),
        }?;
        let client_secret = match &app.client_secret {
            Some(id) => Ok(id),
            None => Err(ClientError::Internal(
                "registering application did not return a client secret"
                    .to_owned(),
            )),
        }?;

        let req = Req::post("/oauth/token")
            .param("client_id", client_id)
            .param("client_secret", client_secret);
        let req = match toktype {
            AppTokenType::ClientCredentialsLogin => req
                .param("grant_type", "client_credentials")
                .param("redirect_uri", REDIRECT_MAGIC_STRING),
            AppTokenType::ClientCredentialsRegister => req
                .param("grant_type", "client_credentials")
                .param("redirect_uri", WEBSITE_REGISTERED)
                .param("scope", "read write push"),
            AppTokenType::AuthorizationCode(code) => req
                .param("grant_type", "authorization_code")
                .param("redirect_uri", REDIRECT_MAGIC_STRING)
                .param("code", code)
                .param("scope", "read write push"),
        };
        let tok = self.api_request_parse::<Token>(req)?.0;
        Ok(tok)
    }

    pub fn verify_app_credentials(
        &mut self,
    ) -> Result<Application, ClientError> {
        let req = Req::get("api/v1/apps/verify_credentials");
        let app = self.api_request_parse::<Application>(req)?.0;
        Ok(app)
    }

    pub fn get_auth_url(
        &self,
        app: &Application,
    ) -> Result<String, ClientError> {
        let client_id = match &app.client_id {
            Some(id) => Ok(id),
            None => Err(ClientError::Internal(
                "registering application did not return a client id"
                    .to_owned(),
            )),
        }?;

        let (_urlstr, url) = Req::get("/oauth/authorize")
            .param("redirect_uri", REDIRECT_MAGIC_STRING)
            .param("client_id", client_id)
            .param("scope", "read write push")
            .param("response_type", "code")
            .url(self.auth.instance_url.as_deref().expect("should have set up an instance URL before calling get_auth_url"))?;
        Ok(url.to_string())
    }

    pub fn register_account(
        &mut self,
        username: &str,
        email: &str,
        password: &str,
        language: &str,
    ) -> Result<Token, ClientError> {
        let req = Req::post("api/v1/accounts")
            .param("username", username)
            .param("email", email)
            .param("password", password)
            .param("agreement", true) // the calling UI should have confirmed
            .param("locale", language);
        let tok = self.api_request_parse::<Token>(req)?.0;
        Ok(tok)
    }

    pub fn register_confirmation(
        &mut self,
        urlstr: &str,
    ) -> Result<(), ClientError> {
        let url = match Url::parse(urlstr) {
            Ok(url) => Ok(url),
            Err(e) => {
                Err(ClientError::UrlParse(urlstr.to_owned(), e.to_string()))
            }
        }?;
        let req = self.client.request(reqwest::Method::GET, url).build()?;
        let (rsp, log) = self.execute_and_log_request(req)?;
        let log_id = log.id;
        self.consume_transaction_log(log);
        let rspstatus = rsp.status();
        if rspstatus.is_redirection() || rspstatus.is_success() {
            Ok(())
        } else {
            Err(ClientError::from_response(
                urlstr,
                rsp,
                &mut self.http_log,
                log_id,
            ))
        }
    }

    /// Try to resolve a Mastodon URL into some kind of object ID on
    /// the current instance. Might return an Account or a Status,
    /// depending on the input URL.
    pub fn resolve_mastodon_url(
        &mut self,
        url: &str,
    ) -> Result<Option<ResolvedUrl>, ClientError> {
        if let Some(resolved) = self.resolved_urls.get(url) {
            return Ok(Some(resolved.clone()));
        }
        let req = Req::get("api/v2/search")
            .param("q", url)
            .param("resolve", true);
        let sr = self.api_request_parse::<SearchResults>(req)?.0;
        self.cache_search_results(&sr);
        let maybe_resolved = sr
            .accounts
            .first()
            .map(|ac| ResolvedUrl::Account(ac.id.clone()))
            .or_else(|| {
                sr.statuses
                    .first()
                    .map(|st| ResolvedUrl::Status(st.id.clone()))
            });
        if let Some(resolved) = &maybe_resolved {
            self.resolved_urls.insert(url.to_owned(), resolved.clone());
        }
        Ok(maybe_resolved)
    }

    pub fn add_to_error_log(&mut self, err: ClientError) {
        let new_id =
            self.error_log.origin + (self.error_log.items.len() as isize);
        self.error_log.push_now(err);
        self.local_feeds[LocalFeedId::Errors]
            .ids
            .push_back(new_id.to_string());
    }

    pub fn get_error_log_entry(
        &self,
        id: &str,
    ) -> (ClientError, DateTime<Utc>) {
        let index: isize = id.parse().expect(
            "We generated these ids ourselves and they should all be valid",
        );
        self.error_log.items[(index - self.error_log.origin) as usize].clone()
    }

    pub fn error_log_lock(&self) -> LogLock {
        self.error_log.lock.clone()
    }

    pub fn get_http_log_entry(&self, id: &str) -> TransactionLogEntry {
        let index: isize = id.parse().expect(
            "We generated these ids ourselves and they should all be valid",
        );
        self.http_log.items[(index - self.http_log.origin) as usize].clone()
    }

    pub fn http_log_lock(&self) -> LogLock {
        self.http_log.lock.clone()
    }

    pub fn prune_logs(&mut self) {
        self.error_log
            .prune(&mut self.local_feeds[LocalFeedId::Errors]);
        self.http_log
            .prune(&mut self.local_feeds[LocalFeedId::HttpTransactions]);
    }
}

/// Decide if a URL looks Mastodonish enough to be worth letting
/// `Client::resolve_mastodon_url` try to resolve it.
pub fn possible_mastodon_url(url: &str) -> bool {
    let url = match Url::parse(url) {
        Ok(url) => url,
        Err(..) => return false,
    };

    lazy_static! {
        static ref PATH_REGEX: Regex =
            Regex::new(r#"^/(@[^/]+(/\d+)?|users/[^/]+(/statuses/\d+)?)$"#)
                .unwrap();
    }

    PATH_REGEX.is_match(url.path())
}

#[test]
fn test_possible_mastodon_url() {
    assert!(!possible_mastodon_url("https://mastodon.example/username"));
    assert!(possible_mastodon_url("https://mastodon.example/@username"));
    assert!(possible_mastodon_url(
        "https://mastodon.example/@username/123"
    ));
    assert!(!possible_mastodon_url(
        "https://mastodon.example/@username/123x"
    ));
    assert!(possible_mastodon_url(
        "https://mastodon.example/users/username"
    ));
    assert!(possible_mastodon_url(
        "https://mastodon.example/users/username/statuses/123"
    ));
    assert!(!possible_mastodon_url(
        "https://mastodon.example/users/username/status/123"
    ));
}
