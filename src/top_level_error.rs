#[derive(Debug)]
pub struct TopLevelError {
    prefix: String,
    message: String,
}

impl std::fmt::Display for TopLevelError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        write!(f, "mastodonochrome: {}{}", self.prefix, self.message)
    }
}

impl std::error::Error for TopLevelError {}

pub trait TopLevelErrorCandidate: std::fmt::Display {
    fn get_prefix() -> String {
        "error: ".to_owned()
    }
}

impl<E: TopLevelErrorCandidate> From<E> for TopLevelError {
    fn from(err: E) -> Self {
        TopLevelError {
            prefix: E::get_prefix(),
            message: err.to_string(),
        }
    }
}

impl TopLevelErrorCandidate for clap::error::Error {
    // clap prints its own "error: "
    fn get_prefix() -> String {
        "".to_owned()
    }
}

impl TopLevelErrorCandidate for std::io::Error {}
