use email_address::EmailAddress;
use reqwest::Url;
use std::cell::RefCell;
use std::ops::Add;
use std::rc::Rc;
use std::str::FromStr;
use sys_locale::get_locale;

use super::activity_stack::UtilityActivity;
use super::auth::{AuthConfig, AuthError};
use super::client::{AppTokenType, Client, ClientError};
use super::coloured_string::*;
use super::config::{ConfigError, ConfigLocation};
use super::editor::{
    count_edit_chars, EditableMenuLine, EditableMenuLineData,
};
use super::text::*;
use super::tui::{
    ActivityState, CursorPosition, LogicalAction, OurKey, OurKey::*,
};
use super::types::{Account, Application, Instance};

struct Username {
    name: String,
    domain: Rc<RefCell<String>>,
}

impl Username {
    fn ok(&self) -> bool {
        !self.name.is_empty()
    }
}

impl EditableMenuLineData for Username {
    fn display(&self) -> ColouredString {
        if self.name.is_empty() {
            ColouredString::uniform("none", 'r')
        } else {
            ColouredString::plain(&self.name)
                + ColouredString::uniform(
                    &format!("@{}", self.domain.borrow()),
                    'f',
                )
        }
    }

    fn to_text(&self) -> String {
        self.name.clone()
    }

    fn update(&mut self, text: &str) {
        let text = text
            .split_once('@')
            .map_or(text, |(prefix, _)| prefix)
            .trim();
        text.clone_into(&mut self.name);
    }
}

struct Email {
    addr: String,
}

impl Email {
    fn ok(&self) -> bool {
        EmailAddress::is_valid(&self.addr)
    }
}

impl EditableMenuLineData for Email {
    fn display(&self) -> ColouredString {
        if self.addr.is_empty() {
            ColouredString::uniform("none", 'r')
        } else if self.ok() {
            ColouredString::plain(&self.addr)
                + ColouredString::uniform(" valid", 'g')
        } else {
            ColouredString::plain(&self.addr)
                + ColouredString::uniform(" invalid!", 'r')
        }
    }

    fn to_text(&self) -> String {
        self.addr.clone()
    }

    fn update(&mut self, text: &str) {
        self.addr = match EmailAddress::from_str(text) {
            Ok(addr) => addr.as_str().to_owned(),
            Err(_) => text.to_owned(),
        };
    }
}

#[derive(Debug)]
struct Password {
    this: Rc<RefCell<String>>, // the password we're editing
    other: Rc<RefCell<String>>, // the one in the other field
    confirmation: bool,        // are we the confirmation field?
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum PasswordDiagnostic {
    OrigGood,
    ConfirmGood,
    Empty,
    TooShort,
    TooLong,
    Mismatch,
    SilentMismatch,
}

impl Password {
    fn diagnostic(&self) -> PasswordDiagnostic {
        let thisstr = self.this.borrow();
        let nchars = count_edit_chars(&thisstr);
        if thisstr.is_empty() {
            PasswordDiagnostic::Empty
        } else if self.confirmation {
            let otherstr = self.other.borrow();
            // No need to diagnose a mismatch if the other password
            // field is empty, because we're already displaying an
            // error message in the other edit line.
            if otherstr.is_empty() {
                PasswordDiagnostic::SilentMismatch
            } else if *thisstr != *otherstr {
                PasswordDiagnostic::Mismatch
            } else {
                PasswordDiagnostic::ConfirmGood
            }
        } else {
            // Diagnose bad password lengths. I'm not 100% sure
            // whether this can be reconfigured per Mastodon
            // instance, but as of
            // https://github.com/mastodon/mastodon commit
            // bd415af9a11fe7057c9f428b7bdaeb8d4fb3a77b,
            // config/initializers/devise.rb line 281 sets
            // 'config.password_length = 8..72'.
            if nchars < 8 {
                PasswordDiagnostic::TooShort
            } else if nchars > 72 {
                PasswordDiagnostic::TooLong
            } else {
                PasswordDiagnostic::OrigGood
            }
        }
    }

    fn ok(&self) -> bool {
        matches!(
            self.diagnostic(),
            PasswordDiagnostic::OrigGood | PasswordDiagnostic::ConfirmGood,
        )
    }
}
impl EditableMenuLineData for Password {
    const SECRET: bool = true;

    fn display(&self) -> ColouredString {
        let nchars = count_edit_chars(&self.this.borrow());
        let masked = ColouredString::plain("*").repeat(nchars);
        match self.diagnostic() {
            PasswordDiagnostic::OrigGood => {
                masked + ColouredString::uniform(" length ok", 'g')
            }
            PasswordDiagnostic::ConfirmGood => {
                masked + ColouredString::uniform(" match", 'g')
            }
            PasswordDiagnostic::Empty => ColouredString::uniform("none", 'r'),
            PasswordDiagnostic::SilentMismatch => masked,
            PasswordDiagnostic::TooShort => {
                masked + ColouredString::uniform(" too short!", 'r')
            }
            PasswordDiagnostic::TooLong => {
                masked + ColouredString::uniform(" too long!", 'r')
            }
            PasswordDiagnostic::Mismatch => {
                masked + ColouredString::uniform(" mismatch!", 'r')
            }
        }
    }

    fn to_text(&self) -> String {
        self.this.borrow().clone()
    }

    fn update(&mut self, text: &str) {
        text.clone_into(&mut self.this.borrow_mut());
    }
}

#[derive(Debug)]
pub enum LoginError {
    Recoverable(ClientError),
    Fatal(String),
}
impl From<ClientError> for LoginError {
    fn from(err: ClientError) -> Self {
        LoginError::Recoverable(err)
    }
}
impl From<ConfigError> for LoginError {
    fn from(err: ConfigError) -> Self {
        LoginError::Fatal(err.to_string())
    }
}
impl From<AuthError> for LoginError {
    fn from(err: AuthError) -> Self {
        LoginError::Fatal(err.to_string())
    }
}

impl std::fmt::Display for LoginError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        match self {
            LoginError::Recoverable(client_err) => {
                write!(f, "{}", client_err)
            }
            LoginError::Fatal(msg) => {
                write!(f, "{}", msg)
            }
        }
    }
}

impl std::error::Error for LoginError {}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum LoginState {
    Initial,
    ServerValid,
    LoginToken,
    LoginTokenError,
    LoginSuccess,
    LoginFailure,
    RegisterInput,
    RegisterFinal,
    RegisterFailure,
}

struct LoginMenu {
    cfgloc: ConfigLocation,
    title: FileHeader,
    alignment_line: MenuKeypressLine,
    para_intro: Paragraph,
    para_server_id: Paragraph,
    el_server: EditableMenuLine<String>,
    ml_login: MenuKeypressLine,
    ml_register: MenuKeypressLine,
    para_login_instructions: Paragraph,
    para_login_url: Paragraph,
    el_logincode: EditableMenuLine<String>,
    para_login_outcome: Paragraph,
    el_username: EditableMenuLine<Username>,
    el_email: EditableMenuLine<Email>,
    el_password: EditableMenuLine<Password>,
    el_password_confirm: EditableMenuLine<Password>,
    ml_rules: MenuKeypressLine,
    cl_accept: CyclingMenuLine<bool>,
    el_register_confirm: EditableMenuLine<String>,
    ml_register_done: MenuKeypressLine,
    para_regconfirm_outcome: Paragraph,
    state: LoginState,
    application: Option<Application>,
}

impl LoginMenu {
    fn new(cfgloc: ConfigLocation) -> Self {
        let title = FileHeader::new(ColouredString::uniform(
            &format!("Log in to a Mastodon server"),
            'H',
        ));

        let alignment_line = MenuKeypressLine::new(
            Pr('X'),
            ColouredString::plain(
                "Authorisation code: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            ),
        );
        let para_intro = Paragraph::new().set_centred(true).set_indent(10, 10)
            .add(ColouredString::general(
                "To log in to a Mastodon server, enter the URL of its website. (Not always the same as the domain name that appears in its usernames.)",
                "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH=======HHHHHHH======HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"));
        let el_server = EditableMenuLine::new(
            Pr('S'),
            ColouredString::plain("Server URL: "),
            "https://".to_owned(),
        );
        let ml_login = MenuKeypressLine::new(
            Pr('L'),
            ColouredString::uniform(
                "Log in to this server as an existing user",
                'H',
            ),
        );
        let ml_register = MenuKeypressLine::new(
            Pr('R'),
            ColouredString::uniform(
                "Register on this server as a new user",
                'H',
            ),
        );

        let para_server_id =
            Paragraph::new().set_centred(true).set_indent(10, 10);
        let para_login_instructions =
            Paragraph::new().set_centred(true).set_indent(10, 10);
        let para_login_url = Paragraph::new();

        let el_logincode = EditableMenuLine::new(
            Pr('C'),
            ColouredString::plain("Authorisation code: "),
            "".to_owned(),
        );

        let el_username = EditableMenuLine::new(
            Pr('N'),
            ColouredString::plain("Name of your new account: "),
            Username {
                name: "".to_owned(),
                domain: Rc::new(RefCell::new("".to_owned())),
            },
        );
        let el_email = EditableMenuLine::new(
            Pr('E'),
            ColouredString::plain("Email address to associate with account: "),
            Email {
                addr: "".to_owned(),
            },
        );
        let password1 = Rc::new(RefCell::new("".to_owned()));
        let password2 = Rc::new(RefCell::new("".to_owned()));
        let password1c = Rc::clone(&password1);
        let el_password = EditableMenuLine::new(
            Pr('P'),
            ColouredString::plain("Password for the instance website: "),
            Password {
                this: password1c,
                other: Rc::clone(&password2),
                confirmation: false,
            },
        );
        let el_password_confirm = EditableMenuLine::new(
            Ctrl('P'),
            ColouredString::plain("Password again for confirmation:   "),
            Password {
                this: password2,
                other: password1,
                confirmation: true,
            },
        );
        let ml_rules = MenuKeypressLine::new(
            Pr('R'),
            ColouredString::uniform("Read the server rules", 'H'),
        );
        let cl_accept = CyclingMenuLine::new(
            Pr('A'),
            ColouredString::plain("Do you accept the server rules? "),
            &[
                (false, ColouredString::uniform("no", 'r')),
                (true, ColouredString::uniform("yes", 'g')),
            ],
            false,
        );

        let para_login_outcome =
            Paragraph::new().set_centred(true).set_indent(10, 10);

        let el_register_confirm = EditableMenuLine::new(
            Pr('U'),
            ColouredString::plain("Confirmation URL: "),
            "".to_owned(),
        );
        let ml_register_done = MenuKeypressLine::new(
            Pr('C'),
            ColouredString::plain(
                "Continue to Main Menu (I followed the link in a browser)",
            ),
        );

        let para_regconfirm_outcome =
            Paragraph::new().set_centred(true).set_indent(10, 10);

        let mut menu = LoginMenu {
            cfgloc,
            title,
            alignment_line,
            para_intro,
            el_server,
            ml_login,
            ml_register,
            para_server_id,
            para_login_instructions,
            para_login_url,
            el_logincode,
            para_login_outcome,
            el_username,
            el_email,
            el_password,
            el_password_confirm,
            ml_rules,
            cl_accept,
            el_register_confirm,
            ml_register_done,
            para_regconfirm_outcome,
            state: LoginState::Initial,
            application: None,
        };
        menu.fix_widths();
        menu.el_server.start_editing();
        menu
    }

    fn fix_widths(&mut self) -> (usize, usize) {
        let mut lmaxwid = 0;
        let mut rmaxwid = 0;
        self.alignment_line.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_server.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_login.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_register.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_logincode.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_username.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_email.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_password.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_password_confirm
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_rules.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.cl_accept.check_widths(&mut lmaxwid, &mut rmaxwid);
        self.el_register_confirm
            .check_widths(&mut lmaxwid, &mut rmaxwid);
        self.ml_register_done
            .check_widths(&mut lmaxwid, &mut rmaxwid);

        self.el_server.reset_widths();
        self.ml_login.reset_widths();
        self.ml_register.reset_widths();
        self.el_logincode.reset_widths();
        self.el_username.reset_widths();
        self.el_email.reset_widths();
        self.el_password.reset_widths();
        self.el_password_confirm.reset_widths();
        self.ml_rules.reset_widths();
        self.cl_accept.reset_widths();
        self.el_register_confirm.reset_widths();
        self.ml_register_done.reset_widths();

        self.el_server.ensure_widths(lmaxwid, rmaxwid);
        self.ml_login.ensure_widths(lmaxwid, rmaxwid);
        self.ml_register.ensure_widths(lmaxwid, rmaxwid);
        self.el_logincode.ensure_widths(lmaxwid, rmaxwid);
        self.el_username.ensure_widths(lmaxwid, rmaxwid);
        self.el_email.ensure_widths(lmaxwid, rmaxwid);
        self.el_password.ensure_widths(lmaxwid, rmaxwid);
        self.el_password_confirm.ensure_widths(lmaxwid, rmaxwid);
        self.ml_rules.ensure_widths(lmaxwid, rmaxwid);
        self.cl_accept.ensure_widths(lmaxwid, rmaxwid);
        self.el_register_confirm.ensure_widths(lmaxwid, rmaxwid);
        self.ml_register_done.ensure_widths(lmaxwid, rmaxwid);

        (lmaxwid, rmaxwid)
    }

    fn verify_server(&mut self, client: &mut Client) -> LogicalAction {
        self.para_server_id.clear();

        let instance_url = self.el_server.get_data();
        let urlstr = match instance_url.find('/') {
            Some(_) => instance_url.to_owned(),
            None => format!("https://{instance_url}"),
        };
        let url = match Url::parse(&urlstr) {
            Ok(url) => url,
            Err(e) => {
                self.para_server_id.push_text(
                    ColouredString::uniform(
                        &format!("Error parsing the URL: {e}"),
                        'r',
                    ),
                    false,
                );
                self.state = LoginState::Initial;
                return LogicalAction::Beep;
            }
        };
        let instance_url = url.as_str().trim_end_matches('/');
        self.el_server.set_text(instance_url);
        client.auth = AuthConfig::default();
        client.auth.instance_url = Some(instance_url.to_owned());

        client.clear_caches();
        let inst_fallible = client.instance();
        client.clear_caches();
        self.application = None;

        let action = match inst_fallible {
            Ok(instance) => {
                self.para_server_id.push_text(
                    ColouredString::uniform(
                        "OK! This is the Mastodon instance ",
                        'H',
                    ),
                    false,
                );
                self.para_server_id.push_text(
                    ColouredString::uniform(&instance.domain, 'K'),
                    false,
                );
                self.para_server_id
                    .push_text(ColouredString::uniform(".", 'H'), false);
                self.el_username
                    .get_data()
                    .domain
                    .borrow_mut()
                    .clone_from(&instance.domain);
                self.state = LoginState::ServerValid;
                LogicalAction::Nothing
            }
            Err(e) => {
                self.para_server_id.push_text(
                    ColouredString::uniform(
                        &format!("Error confirming that instance: {e}"),
                        'r',
                    ),
                    false,
                );
                client.auth = AuthConfig::default();
                self.state = LoginState::Initial;
                LogicalAction::Beep
            }
        };

        self.fix_widths();
        action
    }

    fn get_login_url_fallible(
        &mut self,
        client: &mut Client,
    ) -> Result<String, ClientError> {
        // Register the client and get its details
        let app = client.register_client(false)?;
        let app_token = client
            .get_app_token(&app, AppTokenType::ClientCredentialsLogin)?;
        client.auth.user_token = Some(app_token.access_token);
        let _app: Application = client.verify_app_credentials()?;

        // Get the URL the user will have to visit
        let url = client.get_auth_url(&app)?;

        self.application = Some(app);

        Ok(url)
    }

    fn get_login_url(&mut self, client: &mut Client) -> LogicalAction {
        match self.get_login_url_fallible(client) {
            Ok(url) => {
                self.para_login_instructions.clear();
                self.para_login_instructions.push_text(
                    ColouredString::uniform(
                        "Log in to your user account on the instance website ",
                        'H',
                    ),
                    false,
                );
                self.para_login_instructions.push_text(
                    ColouredString::uniform(
                        client.auth.instance_url.as_deref().expect(
                            "In this login state we should have a URL!",
                        ),
                        'u',
                    ),
                    false,
                );
                self.para_login_instructions.push_text(
                    ColouredString::uniform(
                        " and then visit this URL to get an authorisation code for Mastodonochrome:",
                        'H',
                    ),
                    false,
                );
                self.para_login_url.clear();
                self.para_login_url
                    .push_text(ColouredString::uniform(&url, 'u'), false);
                self.state = LoginState::LoginToken;
                self.el_logincode.start_editing();
                LogicalAction::Nothing
            }
            Err(e) => {
                self.para_login_instructions.clear();
                self.para_login_instructions.push_text(
                    ColouredString::uniform(
                        &format!("Error setting up the login: {e}"),
                        'r',
                    ),
                    false,
                );
                self.para_login_url.clear();
                self.state = LoginState::LoginTokenError;
                LogicalAction::Beep
            }
        }
    }

    fn verify_login_code_fallible(
        &mut self,
        client: &mut Client,
    ) -> Result<(), LoginError> {
        let code = self.el_logincode.get_data().trim();
        let app = self
            .application
            .clone()
            .expect("Can't get here without registering an application");

        // Use our code to get the final user access token and all the
        // account details
        let token = client
            .get_app_token(&app, AppTokenType::AuthorizationCode(code))?;
        client.auth.user_token = Some(token.access_token.clone());
        client.auth.client_id = Some(app.client_id.clone().unwrap());
        client.auth.client_secret = Some(app.client_secret.clone().unwrap());
        Ok(finish_account_setup(client, &self.cfgloc)?)
    }

    fn verify_login_code(&mut self, client: &mut Client) -> LogicalAction {
        match self.verify_login_code_fallible(client) {
            Ok(_) => {
                self.para_login_outcome.clear();
                self.para_login_outcome.push_text(
                    ColouredString::uniform("Success! Logged in as ", 'H'),
                    false,
                );
                self.para_login_outcome.push_text(
                    ColouredString::uniform(&client.our_account_fq(), 'K'),
                    false,
                );
                self.para_login_outcome.push_text(
                    ColouredString::general(
                        ". Now press [SPACE] to continue to the Main Menu.",
                        "HHHHHHHHHHHHHKKKKKHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH",
                    ),
                    false,
                );
                self.state = LoginState::LoginSuccess;
                LogicalAction::Nothing
            }
            Err(LoginError::Recoverable(e)) => {
                self.para_login_outcome.clear();
                self.para_login_outcome.push_text(
                    ColouredString::uniform(
                        &format!("Error logging in: {e}"),
                        'r',
                    ),
                    false,
                );
                self.state = LoginState::LoginFailure;
                LogicalAction::Beep
            }
            Err(LoginError::Fatal(e)) => LogicalAction::Fatal(e.into()),
        }
    }

    fn register_account_fallible(
        &mut self,
        client: &mut Client,
    ) -> Result<(), LoginError> {
        // Register the client and get its details
        let app = client.register_client(true)?;
        let app_token = client
            .get_app_token(&app, AppTokenType::ClientCredentialsRegister)?;
        client.auth.user_token = Some(app_token.access_token);

        assert!(
            self.cl_accept.get_value(),
            "This keystroke should have been locked until you accepted"
        );

        // FIXME: ability to configure this?
        let language = get_locale()
            .as_deref()
            .and_then(|s| s.split('-').next())
            .map(|s| if s.is_empty() { "en" } else { s })
            .unwrap_or("en")
            .to_owned();

        // Send the account registration request
        let token = client.register_account(
            &self.el_username.get_data().name,
            &self.el_email.get_data().addr,
            &self.el_password.get_data().this.borrow(),
            &language,
        )?;

        // Now we have a user token. Save what we have of it.
        let instance_url = client.auth.instance_url.take();
        client.auth = AuthConfig {
            account_id: None, // we don't have this yet
            username: None,   // this isn't confirmed either
            instance_url,
            instance_domain: None, // we'd have to request an Instance again
            client_id: Some(app.client_id.clone().unwrap()),
            client_secret: Some(app.client_secret.clone().unwrap()),
            user_token: Some(token.access_token.clone()),
        };

        client.auth.save(&self.cfgloc)?;

        self.state = LoginState::RegisterFinal;

        Ok(())
    }

    fn register_account(&mut self, client: &mut Client) -> LogicalAction {
        match self.register_account_fallible(client) {
            Ok(_) => {
                self.para_login_outcome.clear();
                self.para_login_outcome.push_text(
                    ColouredString::uniform(
                        "Success! The server should have sent a confirmation to your email address ",
                        'H',
                    ),
                    false,
                );
                self.para_login_outcome.push_text(
                    ColouredString::uniform(
                        &self.el_email.get_data().addr,
                        'K',
                    ),
                    false,
                );
                self.para_login_outcome.push_text(
                    ColouredString::uniform(
                        ". Either paste the confirmation URL from that email below, or visit it in a browser.",
                        'H',
                    ),
                    false,
                );
                self.state = LoginState::RegisterFinal;
                LogicalAction::Nothing
            }
            Err(LoginError::Recoverable(e)) => {
                self.para_login_outcome.clear();
                self.para_login_outcome.push_text(
                    ColouredString::uniform(
                        &format!("Error registering account: {e}"),
                        'r',
                    ),
                    false,
                );
                self.state = LoginState::RegisterFailure;
                LogicalAction::Beep
            }
            Err(LoginError::Fatal(e)) => LogicalAction::Fatal(e.into()),
        }
    }

    fn ready_to_register(&self) -> bool {
        self.el_username.get_data().ok()
            && self.el_email.get_data().ok()
            && self.el_password.get_data().ok()
            && self.el_password_confirm.get_data().ok()
            && self.cl_accept.get_value()
    }

    fn confirm_registration_fallible(
        &mut self,
        client: &mut Client,
        with_url: bool,
    ) -> Result<(), LoginError> {
        if with_url {
            let url = self.el_register_confirm.get_data();
            client.register_confirmation(url)?;
        }
        Ok(finish_account_setup(client, &self.cfgloc)?)
    }

    fn confirm_registration(
        &mut self,
        client: &mut Client,
        with_url: bool,
    ) -> LogicalAction {
        match self.confirm_registration_fallible(client, with_url) {
            Ok(_) => LogicalAction::FinishedLoggingIn,
            Err(LoginError::Recoverable(e)) => {
                self.para_regconfirm_outcome.clear();
                self.para_regconfirm_outcome.push_text(
                    ColouredString::uniform(
                        &format!("Error confirming registration: {e}"),
                        'r',
                    ),
                    false,
                );
                LogicalAction::Beep
            }
            Err(LoginError::Fatal(e)) => LogicalAction::Fatal(e.into()),
        }
    }
}

impl ActivityState for LoginMenu {
    fn draw(
        &self,
        w: usize,
        h: usize,
    ) -> (Vec<ColouredString>, CursorPosition) {
        let push_split_lines =
            |lines: &mut Vec<ColouredString>, output: Vec<ColouredString>| {
                for line in output {
                    for frag in line.split(w.saturating_sub(1)) {
                        lines.push(frag.into());
                    }
                }
            };
        let mut lines = Vec::new();
        let mut cursorpos = CursorPosition::End;
        lines.extend_from_slice(&self.title.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        push_split_lines(&mut lines, self.para_intro.render(w));
        lines.extend_from_slice(&BlankLine::render_static());
        lines.push(self.el_server.render(w, &mut cursorpos, lines.len()));
        push_split_lines(&mut lines, self.para_server_id.render(w));
        if self.state == LoginState::ServerValid {
            lines.extend_from_slice(&BlankLine::render_static());
            lines.extend_from_slice(&self.ml_login.render(w));
            lines.extend_from_slice(&self.ml_register.render(w));
        }
        if self.state == LoginState::LoginToken
            || self.state == LoginState::LoginTokenError
            || self.state == LoginState::LoginSuccess
            || self.state == LoginState::LoginFailure
        {
            lines.extend_from_slice(&BlankLine::render_static());
            push_split_lines(
                &mut lines,
                self.para_login_instructions.render(w),
            );
        }
        if self.state == LoginState::LoginToken
            || self.state == LoginState::LoginSuccess
            || self.state == LoginState::LoginFailure
        {
            lines.extend_from_slice(&BlankLine::render_static());
            push_split_lines(&mut lines, self.para_login_url.render(w));
            lines.extend_from_slice(&BlankLine::render_static());
            lines.push(self.el_logincode.render(
                w,
                &mut cursorpos,
                lines.len(),
            ));
        }
        if self.state == LoginState::LoginSuccess
            || self.state == LoginState::LoginFailure
        {
            lines.extend_from_slice(&BlankLine::render_static());
            push_split_lines(&mut lines, self.para_login_outcome.render(w));
        }
        if self.state == LoginState::RegisterInput
            || self.state == LoginState::RegisterFailure
        {
            lines.extend_from_slice(&BlankLine::render_static());
            lines.push(self.el_username.render(
                w,
                &mut cursorpos,
                lines.len(),
            ));
            lines.push(self.el_email.render(w, &mut cursorpos, lines.len()));
            lines.push(self.el_password.render(
                w,
                &mut cursorpos,
                lines.len(),
            ));
            lines.push(self.el_password_confirm.render(
                w,
                &mut cursorpos,
                lines.len(),
            ));
            lines.extend_from_slice(&self.ml_rules.render(w));
            lines.extend_from_slice(&self.cl_accept.render(w));
        }
        if self.state == LoginState::RegisterFailure
            || self.state == LoginState::RegisterFinal
        {
            lines.extend_from_slice(&BlankLine::render_static());
            push_split_lines(&mut lines, self.para_login_outcome.render(w));
        }
        if self.state == LoginState::RegisterFinal {
            lines.extend_from_slice(&BlankLine::render_static());
            lines.push(self.el_register_confirm.render(
                w,
                &mut cursorpos,
                lines.len(),
            ));
            lines.extend_from_slice(&self.ml_register_done.render(w));
            lines.extend_from_slice(&BlankLine::render_static());
            push_split_lines(
                &mut lines,
                self.para_regconfirm_outcome.render(w),
            );
        }

        while lines.len() + 1 < h {
            lines.extend_from_slice(&BlankLine::render_static());
        }

        let status = FileStatusLine::new();

        let status = match self.state {
            LoginState::Initial => {
                if self.el_server.is_editing() {
                    status.message("Enter server URL and press Return.")
                } else {
                    status.add(Pr('Q'), "Quit", 100)
                }
            }
            LoginState::LoginToken => {
                if self.el_server.is_editing() {
                    status.message("Enter code from server and press Return.")
                } else {
                    status.add(Pr('Q'), "Quit", 100)
                }
            }
            LoginState::LoginSuccess => status.add(Space, "Main Menu", 100),
            LoginState::RegisterInput => {
                if self.ready_to_register() {
                    status.add(Space, "Register account", 100)
                } else {
                    status.message(
                        "Fill in all fields and accept the server rules.",
                    )
                }
            }
            _ => status.add(Pr('Q'), "Quit", 100),
        }
        .add(Pr('?'), "Help", 101)
        .finalise();
        lines.extend_from_slice(&status.render(w));

        (lines, cursorpos)
    }

    fn handle_keypress(
        &mut self,
        key: OurKey,
        client: &mut Client,
    ) -> LogicalAction {
        // Let editable menu lines have first crack at the keypress
        if self.el_server.handle_keypress(key) {
            self.fix_widths();
            if !self.el_server.is_editing() {
                return self.verify_server(client);
            }
            return LogicalAction::Nothing;
        } else if self.el_logincode.handle_keypress(key) {
            self.fix_widths();
            if !self.el_logincode.is_editing() {
                return self.verify_login_code(client);
            }
            return LogicalAction::Nothing;
        } else if self.el_username.handle_keypress(key)
            || self.el_email.handle_keypress(key)
            || self.el_password_confirm.handle_keypress(key)
        {
            self.fix_widths();
            return LogicalAction::Nothing;
        } else if self.el_password.handle_keypress(key) {
            self.fix_widths();
            if !self.el_password.is_editing() {
                self.el_password_confirm.set_text("");
                self.el_password_confirm.start_editing();
            }
            return LogicalAction::Nothing;
        } else if self.el_register_confirm.handle_keypress(key) {
            self.fix_widths();
            if !self.el_register_confirm.is_editing() {
                return self.confirm_registration(client, true);
            }
            return LogicalAction::Nothing;
        }

        match key {
            Pr('?') => LogicalAction::Help,

            Pr('s') | Pr('S') => match self.state {
                LoginState::Initial | LoginState::ServerValid => {
                    self.state = LoginState::Initial;
                    self.el_server.start_editing()
                }
                _ => LogicalAction::Nothing,
            },
            Pr('l') | Pr('L') => {
                if self.state == LoginState::ServerValid {
                    self.get_login_url(client)
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('r') | Pr('R') => {
                if self.state == LoginState::RegisterInput {
                    // R here means 'read rules'
                    LogicalAction::Goto(UtilityActivity::InstanceRules.into())
                } else if self.state == LoginState::ServerValid {
                    // R here means 'register as new user'
                    self.state = LoginState::RegisterInput;
                    LogicalAction::Nothing
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('c') | Pr('C') => {
                if self.state == LoginState::LoginToken
                    || self.state == LoginState::LoginFailure
                {
                    self.el_logincode.start_editing()
                } else if self.state == LoginState::RegisterFinal {
                    self.confirm_registration(client, false)
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('n') | Pr('N') => {
                if self.state == LoginState::RegisterInput {
                    self.el_username.start_editing()
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('e') | Pr('E') => {
                if self.state == LoginState::RegisterInput {
                    self.el_email.start_editing()
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('p') | Pr('P') => {
                if self.state == LoginState::RegisterInput {
                    self.el_password.set_text("");
                    self.el_password.start_editing()
                } else {
                    LogicalAction::Nothing
                }
            }
            Ctrl('P') => {
                if self.state == LoginState::RegisterInput {
                    self.el_password_confirm.set_text("");
                    self.el_password_confirm.start_editing()
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('a') | Pr('A') => {
                if self.state == LoginState::RegisterInput {
                    self.cl_accept.cycle()
                } else {
                    LogicalAction::Nothing
                }
            }
            Pr('u') | Pr('U') => {
                if self.state == LoginState::RegisterFinal {
                    self.el_register_confirm.start_editing()
                } else {
                    LogicalAction::Nothing
                }
            }
            Space => {
                if self.state == LoginState::LoginSuccess {
                    LogicalAction::FinishedLoggingIn
                } else if self.state == LoginState::RegisterInput
                    && self.ready_to_register()
                {
                    self.register_account(client)
                } else {
                    LogicalAction::Nothing
                }
            }

            // In this initial screen we don't require ESC X X to get out
            Pr('q') | Pr('Q') => LogicalAction::Exit,

            _ => LogicalAction::Nothing,
        }
    }

    fn resize(&mut self, w: usize, _h: usize) {
        self.el_server.resize(w);
        self.el_logincode.resize(w);
        self.el_username.resize(w);
        self.el_email.resize(w);
        self.el_password.resize(w);
        self.el_password_confirm.resize(w);
        self.el_register_confirm.resize(w);
    }
}

pub fn finish_account_setup(
    client: &mut Client,
    cfgloc: &ConfigLocation,
) -> Result<(), LoginError> {
    let account: Account = client.verify_account_credentials()?;
    let instance: Instance = client.instance()?;

    let token = client.auth.user_token.take();
    let client_id = client.auth.client_id.take();
    let client_secret = client.auth.client_secret.take();
    client.auth = AuthConfig {
        account_id: Some(account.id),
        username: Some(account.username),
        instance_url: client.auth.instance_url.clone(),
        instance_domain: Some(instance.domain),
        client_id,
        client_secret,
        user_token: token,
    };

    client.auth.save(cfgloc)?;
    Ok(())
}

pub fn login_menu(cfgloc: ConfigLocation) -> Box<dyn ActivityState> {
    Box::new(LoginMenu::new(cfgloc))
}
