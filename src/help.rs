use std::collections::HashMap;
use std::ops::Add;
use std::rc::Rc;

use mastodonochrome_macros::help_markup;

use super::activity_stack::*;
use super::client::{Boosts, Client};
use super::coloured_string::*;
use super::text::*;
use super::tui::OurKey::*;

#[derive(Clone)]
pub struct HelpItem(Rc<dyn TextFragment>);

impl TextFragment for HelpItem {
    fn render_highlighted(
        &self,
        width: usize,
        highlight: Option<Highlight>,
        style: &dyn DisplayStyleGetter,
    ) -> Vec<ColouredString> {
        self.0.render_highlighted(width, highlight, style)
    }
}

struct HelpVec(Vec<Rc<dyn TextFragment>>);

impl Add<HelpVec> for HelpVec {
    type Output = HelpVec;

    fn add(mut self, mut other: HelpVec) -> HelpVec {
        self.0.append(&mut other.0);
        self
    }
}

pub struct Help {
    pub title: String,
    pub ids: Vec<String>,
    pub items: HashMap<String, HelpItem>,
}

impl Help {
    fn new(title: impl Into<String>, help: HelpVec) -> Self {
        let title = title.into();
        let mut ids = Vec::new();
        let mut items = HashMap::new();

        // Always start with a blank line, separating the text from
        // the file header.
        let help = HelpVec(vec![Rc::new(BlankLine::new())]) + help;

        for (i, rc) in help.0.into_iter().enumerate() {
            let id = format!("{i}");
            ids.push(id.clone());
            items.insert(id, HelpItem(rc));
        }

        Help { title, ids, items }
    }
}

fn help_for_general_file(extend_past: bool) -> HelpVec {
    let hv = help_markup!(
        r##"

[_:Moving around the file]

To scroll the text a page at a time, you can press [Space] to move to
the next page, and [-] or [B] to move to the previous page. You can
also use the [PgDn] and [PgUp] keys, or [Right] and [Left], to do the
same thing.

To scroll a line at a time, you can use the [Up] and [Down] arrow
keys. You can also press [Return] to scroll down by a line; if you
press that at the very bottom of the file, it will leave the file and
return to your previous activity.

You can press [0] or [Home] to go to the top of the file, or [Z] or
[End] to go to the bottom.

"##
    );

    let hv = if extend_past {
        hv + help_markup!(
            r##"

If the file has not yet been extended into the past as far as it will
go, then when you go to the very top of the file, you'll see a centred
message saying "[H:Press \[][K:0][H:\] to extend]". If you press [0]
again at this point, Mastodonochrome will try to retrieve more data
from further in the past, and add it to the top of the file, so that
you can scroll further up.

(If you're not at the very top of the file, but that message is still
in view near the top of the screen, it will change its wording to say
"[H:Press \[][K:0][H:\] twice to extend]", because pressing [0] once
goes to the very top of the file, and then pressing [0] a second time
does the extending. However, any other way to get to the top of the
file is good enough; really, it works to just press [0] at the top of
the file, however you got there.)

"##
        )
    } else {
        hv
    };

    hv + help_markup!(
        r##"

You can search for text in the file by pressing [/] to search forward
from the current position, or [\\] to search backward. Both keystrokes
will prompt you in the bottom line to ask what to search for. Pressing
[N] will repeat the same search, for the same text and in the same
direction.

The search string will be interpreted as a regular expression. Press
[FunKey(1)] at the search prompt for more information.

"##
    )
}

fn help_for_examine() -> HelpVec {
    help_markup!(
        r##"

[_:Examining users mentioned in the file]

In this type of file, Mastodon user accounts are visible. If you want
to find out more about one of the mentioned users, you can press [E]
to examine one of them. This will display a [*:highlight] on a user
account name. You can move the highlight to an account name earlier in
the file by pressing [-] or [Up], or later in the file by pressing [+]
or [Down]. Then press [Space] to examine the currently selected user,
or [Q] to abort the selection process.

This will take you to the same page of information about a user that
you can also access by pressing [!Escape][!E] and entering the account
name. From there, you can adjust your options relating to that user,
such as whether you follow them.

"##
    )
}

fn help_for_status(multiple: bool) -> HelpVec {
    let intro = if multiple {
        help_markup!(
            r##"

[_:Operating on Mastodon posts shown in the file]

In this type of file, Mastodon posts are visible, and various
keystrokes will let you operate on them.

"##
        )
    } else {
        help_markup!(
            r##"

[_:Operating on the Mastodon post shown in the file]

This file displays a Mastodon post. You can use all the same
keystrokes here as in files with more than one post, except that where
the text below says 'select a post', there will only be one post
available to select.

"##
        )
    };

    intro
        + help_markup!(
            r##"

To look at more information about an individual post, press [I]. This
will display a [*:highlight] on a post. You can move the highlight to a
post earlier in the file by pressing [-] or [Up], or later in the file
by pressing [+] or [Down]. Then press [Space] to display a page of
detailed information about the currently selected post, or [Q] to
abort the selection process. This will show, for example, the URL of
the post on its host instance's web site, and the number of users who
have favourited or boosted it.

To view the discussion thread surrounding a post, press [T], and then
select it in the same way as above. Pressing [Space] will show the
ancestors of the selected post in the full thread (that is, the post
it was a direct reply to, and the one that was a reply to, and so on
back to the starting post), and also the descendants (everything
that's a reply to it, or a reply to one of those, etc). You can also
press [F] instead of [Space], to show the full thread of the post,
meaning everything that is in the same thread at all, including its
'siblings' as well as ancestors and descendants.

"##
        )
}

fn help_for_whole_status(multiple: bool) -> HelpVec {
    help_for_status(multiple)
        + help_markup!(
            r##"

Some Mastodon posts are not displayed in full by default, because the
poster marked them as containing sensitive content. These are marked
with a [r:\[-\]] symbol, together with any text the poster wrote about
what kind of sensitive content was in it. To unfold one of these to
read it in full, or to fold it up again if you want to, you can press
[U]. The selection process for this works as above, except that only
foldable posts are available to select. Once you've selected the post
you want, pressing [Space] will toggle its folded state.

To favourite a post, press [F], and select the post in the usual way.
Pressing [Space] marks the post as favourited; pressing [D] instead
will un-favourite it again, if you change your mind.

To boost a post, do the same operation starting with [Ctrl('B')]. (The
keypress [B] is already used for scrolling around the file; also,
boosting is a more visible operation than favouriting, so making it a
slightly more awkward keystroke acts as a reminder of that.) Just like
favouriting, you can unboost a post again by pressing [D] once a
boosted post is selected.

Some Mastodon posts contain polls that users can vote in. To vote in a
poll, press [Ctrl('V')]. The selection highlight will move between all
options in polls visible in the file. Press [Space] to mark a poll
option as selected. This does not immediately submit your vote;
instead, the selection now becomes restricted to that poll, but you
can still select other options in the same poll by moving the
selection and pressing [Space] again. If the poll is multiple-choice,
then all your selected options remain selected, and you can press
[Space] on a selected one to unselect it again; if it's single-choice,
then pressing [Space] on another option removes the selection on the
first one. Once you're happy with your choices, press [Ctrl('V')]
again to submit your vote.

Finally, if you press [S] and select a post, you can write and
post a reply to it.

"##
        )
}

fn help_for_editor_keys_oneline() -> HelpVec {
    help_markup!(
        r##"

To insert new text at the cursor position, just type it in the normal
way. If you type more text than will fit in the space on the screen,
you'll see a [>:<] marker at the left, indicating that there's more
text to the left that isn't currently shown. If you move the cursor
backwards towards that text, the display will pan to keep the cursor
in view, and a [>:>] marker might appear at the right, showing that
now there's more text in that direction.

Press the arrow keys [Left] and [Right] to move backwards and forwards
in the text you're editing. You can also use [Ctrl('B')] and
[Ctrl('F')] respectively (to go [_:b]ackwards and [_:f]orwards), in
the style of Emacs.

To delete the character to the left of the cursor position (for
example, if it's the one you just typed), use the usual [Backspace]
key (typically just above [!Return]). To delete the character to the
right, use the [Del] key on the keypad, or [Ctrl('D')].

You can press [Ctrl('T')] to move forward by a [_:word] at a time, and
[Ctrl('W')] to move backward by a word.

You can press [Ctrl('A')] or [Home] to go to the beginning of the
line, and [Ctrl('E')] or [End] to go to the end of the line.

Pressing [Ctrl('K')] cuts everything from the cursor position to the
end of the line. This text goes into a paste buffer which you can
paste back in by pressing [Ctrl('Y')].

"##
    )
}

fn help_for_editor_keys_composer() -> HelpVec {
    help_markup!(
        r##"

To insert new text at the cursor position, just type it in the normal
way. If you type more text than will fit on a line, then the editor
will wrap your paragraphs automatically. It remembers which parts of
your text are part of the same paragraph, so if you insert or delete
text in the middle of a paragraph, it will re-wrap automatically.

Pressing [Return] inserts a new line, or rather, a paragraph break.

Press the arrow keys [Left], [Right], [Up] and [Down] to move around
the text you're editing. You can also use [Ctrl('B')] and [Ctrl('F')]
to go left ('[_:b]ackwards') and right ('[_:f]orwards'), and
[Ctrl('P')] and [Ctrl('N')] to go up ('[_:p]revious line') and down
('[_:n]ext line'), in the style of Emacs.

To delete the character to the left of the cursor position (for
example, if it's the one you just typed), use the usual [Backspace]
key (typically just above [!Return]). To delete the character to the
right, use the [Del] key on the keypad, or [Ctrl('D')]. If you delete
a paragraph break, the two paragraphs on either side of it will become
a single paragraph and be re-wrapped appropriately.

You can press [Ctrl('T')] to move forward by a [_:word] at a time, and
[Ctrl('W')] to move backward by a word.

You can press [Ctrl('V')] to move down the text by a whole screenful,
and [Ctrl('Z')] to move up by a screenful.

You can press [Ctrl('A')] or [Home] to go to the beginning of the
current line, and [Ctrl('E')] or [End] to go to the end of the same
line. If the line is part of a wrapped paragraph, these keys will go
to the beginning and end of the single line as it appears on the
screen, not the whole paragraph.

You can press [Ctrl('O')][Up] or [Ctrl('O')][Ctrl('P')] to go to the
very beginning of the whole buffer, and [Ctrl('O')][Down] or
[Ctrl('O')][Ctrl('N')] to go to the end of the buffer.

Pressing [Ctrl('K')] cuts everything from the cursor position to the
end of the current screen line. This text goes into a paste buffer
which you can paste back in by pressing [Ctrl('Y')].

If you press [Ctrl('K')] when the cursor is on a paragraph break, then
just the paragraph break is deleted (again, joining the paragraphs
before and after it) and the paste buffer is not overwritten.

If you press [Ctrl('K')] when the cursor is not on the last line of a
wrapped paragraph, the text to the end of the current screen line will
still be cut, and also, a paragraph break will be [_:inserted] in its
place. (This means that the rest of the paragraph won't immediately
jump, and also means that pressing the sequence [Ctrl('K')][Ctrl('K')]
will do something as similar as possible whether or not you're on the
last line of a paragraph.)

"##
    )
}

fn help_for_composer() -> HelpVec {
    help_markup!(
        r##"

As you enter text, Mastodonochrome will count up the number of
characters, and if you go over the character limit for your instance,
it will warn you by [!:highlighting] the overflow text.

Mastodonochrome will do its best to count characters according to the
same rules as the Mastodon server: URLs have a fixed cost no matter
how long they are, and mentioning another user by writing
[@:@username@domain] only counts the [@:@username] section towards the
character limit.

Mastodon posts are composed in the form of plain text without markup.
(At least, usually, and that's the only format that Mastodonochrome
supports.) But after you submit your post, the Mastodon server will
automatically detect [#:#hashtags], [@:@mentions] and [u:https://URLs]
and mark them up in the HTML version of the post that it shows to
readers. To help you see how this will work, Mastodonochrome's editor
will highlight those things as you edit, again doing its best to match
the server's rules for where they start and end. For example, you can
post a URL at the end of a sentence with confidence, because the
editor's highlighting will show that the sentence's full stop isn't
counted as part of the URL, and the Mastodon server won't count it
that way either.

When you've finished editing your post and want to submit it, enter
the character '.' on a line by itself, or alternatively press
[Ctrl('O')][Space]. This won't post the text immediately: instead,
you'll be taken to a final menu where you can set options about the
post, such as its visibility.

If you want to abandon your post completely, enter the text ".quit" on
a line by itself, or alternatively press [Ctrl('O')][Q]. This [_:is]
immediate: it will wipe out the text you were editing, and no copy
will be saved.

[_:Editing keys]

"##
    ) + help_for_editor_keys_composer()
}

fn post_composer_menu_common_start() -> HelpVec {
    help_markup!(
        r##"

The post-composer menu appears when you've finished editing a Mastodon
post, and either entered [!Return][.][!Return] or [!Ctrl('O')][!Space]
to submit it. In this menu, you can adjust options for the post before
finally sending it to the Mastodon server.

When you've finished setting the options, you can press [Space] to
post your finished toot. Alternatively, if you change your mind, you
can press [Q] to cancel the post completely, or [A] to go back to the
editor and change its wording again.

Beware that pressing [Q] at this point takes effect [_:immediately]!
The text of your draft post will be thrown away, and no copy is saved.

"##
    )
}

fn post_composer_menu_common_end() -> HelpVec {
    help_markup!(
        r##"
[_:Content warnings]

You can mark your post as "sensitive", which means that clients will
not show the text of it by default. This doesn't [_:stop] people from
reading it, but it means they have to take a deliberate action to do
so.

You might use this setting for many reasons. Two examples are if
you're discussing spoilers for some fiction that not everybody has
read or seen yet, or if you're discussing a topic that might disturb,
upset or trigger some readers.

When you mark a post as sensitive, you can also include a piece of
text that will be shown to the reader even before they unfold the
text. This is usually used to tell them what type of sensitive
material is contained in the post, so that they can make a sensible
decision about [_:whether] they want to read it. For example,
"Spoilers for New Shiny Series", or "Discussion of stoat abuse".

(However, those are not the only things you can use the content
warning string for. Some people post jokes by using the content
warning as the question, and the main text of the post as the
punchline!)

Press [S] to mark the post as sensitive, or un-mark it if you change
your mind. Press [W] to edit the content warning text.

It is [_:possible] to mark your post as sensitive without entering a
content warning, but we don't recommend it, because it doesn't give
readers useful information about whether to unfold the post.
Therefore, if you press [S] in this menu to mark the post as
sensitive, Mastodonochrome will automatically prompt you for some
warning text. If you really need to, you can leave the text blank.

[_:Language]

Mastodon posts are normally tagged to indicate what language they're
written in. This allows software to filter based on the language. In
particular, when you follow a user, you can select which languages you
want to see posts in. So if you follow a bilingual user, and you only
speak one of their languages, you can exclude posts in the other
language from your home timeline, which you wouldn't be able to read
anyway.

Press [L] to edit the language tag for the post you're about to
submit. The tag should be a two-letter code, in the form standardised
by ISO 639-1. For example, "en" means English, "fr" means French, and
"es" means Spanish. For a longer list, see the column of two-letter
codes at
[u:https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes]

The Mastodon server lets you configure a default language to be
applied to your posts. If you've set one of those, Mastodonochrome
will fill that in as the default value of this field for any new post
you make. (You can set this field through Mastodonochrome via your
user options menu, at [!Escape][Y][O].)

If you haven't set a default language in your Mastodon account
settings, Mastodonochrome will try to infer your language from the
configuration of the computer you're running it on: it will try to
choose the same language that that computer will use to display
application text and error messages.

If Mastodonochrome can't even determine [_:that] for some reason, it
will fall back to "en" for English, since that's the most common
default language of computer systems that haven't been told otherwise.

[_:Editing keys]

When you're editing the content warning or language strings, here are
the keys you can use to do it.

"##
    ) + help_for_editor_keys_oneline()
}

pub fn help_for_activity(client: &Client, activity: &Activity) -> Help {
    match activity {
        Activity::NonUtil(NonUtilityActivity::MainMenu) => Help::new(
            "Help for the Mastodonochrome Main Menu",
            help_markup!(
                r##"

Welcome to Mastodonochrome!

The Main Menu is the starting point of the whole of Mastodonochrome.
From here, you can reach all the other menus, feeds and files.

You can return to the Main Menu from anywhere else by pressing
[Escape][!G].

Pressing [Escape] by itself takes you to the Utilities Menu, which is
the other central point of Mastodonochrome. [+:You can press] [Escape]
[+:anywhere], with the same effect: it will take you to the Utilities
Menu from wherever you are. From there, you can get back here by
pressing [!G], or exit Mastodonochrome completely by pressing [!X]
twice. (So the complete key sequence to exit from anywhere is
[Escape][!X][!X].)

Another keystroke you can press [+:anywhere at all] is [FunKey(1)],
which displays help for whatever part of the user interface you're in,
such as the help you're reading right now.

In most parts of the interface, [?] is also a help key, but in editing
contexts that stops working (because it would be strange not to have
it enter a literal '?'). [FunKey(1)] should work everywhere.

[_:Other actions available from the Main Menu]

You can view 'timelines' containing other users' posts. [H] takes you
to your home timeline, consisting of posts from the users you follow.
[P] and [L] take you to the two public timelines: [P] is the timeline
of all new public posts that your Mastodon instance knows about, and
[L] contains only posts made by people on your instance itself.

Any hashtag comes with an associated timeline. Pressing [#] will let
you view them. You'll be prompted on the bottom line of the screen for
a tag name, and then you can view the timeline of that tag. The tag
name can be entered with or without the initial '#'. For example, if
you feel like reading posts that include the hashtag [#:#stoats], you
can press [#], and then enter either of "[c:stoats]" or "[c:#stoats]"
at the prompt.

The [I] menu option lets you look up an individual post, if you know
the id that your server has assigned it. Again, you'll be prompted for
the post id in the bottom line of the screen. [+:Note that post ids
are not consistent across the whole Fediverse]: each instance will
assign a different id to the same post. Post ids are visible at the
ends of web URLs to the post (provided the URL is from your own
instance); you can also obtain them when looking at a post in
Mastodonochrome itself, by pressing [!I] for post information.

Finally, [C] lets you write a new Mastodon post of your own from
scratch. This will be a top-level post, not part of any existing
thread. If you want to contribute to a thread, you should instead find
the post you want to reply to, and while looking at it, press [!S] to
send a reply.

"##
            ),
        ),

        Activity::Util(UtilityActivity::UtilsMenu) => Help::new(
            "Help for the Mastodonochrome Utilities Menu",
            help_markup!(
                r##"

The Utilities Menu is one of the central points of Mastodonochrome.
[+:You can get here from anywhere at all] by pressing [!Escape].

The two most important things you can do from here are to go back to
the Main Menu, by pressing [G], or to exit Mastodonochrome completely
by pressing [X].

(After you press [X], the Exit Menu will ask you to press [!X] a
second time to confirm. So the full key sequence to exit
Mastodonochrome from anywhere is [!Escape][X][!X].)

[_:Other actions available from the Utilities Menu]

Pressing [R] lets you read a feed of your mentions: any post made on
Mastodon which mentions your user name. Usually this means somebody is
talking to you. Mastodonochrome will display this feed automatically
as soon as it knows that someone has mentioned you in a new post, but
you can get to it on purpose by pressing [R] from here.

To find out information about a particular Mastodon user, you can
press [E] from this menu, and enter their username at the prompt in
the bottom line of the screen. The resulting information page will
tell you all the public information about the user, and from there,
you can also read a feed of that user's posts, or adjust your options
relating to the user (such as whether you follow them, or have blocked
them).

When you type an account name, you can spell it with or without an
initial @ sign: either "[c:@user@domain]" or just "[c:user@domain]".
If the user's account is on the same Mastodon instance as you, you can
leave off the @domain suffix, and just enter either "[c:user]" or
"[c:@user]".

If you want to see your own user information, you can enter your own
account name at the [E] prompt, but it's quicker to press [Y]. Either
way, the information page is mostly the same, but when you view your
own account, the available options page will be completely different,
and lets you change things like whether your account is locked, and
what language is assigned to your posts by default.

Pressing [L] takes you to a menu where Mastodonochrome keeps logs. As
of 2024-02-09, there isn't much here yet, but one thing you can reach
from the Logs Menu is what Mastodonochrome calls the "Ego Log": a feed
of information about who's recently followed you, or favourited or
boosted your posts.

[_:Using the Alt modifier key]

In general, Mastodonochrome will interpret the Alt modifier key with a
letter as equivalent to pressing [!Escape] before that letter. So any
keypress available from the Utilities Menu can be reached more quickly
by pressing it with Alt from anywhere. For example, the key
combination Alt + [R] is equivalent to [!Escape][R].

However, this only works if your terminal handles the Alt key by
prefixing the same control character that the actual [!Escape] key
sends. PuTTY does this, and so do commonly used Unix terminals such as
gnome-terminal. But traditional xterm defaults to doing something
different, which can't be distinguished reliably by Mastodonochrome.
If you want to use the Alt key in xterm, you must select the
non-default option "Meta Sends Escape" from the xterm interactive
menu, or configure it another way.

"##
            ),
        ),

        Activity::Help(HelpActivity::Help(..)) => Help::new(
            "Help for the Mastodonochrome help viewer",
            help_markup!(
                r##"

You're viewing a help page in Mastodonochrome.

To leave the help and return to what you were doing before, press [Q].

To scroll the text a page at a time, you can press [Space] to move to
the next page, and [-] or [B] to move to the previous page. You can
also use the [PgDn] and [PgUp] keys, or [Right] and [Left], to do the
same thing.

To scroll a line at a time, you can use the [Up] and [Down] arrow
keys. You can also press [Return] to scroll down by a line, and if you
press it again at the very bottom of the help file, it will leave the
help and return to your previous activity.

"##
            ),
        ),

        Activity::Util(UtilityActivity::ExitMenu) => Help::new(
            "Help for the Mastodonochrome Exit Menu",
            help_markup!(
                r##"

You can exit Mastodonochrome from anywhere by pressing
[!Escape][!X][X].

The Exit Menu is the place you reach after two of those three
keystrokes. From here, pressing [X] one more time will exit
Mastodonochrome, and return you to the shell you ran it from.

If you didn't mean to come to the Exit Menu, and you don't want to
exit Mastodonochrome after all, you can press [Return] or [Q] to
return to whatever you were doing before that.

"##
            ),
        ),

        Activity::Util(UtilityActivity::LogsMenu1) => Help::new(
            "Help for the Client Logs Menu",
            help_markup!(
                r##"

The Client Logs menu contains log files relating to things that
happened in Mastodonochrome itself. It's reached by pressing
[!Escape][L].

Press [H] for a log of recent HTTP transactions that Mastodonochrome
has sent to the Mastodon server, for debugging purposes.

Pressing [L] again will take you to the Server Logs menu.

"##
            ),
        ),

        Activity::Util(UtilityActivity::LogsMenu2) => Help::new(
            "Help for the Server Logs Menu",
            help_markup!(
                r##"

The Client Logs menu contains log files relating to things that
happened on the Mastodon server. It's reached by pressing
[!Escape][!L][!L].

The only log currently available here is the Ego Log, which contains
the content-free parts of the full Mastodon notifications feed: list
of events in which users followed you, or favourited or boosted one of
your posts. Press [E] to view that.

"##
            ),
        ),

        Activity::NonUtil(NonUtilityActivity::HomeTimelineFile) => Help::new(
            "Help for your Home Timeline",
            help_markup!(
                r##"

Your Home Timeline shows Mastodon posts from all the users you follow.

Mostly this will only include those users' top-level posts, i.e. the
ones which are not replies to existing threads. However, some replies
do show up here: if [_:both] of the user posting the reply, and the
user being replied to, are people you follow, then the reply will show
up.

This file starts off by only showing a small number of recent posts in
your home timeline. To extend it further into the past, scroll to the
top of the file and press [0]. See more details below.

When new posts appear on your home timeline, this file automatically
updates itself. The text shown in your window won't change, but the
status bar at the bottom will update to reflect the new percentage of
the distance through the file. So if you're at the bottom of the file,
with everything read, then the status bar will say "(100%)"; if a new
post now appears, it will change to some smaller percentage, such as
"(97%)", which indicates that there is now more to read, if you scroll
down.

"##
            ) + help_for_general_file(true)
                + help_for_examine()
                + help_for_whole_status(true),
        ),

        Activity::NonUtil(NonUtilityActivity::PublicTimelineFile) => {
            Help::new(
                "Help for the Public Timeline",
                help_markup!(
                    r##"

The Public Timeline shows recent public-visibility Mastodon posts that
have arrived on your server from across the whole Fediverse.

This file starts off by only showing a small number of recent posts in
the public timeline. To extend it further into the past, scroll to the
top of the file and press [0]. See more details below.

At present, this timeline does not automatically update when new
public posts are made. (If it did, it would move extremely fast.) If
you leave this file by pressing [Q], and then return to it, you'll see
an updated set of posts.

"##
                ) + help_for_general_file(true)
                    + help_for_examine()
                    + help_for_whole_status(true),
            )
        }

        Activity::NonUtil(NonUtilityActivity::LocalTimelineFile) => Help::new(
            "Help for your Local Timeline",
            help_markup!(
                r##"

The Local Timeline shows recent public-visibility Mastodon posts that
have been posted by users on your server.

This file starts off by only showing a small number of recent posts in
the local timeline. To extend it further into the past, scroll to the
top of the file and press [0]. See more details below.

At present, this timeline does not automatically update when new
public posts are made. (If it did, it would move extremely fast.) If
you leave this file by pressing [Q], and then return to it, you'll see
an updated set of posts.

"##
            ) + help_for_general_file(true)
                + help_for_examine()
                + help_for_whole_status(true),
        ),

        Activity::NonUtil(NonUtilityActivity::HashtagTimeline(..)) => {
            Help::new(
                "Help for hashtag timelines",
                help_markup!(
                    r##"

A hashtag timeline shows recent public-visibility Mastodon posts that
have arrived on your server from across the whole Fediverse,
mentioning a particular hashtag.

This file starts off by only showing a small number of recent posts in
the hashtag's timeline. To extend it further into the past, scroll to
the top of the file and press [0]. See more details below.

At present, this timeline does not automatically update when new posts
are made with the hashtag. If you leave this file by pressing [Q], and
then return to it, you'll see an updated set of posts.

"##
                ) + help_for_general_file(true)
                    + help_for_examine()
                    + help_for_whole_status(true),
            )
        }

        Activity::NonUtil(NonUtilityActivity::ThreadFile(..)) => Help::new(
            "Help for post thread file",
            help_markup!(
                r##"

This file shows a collection of Mastodon posts that are part of a
single thread.

This file doesn't automatically update if more posts are added to the
thread. If you want to check whether the thread has moved on, you will
have to exit this activity (for example, by pressing [Q]), and reload
it.

"##
            ) + help_for_general_file(false)
                + help_for_examine()
                + help_for_whole_status(true),
        ),

        Activity::NonUtil(NonUtilityActivity::UserPosts(_, boosts, _)) => {
            match boosts {
                Boosts::Show => Help::new(
                    "Help for user posts file",
                    help_markup!(
                        r##"

This file shows a collection of Mastodon posts all either written or
boosted by the same user.

This file starts off by only showing a small number of recent posts in
the hashtag's timeline. To extend it further into the past, scroll to
the top of the file and press [0]. See more details below.

This file doesn't automatically update if the user makes new posts or
boosts while you are watching it. If you want to check whether that
has happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
                    ) + help_for_general_file(true)
                        + help_for_examine()
                        + help_for_whole_status(true),
                ),
                Boosts::Hide => Help::new(
                    "Help for user posts file",
                    help_markup!(
                        r##"

This file shows a collection of Mastodon posts all by the same user.

This file starts off by only showing a small number of recent posts
from the user. To extend it further into the past, scroll to the top
of the file and press [0]. See more details below.

This file doesn't automatically update if the user makes new posts
while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
                    ) + help_for_general_file(true)
                        + help_for_examine()
                        + help_for_whole_status(true),
                ),
            }
        }

        Activity::Util(UtilityActivity::ReadMentions) => Help::new(
            "Help for Read Mentions feed",
            help_markup!(
                r##"

The Read Mentions feed shows Mastodon posts by other users, mentioning
you.

You will be automatically moved into this file when new posts are made
mentioning you. The rest of the time, you can reach this file via
[!Escape][R].

If a new post mentions you while you're already reading this file,
your file position will be moved to show the first post that
Mastodonochrome doesn't already think you've read.

The information in this feed forms part of what a more typical
Mastodon client would show as your notifications feed. However,
Mastodonochrome breaks it up into smaller pieces. Another part of your
notifications feed is available in the Ego Log, at [!Escape][!L][!L][!E].

This file starts off by only showing a small number of recent posts
that mentioned you. To extend it further into the past, scroll to the
top of the file and press [0]. See more details below.

"##
            ) + help_for_general_file(true)
                + help_for_examine()
                + help_for_whole_status(true),
        ),

        Activity::Util(UtilityActivity::EgoLog) => Help::new(
            "Help for Ego Log feed",
            help_markup!(
                r##"

The Ego Log shows Mastodon notifications for users following you, or
favouriting or boosting one of your posts.

The information in this feed forms part of what a more typical
Mastodon client would show as your notifications feed. However,
Mastodonochrome breaks it up into smaller pieces. Another part of your
notifications feed is available in the Read Mentions feed, at
[!Escape][!R].

This file starts off by only showing a small number of recent
notification entries. To extend it further into the past, scroll to
the top of the file and press [0]. See more details below.

When new entries appear in this log, this file automatically updates
itself. The text shown in your window won't change, but the status bar
at the bottom will update to reflect the new percentage of the
distance through the file. So if you're at the bottom of the file,
with everything read, then the status bar will say "(100%)"; if a new
post now appears, it will change to some smaller percentage, such as
"(97%)", which indicates that there is now more to read, if you scroll
down.

"##
            ) + help_for_general_file(true)
                + help_for_examine()
                + help_for_whole_status(true),
        ),

        Activity::Util(UtilityActivity::ErrorLog) => Help::new(
            "Help for Error Log",
            help_markup!(
                r##"

The Error Log shows problems that Mastodonochrome has encountered
during its run.

Usually these problems involve the Mastodon server returning an
unexpected error response to an HTTP request. In some cases,
Mastodonochrome can anticipate error responses and do something
sensible with them, such as reporting that a user account name or post
ID you entered did not exist. But not all possible error responses
have a sensible interpretation, and Mastodonochrome may not have
anticipated even all of the ones that do, so you may find yourself
thrown into this file unexpectedly. If you know what you did to cause
it, then a bug report might help the developers to add better handling
for it.

"##
            ) + help_for_general_file(false),
        ),

        Activity::Util(UtilityActivity::HttpLog) => Help::new(
            "Help for HTTP Log",
            help_markup!(
                r##"

The HTTP Log lists a summary of recent HTTP transactions that
Mastodonochrome has performed while talking to the Mastodon server.

You might want to look at this if you were collecting information for
a bug report, or for curiosity to see what Mastodon protocol requests
look like (perhaps if you're developing a different client!). Or, of
course, if you're one of the developers of [_:this] client, trying to
fix or understand something.

Each transaction is shown on a single line, containing the time of the
request, the HTTP method (e.g. GET or POST), the URL being retrieved,
and the response status code. URLs are trimmed to remove the boring
parts: the base URL of the Mastodon server (because it's the same
every time) and the prefix "/api/v1/" or "/api/v2/" on the pathname
within that web server.

To see more detail about a specific transaction, press [I]. This will
display a [*:highlight] on a line of the log. You can move the
highlight to a line earlier in the file by pressing [-] or [Up], or
later in the file by pressing [+] or [Down]. Then press [Space] to
display a page of detailed information about the currently selected
HTTP transaction, or [Q] to abort the selection process. This will
show, for example, the full headers of the HTTP request and response.

"##
            ) + help_for_general_file(false),
        ),

        Activity::Util(UtilityActivity::InfoHttp(..)) => Help::new(
            "Help for HTTP transaction information",
            help_markup!(
                r##"

This file lists the details of a recent HTTP transaction that
Mastodonochrome performed while talking to the Mastodon server.

The log shows the HTTP request (method and URL), the HTTP response
code, and all the headers sent to and from the server.

None of these items is in the exact format it has on the wire, but
they're the closest we can reasonably get from the HTTP library that
Mastodonochrome uses. The header names are normalised to lower case no
matter what was really sent; the library has discarded the exact
response reason sent by the server along with the status code and
substituted its own canonical reason string.

"##
            ) + help_for_general_file(false),
        ),

        Activity::Util(UtilityActivity::InfoStatus(..)) => Help::new(
            "Help for the Post Information page",
            help_markup!(
                r##"

The Post Information page shows the full details of a single Mastodon
post, either via entering its id via [!I] from the Main Menu, or by
pressing [!I] and selecting a post elsewhere.

This page shows the post itself, and all the additional information
provided by the Mastodon server but not shown in Mastodonochrome's
other file types. In particular, you can find a URL to the post as
shown on the website of its originating Mastodon server, and the full
account names of users mentioned in the post.

The page shows the number of users who have favourited or boosted the
post. You can also see a full list of those users by pressing [L][!F]
or [L][!B] respectively.

"##
            ) + help_for_general_file(false)
                + help_for_examine()
                + help_for_whole_status(false),
        ),

        Activity::Util(UtilityActivity::ExamineUser(account_id)) => {
            if account_id == &client.our_account_id() {
                Help::new(
                    "Help for the Examine User page (self edition)",
                    help_markup!(
                        r##"

This Examine User page shows all the available information about your
own Mastodon account.

In addition to the information on this page itself, you can find
further information via additional keystrokes:

You can see a list of your followers, or of the people you have
followed, by pressing [L][!I] or [L][!O] respectively.

If your account is locked, so that you have to approve users before
they can follow you, then [L][!F] will show a list of people who
currently have pending requests to follow you, so that you can approve
or reject the requests. You do this by examining each user in turn,
and pressing [!R] from their user options page.

You can see all your own posts by pressing [P][!A], [P][!O] or [P][!T]
depending on what kinds of post you want. [P][!A] shows everything you
have posted, including boosts of other users' posts, and replies to
other users. [P][!O] excludes boosts, so that you only see posts
written by you yourself. [P][!T] also excludes replies to other users,
so that you only see your top-level posts.

Pressing [O] from this page takes you to the Your Options page, where
you can set options relating to your user account, such as whether
it's locked, or what your display name is.

Pressing [V] from this page takes you to the Error Log.

"##
                    ) + help_for_general_file(false),
                )
            } else {
                Help::new(
                    "Help for the Examine User page",
                    help_markup!(
                        r##"

The Examine User page shows all the available information about a
Mastodon user account, either via entering the account name via
[!Escape][!E], or by pressing [!E] and selecting an account name from
a file you're reading.

In addition to the information on this page itself, you can find
further information via additional keystrokes:

You can see a list of the user's followers, or of the people the user
has followed, by pressing [L][!I] or [L][!O] respectively.

You can see the user's own posts by pressing [P][!A], [P][!O] or
[P][!T] depending on what kinds of post you want. [P][!A] shows
everything posted by the user, including boosts of other users' posts,
and replies to other users. [P][!O] excludes boosts, so that you only
see posts written by the user themself. [P][!T] also excludes replies
to other users, so that you only see the user's top-level posts.

Pressing [O] from this page takes you to the User Options page, where
you can set options relating to the user, such as whether you follow
them.

"##
                    ) + help_for_general_file(false),
                )
            }
        }

        Activity::Util(UtilityActivity::ListStatusFavouriters(..)) => {
            Help::new(
                "Help for the List Favouriters page",
                help_markup!(
                    r##"

This page is a list of the users who have favourited a particular
post.

This file starts off by only showing a small number of recent
favouriters. To extend it further into the past, scroll to the top of
the file and press [0]. See more details below.

This file doesn't automatically update if more users favourite the
post while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
                ) + help_for_general_file(true)
                    + help_for_examine(),
            )
        }

        Activity::Util(UtilityActivity::ListStatusBoosters(..)) => Help::new(
            "Help for the List Boosters page",
            help_markup!(
                r##"

This page is a list of the users who have boosted a particular post.

This file starts off by only showing a small number of recent
boosters. To extend it further into the past, scroll to the top of the
file and press [0]. See more details below.

This file doesn't automatically update if more users boost the post
while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
            ) + help_for_general_file(true)
                + help_for_examine(),
        ),

        Activity::Util(UtilityActivity::ListUserFollowers(..)) => Help::new(
            "Help for the List User Followers page",
            help_markup!(
                r##"

This page is a list of the users who a particular other user has
followed.

This file starts off by only showing a small number of users that the
user has followed recently. To extend it further into the past, scroll
to the top of the file and press [0]. See more details below.

This file doesn't automatically update if the user follows more people
while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
            ) + help_for_general_file(true)
                + help_for_examine(),
        ),

        Activity::Util(UtilityActivity::ListUserFollowees(..)) => Help::new(
            "Help for the List User Followees page",
            help_markup!(
                r##"

This page is a list of the users who a particular other user has
followed.

This file starts off by only showing a small number of users who have
followed the user recently. To extend it further into the past, scroll
to the top of the file and press [0]. See more details below.

This file doesn't automatically update if more people follow the user
while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
            ) + help_for_general_file(true)
                + help_for_examine(),
        ),

        Activity::Util(UtilityActivity::ListFollowRequesters) => Help::new(
            "Help for the List Follow Requesters page",
            help_markup!(
                r##"

This page is a list of users who have requested to follow you, and
because your account is locked, have to wait for your approval.

If you examine a user using [E] and then go to their options page with
[O], the options page will include an option to approve or reject
their request to follow you.

This file starts off by only showing a small number of users who have
requested to follow you recently. To extend it further into the past,
scroll to the top of the file and press [0]. See more details below.

This file doesn't automatically update if more people ask to follow
you while you are watching it. If you want to check whether that has
happened, you will have to exit this activity (for example, by
pressing [Q]), and reload it.

"##
            ) + help_for_general_file(true)
                + help_for_examine(),
        ),

        Activity::Util(UtilityActivity::InstanceRules) => Help::new(
            "Help for the Instance Rules page",
            help_markup!(
                r##"

This page shows the rules set by the administrators of your Mastodon
server. If you're registering a new account on the server using
Mastodonochrome, you should read these rules and make sure you agree
with them, before completing your account registration.

"##
            ) + help_for_general_file(false),
        ),

        Activity::Compose(ComposeActivity::ComposeToplevel) => Help::new(
            "Help for the post composer",
            help_markup!(
                r##"

In this text editor, reached by pressing [!C] from the Main Menu,
you're composing text to be posted as a new 'top-level' Mastodon post,
that is, one which is not a reply to any other post.

"##
            ) + help_for_composer(),
        ),

        Activity::Compose(ComposeActivity::ComposeReply(..)) => Help::new(
            "Help for the post composer",
            help_markup!(
                r##"

In this text editor, reached by pressing [!S][!Space] from any file
where you're reading posts, you're composing text to be posted as a
reply to another Mastodon post. The "Re:" line above the [-:---]
separator indicates this, and reminds you of which post you asked to
reply to.

"##
            ) + help_for_composer(),
        ),

        Activity::Compose(ComposeActivity::EditExistingPost(..)) => Help::new(
            "Help for the post composer",
            help_markup!(
                r##"

In this text editor, reached by pressing [!S][!Space] from any file
where you're reading posts, you're composing text to be posted as a
replacement for an existing Mastodon post of your own.

If the post you're editing is a reply to an existing one, then the
"Re:" line above the [-:---] separator indicates this, and reminds you
of which post it's a reply to. If not, then there is no "Re:" line.

"##
            ) + help_for_composer(),
        ),

        Activity::Overlay(OverlayActivity::GetUserToExamine) => Help::new(
            "Help for the bottom-line editor (account name edition)",
            help_markup!(
                r##"

Mastodonochrome is currently prompting you for the account name of a
user to examine.

You can spell the account name with or without an initial @ sign:
either "[c:@user@domain]" or just "[c:user@domain]". If the user's
account is on the same Mastodon instance as you, you can leave off the
@domain suffix, and just enter either "[c:user]" or "[c:@user]".

You can also enter the URL of the user's profile on their instance's
website, typically of the form [u:https://example.com/@username].

Once you've entered the name, press [Return] to go to the Examine User
page telling you about that user.

If you change your mind and decide not to examine any user at all,
clear all the text from the editor and press Return while the line is
empty. A quick way to do that from anywhere is to press
[Ctrl('A')][Ctrl('K')] (see below).

[_:Editing keys]

"##
            ) + help_for_editor_keys_oneline(),
        ),

        Activity::Overlay(OverlayActivity::GetPostIdToRead) => Help::new(
            "Help for the bottom-line editor (post id edition)",
            help_markup!(
                r##"

Mastodonochrome is currently prompting you for the id of a single
Mastodon post to view information about.

This method of finding a Mastodon post is probably mostly useful
during debugging, or client development. But it might be useful in
other situations too.

For example, if you've found a Mastodon post in Mastodonochrome by
some other method, you can find out its id by viewing its full post
information via [!I]. Then you might communicate that id to another
user with an account on the same Mastodon instance, who could look it
up by this method.

You can also paste in the full URL that displays a Mastodon post on
the web, typically of the form
[u:https://example.com/@username/123456789123456789], even if the
server name is not the same as the server you're logged in to. If that
URL corresponds to a post that your own server has a copy of,
Mastodonochrome will be able to look it up.

Anyway: once you've entered the id, press [Return] to go to the Post
Information page telling you about that user.

If you change your mind and decide not to view any post at all, clear
all the text from the editor and press Return while the line is empty.
A quick way to do that from anywhere is to press
[Ctrl('A')][Ctrl('K')] (see below).

[_:Editing keys]

"##
            ) + help_for_editor_keys_oneline(),
        ),

        Activity::Overlay(OverlayActivity::GetHashtagToRead) => Help::new(
            "Help for the bottom-line editor (hashtag edition)",
            help_markup!(
                r##"

Mastodonochrome is currently prompting you to enter the name of a
[#:#hashtag], so that you can view a feed of recent posts mentioning
that hashtag.

The tag name can be entered with or without the initial '#'. For
example, if you feel like reading posts that include the hashtag
[#:#stoats], you can press [#], and then enter either of "[c:stoats]"
or "[c:#stoats]" at the prompt.

Hashtags are case insensitive. If you enter "[c:stoats]", you'll find
posts mentioning any of [#:#stoats], [#:#Stoats], [#:#STOATS],
[#:#sToAtS] or anything else along those lines.

Once you've entered the tag you want, press [Return] to go to the Post
Information page telling you about that user.

If you change your mind and decide not to view any post at all, clear
all the text from the editor and press Return while the line is empty.
A quick way to do that from anywhere is to press
[Ctrl('A')][Ctrl('K')] (see below).

[_:Editing keys]

"##
            ) + help_for_editor_keys_oneline(),
        ),

        Activity::Overlay(OverlayActivity::GetSearchExpression(..)) => {
            Help::new(
                "Help for the bottom-line editor (search edition)",
                help_markup!(
                    r##"

Mastodonochrome is currently prompting you to enter a search term, to
search for in the file of information you're reading.

Once you've entered the search term you want, press [Return] to go to
the Post Information page telling you about that user.

If you enter nothing at all (a blank line), Mastodonochrome will reuse
the previous search term you entered. You can also achieve this by
pressing [!N] in the file, which will search for the same text as
before, in the same direction.

The text you enter will be treated as a [_:regular expression]. This
means that not every character is treated literally. Instead, some
characters have special meanings:

The character "[c:.]" is a placeholder for "any character at all". So
searching for "[c:a.c]" will match not only "a.c" itself, but any
other text with a single character between "a" and "c", such as "abc",
"axc", "a c", ...

You can match either of two different pieces of text by separating
them with the "[c:|]" symbol. For example, the search term
"[c:stoat|weasel]" matches either the word "stoat" or the word
"weasel", whichever is found in the text first.

Writing "[c:?]" after a character makes it optional, so that
"[c:ab?c]" matches either "abc" or just "ac". Writing "[c:+]" after a
character allows it to repeat, so that "[c:ab+c]" matches "abc",
"abbc", "abbbc", ... Writing "[c:*]" combines the two: the character
is allowed to repeat [_:or] to be completely missing. So "[c:ab*c]"
matches any of these strings, including both "ac" and "abbbc".

You can wrap text in parentheses to narrow the scope of "[c:|]" or to
widen the scope of "[c:?]", "[c:+]" or "[c:*]". For example, "[c:a
(red|green) ball]" matches either "a red ball" or "a green ball", and
"[c:ba(na)+]" matches any of "bana", "banana", "bananana",
"banananana", etc.

If you want to search for any of these special characters in a literal
sense (that is, find text in the file that actually has a '.'
character in it, or a '*', or '('), you can precede the character with
"[c:\\]" to make it literal. So "[c:\\*]" searches for an actual '*'.
This also applies to the '\\' character itself: "[c:\\\\]" searches
for an actual '\\'.

This is only a brief introduction to the syntax of regular
expressions. There are many longer explanations available on the web.
But the best documentation in this case is the documentation for the
Rust [c:regex] crate, because that's the version of regular
expressions that Mastodonochrome's search function actually uses:
[u:https://docs.rs/regex/latest/regex/#syntax]

[_:Editing keys]

"##
                ) + help_for_editor_keys_oneline(),
            )
        }

        Activity::Overlay(OverlayActivity::BottomLineError(..)) => Help::new(
            "Help for bottom-line error messages",
            help_markup!(
                r##"

Mastodonochrome is currently displaying an error message in the bottom
line. This happens in situations where you've just entered something
at a bottom-line editor prompt which was not valid in some way: for
example, a user account name that doesn't exist, or a search string
that's not a legal regular expression.

There's nothing much you can do here except to read the error message
and then press [Return] to go back to whatever you were doing before
the bottom-line prompt.

"##
            ),
        ),

        Activity::Compose(ComposeActivity::PostComposeMenu) => Help::new(
            "Help for the post-composer menu (top-level edition)",
            post_composer_menu_common_start()
                + help_markup!(
                    r##"

[_:Visibility]

Mastodon posts can have four different visibility settings. Press [V]
to choose which one will be applied to this post.

The widest visibility setting is "[g:public]". This means that anyone
can see the post, and it will be shown to people watching public
timelines.

Visibility "unlisted" still means that anyone [_:can] see the post, if
they go looking on purpose (e.g. by looking at a list of all your
posts, or by entering the post ID if they can find it out). Unlike
"[g:public]", it won't appear on the public timelines. But anyone who
follows you will still see it on their home timeline. So this makes
the post less prominent, without actually keeping it secret from
anybody.

Visibility "[r:private]" means that most people can't see the post at
all: [_:even] if they deliberately try to look it up, the server will
refuse them access. The only people who can see it are people who
follow you, and any users that you [@:@mentioned] in the text of the
post.

Finally, visibility "[r:direct]" means that [_:only] the
[@:@mentioned] users can see the post at all. In other words, this is
a direct message to those users.

(The two middle visibilities, "unlisted" and "[r:private]", are
probably most useful if your account is also locked, so that you get
to control who your followers are. If you don't, then restricting
things to just your followers isn't as helpful as you might want.)

"##
                )
                + post_composer_menu_common_end(),
        ),

        Activity::Compose(ComposeActivity::PostReplyMenu(..)) => Help::new(
            "Help for the post-composer menu (reply edition)",
            post_composer_menu_common_start()
                + help_markup!(
                    r##"

[_:Visibility]

Mastodon posts can have four different visibility settings. Press [V]
to choose which one will be applied to this post.

The widest visibility settings are "[g:public]" and "unlisted". Both
of these mean that anyone can see the post. The usual difference
between them is that "unlisted" posts don't show up on public
timelines. However, public timelines don't show reply posts as far as
I know, so when the post you're editing at the moment is a reply, I
don't [_:think] there's any difference.

Visibility "[r:private]" means that most people can't see the post at
all: [_:even] if they deliberately try to look it up, the server will
refuse them access. The only people who can see it are people who
follow you, and any users that you [@:@mentioned] in the text of the
post.

Finally, visibility "[r:direct]" means that [_:only] the
[@:@mentioned] users can see the post at all. In other words, this is
a direct message to those users.

(The two middle visibilities, "unlisted" and "[r:private]", are
probably most useful if your account is also locked, so that you get
to control who your followers are. If you don't, then restricting
things to just your followers isn't as helpful as you might want.)

"##
                )
                + post_composer_menu_common_end(),
        ),

        Activity::Compose(ComposeActivity::PostEditMenu(..)) => Help::new(
            "Help for the post-composer menu (edit edition)",
            post_composer_menu_common_start()
                + help_markup!(
                    r##"

[_:Visibility]

You can't set the visibility of this post, because you're editing an
existing one, and Mastodon doesn't let you change the visibility of a
post once it's been created.

"##
                )
                + post_composer_menu_common_end(),
        ),

        Activity::NonUtil(NonUtilityActivity::LoginMenu) => Help::new(
            "Help for the Mastodonochrome login process",
            help_markup!(
                r##"

Welcome to Mastodonochrome!

You're in the login page, which means that Mastodonochrome doesn't yet
know what Mastodon account it should be accessing, or have any login
details to access that account.

From here, you can authorise Mastodonochrome to access an existing
account of yours. You can also try to register a new account on a
Mastodon instance, as an alternative to doing that via its website. In
both cases (if you're successful), Mastodonochrome will end up logged
in to the account, and able to read and post.

If you decide to abandon the process, you can quit Mastodonochrome
again by typing the key sequence [Escape][!X][!X]. (This sequence
makes a little more sense in the context of the user interface you see
after logging in, but it's the standard exit sequence that works
anywhere in Mastodonochrome, including the login page.)

To start the process, enter the URL of your Mastodon server's
[_:website]. This is [_:usually] the same domain name as the one you
see in user account names, but [_:not always]. For example, the domain
"mastodon.social" (where you'll find users with names of the form
[@:@example@mastodon.social]) corresponds to the website
[u:https://mastodon.social/]. But for some servers, the two domain
names are different. A common choice is to put "social." at the front
of the URL, so that users of the form [@:@example@example.com] would
use the URL [u:https://social.example.com/] rather than just plain
[u:https://example.com/]. (This might happen if the main domain name
is used for lots of things and the Mastodon server is only one of
them.)

Once you enter a URL, Mastodonochrome will check to make sure there
really is a Mastodon instance running there, and if so, it will offer
you two options: press [L] to begin the process of logging in to an
existing account on that server, or [R] to register a new account.

[_:Logging in to an existing account]

To log Mastodonochrome in to an existing account, you must first log
in to the account on the server's website. Once you've done that, use
the same browser to visit the long URL that Mastodonochrome prints
(which will have [u:oauth/authorize] near the start, and then a lot of
incomprehensible nonsense).

If Mastodonochrome has had to wrap that URL across multiple lines of
the terminal, then copy-pasting it out of the terminal will insert
newlines in the pasted text. Depending on your browser, you may have
to compensate for that when pasting the URL into it. But some browsers
don't mind: for example, Firefox will discard the newlines
automatically. If your browser does have trouble with it, one option
is to widen your terminal window until the URL doesn't wrap any more.

Once your browser loads the "authorize" URL while it's already logged
in to your Mastodon account, you should see a web page saying
something like this:

[i.2][+:Authorisation required]

[i.2][+:Mastodonochrome] would like permission to access your account.
It is a third-party application. [+:If you do not trust it, then you
should not authorise it.]

The page also lists the operations that Mastodonochrome is asking for
permission to do in your Mastodon account. If you're happy with that,
click the "Authorise" button. The server will present a code that you
can copy and paste back into Mastodonochrome's "Authorisation code:"
prompt.

Once you enter that code at the prompt, login is complete.
Mastodonochrome will confirm the account name that you've just logged
in as, and you can press [Space] to continue to the Mastodonochrome
Main Menu and start reading and posting.

You might be wondering why this login process is so awkward. Some
other Mastodon clients just ask you for the same password you would
use to log in to the server's website, and they deal with all of this
authorisation code business behind the scenes. But that is your
[+:master] password for the whole account, which is able to authorise
or de-authorise as many clients as it wants. So when you use that
system, you have to trust the client to only use the password for what
you really wanted, and not keep a copy and use it maliciously (or leak
it by accident). Mastodonochrome never knows your master account
password in the first place, so you don't have to worry that it might
abuse or leak it.

[_:Registering a new account]

If you press [R] instead of [L] after entering the server URL, you can
instead try to register a new account on the Mastodon server.

Probably the [_:more normal] way to do this is to register your
account directly on the website, and then log clients into it
afterwards, using the process described in the previous section. But
Mastodonochrome lets you do the whole account registration process
within itself if you want to.

(This is perhaps most useful to client developers, who can use it to
register a lot of test accounts on test servers easily. But it should
work for other people too.)

Once you press [R], a lot of extra fields will appear that must be
filled in. To activate each field, press the key shown to the left.
The initial fields will then prompt you to enter some text.

Press [N] to enter the account name you want. You only need to enter
the "local part", i.e. the account name within your server. The server
domain will be appended automatically. So if you're registering on a
server called "example.com", and you enter "foo" at this prompt,
you're asking for the name "foo@example.com".

Press [E] to enter an email address. The Mastodon server will need
this to communicate with you about your account when you're not logged
in: it can notify you of users sending you messages, or tell you more
important things. This email address will also be used during account
creation, to send you a confirmation URL.

Press [P] to enter a password for your account. This is the password
you will use to log in to the server's [_:website] once your account
is created. That is, it's the master password for the whole account,
which allows you to add and remove authorisation for any number of
clients to access the account. After you enter this, you will be
prompted to enter it a second time, to check that the two match. If
you need to retype just the second copy, press [Ctrl('P')] to activate
that form field.

Finally, the Mastodon server will expect you to read its rules, and
agree to them. These will usually contain things that should be
obvious, like "don't harass other users", but different servers may
also impose extra rules about certain types of content (for example,
nudity). So you should read the rules and confirm that you agree to
them, before creating an account on that server. Press [R] to view the
rules; after you've read them, if you agree, press [A] to accept them.

Once all of the form fields are filled in with acceptable values, and
you've agreed to the server rules, you can press [Space] to actually
ask the server to register your account.

If this is successful, the server will send an email to the address
you entered, containing a URL to visit to confirm your account
registration. This will start with the server's domain name, and have
[u:auth/confirmation] near the start.

Once you receive that email, there are two ways you can finish the
process: visit the confirmation URL in a browser, or paste it into
Mastodonochrome.

If you visit the confirmation URL in a browser, then your account
registration will be complete, and you'll be able to log in to the
server's website using the email address and password you just set up.
If you haven't shut down Mastodonochrome at this point, and it's still
in the middle of the registration process, then you can press [C] to
confirm that you've finished creating your account, and continue to
the Main Menu where you can read and post.

A second option is to paste the confirmation URL out of the email into
Mastodonochrome itself, and let it complete the account registration.
You do this by pressing [U] to activate the "Confirmation URL:" editor
field, and then pasting the URL in and pressing [Return]. This too
should complete registration and login, and continue to the Main Menu.

If you [_:have] shut down Mastodonochrome before responding to the
confirmation email, either on purpose or by accident, then all is not
lost! Visit the confirmation URL in a browser, and then your account
will still be created, and Mastodonochrome will still have its login
details ready to use. So just start Mastodonochrome again after
visiting the confirmation URL, and it should log in successfully, and
go directly to the Main Menu.

[_:Editing keys]

When you're editing any of the editable fields on this page (the
server URL, the authorisation code field during login, or many fields
during account registration), here are the keys you can use to do it.

"##
            ) + help_for_editor_keys_oneline(),
        ),

        Activity::Util(UtilityActivity::UserOptions(account_id)) => {
            if account_id == &client.our_account_id() {
                Help::new(
                    "Help for the Your User Options menu",
                    help_markup!(
                        r##"

This menu lets you set options relating to your own Mastodon account.
You can adjust any or all of the fields shown, and when you've
finished, press [Space] to make your changes permanent. Or you can
press [Q] or [Return] to abandon the changes and leave your settings
as they were.

[_:User information]

You can set your display name by pressing [N]. This is typically shown
alongside your account name on each post you make.

[!:FIXME]: this menu should also let you edit your longer account bio,
and set up information fields like links to other websites and accounts.

[_:Default visibility]

Mastodon posts can have four different visibility settings. Press [V]
to choose the default one, which will be applied to your posts unless
you specify otherwise when making a particular post.

The widest visibility setting is "[g:public]". This means that anyone
can see the post, and it will be shown to people watching public
timelines (unless it's a reply to another post, which public timelines
don't show).

Visibility "unlisted" still means that anyone [_:can] see the post, if
they go looking on purpose (e.g. by looking at a list of all your
posts, or by entering the post ID if they can find it out). Unlike
"[g:public]", it won't appear on the public timelines. But anyone who
follows you will still see it on their home timeline. So this makes
the post less prominent, without actually keeping it secret from
anybody.

Visibility "[r:private]" means that most people can't see the post at
all: [_:even] if they deliberately try to look it up, the server will
refuse them access. The only people who can see it are people who
follow you, and any users that you [@:@mentioned] in the text of the
post.

Finally, visibility "[r:direct]" means that [_:only] the
[@:@mentioned] users can see the post at all. In other words, this is
a direct message to those users.

(The two middle visibilities, "unlisted" and "[r:private]", are
probably most useful if your account is also locked, so that you get
to control who your followers are. If you don't, then restricting
things to just your followers isn't as helpful as you might want.)

[_:Default language]

Mastodon posts are normally tagged to indicate what language they're
written in. This allows software to filter based on the language. In
particular, when you follow a user, you can select which languages you
want to see posts in. So if you follow a bilingual user, and you only
speak one of their languages, you can exclude posts in the other
language from your home timeline, which you wouldn't be able to read
anyway.

Press [L] to set or edit a default language tag that will be applied
to all your posts, unless you specify otherwise when making a
particular post. The tag should be a two-letter code, in the form
standardised by ISO 639-1. For example, "en" means English, "fr" means
French, and "es" means Spanish. For a longer list, see the column of
two-letter codes at
[u:https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes]

[_:Default sensitivity setting]

You can mark Mastodon posts as "sensitive", which means that clients
will not show the text of them by default. This doesn't [_:stop]
people from reading it, but it means they have to take a deliberate
action to do so.

You might use this setting for many reasons. Two examples are if
you're discussing spoilers for some fiction that not everybody has
read or seen yet, or if you're discussing a topic that might disturb,
upset or trigger some readers.

Press [S] to toggle whether your posts are marked as sensitive [_:by
default], if you don't specify otherwise when making a particular
post.

[_:Account privacy settings]

Press [Ctrl('K')] to control whether your account is [_:locked]. This
means that users who want to follow you have to ask your permission.
Once you turn on this setting, if a user tries to follow you, it won't
immediately take effect. Instead, you'll see "This user has requested
to follow you" when you examine them; the Mastodonochrome Main Menu
will show you a notification that you have pending follow requests;
and you can see a list of the current pending requests by going to
your user information page ([!Escape][!Y]) and then pressing [!L][!F].

Press [Ctrl('K')] to control whether the lists of users who follow
you, and who you follow, are made public for other users to see.
Normally, if you hide this, users can still see [_:how many] users are
on each list, but not who they are. (And some versions of the Mastodon
software even allow that to be hidden.)

Press [Ctrl('D')] to control whether your account is shown in
directories of users on your server.

Press [Ctrl('X')] to control whether your account's posts are included
in the results from the server's search feature.

[_:Type-of-account settings]

Press [Ctrl('B')] to control whether your account is flagged as a
'bot', that is, primarily automated rather than primarily a human.

"##
                    ),
                )
            } else {
                Help::new(
                    "Help for the User Options menu",
                    help_markup!(
                        r##"

This menu lets you set options about your Mastodon account's
relationship to another user's account.

You can adjust any or all of the fields shown, and when you've
finished, press [Space] to make your changes permanent. Or you can
press [Q] or [Return] to abandon the changes and leave your settings
as they were.

[_:Following a user]

Press [F] to toggle whether you follow this user. If you do, then
their posts show up on your home timeline (unless they are replies to
another user you don't follow, or are posted with too-limited
visibility).

When you follow a user, you have some options about which of their
posts you see. If you only want to see original content from the user,
and not other people's posts they have boosted, then you can toggle
the "Include their boosts" option by pressing [B].

If the user posts in more than one language, and you only want to see
some of the languages, then you can press [L] to enter a list of the
languages you [_:do] want to see.

The languages string should be a comma-separated list of two-letter
language codes, in the form standardised by ISO 639-1. For example,
"en" means English, "fr" means French, and "es" means Spanish. For a
longer list, see the column of two-letter codes at
[u:https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes]

[_:Avoiding a user]

Press [Ctrl('B')] to toggle whether you have blocked this user. This
means that you don't see its posts in any of the normal timelines,
such as your Home Timeline. It's stronger than just "not following",
because you won't see a blocked user's posts [_:even] if one of them
is boosted by a different user who you do follow. However, the blocked
user can still get your attention by [@:@mention]ing you directly.

Press [Ctrl('U')] to toggle whether you have muted this user. This has
the same effect as blocking, but is even stronger, because even an
[@:@mention] from the user won't show up in your mentions feed.

[_:Editing keys]

When you're editing the language list, here are the keys you can use
to do it.

"##
                    ) + help_for_editor_keys_oneline(),
                )
            }
        }
    }
}
